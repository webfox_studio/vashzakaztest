<?php defined('SYSPATH') or die('No direct script access.');


//-- Environment setup --------------------------------------------------------

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('Europe/Minsk');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

/**
 * Set the default language
 */
I18n::lang('en-us');
//-- Configuration and initialization -----------------------------------------

/**
 * Set Kohana::$environment if $_ENV['KOHANA_ENV'] has been supplied.
 * 
 */
if (isset($_ENV['KOHANA_ENV']))
{
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(array(
	'base_url'   => '/',
	'index_file' => '/',
    'caching'    => TRUE,
    // 'errors' => FALSE
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Kohana_Log_File(APPPATH.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Kohana_Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
       'auth'       => MODPATH.'auth',       // Basic authentication
	// 'cache'      => MODPATH.'cache',      // Caching with multiple backends
	// 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
	   'database'   => MODPATH.'database',   // Database access
	// 'image'      => MODPATH.'image',      // Image manipulation
	   'orm'        => MODPATH.'orm',        // Object Relationship Mapping
	// 'oauth'      => MODPATH.'oauth',      // OAuth authentication
	   'pagination' => MODPATH.'pagination', // Paging of results
	// 'unittest'   => MODPATH.'unittest',   // Unit testing
	// 'userguide'  => MODPATH.'userguide',  // User guide and API documentation
       'lang'       => MODPATH.'lang',       // Lang
	   'formo'      => MODPATH.'formo',      // Formo
    // 'spig'       => MODPATH.'sprig',      // Sprig
	// 'currency'   => MODPATH.'currency',   // Currency
       'twitter'    => MODPATH.'twitter',    // Получает сообщения Twitter'а
       'settings'   => MODPATH.'settings',   // Получает/Сохраняет параметры/настройки
       'email'      => MODPATH.'email',      // Отправка e-mail Mail Swift
    // 'layout'     => MODPATH.'layout',     // Модуль управления содежримым страницы
    // 'jelly'      => MODPATH.'jelly',      // Jelly
       'captcha'      => MODPATH.'captcha',      // Captcha
	));

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.admin/requestform/close/8
 */
// /admin/tracking/edit/<id>
Route::set('tracking/edit', 'admin/tracking/edit/<id>', array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'tracking',
        'action'     => 'edit',
    ));
Route::set('tracking/editStatus', 'admin/tracking/editStatus/<id>', array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'tracking',
        'action'     => 'editStatus',
    ));
Route::set('tracking/addNotes', 'admin/tracking/addNotes')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'tracking',
        'action'     => 'addNotes',
    ));
Route::set('tracking/addStatus', 'admin/tracking/addStatus')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'tracking',
        'action'     => 'addStatus',
    ));
Route::set('tracking/delStatus', 'admin/tracking/delStatus/<id>', array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'tracking',
        'action'     => 'delStatus',
    ));

Route::set('admin/allinclusive/delivery', 'admin/allinclusive/delivery(/<type>(/<id>))',array('type' => 'edit|add|delete'), array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'allinclusive',
        'action'     => 'delivery',
    ));

Route::set('admin/allinclusive/productasd', 'admin/allinclusive/category(/<type>(/<id>))',array('type' => 'edit|add|delete'), array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'allinclusive',
        'action'     => 'category',
    ));

Route::set('admin/allinclusive/product', 'admin/allinclusive(/<type>(/<id>))',array('type' => 'edit|add|delete'), array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'allinclusive',
        'action'     => 'index',
    ));

Route::set('allinclusive_town11', 'page/discounts/allinclusive/<cat>', array('cat' => '[0-9]+'))
	->defaults(array(
		'controller' => 'allinclusive',
		'action'     => 'index',
	));


Route::set('allinclusive_town', 'page/discounts/allinclusive/town/<town_id>', array('town_id' => '[0-9]+'))
	->defaults(array(
		'controller' => 'allinclusive',
		'action'     => 'ajax',
	));

Route::set('allinclusive', 'page/discounts/allinclusive', array('id' => '[0-9]+'))
    ->defaults(array(
        'controller' => 'allinclusive',
        'action'     => 'index',
    ));

Route::set('requestform7', 'admin/requestform/arhiv/<id>', array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'requestform',
        'action'     => 'arhiv',
    ));

Route::set('requestform6', 'admin/requestform/set_notimportant/<id>', array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'requestform',
        'action'     => 'set_notimportant',
    ));
//дополнительные страницы
Route::set('requestform5', 'admin/requestform/set_important/<id>', array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'requestform',
        'action'     => 'set_important',
    ));

//дополнительные страницы
Route::set('requestform4', 'admin/requestform/open/<id>', array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'requestform',
        'action'     => 'open',
    ));

//дополнительные страницы
Route::set('requestform3', 'admin/requestform/close/<id>', array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'requestform',
        'action'     => 'close',
    ));

//дополнительные страницы
Route::set('requestform2', 'admin/requestform/z<id>', array('id' => '[0-9]+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'requestform',
        'action'     => 'zakaz',
    ));

// tracking
Route::set('trackings', 'trackings')
    ->defaults(array(
        'controller' => 'tracking',
        'action'     => 'index',
    ));

//дополнительные страницы
Route::set('requestform1', 'requestform/good')
    ->defaults(array(
        //'directory'  => 'admin',
        'controller' => 'requestform',
        'action'     => 'good',
    ));

//дополнительные страницы
Route::set('requestform', 'admin/requestform(/<wer>)')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'requestform',
        'action'     => 'index',
    ));
//дополнительные страницы
Route::set('requestform11', 'requestform')
    ->defaults(array(
        //'directory'  => 'admin',
        'controller' => 'requestform',
        'action'     => 'index',
    ));
//дополнительные страницы
Route::set('additionpage1', 'additionalpage/<idpage>')
    ->defaults(array(
        //'directory'  => 'admin',
        'controller' => 'additionalpage',
        'action'     => 'index',
    ));

//дополнительные страницы
Route::set('admin/additionpage', 'admin/editors/additionalpage(/<type>(/<idpage>))',array('type' => 'edit|add|del|cat|addcat|editcat|delcat|archive'), array('id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'additionalpage',
        'action'     => 'index',
    ));

Route::set('admin/zakaz_pip', 'admin/editors/orderform<id_zakaz>', array('id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'editors',
        'action'     => 'orderform',
    ));

Route::set('admin/posilkaform_pip', 'admin/editors/posilkaform<id_zakaz>', array('id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'editors',
        'action'     => 'posilkaform',
    ));

Route::set('order_new_pip', 'orders/new<id_zakaz>', array('id' => '\d+'))
    ->defaults(array(
        'controller' => 'orders',
        'action'     => 'new',
    ));

Route::set('parcels_new_pip', 'parcels/new<id_zakaz>', array('id' => '\d+'))
    ->defaults(array(
        'controller' => 'orders',
        'action'     => 'new',
        'is_parcel'  => TRUE,
    ));

Route::set('messages/delete/new', 'messages/delete/new/<id>')
    ->defaults(array(
        'controller' => 'messages',
        'action'     => 'newdelete',
    ));

Route::set('admin/adminfiles/important', 'admin/adminfiles/important')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'adminfiles',
        'action'     => 'important',
    ));

//роут для закрытия заказа клиентом
Route::set('admin/adminfiles/close', 'admin/adminfile/close/<id>', array('id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'adminfiles',
        'action'     => 'close',
  //      'is_parcel'  => FALSE,
    ));

//роут для открытия заказа клиентом
Route::set('admin/adminfiles/open', 'admin/adminfile/open/<id>', array('id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'adminfiles',
        'action'     => 'open',
     //   'is_parcel'  => FALSE,
    ));

// изменения для файлов админа
Route::set('admin/adminfiles', 'admin/adminfiles(/<operator>(/<id>))',array('operator' => '[a-zA-Z-]+'), array('id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'adminfiles',
        'action'     => 'index',
        //'is_parcel'  => TRUE,
    ));


Route::set('index', '')
    ->defaults(array(
        'controller' => 'nmain',
        'action'     => 'index',
    ));

Route::set('auth', '<action>', array('action' => 'login|logout|register|access_denied|remind'))
    ->defaults(array(
        'controller' => 'user',
        'action'     => 'index',
    ));
Route::set('board', '/board/index.php?page=<page>')
    ->defaults(array(
    ));

Route::set('news/item', 'news/<id>', array('id' => '\d+'))
    ->defaults(array(
        'controller' => 'news',
        'action'     => 'item',
    ));

Route::set('reviews', 'reviews')
    ->defaults(array(
        'controller' => 'reviewsall',
        'action'     => 'all'
    ));

Route::set('payment', 'payment/id<id>', array('id' => '\d+'))
    ->defaults(array(
        'controller' => 'payments',
        'action'     => 'item',
    ));

Route::set('payments', 'payments/id<id>', array('id' => '\d+'))
    ->defaults(array(
        'controller' => 'payments',
        'action'     => 'item',
    ));



Route::set('order', 'order/id<id>', array('id' => '\d+'))
    ->defaults(array(
        'controller' => 'orders',
        'action'     => 'item',
    ));

Route::set('parcel', 'parcel/id<id>', array('id' => '\d+'))
    ->defaults(array(
        'controller' => 'orders',
        'action'     => 'item',
        'is_parcel'  => TRUE,
    ));

Route::set('parcels', 'parcels(/<action>(/<id>))')
    ->defaults(array(
        'controller' => 'orders',
        'action'     => 'index',
        'is_parcel'  => TRUE,
    ));

Route::set('admin/orders', 'admin/orders')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'orders',
        'action'     => 'index',
        'is_parcel'  => FALSE,
    ));

Route::set('admin/faq', 'admin/adminfaq')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'faq',
        'action'     => 'index'
    ));

Route::set('files', 'files')
    ->defaults(array(
        'controller' => 'files',
        'action'     => 'index'
    ));

Route::set('admin/payment', 'admin/payment/<id>(.<message>:<act>)',
		array('id' => '\d+', 'message' => '\d+', 'act' => 'del|edit'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'payments',
        'action'     => 'item',
        'is_parcel'  => FALSE,
    ));


Route::set('admin/order', 'admin/order/<id>(.<message>:<act>)',
		array('id' => '\d+', 'message' => '\d+', 'act' => 'del|edit'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'orders',
        'action'     => 'item',
        'is_parcel'  => FALSE,
    ));
Route::set('order_images', 'order_images/<file>', array('file' => '.*'))
    ->defaults(array(
        'controller' => 'media',
        'action' => 'index'
    ));

Route::set('admin/parcels', 'admin/parcels')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'orders',
        'is_parcel'  => TRUE,
    ));

Route::set('admin/parcel', 'admin/parcel/<id>(.<message>:<act>)',
		array('id' => '\d+', 'message' => '\d+', 'act' => 'del|edit'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'orders',
        'action'     => 'item',
        'is_parcel'  => TRUE,
    ));

//роут для закрытия заказа клиентом
Route::set('orders/client', 'orders/close/<id>', array('id' => '\d+'))
    ->defaults(array(
       // 'directory'  => 'admin',
        'controller' => 'orders',
        'action'     => 'close',
        'is_parcel'  => FALSE,
    ));

//роут для открытия заказа клиентом
Route::set('orders/client', 'orders/open/<id>', array('id' => '\d+'))
    ->defaults(array(
       // 'directory'  => 'admin',
        'controller' => 'orders',
        'action'     => 'open',
        'is_parcel'  => FALSE,
    ));


Route::set('admin/orders/client', 'admin/orders/client/<id>', array('id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'orders',
        'action'     => 'client',
        'is_parcel'  => FALSE,
    ));

Route::set('admin/parcels/client', 'admin/parcels/client/<id>', array('id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'orders',
        'action'     => 'client',
        'is_parcel'  => TRUE,
    ));


Route::set('admin/orders/', 'admin/orders(/<action>(/<id>))')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'orders',
        'action'     => '<action>',
        'id'         => '<id>',
        'is_parcel'  => FALSE,
    ));

Route::set('admin/parcels/', 'admin/parcels(/<action>(/<id>))')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'orders',
        'action'     => '<action>',
        'id'         => '<id>',
        'is_parcel'  => TRUE,
    ));

Route::set('admin/messages/client', 'admin/messages/client/<id>(:<box>)', array('box' => 'in|out|deleted', 'id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'messages',
        'action'     => 'client',
        'box'        => 'in',
    ));

Route::set('admin/client', 'admin/client/<id>', array('id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'clients',
        'action'     => 'client',
    ));


Route::set('admin/all_log', 'admin/all_log')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'index',
        'action'     => 'all_log',
    ));
Route::set('admin/day_log', 'admin/day_log/<id>', array('id' => '\d+'))
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'index',
        'action'     => 'day_log',
    ));

Route::set('admin/change_status', 'admin/change_status')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'index',
        'action'     => 'change_status',
    ));

Route::set('admin/change_status_skype', 'admin/change_status_skype')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'index',
        'action'     => 'change_status_skype',
    ));

Route::set('admin', 'admin(/<controller>(/<action>(/<id>)))')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'index',
        'action'     => 'index',
  ));

Route::set('admin', 'admin(/<controller>(/<action>(/<id>(/<num>))))')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'index',
        'action'     => 'index',
  ));
Route::set('page', 'admin(/<controller>(/<action>(/<subpage>)))')
    ->defaults(array(
        'directory'  => 'admin',
        'controller' => 'index',
        'action'     => 'index',
        'subpage'    => NULL,
    ));

Route::set('page', 'page/<page>(/<subpage>)')
    ->defaults(array(
        'controller' => 'page',
        'action'     => 'index',
        'page'       => '<page>',
        'subpage'    => NULL,
    ));

Route::set('search', 'search')
    ->defaults(array(
        'controller' => 'search',
        'action'     => 'index',
    ));

Route::set('default', '(<controller>(/<action>(/<id>)))')
	->defaults(array(
		'controller' => 'index',
		'action'     => 'index',
	));

$files = Kohana::list_files('classes/form', array(APPPATH));

if (is_array($files))
    foreach($files as $file)
        Kohana::load($file);

if ( ! defined('SUPPRESS_REQUEST'))
{
	/**
	 * Execute the main request. A source of the URI can be passed, eg: $_SERVER['PATH_INFO'].
	 * If no source is specified, the URI will be automatically detected.
	 */
    $request = Request::instance();

    try
    {
        $request->execute();
    }
    catch (ReflectionException $e)
    {
//        $request->redirect('error/404', 404);
        $request->status = 404;
        $request->response = View::factory('error/404');
    }

	echo $request->send_headers()
		->response;
}
