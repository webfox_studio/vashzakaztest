<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="<?= isset($key) ? $key : '' ?>" />
    <meta name="description" content="<?= isset($desc) ? $desc : '' ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=0.5, user-scalable=yes">
    <meta name = "format-detection" content = "telephone=no" />
    <title>
        <?= isset($title) ? $title : 'Доставка товаров из США' ?>
    </title>
    <?=isset($css_js) ? $css_js : ''?>
</head>

<body>
    
    <header><?=isset($header) ? $header : ''?></header>
            <div class="content_box">
            <div class="left_col">
                <?=isset($panel_left) ? $panel_left : ''?>
            </div>
            <div class="right_col">
                <?=isset($panel_right) ? $panel_right : ''?>
            </div>
            <div class="main_col">
                <?=isset($content) ? $content : ''?>
            </div>
            <div class="clear"></div>
        </div>
     <div class="clear"></div>
     
    <footer><?=isset($footer) ? $footer : ''?></footer>
    <!-- Yandex.Metrika counter -->
    <div style="display:none;">
        <script type="text/javascript">
            (function (w, c) {
                (w[c] = w[c] || []).push(function () {
                    try {
                        w.yaCounter10481140 = new Ya.Metrika({
                            id: 10481140,
                            enableAll: true
                        });
                    } catch (e) {}
                });
            })(window, "yandex_metrika_callbacks");
        </script>
    </div>
    
    <script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script>
    <noscript>
        <div><img src="//mc.yandex.ru/watch/10481140" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <script type="text/javascript">
        var is_online_in_process = false;
        $(document).ready(function () {
            $('#search').focus(
                function () {
                    $(this).css('background-color', '#ffffff');
                    if ($(this).val() == 'ПОИСК ПО САЙТУ') $(this).val('');
                }
            );

            $('#search').blur(
                function () {
                    $(this).css('background-color', '#3E4D6A');
                    if ($(this).val() == '') $(this).val('ПОИСК ПО САЙТУ');
                }
            );


        });
    </script>
    
</body>
</html>