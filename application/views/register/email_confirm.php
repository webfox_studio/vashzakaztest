<table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h20 f9">
        <th valign="top">Регистрация</th>
    </tr>
    <tr>

        <td class="bgc1" style="padding: 8px" valign="center">
            <form action="" method="POST">
                <input type="hidden" name="email" value="<?=isset($email) ? $email : ''?>">
                <input type="hidden" name="reg_step" value="3">
                <div style="color: blue; margin: 0px 24px">
                    <ul>
                        <li>На этот E-mail отправлен код подтверждения. Дождитесь.</li>
						<li>Время посылки письма может занять 10-20 мин.</li>
                        <li>Если так ничего и не пришло, приносим извинения, используйте другой почтовый сервер, например mail.ru</li>
                    </ul>
                </div>
                <? if (isset($code_error)):?>
                <div align="center" class="mb8">
                    <font color="red">Неверный код</font>
                </div>
                <? endif;?>
                <table align="center">
                    <tr>
                        <td>
                            <table align="center" class="bgc2" cellspacing="1" cellpadding="1">
                                <tr class="bg1 ac">
                                    <td width="160">E-mail:</td>
                                    <th class="h18" width="240"><?=isset($email) ? $email : ''?></th>
                                </tr>
                                <tr class="bgc1 ac">
                                    <td class="bg1" width="160">Код подтверждения:</td>
                                    <td width="240">
                                        <input class="w100p" type="text" name="code" value="<?=isset($code) ? $code : ''?>">
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <input class="bg1 b mt4" type="submit" value="Продолжить">
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
