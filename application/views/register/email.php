<table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h20 f9">
        <th valign="top">Регистрация</th>
    </tr>
    <tr>
        <td class="bgc1" style="padding: 8px" valign="center">
            <form action="" method="POST">
                <input type="hidden" name="reg_step" value="2">
                <? if (isset($email_error)):?>
                <div align="center" class="mb8">
                    <font color="red">Введенный E-mail не корректен</font>
                </div>
                <? endif;?>
                <? if (isset($is_confirmed)):?>
                <div align="center" class="mb8">
                    <font color="red">Данный e-mail уже зарегистрирован</font>
                </div>
                <? endif;?>
                <table align="center" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table align="center" class="bgc2" cellspacing="1" cellpadding="1">
                                <tr class="bgc1 ac">
                                    <td class="bg1" width="160">E-mail:</td>
                                    <td width="240">
                                        <input class="w100p" type="text" name="email" value="<?=isset($email) ? $email : ''?>">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <input class="bg1 b mt4" type="submit" value="Продолжить">
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
