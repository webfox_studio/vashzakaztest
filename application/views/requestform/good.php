<table width="100%">
    <tr class="bg1 ac h20 f9">
        <th valign="top">Отправка запроса на расчет</th>
    </tr>
    <tr>
        <td class="bgc1" style="padding: 8px" valign="center">
            <h3 style="font-size: 22px !important; text-align: center; font-weight: bold; color: orange;"><span style="color: #ff6600;">Спасибо!</span></h3>
<p style="text-align: center; font-size: 18px; font-weight: bold;"><span style="color: #3366ff;">Ваша заявка на предварительный расчет успешно отправлена. Пожалуйста, дождитесь ответа от наших сотрудников по указанным Вами контактным данным*.</span></p>
<p style="text-align: center; font-size: 18px; font-weight: bold;">&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: center; font-size: 16px; font-weight: bold;"><span style="font-size: small; color: #0000ff;"><em>* Если ответ не прийдет в течении 24 часов, обратитесь к <a href="../../../page/contacts">оператору</a> на сайте за разъяснением по поводу отсутствия результата</em></span></p>        </td>
</table>


