<h3>Расчет стоимости доставки груза из США</h3>

<div class="block effect6">
    <div>
        <?php if (is_array($error)): ?>
            <p class="red">Проверьте корректность заполнения полей.</p>
        <?php endif; ?>
    </div>
    <div>
        <div>
            <form method="post">
                <ul>
                    <li><h4 class="blue">Вид услуги <span class="red">*</span></h4>
                        <ul>
                            <li><label><input type="radio" name="servise"
                                              value="Полное сопровождение" <?php if (isset($_POST['servise']) AND $_POST['servise'] == 'Полное сопровождение') {
                                        echo 'checked';
                                    } ?>>&nbsp;Полное сопровождение <span style="">(покупка товара и доставка с помощью нашей компании)</span></label>
                            </li>
                            <li><label><input type="radio" name="servise"
                                              value="Виртуальный склад" <?php if (isset($_POST['servise']) AND $_POST['servise'] == 'Виртуальный склад') {
                                        echo 'checked';
                                    } ?> >&nbsp;Виртуальный склад <span style="">(самостоятельная покупка, доставка с помощью нашей компании)</span></label>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li><h4 class="blue">Пожелания относительно доставки <span class="red">*</span></h4></li>
                    <ul>
                        <li><label><input type="radio" name="pozhelaniyaa" value="Максимально экономно">&nbsp;Максимально
                                экономно<span style="">(минимальная стоимость международной пересылки)</span></label>
                        </li>
                        <li><label><input type="radio" name="pozhelaniyaa" value="Максимально быстро">&nbsp;Максимально
                                быстро<span style="">(допустима более дорогая стоимость)</span></label></li>
                        <li><label><input type="radio" name="pozhelaniyaa" value="Максимально надежно">&nbsp;Максимально
                                надежно<span style="">(отсутствие проблем в отношении целостности товара, страховое покрытие, растаможка)</span></label>
                        </li>
                    </ul>
                </ul>
                <ul>
                    <li><h4 class="blue">Ссылка на товар и другая важная информация(цвет, размер, количество и т.п.)
                            <span class="red">*</span></h4></li>
                    <li><textarea name="vazh_info" rows="4" cols="70"><?php if (isset($_POST['vazh_info'])) {
                                echo $_POST['vazh_info'];
                            } ?></textarea></li>
                </ul>
                <ul>
                    <li>Общая стоимость покупки: <span class="red">*</span> $ <input type="text" name="price" size="20"
                                                                                     value="<?php if (isset($_POST['price'])) {
                                                                                         echo $_POST['price'];
                                                                                     } ?>"></li>
                </ul>
                <ul>
                    <li><h4 class="blue">Вес</h4></li>
                    <li>
                        <table border="0">
                            <tr>
                                <td><label><input type="radio" name="ves"
                                                  value="lb" <?php if (isset($_POST['ves']) AND $_POST['ves'] == 'lb') {
                                            echo 'checked';
                                        } ?> > lb</label></td>
                                <td rowspan="2"><label><input type="text" name="ves_kol"
                                                              value="<?php if (isset($_POST['ves_kol'])) {
                                                                  echo $_POST['ves_kol'];
                                                              } ?>"></label></td>
                            </tr>
                            <tr>
                                <td><label><input type="radio" name="ves"
                                                  value="кг" <?php if (isset($_POST['ves']) AND $_POST['ves'] == 'кг') {
                                            echo 'checked';
                                        } ?>> кг</label></td>
                            </tr>
                        </table>
                    </li>
                </ul>
                <ul>
                    <li><h4 class="blue">Габариты</h4></li>
                    <li>
                        <table>
                            <tr>
                                <td><label><input type="radio" name="gabarit"
                                                  value="in" <?php if (isset($_POST['gabarit']) AND $_POST['gabarit'] == 'in') {
                                            echo 'checked';
                                        } ?> > in</label></td>
                                <td rowspan="2"><input type="text" name="ves_kol_1"
                                                       value="<?php if (isset($_POST['ves_kol_1'])) {
                                                           echo $_POST['ves_kol_1'];
                                                       } ?>" size="3"></td>
                                <td rowspan="2"><input type="text" name="ves_kol_2"
                                                       value="<?php if (isset($_POST['ves_kol_2'])) {
                                                           echo $_POST['ves_kol_2'];
                                                       } ?>" size="3"></td>
                                <td rowspan="2"><input type="text" name="ves_kol_3"
                                                       value="<?php if (isset($_POST['ves_kol_3'])) {
                                                           echo $_POST['ves_kol_3'];
                                                       } ?>" size="3"></td>
                            </tr>
                            <tr>
                                <td><label><input type="radio" name="gabarit"
                                                  value="sm" <?php if (isset($_POST['gabarit']) AND $_POST['gabarit'] == 'sm') {
                                            echo 'checked';
                                        } ?> > см</label></td>
                            </tr>
                        </table>
                    </li>
                </ul>
                <ul>
                    <li><h4 class="blue">Данные получателя</h4></li>
                    <ul>
                        <li>
                            <table>
                                <tr>
                                    <td>Страна <span class="red">*</span></td>
                                    <td><input type="text" name="country" value="<?php if (isset($_POST['country'])) {
                                            echo $_POST['country'];
                                        } ?>" size="58"></td>
                                </tr>
                                <tr>
                                    <td>Город <span class="red">*</span></td>
                                    <td><input type="text" name="gorod" value="<?php if (isset($_POST['gorod'])) {
                                            echo $_POST['gorod'];
                                        } ?>" size="58"></td>
                                </tr>
                                <tr>
                                    <td>Почтовый индекс:</td>
                                    <td><input type="text" name="index" value="<?php if (isset($_POST['index'])) {
                                            echo $_POST['index'];
                                        } ?>" size="58"></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </ul>
                <ul>
                    <li><h4 class="blue">Дополнительная информация</h4></li>
                    <li><textarea name="dop_info" rows="4" cols="70"><?php if (isset($_POST['dop_info'])) {
                                echo $_POST['dop_info'];
                            } ?></textarea></li>
                </ul>

                <ul>
                    <li><h4 class="blue">Контактные данные</h4></li>
                    <ul>
                        <li>
                            <table>
                                <tr>
                                    <td>Ф.И.О. <span class="red">*</span></td>
                                    <td><input type="text" name="fio" value="<?php if (isset($_POST['fio'])) {
                                            echo $_POST['fio'];
                                        } ?>" size="58"></td>
                                </tr>
                                <tr>
                                    <td>E-mail: <span class="red">*</span></td>
                                    <td><input type="text" name="email" value="<?php if (isset($_POST['email'])) {
                                            echo $_POST['email'];
                                        } ?>" size="58"></td>
                                </tr>
                                <tr>
                                    <td>Адрес страницы в соц. сети</td>
                                    <td><input type="text" name="contact" value="<?php if (isset($_POST['contact'])) {
                                            echo $_POST['contact'];
                                        } ?>" size="58"></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </ul>

                <ul>

                    <ul>
                        <li>
                            <table>
                                <tr>
                                    <td style="width: 185px;"><?php echo $captcha; ?></td>
                                    <td><input class="form-control" name="site_captcha" type="text" /></td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </ul>


                <table>
                    <tr><td colspan="3"></td></tr>
                    <tr>
                        <td colspan="3">
                            <p><span class="red">*</span> - поля, обязательные для заполнения</p>
                            <input type="submit" name="submit" value="Отправить запрос" class="button sub_raschet">
                            <input class="button" type="reset" value="Очистить">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>