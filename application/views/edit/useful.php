<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><span style="font-size: medium; color: #800000;"><strong>График работы с клиентами</strong></span></p>
<table class="bgc2 w100p ac" cellspacing="1" cellpadding="1">
<tbody>
<tr class="bg1 b ar u h22">
<td class="pr8 f7" colspan="3"><strong>Офис в США - время московское (МСК)</strong></td>
</tr>
<tr class="bgc1 f7">
<td class="p4 bgc3" rowspan="2" width="125">Интерактивная поддержка online:<br /> ICQ, Skype</td>
<td class="p4"><span style="font-size: small;">20:00 - 02:00 *</span></td>
<td class="p4">Понедельник &ndash; Суббота</td>
</tr>
<tr class="bgc1 f7">
<td class="p4" colspan="2"><span style="color: #ef2929;">* уточняющая информация в Примечаниях!</span></td>
</tr>
<tr class="bgc1 f7">
<td class="p4 bgc3" width="130">Ответы на почту e-mail, на сайте, на форумах:</td>
<td class="p4"><br /><span style="font-size: small;">c 18.00 до 8.00</span><br /> (макс. срок ответа &ndash; 24 часа)</td>
<td class="p4">Понедельник - Суббота</td>
</tr>
<tr class="bgc1 f7 al">
<td style="padding: 4px 0px 4px 0px;" colspan="3">
<p>&nbsp;</p>
<p><span style="text-decoration: underline; color: #000000;"><strong>Примечания</strong></span><span style="font-size: small;"><strong><span style="color: #99cc00;"> </span></strong></span></p>
<p><span style="font-size: small;"><strong><span style="color: #99cc00;">&nbsp;&nbsp; </span></strong></span><span style="font-size: medium;"><strong><span style="color: #0000ff;">Четверг, 31 марта&nbsp;</span></strong></span>- <span style="font-size: small;">поддержка по ICQ/Skype c <span style="color: #ff6600;"><strong>21.00</strong></span> до <span style="color: #ff6600;"><strong>23.30</strong></span>, ответы по email/на сайте. Прием, обработка заказов, регистрация и отправка посылок</span><span style="font-size: medium;"> </span></p>
</td>
</tr>
</tbody>
</table>
<p style="text-align: center;"><strong>Внимание! </strong></p>
<p style="text-align: left;">&nbsp;К сожалению,&nbsp; как уже сообщалось прежде в Новостях и рассылке, ряд функций сайта, что успешно работали на старом сайте, все еще недоступны на новом сайте. Приносим свои извинения за неудобство и просим отнестись пониманием к данной ситуации. Пока что не все зависит от нас, но мы делаем все возможное, чтобы исправить проблему.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">&nbsp;Основные моменты, что не работают сейчас, это форум сайта, страница "Полезное", чат с администраторами, уведомления о новых ответах на сайте, что автоматически присылаются на емейл, обновление информации на страницах сайта (кроме главной страницы Новостей), датирование заказов и сообшений, Калькулятор предварительных расчетов (кроме браузера Мозилла), обучающее видео и некоторые более мелкие недоработки.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;">&nbsp;Работают, но не у всех клиентов или со сбоями, следующие функции - обновление отчетов в личном аккаунте,&nbsp; редактирование постов (разметка, ссылки), строчка баланса в главном меню аккаунта на сайте и возможно другие.</p>
<p style="text-align: left;">&nbsp;По мере исправления проблем будем сообщать здесь и в Твиттере, сообщения которого также автоматически переносятся на главную страницу сайта.</p>
<p style="text-align: left;">&nbsp;</p>
<p style="text-align: left;"><span style="color: #ff0000;">&nbsp;Обновление информации от 27 марта</span> - на данный момент исправлены и должны нормально работать следующие функции:</p>
<p style="text-align: left;">1) регистрация на сайте</p>
<p style="text-align: left;">2) восстановление пароля</p>
<p style="text-align: left;">3) Калькулятор предварительных расчетов</p>
<p style="text-align: left;">4) автоматические уведомления о новых ответах в Заказах, Посылках и Сообщениях</p>
<p style="text-align: left;">5) обновление отчетов клиентов и строчка баланса в личном меню на сайте</p>
<p>&nbsp;</p>
<p style="text-align: center;">&nbsp; С уважением, <em>VashZakaz (19 марта 2011)<br /></em></p>