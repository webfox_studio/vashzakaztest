<h3>Авторизация</h3>

<div class="block effect1">
    <div class="account">
        <?= isset($form) ? Block::form('Login_User', array('form' => $form)) : '' ?>
        <div class="reg"><a href="<?=Url::site('register')?>">Регистрация</a></div>
    </div>
    
</div>
<div class="mobile_menu">
	Меню
</div>
<div class="head-menu mobile">
    <div class="topmenu">
        <ul class="nav">
            <li class="btn">
                <a href="/news/all#news/">Новости</a>
                <div class="sub" style="opacity: 0;">
                    <ul>
                                            </ul>
                </div>
            </li>
            <li class="btn">
                <a href="/faq/">Вопросы</a>
                <div class="sub" style="opacity: 0;">
                    <ul>
                                                    <li>
                                <a href="/faq/#1.1">
                                    Посреднические услуги. VashZakaz                                </a>
                            </li>
                                                        <li>
                                <a href="/faq/#2.1">
                                    Наш сайт. Клиентский аккаунт                                </a>
                            </li>
                                                        <li>
                                <a href="/faq/#3.1">
                                    VASHZAKAZ покупает (Полное сопровождение)                                </a>
                            </li>
                                                        <li>
                                <a href="/faq/#4.1">
                                    КЛИЕНТ покупает (Виртуальный склад)                                </a>
                            </li>
                                                        <li>
                                <a href="/faq/#5.1">
                                    Денежные переводы. Оплаты                                </a>
                            </li>
                                                        <li>
                                <a href="/faq/#6.1">
                                    Доставка.  Страховка. Таможня                                </a>
                            </li>
                                                </ul>
                </div>
            </li>
            <li class="btn">
                <a href="/useful/">Полезное</a>
                <div class="sub" style="opacity: 0;">
                    <ul>
                                                    <li>
                                <a href="/useful/cat/9">
                                    О наших услугах                                </a>
                            </li>
                                                        <li>
                                <a href="/useful/cat/6">
                                    Международная доставка товаров                                </a>
                            </li>
                                                        <li>
                                <a href="/useful/cat/7">
                                    Безопасность в сети интернет                                </a>
                            </li>
                                                        <li>
                                <a href="/useful/cat/8">
                                    Все о покупках в США                                </a>
                            </li>
                                                </ul>
                </div>
            </li>
            			                <li class="btn">
                    <a href="/page/tariffs">Полное сопровождение</a>
                    <div class="sub" style="opacity: 0;">
                        <ul>
                                                                                                <li>
                                        <a href="/page/tariffs/main">Основные услуги</a>
                                    </li>
                                                                                                                                    <li>
                                        <a href="/page/tariffs/additional">Дополнительные услуги</a>
                                    </li>
                                                                                                                                    <li>
                                        <a href="/page/tariffs/alterorder">Альтернативный заказ</a>
                                    </li>
                                                                                        </ul>
                    </div>
                </li>
				            			                <li class="btn">
                    <a href="/page/discounts">Акции</a>
                    <div class="sub" style="opacity: 0;">
                        <ul>
                                                                                                <li>
                                        <a href="/page/discounts/discountmain">общая информация</a>
                                    </li>
                                                                                                                                                                                                <li>
                                        <a href="/page/discounts/Discounts_of_the_company">Скидки от компании</a>
                                    </li>
                                                                                        </ul>
                    </div>
                </li>
				            			                <li class="btn">
                    <a href="/page/shipping">Доставка</a>
                    <div class="sub" style="opacity: 0;">
                        <ul>
                                                                                                <li>
                                        <a href="/page/shipping/shipmain">Общие положения</a>
                                    </li>
                                                                                                                                    <li>
                                        <a href="/page/shipping/shipinsur">Страхование</a>
                                    </li>
                                                                                                                                    <li>
                                        <a href="/page/shipping/custom">Таможня</a>
                                    </li>
                                                                                                                                    <li>
                                        <a href="/page/shipping/restrictions">Ограничения</a>
                                    </li>
                                                                                                                                    <li>
                                        <a href="/page/shipping/countries">Страны</a>
                                    </li>
                                                                                        </ul>
                    </div>
                </li>
				            			                <li class="btn">
                    <a href="/page/mforwarding">Виртуальный склад</a>
                    <div class="sub" style="opacity: 0;">
                        <ul>
                                                                                                <li>
                                        <a href="/page/mforwarding/mainmailf">Основные услуги</a>
                                    </li>
                                                                                                                                    <li>
                                        <a href="/page/mforwarding/additionalmf">Дополнительные услуги</a>
                                    </li>
                                                                                                                                    <li>
                                        <a href="/page/mforwarding/rulesmailf">Условия и ограничения</a>
                                    </li>
                                                                                        </ul>
                    </div>
                </li>
				            			                <li class="btn">
                    <a href="/page/moneytransfer">Денежные переводы</a>
                    <div class="sub" style="opacity: 0;">
                        <ul>
                                                                                                <li>
                                        <a href="/page/moneytransfer/commissiontrf">Комиссии переводов</a>
                                    </li>
                                                                                                                                    <li>
                                        <a href="/page/moneytransfer/transferdetails">Условия и описания</a>
                                    </li>
                                                                                        </ul>
                    </div>
                </li>
				            			                        <li class="btn"><a target="_blank" href="/board">Форум</a></li>
        </ul>
    </div>
</div>
