<h3>Персональный кабинет</h3>

<div class="block effect6">
    
    <div>Добро пожаловать,</div>
    <div><a class="nd blue" href="<?= Url::site('user/data') ?>" title="Информация пользователя"><?= Auth::instance()->get_user()->user_data->name ?></a></div>
    <div class="hb"><? if (Auth::instance()->logged_in('admin') OR Auth::instance()->logged_in('redactor')): ?> <a class="red" href="<?= Url::site('admin') ?>"><strong>Admin</strong></a><? endif; ?> | <a href="<?= Url::site('user/logout') ?><? if ( isset($_COOKIE['phpbb_sid'])): ?>?sid=<?=$_COOKIE['phpbb_sid']?><?endif;?>">Выход</a></div>
    
    <div class="clear"></div>
    <div class=" effect6 text2">Ваш аккаунт: <?//if($balans > 0):?><!--green--><?//else:?><!--red--><?//endif?><!--">$ --><?//= $balans ? $balans : 0 ?><br>Ваш баланс:</span> <span class="ar <?if($balans > 0):?>green<?else:?>red<?endif?>">$ <?= $balans ? $balans : 0 ?></span></div>
    
    <div class="dot">&nbsp;</div>
    <div id="accountMenu">
        <div style="text-align: center; cursor: pointer; padding: 10px; font-weight: bold;">МЕНЮ</div>
    </div>
    <div class="accountMenuList">
        <div class="hb dot"><img src="/media/img/im_19.png"><a href="<?=Url::site('user/data')?>">Персональные данные</a></div>
        <div class="hb dot"><img src="/media/img/im_20.png"><?php if($count_new_messages OR $count_new_orders OR $count_new_parcels OR $count_new_files OR $count_new_payments){ ?><a class="blink" href="<?=Url::site('user/latestupdates')?>">Последние обновления</a> <?php } else { ?> <a href="<?=Url::site('user/latestupdates')?>">Последние обновления</a><?php } ?></div>
        <div class="hb dot"><img src="/media/img/im_23.png"><a href="<?=Url::site('messages/send')?>">Отправить сообщение</a></div>
        <div class="hb dot"><img src="/media/img/im_38.png"><a href="<?=Url::site('messages')?>">Сообщения<? if(isset($count_new_messages) AND $count_new_messages): ?> (<?=$count_new_messages?>)<? endif; ?></a></div>
        <div class="hb dot"><img src="/media/img/im_2.png"><a href="<?=Url::site('payments/new')?>">Сообщить о переводе</a></div>
        <div class="hb dot"><img src="/media/img/im_22.png"><a href="<?=Url::site('payments')?>">Денежные переводы<? if(isset($count_new_payments) AND $count_new_payments): ?> (<?=$count_new_payments?>)<? endif; ?></a></div>
        <div class="hb dot"><img src="/media/img/im_5.png"><a href="<?=Url::site('orders/new')?>">Оформить заказ</a></div>
        <div class="hb dot"><img src="/media/img/im_18.png"><a href="<?=Url::site('orders')?>">Заказы<? if(isset($count_new_orders) AND $count_new_orders): ?> (<?= $count_new_orders ?>)<? endif; ?></a></div>
        <div class="hb dot"><img src="/media/img/im_8.png"><a href="<?=Url::site('parcels/new')?>">Посылку к отправке</a></div>
        <div class="hb dot"><img src="/media/img/im_35.png"><a href="<?=Url::site('parcels')?>">Посылки<? if(isset($count_new_parcels) AND $count_new_parcels): ?> (<?= $count_new_parcels ?>)<? endif; ?></a></div>
        <div class="hb dot"><img src="/media/img/im_21.png"><a href="<?=$show_report?>">Скачать отчет</a></div>
        <div class="hb dot"><img src="/media/img/im_15.png"><a href="<?=Url::site('files')?>">Файлы<? if(isset($count_new_files) AND $count_new_files): ?> (<?= $count_new_files ?>)<? endif; ?></a></div>
        <div class="hb dot"><img src="/media/img/sovet.png"><a href="<?=Url::site('offers')?>">Советы и замечания</a></div>
        <div class="hb dot"><img src="/media/img/im_4.png"><a href="<?=Url::site('subscription')?>">Инфор-ные сообщения</a></div>

        <?php if(Auth::instance()->get_user()->user_data->user_id == 8): ?>
            <div class="hb dot"><img src="/media/img/im_35.png"><a href="<?=Url::site('trackings')?>">Трекинги</a></div>
        <?php endif; ?>
    </div>
</div>

<script language="JavaScript">
        t = 500;
        blk = ["white", "black"];
        spacei=0;
        slinks = document.getElementsByTagName("a");
        function blk_b()
        {
        for (s=0;s<slinks.length;s++){
        if(slinks[s].className == "blink"){
        slinks[s].style.color=blk[spacei];
        }}
        spacei++;
        if(spacei == blk.length) {spacei=0;}
        setTimeout("blk_b()", t);
        };
        window.onload=blk_b();
</script>