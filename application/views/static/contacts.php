<p><a name="shed"></a></p>
<h3>График работы с клиентами</h3>
<div class="text effect1">
<div>Время работы московское (МСК)</div>
<div class="table">
<div class="row">
<div class="cell" style="width: 180px;">Звонки и чат в Skype:</div>
<div class="cell"><strong>Понедельник-Пятница, 19.00-24.00</strong><strong>&nbsp;</strong><strong> </strong>(регулярные сроки)</div>
</div>
<div class="row">
<div class="cell" style="width: 180px;">Ответы на сайте, по эл. почте</div>
<div class="cell"><strong class="red">&nbsp;</strong><strong>Понедельник-Суббота, 17.00-8.00 </strong>(макс. срок &ndash; 24 часа)</div>
</div>
<div class="row">
<div class="cell" style="width: 180px;">Ответы на форумах, в соцсетях т.п.</div>
<div class="cell"><strong>&nbsp;</strong><strong>Понедельник-Суббота, 17.00-8.00 </strong>(макс. срок &ndash; 48 часов)</div>
</div>
</div>
</div>
<h3>Контакты</h3>
<div class="text effect1">
<div><strong>ВАШ АККАУНТ</strong> на нашем сайте &mdash; оформление заказов, формирование посылок, реквизиты для оплат, извещения о переводах, уточнения, изменения по заказам и&nbsp;т.п.</div>
<div><span class="red">(!)</span> &mdash; это основной вариант контактирования с нами!</div>
<div><strong><em>&nbsp;</em></strong></div>
</div>
<h3>Дополнительные контакты</h3>
<div class="text effect1"><!--
<div>Помощь в оформлении заказов, посылок, оплат, предварительных расчетов, все возможные вопросы &laquo;новичков&raquo;:</div>
-->     <!--
<div><a href="skype:lucia13666?call" _mce_href="skype:lucia13666?call"><img title="Добавить в список контактов" src="../../images/sk_al.png" _mce_src="../../images/sk_al.png" alt="" width="240" /></a></div>
-->     <!--
<div>Срочные вопросы, сложные заказы, обсуждение вариантов сотрудничества, претензии, запросы дополнительных услуг, вопросы по содержимому посылок, упаковке товаров и т.п.</div>
-->     <!--
<div><a href="skype:tolich25?call" _mce_href="skype:tolich25?call"><img title="Добавить в список контактов" src="../../images/sk_yar.png" _mce_src="../../images/sk_yar.png" alt="" width="240" /></a></div>
-->
<div>Помощь в оформлении заказов, посылок, оплат, предварительных расчетов, все возможные вопросы &laquo;новичков&raquo;:</div>
<div><a href="skype:lucia13666?call"><img title="Добавить в список контактов" src="../../images/skype2.png" alt="" width="340" /></a></div>
<div>
<script type="text/javascript">// <![CDATA[
(function() {var script=document.createElement("script");script.type="text/javascript";script.async =true;script.src="https://telegram.im/widget-button/index.php?id=@VashZakaz";document.getElementsByTagName("head")[0].appendChild(script);})();
// ]]></script>
<a class="telegramim_button telegramim_shadow" style="font-size: 12px; width: 320px; background: #3d85c6; box-shadow: 1px 1px 5px #3d85c6; color: #ffffff; border-radius: 7px;" href="https://telegram.im/@VashZakaz" target="_blank"><em>&nbsp;</em> Добавить в контакты: Алёна</a></div>
<br />
<div><a href="skype:violet.vt?call"><img title="Добавить в список контактов" src="../../images/skype3.png" alt="" width="340" /></a></div>
<div>
<script type="text/javascript">// <![CDATA[
(function() {var script=document.createElement("script");script.type="text/javascript";script.async =true;script.src="https://telegram.im/widget-button/index.php?id=@VashZakaz";document.getElementsByTagName("head")[0].appendChild(script);})();
// ]]></script>
<a class="telegramim_button telegramim_shadow" style="font-size: 12px; width: 320px; background: #3d85c6; box-shadow: 1px 1px 5px #3d85c6; color: #ffffff; border-radius: 7px;" href="https://telegram.im/@VZ_Oksana" target="_blank"><em>&nbsp;</em> Добавить в контакты: Оксана</a></div>
<br />
<div>Срочные вопросы, сложные заказы, обсуждение вариантов сотрудничества, претензии, запросы дополнительных услуг, вопросы по содержимому посылок, упаковке товаров и т.п.</div>
<div><a href="skype:tolich25?call"><img title="Добавить в список контактов" src="../../images/skype1.png" alt="" width="340" /></a></div>
<div>
<script type="text/javascript">// <![CDATA[
(function() {var script=document.createElement("script");script.type="text/javascript";script.async =true;script.src="https://telegram.im/widget-button/index.php?id=@VashZakaz";document.getElementsByTagName("head")[0].appendChild(script);})();
// ]]></script>
<a class="telegramim_button telegramim_shadow" style="font-size: 12px; width: 320px; background: #3d85c6; box-shadow: 1px 1px 5px #3d85c6; color: #ffffff; border-radius: 7px;" href="https://telegram.im/@VashZakaz" target="_blank"><em></em> Добавить в контакты: Ярослав</a></div>
<br /> <br />
<div>Пересылка копий самостоятельно оплаченых инвойсов, подтверждений о покупках, выигрышах лотов через свои (клиентские) аккаунты и т.п. &mdash; <a href="mailto:self.paid.invoice@gmail.com" target="_blank">self.paid.invoice@gmail.com</a></div>
<div>Предложения по сотрудничеству, неожиданные и срочные вопросы, пересылка документов для верификации самостоятельных покупок и квитанций оплат, вопросы страховки, таможенной очистки - <a href="mailto:vashzakaz@vashzakaz.us" target="_blank"><span class="username">vashzakaz@vashzakaz.us</span></a></div>
<div><span class="username"><br /></span></div>
<div>В особо срочных случаях возможен также звонок по <a href="../../page/about">телефону в США</a></div>
</div>
<h3>Мы в социальных сетях</h3>
<div class="text effect1"><a href="http://vkontakte.ru/club10603912" target="_blank"><img src="../../media/img/vkontakte.jpg" border="0" alt="Мы Вконтакте" height="30px" /></a>&nbsp;&nbsp;&nbsp;&nbsp; <a href="http://www.facebook.com/pages/VashZakaz/153129784732527?v=wall" target="_blank"><img src="../../media/img/fb.jpg" border="0" alt="Мы в Фейсбуке" height="30px" /></a>&nbsp;&nbsp;&nbsp;<a href="http://vashzakaz.livejournal.com/382.html" target="_blank"><img src="../../media/img/livejournal.gif" alt="" height="30" /></a>&nbsp; <a href="http://twitter.com/VashZakazUS" target="_blank"><img src="../../media/img/twitter.png" border="0" alt="Мы в Твиттере" height="30px" /></a>&nbsp;
<div id="_mcePaste" class="mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow: hidden;"><!--[if gte mso 9]><xml> <o:OfficeDocumentSettings> <o:RelyOnVML /> <o:AllowPNG /> </o:OfficeDocumentSettings> </xml><![endif]-->
<p class="MsoNormal" style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: normal;">поддержка по ICQ/Skype с <strong><span>21.30</span></strong> до <strong><span>23.00</span></strong> , периодические ответы по email, на сайте, оформление заказов, регистрация и отправка посылок</p>
</div>
</div>