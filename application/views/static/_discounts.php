<table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h20 f9">
        <th valign="top">
            <a href=index.php?page=discounts>Скидки</a>              </th>
    </tr>
    <tr>
        <td class="bgc1" style="padding: 8px" valign="center">
            <a name="clientdiscount"></a>
            <p style="text-align: center;"><span style="font-size: medium;"><strong>Скидки клиентов</strong></span></p>

            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;"><strong><span style="text-decoration: underline;">Индивидуальная скидка &laquo;Доверие&raquo;:</span></strong></p>
            <p style="text-align: center;"><strong><span style="text-decoration: underline;"><br /></span></strong></p>
            <p class="aj">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Предлагается индивидуально, на наше усмотрение, для отдельных  клиентов как скидка к комиссии за услуги или бонус-премия на счет  клиента. Может предоставляться за разного рода услуги, помощь нашей  компании, длительное сотрудничество или просто, за Вашу поддержку и  взаимопонимание.</p>
            <p>&nbsp;</p>
            <p style="text-align: center;"><strong><span style="text-decoration: underline;">Скидка &laquo;Активное сотрудничество&raquo;:</span></strong></p>
            <p style="text-align: center;"><strong><span style="text-decoration: underline;"><br /></span></strong></p>
            <p class="aj">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Мы действительно ценим ваш вклад в нашу компанию и хотим  предложить выгодные скидки, зависимо от прибыли, которую получаем от  Вас. Скидка подсчитывается соответственно общей сумме комиссии, включая  упаковку, в конце месяца и по его результатам действует на протяжении  следующего месяца:</p>

            <ul>
                <li><strong>250.00$</strong> комиссии - <span style="color: #993300; font-size: small;"><strong>8%</strong></span> комиссия в следующем месяце</li>
                <li><strong>500.00$</strong> комиссии - <span style="color: #993300; font-size: small;"><strong>7%</strong></span> комиссия в следующем месяце</li>
            </ul>
            <p style="text-align: center;"><strong><span style="text-decoration: underline;">Скидка &laquo;Дорогой заказ&raquo;:</span></strong></p>

            <p style="text-align: center;"><strong><span style="text-decoration: underline;"><br /></span></strong></p>
            <p class="aj">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Предлагается на дорогостоящие лоты в одного продавца на аукционах  или соответственно заказы в одном магазине. Точная сумма скидки  устанавливается при индивидуальном рассмотрении зависимо от наличия  других скидок, бонусов, сложности заказа, количества необходимых услуг и  т.п.</p>
            <ul>
                <li>Заказы стоимостью <strong>500-750$</strong> -  комиссия <span style="color: #993300; font-size: small;"><strong>8 %</strong></span></li>
                <li>Заказы стоимостью <strong>750-1000$</strong> -  комиссия <span style="color: #993300; font-size: small;"><strong>7 %</strong></span></li>
                <li>Заказы стоимостью <strong>1000-1500$</strong> -  комиссия <span style="color: #993300; font-size: small;"><strong>6 %</strong></span></li>

                <li>Заказы стоимостью свыше <strong>1500$</strong> -  комиссия <span style="color: #993300; font-size: small;"><strong>5 %</strong></span> или по договоренности</li>
            </ul>
            <p>&nbsp;</p>
            <p><strong>* <span style="text-decoration: underline;">Примечание:</span></strong></p>
            <p><span style="color: blue;">
                    <p class="aj i">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Изложенные на этой странице предложения на данный момент относятся к заказам через <span style="text-decoration: underline;">американский офис</span>. В отношении заказов по Европе скидки могут предоставляться индивидуально, при оформлении заказа.</p>

                </span></p><br /><br /><p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;">&nbsp;</p>

            <a name="forumdiscount"></a>
            <p style="text-align: center;">&nbsp;<span style="font-size: medium;"><strong>Скидки форумчан</strong></span></p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;Мы предлагаем новую систему <span style="font-size: small; color: #000000;">скидок</span> для активных участников нашего нового <a href="index.php?page=board"><span style="font-size: small;">форума</span></a>.</p>
            <p>&nbsp;Мы ценим Ваше время, внимание и активность. Потому предлагаем двойную выгоду - используйте информацию форума, чтобы самому стать более успешным покупателем, предпринимателем, делитесь Вашими знаниями с другими участниками и в тоже самое время получите материальную пользу в виде предсталенных ниже скидок на наши услуги для Вас, как уважаемых клиентов нашей компании (нынешних или потенциальных)</p>
            <p>&nbsp;</p>

            <p>&nbsp; <span style="font-size: small; color: #339966;">* участники форума, имеющие <span style="color: #993300;"><strong>10</strong></span> сообщений получают скидку в размере <span style="color: #ff6600;"><strong>20%</strong></span> от основной комиссии VashZakaz на любой выбранный Вами заказ</span></p>
            <p><span style="font-size: small; color: #339966;">&nbsp; * участники форума, имеющие <span style="color: #993300;"><strong>25</strong></span> сообщений получают скидку в размере <span style="color: #ff6600;"><strong>25%</strong></span> от основной комиссии VashZakaz на заказ <strong>+</strong> <span style="color: #ff6600;"><strong>25% </strong></span>скидку на стоимость его упаковки</span></p>

            <p><span style="font-size: small; color: #339966;">&nbsp;* участники форума, имеющие <span style="color: #993300;"><strong>50</strong></span> сообщений получают скидку в размере <span style="color: #ff6600;"><strong>50%</strong></span> от основной комиссии VashZakaz на заказ<strong> +</strong> <span style="color: #ff6600;"><strong>50% </strong></span>скидку на стоимость его упаковки</span></p>
            <p><span style="font-size: small; color: #339966;">&nbsp;* участники форума, имеющие <span style="color: #993300;"><strong>100</strong></span> сообщений получают скидку в размере <span style="color: #ff6600;"><strong>100%</strong></span> от основной комиссии VashZakaz на <span style="text-decoration: underline;">1 заказ</span> <strong>+ </strong><span style="color: #ff6600;"><strong>100% </strong></span>скидку на стоимость его упаковки либо <span style="color: #ff6600;"><strong>100%</strong></span> скидку регистрации посылок на складе VZ на протяжении <strong>1 месяца</strong></span></p>

            <p><span style="font-size: small; color: #339966;">&nbsp;<strong></strong></span></p>
            <p>&nbsp;</p>
            <p><span style="font-size: small;"><em><span style="color: #339966;"><span style="color: #3366ff;"><strong><span style="color: #ff0000;"><span style="color: #000000;">&nbsp;</span></span></strong></span></span></em></span><span style="font-size: small; color: #339966;"><span style="color: #3366ff;"><strong><span style="font-size: x-small; color: #ff0000;"><span style="color: #000000;">&nbsp;</span></span></strong><span style="color: #ff0000;"><span style="color: #000000;"> Для получения той или иной скидки нужно обратиться с соответственным запросом при формировании выбранного для скидки заказа или на сайте в <strong>Сообщениях</strong> для уточнения такой возможности в дальнейшем. </span></span></span></span></p>
            <p><span style="font-size: small; color: #339966;"><span style="color: #3366ff;"><span style="color: #ff0000;"><span style="color: #000000;">&nbsp;Система предоставляемых здесь скидок (впрочем как и любые другие условия сотрудничества) вполне возможно будет изменена или удалена в будущем, но мы твердо гарантируем, что все заработанные скидки будут выполнены соответственно предоставленным выше условиям.</span></span></span></span></p>
            <p><span style="font-size: small;"><em><span style="color: #339966;"><span style="color: #3366ff;"><span style="color: #ff0000;"><span style="color: #000000;">&nbsp;Просим также учесть, если кто станет наполнять форум флудом, флеймом, бессодержательными,&nbsp; пустыми сообщениями, спамом и несанкционированной рекламой, то скидок может и не заработать, но точно заработает проблемы с администрацией форума, предупреждения и в перспективе бан как участник форума</span></span></span></span></em></span></p>
            <p><span style="font-size: small; color: #339966;"><span style="color: #3366ff;"><span style="font-size: small; color: #000000;">&nbsp;Добро пожаловать на <a href="index.php?page=board">форум</a>!&nbsp; </span><strong><span style="font-size: x-small; color: #ff0000;"><span style="color: #000000;"><br /></span></span></strong></span></span></p><br /><br />            </td>

    </tr>
</table>
