<?php
$content = ORM::factory('discount')->order_by('addDate', 'DESC')->find_all();
?>

<h3>Акции: Скидки от компании</h3>
<div class="text effect6">
    <div>
        <p>В данном разделе Вы сможете ознакомиться с акциями и скидками, которые предоставляет непосредственно наша компания.</p>
        <p>Это могут быть как сезонные или праздничные, так и экслюзивные скидки, купоны и промокоды, которые магазины предоставляют непосредственно нашей компании.</p>
    </div>
    <br>
    <hr>
    <br>
    <div>
        <?php foreach ($content as $key => $value): ?>
            <div style="min-height: 150px; border-bottom: 1px dotted gray; margin-bottom: 20px;padding-bottom: 20px;">
                <div class="text1">
                    <h3><?= $value->title ?></h3>
                </div>
                <div>
                <span>
                    <img class="news_img" style="width:100px;float: left;margin: 0 1em 0 0;"
                         src="/upload/discount/<?= $value->img ?>">
                </span>
                    <span><?= $value->description ?></span>
                    <div class="data"><?=date('d.m.Y', strtotime($value->addDate))?></div>
                </div>

            </div>
        <?php endforeach; ?>
    </div>
</div>