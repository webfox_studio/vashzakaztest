<table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h20 f9">
        <th valign="top">
            <a href=index.php?page=shipping>Доставка</a>              </th>
    </tr>
    <tr>
        <td class="bgc1" style="padding: 8px" valign="center">
            <p style="text-align: center;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>

            <a name="ship"></a>
            <p style="text-align: center;">&nbsp;</p>
            <p style="text-align: center;"><span style="font-size: small;"><strong>ТАРИФЫ* НА ПЕРЕСЫЛКУ</strong></span><strong>&nbsp;</strong></p>
            <p style="text-align: center;"><strong>в Россию**, Украину, Белорусь</strong></p>
            <p style="text-align: center;"><strong>&nbsp; и Молдову через USPS (государственная почта США)</strong></p>
            <p>&nbsp;</p>
            <p><span style="color: #f57900;">* Внимание! Данные тарифы включают в себя стоимость услуг по упаковке и стоимость упаковочных материалов. Также для <em>Priority mail</em> включена услуга по отправке в местном почтовом отделении (бесплатно для EMS)</span></p>
            <p><span style="color: #ff6600;">&nbsp;&nbsp; В&nbsp;отдельных случаях (особо сложная, дорогостоящая упаковка,   использование&nbsp;доп. проф. сервиса&nbsp;и т.п.), расценки могут несколько   отличаться и оговариваться с клиентом&nbsp;индивидуально.</span></p>

            <p><span style="color: #ff0000;"><br /></span></p>
            <p>** Максимально допустимые габариты и вес, что указанны в таблице, относятся именно к России - по остальным республикам этот вопрос необходимо уточнять с нами</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <table class="bgc2" cellspacing="1" cellpadding="1" width="707" height="1270" align="center">
                <col width="217"></col> <col width="229"></col> <col width="242"></col>
                <tbody>
                    <tr class="bg1" height="18">
                        <td width="217">
                            <div><strong>&nbsp;</strong></div>
                            <br /></td>
                        <td width="229">
                            <div><strong>&nbsp;Express Mail International (EMS)*<br /></strong></div>

                        </td>
                        <td width="242">
                            <div><strong>&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Priority Mail International**<br /></strong></div>
                        </td>
                    </tr>
                    <tr class="bg1" height="51">
                        <td style="text-align: center;" width="217" height="51">
                            <div><strong>Bec, в    фунтах (кг)</strong></div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>Макс. длина -    152,40 см<br />Сумма длины и обхвата (периметр боковой стенки) - 274,30 см</strong></div>

                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>Макс. длина -    106,70 см<br />Сумма длины и обхвата (периметр боковой стенки) - 200,70 см</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>1 (    0,46 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>41.00</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">

                            <div><strong>38.25</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>2 (    0,91 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>45.35</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>42.00</strong></div>
                        </td>

                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>3 (    1,36 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>49.70</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>45.75</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">

                            <div>4 (    1,80 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>54.05</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>49.50</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>5 (    2,27 )</div>
                        </td>

                        <td style="text-align: center;" width="229">
                            <div><strong>58.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>53.25</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>6 (    2,73 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>65.75</strong></div>

                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>60.20</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>7 (    3,18 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>70.10</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>64.15</strong></div>

                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>8 (    3,64 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>74.45</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>68.10</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">

                        <td style="text-align: center;" width="217" height="17">
                            <div>9 (    4,09 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>78.80</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>72.05</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>10 (    4,54 )</div>

                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>83.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>76.00</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>11 (    5,00 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>89.40</strong></div>

                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>81.95</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>12 (    5,46 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>93.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>85.90</strong></div>

                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>13 (    5,91 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>97.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>89.85</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">

                        <td style="text-align: center;" width="217" height="17">
                            <div>14 (    6,36 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>102.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>93.80</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>15 (    6,82 )</div>

                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>106.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>97.75</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>16 (    7,27 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>113.65</strong></div>

                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>104.70</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>17 (    7,73 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>117.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>108.65</strong></div>

                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>18 (    8,18 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>122.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>112.60</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">

                        <td style="text-align: center;" width="217" height="17">
                            <div>19 (    8,64 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>126.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>116.55</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>20 (    9,09 )</div>

                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>130.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>120.50</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>21 (    9,55 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>134.90</strong></div>

                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>124.45</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>22 (    10,00 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>139.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>128.40</strong></div>

                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>23 (    10,46 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>143.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>132.35</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">

                        <td style="text-align: center;" width="217" height="17">
                            <div>24 (    10,91 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>147.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>136.30</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>25 (    11,36 )</div>

                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>151.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>140.25</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>26 (    11,82 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>159.15</strong></div>

                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>147.20</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>27 (    12,27 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>163.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>151.15</strong></div>

                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>28 (    12,73 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>167.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>155.10</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">

                        <td style="text-align: center;" width="217" height="17">
                            <div>29 (    13,18 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>171.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>159.05</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>30 (    13,64 )</div>

                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>176.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>163.00</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>31 (    14,09 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>180.40</strong></div>

                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>166.95</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>32 (    14,55 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>184.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>170.90</strong></div>

                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>33 (    15,00 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>188.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>174.85</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">

                        <td style="text-align: center;" width="217" height="17">
                            <div>34 (    15,46 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>193.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>178.80</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>35 (    15,91 )</div>

                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>197.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>182.75</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>36 (    16,36 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>203.65</strong></div>

                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>188.70</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>37 (    16,82 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>207.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>192.65</strong></div>

                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>38 (    17,27 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>212.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>196.60</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">

                        <td style="text-align: center;" width="217" height="17">
                            <div>39 (    17,73 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>216.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>200.55</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>40 (    18,18 )</div>

                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>220.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>204.50</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>41 (    18,64 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>224.90</strong></div>

                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>208.45</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>42 (    19,09 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>229.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>212.40</strong></div>

                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>43 (    19,55 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>233.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>216.35</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">

                        <td style="text-align: center;" width="217" height="17">
                            <div>44 (    20,00 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>237.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242">
                            <div><strong>220.30</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>45 (    20,46 )</div>

                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>241.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>46 (    20,91 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>248.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>

                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>47 (    21,36 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>252.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>48 (    21,82 )</div>
                        </td>

                        <td style="text-align: center;" width="229">
                            <div><strong>256.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>49 (    22,27 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>260.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>

                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>50 (    22,73 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>265.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>51 (    23,18 )</div>
                        </td>
                        <td style="text-align: center;" width="229">

                            <div><strong>269.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>52 (    23,64 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>273.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">

                        <td style="text-align: center;" width="217" height="17">
                            <div>53 (    24,09 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>277.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>54 (    24,55 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>282.15</strong></div>

                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>55 (    25,00 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>286.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>56 (    25,46 )</div>

                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>293.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>57 (    25,91 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>297.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>

                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>58 (    26,36 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>302.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>59 (    26,82 )</div>
                        </td>

                        <td style="text-align: center;" width="229">
                            <div><strong>306.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>60 (    27,27 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>310.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>

                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>61 (    27,73 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>314.90</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>62 (    28,18 )</div>
                        </td>
                        <td style="text-align: center;" width="229">

                            <div><strong>319.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>63 (    28,64 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>323.40</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">

                        <td style="text-align: center;" width="217" height="17">
                            <div>64 (    29,09 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>327.65</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>65 (    29,55 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>331.90</strong></div>

                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                    <tr class="bgc1" height="17">
                        <td style="text-align: center;" width="217" height="17">
                            <div>66 (    30.00 )</div>
                        </td>
                        <td style="text-align: center;" width="229">
                            <div><strong>336.15</strong></div>
                        </td>
                        <td style="text-align: center;" width="242"><br /></td>
                    </tr>
                </tbody>
            </table><br /><br />
            <a name="strah"></a>
            <p style="text-align: center;"><span style="font-size: small;"><strong>СТРАХОВАНИЕ</strong></span></p>

            <p style="text-align: center;"><span style="font-size: small;"><strong><br /></strong></span></p>
            <p>&nbsp;</p>
            <table class="bgc2" border="0" cellspacing="1" cellpadding="1" width="739" height="159" align="center">
                <tbody>
                    <tr class="bg1">
                        <td style="text-align: center;" width="33%">
                            <div><strong>Стоимость</strong></div>
                        </td>
                        <td style="text-align: center;" width="33%">
                            <div><strong>EMS</strong></div>
                        </td>
                        <td style="text-align: center;" width="33%">
                            <div><strong>Priority mail</strong></div>
                        </td>

                    </tr>
                    <tr class="bgc1">
                        <td style="text-align: center;">
                            <div>$0 - $50</div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$0.00</strong></div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$2.30</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1">
                        <td style="text-align: center;">

                            <div>$51 - $100</div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$0.00</strong></div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$3.40</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1">
                        <td style="text-align: center;">
                            <div>$101 - $200</div>
                        </td>

                        <td style="text-align: center;">
                            <div><strong>$0.80</strong></div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$4.50</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1">
                        <td style="text-align: center;">
                            <div>$201 - $300</div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$2.25</strong></div>

                        </td>
                        <td style="text-align: center;">
                            <div><strong>$5.60</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1">
                        <td style="text-align: center;">
                            <div>$301 - $400</div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$2.25</strong></div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$6.70</strong></div>

                        </td>
                    </tr>
                    <tr class="bgc1">
                        <td style="text-align: center;">
                            <div>$401 - $500</div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$2.25</strong></div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$7.80</strong></div>
                        </td>
                    </tr>
                    <tr class="bgc1">

                        <td style="text-align: center;">
                            <div>$501 - $1000</div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$3.70</strong></div>
                        </td>
                        <td style="text-align: center;">
                            <div>добавить <strong>$1.10</strong> за каждые 100.00</div>
                        </td>
                    </tr>
                    <tr class="bgc1">

                        <td style="text-align: center;">
                            <div>$1001 - $1500</div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$5.15</strong></div>
                        </td>
                        <td style="text-align: center;">
                            <div>добавить <strong>$1.10</strong> за каждые 100.00</div>
                        </td>
                    </tr>
                    <tr class="bgc1">

                        <td style="text-align: center;">
                            <div>$1501 - $2000</div>
                        </td>
                        <td style="text-align: center;">
                            <div><strong>$6.60</strong></div>
                        </td>
                        <td style="text-align: center;">
                            <div>добавить <strong>$1.10</strong> за каждые 100.00</div>
                        </td>
                    </tr>
                    <tr class="bgc1">

                        <td style="text-align: center;">
                            <div>$2001 - $5000</div>
                        </td>
                        <td style="text-align: center;">
                            <div>добавить <strong>$1.45</strong> за каждые 500.00</div>
                        </td>
                        <td style="text-align: center;">
                            <div>добавить <strong>$1.10</strong> за каждые 100.00</div>

                        </td>
                    </tr>
                </tbody>
            </table>
            <br /><br />
            <a name="shiptype"></a>
            <p style="text-align: center;"><strong><span style="font-size: small;">ВИДЫ ДОСТАВКИ</span></strong></p>
            <p style="text-align: center;"><strong><span style="font-size: small;"><br /></span></strong></p>
            <p>&nbsp;</p>
            <p>&nbsp;&nbsp;&nbsp; <strong>* Express Mail Service (EMS)</strong> - это скоростная курьерская доставка по принципу &laquo;от двери до двери&raquo;. В США является отделением государственной почты USPS. Услуга по экспресс-доставке отправлений ЕМС оказывается более чем в 190 странах мира. В России эту услугу оказывает &laquo;EMS Почта России&raquo;. Предельный вес экспресс-отправлений EMS: 30 кг (66 фунтов). Максимальная длина любого измерения в упаковке должна составлять не более 91,44 см. Сумма длины и периметра наибольшего поперечного сечения не должна превышать 200,66 см. На практике случается иногда немного обходить эти ограничения. Трекинг-номера этого вида отправки дают отличную возможность отслеживать все перемещения вашей посылки в online режиме, на почтовых сайтах. Страховка покрывает как стоимость посылки, так и саму пересылку.</p>
            <p><strong>&nbsp;&nbsp;&nbsp; </strong></p>

            <p><strong>&nbsp; ** Priority Mail International</strong> - отделение государственной почты США, доставка являет собой обычную авиапочту, но включает в себя также возможность страхования посылок и во многих случаях отслеживания перемещений посылки online. Получение посылки - в Вашем почтовом отделении. Несколько дешевле по сравнению с EMS, но не всегда отслеживается на почтовых сайтах и имеет обычно несколько более длительные сроки доставки. Страхуется лишь стоимость посылки, но нет покрытия стоимости самой доставки. Предельный вес: 20 кг (44 фунта). Максимальная длина любого измерения в упаковке должна составлять не более 106,68 см. Сумма длины и периметра наибольшего поперечного сечения не должна превышать 200,66 см.</p>
            <p>&nbsp;</p>
            <p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Meest, UPS, Feddex</strong> и т.п. &ndash; частные компании, выполняющие функции почтовых куръеров по международной доставке посылок и транспортировке грузов. Имеют свои особенности и отличия, преимущества и недостатки по сравнению с государственной почтой. Все расценки и другие нюансы по данным видам отправки уточняются и оговариваются <strong><span style="text-decoration: underline;">по индивидуальных запросах клиентов</span></strong> при формировании заказа или оформлении посылки на отправку.</p>
            <p>&nbsp;</p>

            <p>&nbsp;&nbsp;&nbsp;&nbsp; Рассмотрение вопросов доставки <strong>негабаритных товаров</strong>&nbsp;возможно только в индивидуальном порядке. Более полная информация предоставляется при детальном рассмотрении и уточнении самих заказов.</p><br /><br />            </td>
    </tr>
</table>
