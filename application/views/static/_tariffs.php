<table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h20 f9">
        <th valign="top">
            <a href=index.php?page=tariffs>Тарифы</a>              </th>
    </tr>
    <tr>
        <td class="bgc1" style="padding: 8px" valign="center">
            <a name="main"></a>
            <p style="text-align: center;"><span style="font-size: small;"><strong>ОСНОВНЫЕ УСЛУГИ</strong></span></p>

            <p style="text-align: center;"><span style="font-size: small;"><strong><br /></strong></span></p>
            <p>&nbsp;<span style="color: #ff6600;">Наши комиссионные расценки являются номинальными и позволяют поддерживать работу компании и оказывать необходимые услуги. Однако мы всегда готовы обсудить с Вами другие, индивидуальные условия по оказанию услуг, прислушаться к Вашим пожеланиям и рассмотреть интересные предложения о взаимовыгодном сотрудничестве</span></p>
            <p>&nbsp;</p>
            <p><strong><span style="text-decoration: underline;">Основная комиссия на услуги компании:</span></strong><br /><br /></p>
            <table border="0" cellspacing="0" cellpadding="0" width="90%" align="center">
                <tbody>
                    <tr>
                        <td><span style="color: darkblue; font-size: small;"><strong>10%</strong></span></td>
                        <td width="20">
                            <div>&ndash;</div>
                        </td>
                        <td><span>от стоимости одного заказа*</span></td>
                    </tr>

                    <tr>
                        <td><span style="color: darkblue; font-size: small;"><strong>5-8%</strong></span></td>
                        <td>
                            <div>&ndash;</div>
                        </td>
                        <td><span> с учетом скидок**</span></td>
                    </tr>
                </tbody>
            </table>
            <p><br /><span style="font-size: x-small;"><em>* <span style="color: #ef2929;"><strong>заказ</strong></span> включает в себя все товары (товар) перечисленные в одном инвойсе, пришедшем от продавца или из магазина, который был оплачен нашей компанией или клиентом самостоятельно</em></span><br /><span style="font-size: x-small;"><em>** см. соответственно страницу <a href="index.php?page=discounts">Скидки</a></em></span></p>

            <p><span style="font-size: xx-small;">
                    <p>&nbsp;</p>
                    <p><strong>Ограничения сервиса относительно основной комиссии***:</strong></p>
                    <p><strong><br /></strong></p>
                </span></p>
            <table border="0" cellspacing="0" cellpadding="0" width="90%" align="center">
                <tbody>
                    <tr>
                        <td><span style="color: darkblue; font-size: x-small;"><strong>$5</strong></span></td>
                        <td width="20">
                            <div>&ndash;</div>
                        </td>
                        <td><span style="font-size: xx-small;">минимальная комиссия на заказ с аукциона eBay</span></td>
                    </tr>

                    <tr>
                        <td><span style="color: darkblue; font-size: x-small;"><strong>$10</strong></span></td>
                        <td>
                            <div>&ndash;</div>
                        </td>
                        <td><span style="font-size: xx-small;">минимальная комиссия на заказ с Интернет-магазина</span></td>
                    </tr>
                </tbody>
            </table>
            <p><span style="font-size: xx-small;">
                    <p><br /><em>*** просим обратить внимание, что это не дополнительные тарифы, а лишь ограничения к основной комиссии на услуги</em></p>
                    <p>&nbsp;</p>
                    <p><strong>Бесплатные основные услуги (входящие в основную комиссию):</strong></p>

                    <ul>
                        <li>расчеты стоимости предоплаты и окончательного расчета, предоставление реквизитов для денежных переводов</li>
                        <li>оформление принятых заказов, их оплата с наших акаунтов</li>
                        <li>использование автоматизированной программы &laquo;снайпер&raquo; на аукционе eBay, позволяющих поставить вашу ставку на последних секундах розыграша</li>
                        <li>обязательный осмотр полученных посылок на предмет наличия товара, <strong>&nbsp;</strong>визуальный осмотр целостности упаковки</li>
                        <li>содержание товаров на складе сроком до одного месяца</li>
                        <li>предоставление отдельных виртуальных счетов в системе учета</li>

                        <li>ведение  файлов отчетов клиентам с предоставлением возможности контроля всех аспектов сотрудничества</li>
                        <li>предоставление отдельных акаунтов клиентам на нашем сайте с   отражением текущего баланса и отчета online</li>
                        <li>интерактивная online поддержка (ICQ, Skype) соответственно графику на нашем сайте</li>
                        <li>предоставление трекинг-номеров для отслеживания перемещений отправленной посылки в online</li>
                    </ul>
                </span></p>
            <p>&nbsp;</p>
            <p><strong><span style="text-decoration: underline;">Прием на склад и регистрация *:</span></strong></p>
            <p><strong><span style="text-decoration: underline;"><br /></span></strong></p>
            <table border="0" cellspacing="0" cellpadding="0" width="90%" align="center">
                <tbody>
                    <tr>

                        <td width="60"><span style="color: darkblue; font-size: small;"><strong>$2.50</strong></span></td>
                        <td width="20">
                            <div>&ndash;</div>
                        </td>
                        <td><span style="font-size: x-small;">посылки весом до 25 фунтов</span></td>
                    </tr>
                    <tr>
                        <td><span style="color: darkblue; font-size: small;"><strong>$5.00</strong></span></td>
                        <td>
                            <div>&ndash;</div>
                        </td>
                        <td><span style="font-size: x-small;">посылки весом от 25 до 50 фунтов</span></td>
                    </tr>

                    <tr>
                        <td><span style="color: darkblue; font-size: small;"><strong>$10.00</strong></span></td>
                        <td>
                            <div>&ndash;</div>
                        </td>
                        <td><span style="font-size: x-small;">посылки весом свыше 50 фунтов</span></td>
                    </tr>
                </tbody>
            </table>
            <p><br />* <span style="font-size: xx-small;">В случае приема дополнительных к основной посылок по одному и тому же заказу тариф составляет <strong>40%</strong> от основного тарифа приема на склад</span>﻿</p>
            
            <br /><br />
            <a name="additional"></a>
            <p style="text-align: center;"><span style="font-size: small;"><strong>ДОПОЛНИТЕЛЬНЫЕ УСЛУГИ</strong></span></p>

            <p>&nbsp;</p>
            <p><strong><span style="text-decoration: underline;">Базовая проверка товара:</span> <span style="color: darkblue; font-size: small;">$20.00/час</span></strong><strong><span style="color: darkblue; font-size: small;">*</span></strong><br /> Соответствие описанию, количество, цвет, качество, работоспособность и т.п. <br />В более сложных случаях, требующих участия специалистов - <strong>цена договорная</strong></p>
            <p><span style="color: #ff6600;"><strong><em>* $20.00/ час отображают номинальную стоимость, может быть меньше или больше, зависимо от потраченного времени и по нашему усмотрению</em></strong></span></p>
            <p>&nbsp;</p>
            <p><strong><span style="text-decoration: underline;">Другие услуги:</span> <span style="color: darkblue; font-size: small;">$20.00/час*</span> (не входящие в список бесплатных основных услуг)</strong><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1) ведение разборок с почтовыми службами, магазинами, диспуты</p>

            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2) переводы текстов, описаний, составление писем продавцам</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3) поиск специфических товаров, консультации по различным вопросам</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4) распечатка документов для таможенной очистки и т.п.</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5) отмена заказов, возврат денег клиенту и т.п.</p>
            <p><span style="color: #ff6600;"><strong><em>* $20.00/ час отображают номинальную стоимость, может быть меньше или больше, зависимо от потраченного времени и по нашему усмотрению</em></strong></span></p>
            <p>&nbsp;</p>
            <p><strong><span style="text-decoration: underline;">Фотоотчет:</span> <span style="color: darkblue; font-size: small;">$10.00</span></strong><br /> Предоставляется по желанию клиента. Отправка фото по email или через программу Skype. В случае обнаруженных нами проблем с посылкой или товаром фотоотчет может быть предоставлен по нашей инициативе, безоплатно.</p>

            <p>&nbsp;</p>
            <p><strong><span style="text-decoration: underline;">Экспресс-услуги:</span> <span style="color: darkblue; font-size: small;">2 * %</span> (основная комиссия в двойном размере)</strong> <br />Если главное в заказе &ndash; вопрос времени, а не в цене, мы готовы помочь.<br /> Услуга включает в себя:</p>
            <ul>
                <li>приоритетное по времени оформление заказа</li>
                <li>поиск и рекомендация наиболее быстрых способов доставки товара как внутри США, так и на адрес клиента</li>

                <li>отслеживание следования товара до нашего склада (если возможность предоставлена продавцом)</li>
                <li>регистрация и обработка посылки в приоритетном порядке</li>
                <li>решение остальных вопросов и отправка в приоритетном порядке</li>
            </ul>
            <p>&nbsp;</p>
            <p><strong><span style="text-decoration: underline;">Длительное хранение на складе:</span> <span style="color: darkblue; font-size: small;">$5 + $1</span>/фунт в месяц*</strong><br /> <em><strong>* Внимание!</strong> При длительной неуплате за отправку и хранение на складе, отсутствии при этом любых объяснений со стороны клиента, товар сохраняется на складе <strong><span style="text-decoration: underline;">6 месяцев</span></strong>, после чего продается на аукционе для покрытия складских расходов и иных возможных неуплаченных счетов клиента</em></p>

            <p>﻿</p>
            
            <br /><br />

            <a name="commission"></a>
            <p style="text-align: center;"><span style="font-size: small;"><strong>КОМИССИИ ДЕНЕЖНЫХ СИСТЕМ</strong></span></p>
            <p>&nbsp;</p>
            <table class="bgc2 mb16 mt16" cellspacing="1" cellpadding="1" width="440" height="335" align="center">
                <tbody>
                    <tr class="bg1">
                        <th rowspan="2">Платежная система</th><th colspan="2">Комиссия</th>
                    </tr>
                    <tr class="bg1">
                        <th>получение</th><th>отправка</th>
                    </tr>
                    <tr class="bgc1 ac h22">

                        <td class="bgc3"><a href="http://www.contact-sys.com/" target="_blank">Контакт</a>, <a href="http://www.unistream.ru/" target="_blank">Юнистрим</a></td>
                        <td>$0.50</td>
                        <td>3%</td>
                    </tr>
                    <tr class="bgc1 ac h22">
                        <td class="bgc3"><a href="http://www.anelik.ru/" target="_blank">Анелик</a></td>
                        <td>$0.5 для $0-$3000<br />$5.5 для $3000 и выше</td>
                        <td>3% для $100-$3000<br />0.9% для $3000-$4000<br />0.8% для $4000-$5000</td>

                    </tr>
                    <tr class="bgc1 ac h22">
                        <td class="bgc3"><a href="http://www.paypal.com/" target="_blank">PayPal</a></td>
                        <td>3.5% + $0.3</td>
                        <td>0%</td>
                    </tr>
                    <tr class="bgc1 ac h22">
                        <td class="bgc3"><a href="http://webmoney.ru/" target="_blank">WebMoney (WMZ)</a></td>
                        <td>4.0%</td>
                        <td>0.8%</td>
                    </tr>

                    <tr class="bgc1 ac h22">
                        <td class="bgc3"><a href="http://privatbank.ua/" target="_blank">ПриватБанк</a></td>
                        <td>4%</td>
                        <td>0.75% *</td>
                    </tr>
                    <tr class="bgc1 ac h22">
                        <td class="bgc3"><a href="http://www.westernunion.ru/" target="_blank">Western Union</a></td>
                        <td>$5</td>
                        <td>% *</td>
                    </tr>
                    <tr class="bgc1 ac h22">

                        <td class="bgc3"><a href="http://www.moneygram.com/" target="_blank">MoneyGram</a></td>
                        <td>$5</td>
                        <td>% *</td>
                    </tr>
                    <tr class="bgc1 ac h22">
                        <td class="bgc3"><a href="http://checkout.google.com/" target="_blank">Google Checkout</a></td>
                        <td>3.9% + $0.3</td>
                        <td>0%</td>
                    </tr>
                    <tr class="bgc1 ac h22">
                        <td class="bgc3"><a href="https://www.mobi-money.ru/" target="_blank">MOBI.Деньги **</a></td>

                        <td>1%</td>
                        <td>6%</td>
                    </tr>
                    <tr class="bgc1 ac h22">
                        <td class="bgc3"><a href="http://www.alfabank.ru/" target="_blank">АльфаБанк</a></td>
                        <td>3%</td>
                        <td>% *</td>
                    </tr>
                    <tr class="bgc1 ac h22">
                        <td class="bgc3">Банковский перевод</td>
                        <td>$10</td>

                        <td>% *</td>
                    </tr>
                    <tr class="bgc1 ac h22">
                        <td class="bgc3">Личная передача в Москве</td>
                        <td>3%</td>
                        <td>$0.00</td>
                    </tr>
                    <tr class="bgc1 ac h22">
                        <td colspan="3">* уточняйте в банке, отделении<br />** мгновенные мобильные платежи<br />
                            <p>&nbsp;</p>
                            <p><span style="color: #ff6600;"><strong><em>Внимание! Реквизиты для переводов предоставляются только при оформлении заказа по конкретному выбранному для оплаты виду денежных переводов</em></strong></span></p>

                        </td>
                    </tr>
                </tbody>
            </table>
            <p>&nbsp;</p>
            <p>﻿</p><br /><br />            </td>
    </tr>
</table>
