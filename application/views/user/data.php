<h3>Пользовательская информация</h3>
<?if(isset($not_approved)):?>
<h4 class="blue">Информация находится на рассмотрении</h4>
<?endif;?>
<?if(isset($changes_denied)):?>
<h4 class="blue">В изменении данных аккаунта отказано.
<h4>Причина:</h4>
<?=$user->cause?>
</h4>
<?endif;?>

<div class="t-wrapper">
    <div class="table">
        <div class="row">
            <div class="cell">Логин:</div>
            <div class="cell"><?= $username ?></div>
        </div>
        <div class="row">
            <div class="cell">Email:</div>
            <div class="cell"><?= $email ?></div>
        </div>
        <div class="row">
            <div class="cell">Имя:</div>
            <div class="cell"><?= $user->name ?></div>
        </div>
        <div class="row">
            <div class="cell">Страна:</div>
            <div class="cell"><?= $user->country ?></div>
        </div>
        <div class="row">
            <div class="cell">Почтовый индекс:</div>
            <div class="cell"><?= $user->zip ?></div>
        </div>
        <div class="row">
            <div class="cell">Область / регион:</div>
            <div class="cell"><?= $user->region ?></div>
        </div>
        <div class="row">
            <div class="cell">Населенный пункт:</div>
            <div class="cell"><?= $user->city ?></div>
        </div>
        <div class="row">
            <div class="cell">Остальной адрес:</div>
            <div class="cell"><?= $user->address ?></div>
        </div>
        <div class="row">
            <div class="cell">Контактный телефон(ы):</div>
            <div class="cell"><?= $user->phones ? $user->phones : '' ?></div>
        </div>
        <div class="row">
            <div class="cell">ICQ:</div>
            <div class="cell"><?= $user->icq ? $user->icq : '' ?></div>
        </div>
        <div class="row">
            <div class="cell">Skype:</div>
            <div class="cell"><?= $user->skype ? $user->skype : '' ?></div>
        </div>
        <div class="row">
            <div class="cell">Агент Mail.ru:</div>
            <div class="cell"><?= $user->mail_ru ? $user->mail_ru : '' ?></div>
        </div>
        <div class="row">
            <div class="cell">Другой контакт:</div>
            <div class="cell"><?= $user->another ? $user->another : '' ?></div>
        </div>
    </div>
</div>
<div class="fr button"><a class="button_link" href="<?=Url::site('user/changedata')?>">Изменить</a></div>
