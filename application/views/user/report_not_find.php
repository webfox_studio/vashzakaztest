<br><br>
<div class="block effect6 text">    
    <h4>Отчет отсутствует на сервере.</h4>
    <h4>Ваш клиентский отчет в формате файла Excel еще не создан на данный момент.<br>Попробуйте скачать файл повторно в течении <b>1-2 дней</b> после оформления заказа (отправки денежного перевода).<br>Если проблема не решится за это время - обратитесь к администрации сайта за информацией.</h4>
    <div class="p4">Перейти на <a href="<?=Url::site('/')?>">главную</a></div>
</div>