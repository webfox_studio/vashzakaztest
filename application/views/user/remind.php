<table cellspacing="1" cellpadding="1" class="bgc2 w100p mb16">
     <tbody>
     	<tr class="bg1 ac h20 f9">
          	<th>Напоминание пароля</th>
        </tr>
        <tr>
         	<td valign="center" style="padding: 8px;" class="bgc1">
         		<?php if ($error): ?>
         		<p style="color:red; padding:5px; text-align:center">
         		   <?php if (is_array($error)): ?>
         		   		<?php foreach ($error as $message): ?>
    						<?php echo $message ?> <br />
						<?php endforeach ?>
				   <?php else: ?>
        				<?php echo $error?></p>
		        	<?php endif ?>
		        <?php endif ?>
		        <?php if ($IsRestore == false): ?>
         		<form method="POST">
					<table align="center" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<table align="center" class="bgc2" cellspacing="1" cellpadding="1">
									<tr class="bgc1 ac">
  										<td class="bg1" width="160">E-mail:</td>
  										<td width="240">
    										<input class="w100p" type="text" name="email" value="">
    										<input type="hidden" name="remind" value="">
  										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td align="right">
								<input class="bg1 b mt4" type="submit" value="Напомнить">
							</td>
						</tr>
					</table>
				</form>
				<?php else: ?>
				<form method="POST">
					<table align="center" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<table align="center" class="bgc2" cellspacing="1" cellpadding="1">
									<tr class="bgc1 ac">
  										<td class="bg1" width="160">E-mail:</td>
  										<td width="240">
    										<input class="w100p" type="text" name="email" value="<?php echo $email ?>" disabled>
    										<input type="hidden" name="restore" value="">
  										</td>
									</tr>
									<tr class="bgc1 ac">
  										<td class="bg1" width="160">Новый пароль:</td>
  										<td width="240">
    										<input class="w100p" type="password" name="password" value="">
  										</td>
									</tr>
									<tr class="bgc1 ac">
  										<td class="bg1" width="160">Подтверждение пароля:</td>
  										<td width="240">
    										<input class="w100p" type="password" name="confirm_password" value="">
  										</td>
								</table>
							</td>
						</tr>
						<tr>
							<td align="right">
								<input class="bg1 b mt4" type="submit" value="Сменить">
							</td>
						</tr>
					</table>
				</form>
				<?php endif ?>
			</td>
         </tr>
     </tbody>
</table>

