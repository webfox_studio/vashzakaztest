<h3>Обновления</h3>

<div id="pilast">
    <table border="0" width="100%">
        <tr>
            <td style="width: 50%; text-align: center"><iframe frameborder="no" scrolling="no" src="http://time.yandex.ru/pages/widget/informer/index.html?geoid=103209&theme=analog&lang=ru"></iframe></td>
            <td style="width: 50%; text-align: center"><iframe frameborder="no" scrolling="no" src="http://time.yandex.ru/pages/widget/informer/index.html?geoid=213&theme=analog&lang=ru"></iframe></td>
        </tr>
    </table>
</div>

<div class="t-wrapper">
    <h4>НОВОЕ В ВАШЕМ АККАУНТЕ</h4>
    <div class="table effect6">
        <div class="row">
            <div class="thhead pimessegesjq">Сообщения<? if(isset($count_new_messages) AND $count_new_messages): ?> (<?=$count_new_messages?>)<? endif; ?><span class="fr"><a class="red nd" href="#">Развернуть &DoubleRightArrow;</a></span>
            </div>
        </div>
        <div class="row">
            <div class="cell pl3" id="pimessegesnone">
         
                <?php if($messages_new_in['0']['id'] != ''){ ?>
                <p>Входящие</p>
                <?php for($i = 0; $i < count($messages_new_in); $i++): ?>
                    <p><a href="/messages/item/<?=$messages_new_in[$i]['id']?>"><?=$messages_new_in[$i]['time']?><span class="picontent"><?=mb_substr($messages_new_in[$i]['title'],0,30)?>...</span></a></p>
                <?php endfor; ?>
                <?php } else { ?>
                        <p>Входящие сообщения отсутствуют.</p> 
                <?php } ?>
                
                <?php if($messages_new_out['0']['id'] != ''){ ?>
                <p>Исходящие</p>
                <?php for($i = 0; $i < count($messages_new_out); $i++): ?>
                    <p><a href="/messages/item/<?=$messages_new_out[$i]['id']?>"><?=$messages_new_out[$i]['time']?><span class="picontent"><?=mb_substr($messages_new_out[$i]['title'],0,30)?>...</span></a></p>
                <?php endfor; ?>
                <?php } else { ?>
                        <p>Исходящие сообщения отсутствуют.</p> 
                <?php } ?>
            
                <? //$title?>
            </div>
        </div>

       <div class="row">
            <div class="thhead pizakazjq">Заказы<? if(isset($count_new_orders) AND $count_new_orders): ?> (<?= $count_new_orders ?>)<? endif; ?><span class="fr"><a class="red nd" href="#">Развернуть &DoubleRightArrow;</a></span>
            </div>
        </div>
        <div class="row">                
            <div class="cell pl3" id="pizakaznone">

                <?php if($orders_new['0']['id'] != ''){ ?>
                <?php for($i = 0; $i < count($orders_new); $i++): ?>
                    <p><a href="/order/id<?=$orders_new[$i]['id']?>"><?=$orders_new[$i]['date_time']?><span><?=mb_substr($orders_new[$i]['title'],0,30)?>...</span></a></p>
                <?php endfor; ?>
                <?php } else { ?>
                        <p>У вас нет новых заказов</p> 
                <?php } ?>
            </div>
   </div>

    <div class="row">
            <div class="thhead piposulkajq">Посылки<? if(isset($count_new_parcels) AND $count_new_parcels): ?> (<?= $count_new_parcels ?>)<? endif; ?><span class="fr"><a class="red nd" href="#">Развернуть &DoubleRightArrow;</a></span>
            </div>
        </div>
    <div class="row">                
            <div class="cell pl3" id="piposulkanone">

                <?php if($parcels_new['0']['id'] != ''){ ?>
                <?php for($i = 0; $i < count($parcels_new); $i++): ?>
                    <p><a href="/parcel/id<?=$parcels_new[$i]['id']?>"><?=$parcels_new[$i]['date_time']?><span class="picontent"><?=mb_substr($parcels_new[$i]['title'],0,30)?>...</span></a></p>
                <?php endfor; ?>
                <?php } else { ?>
                        <p>У вас нет новых посылок</p> 
                <?php } ?>
                        
            </div>
   </div>
    
    <div class="row">
            <div class="thhead pipaymentjq">Денежные переводы<? if(isset($count_new_payments) AND $count_new_payments): ?> (<?= $count_new_payments ?>)<? endif; ?><span class="fr"><a class="red nd" href="#">Развернуть &DoubleRightArrow;</a></span>
            </div>
    </div>
    <div class="row">
        <div class="cell pl3" id="pipaymentnone">

                <?php if($payments_new['0']['id'] != ''){ ?>
                <?php for($i = 0; $i < count($payments_new); $i++): ?>
                    <p><a href="/payment/id<?=$payments_new[$i]['id']?>"><?=$payments_new[$i]['date_time']?><span class="picontent"><?=mb_substr($payments_new[$i]['title'],0,30)?>...</span></a></p>
                <?php endfor; ?>
                <?php } else { ?>
                        <p>У вас нет переводов</p> 
                <?php } ?>
                        
        </div>
   </div>
        
    <div class="row">
        <div class="thhead pifajlotchetajq">Файл отчета ( <?php if($file_otcheta['error'] == ''){ echo 'Изменен: ' . $file_otcheta['time'] .' назад'; } else{ echo 'Файл еще не создан';}?> )<span class="fr"><a class="red nd" href="#">Развернуть &DoubleRightArrow;</a></span>
            </div>
    </div>
    <div class="row">
        <div class="cell pl3" id="pifajlotchetanone">
           <div><a href="/user/report">Скачать файл отчета</a></div>
        </div>
   </div>

    <div class="row">
        <div class="thhead pinewfajljq">Новые файлы<? if(isset($count_new_files) AND $count_new_files): ?> (<?= $count_new_files ?>)<? endif; ?><span class="fr"><a class="red nd" href="#">Развернуть &DoubleRightArrow;</a></span>
            </div>
    </div>
    <div class="row">
        <div class="cell pl3" id="pinewfajlnone">
                <?php if($files_new['0']['id'] != ''){ ?>
                <?php for($i = 0; $i < count($files_new); $i++): ?>
                    <p><a href="/files/download/<?=$files_new[$i]['id']?>"><?=mb_substr($files_new[$i]['name'],0,30)?>...</a></p>
                <?php endfor; ?>
                <?php } else { ?>
                        <p>У вас нет новых файлов</p> 
                <?php } ?>
            </div>
   </div>
</div>

<h4>НОВОЕ НА НАШЕМ САЙТЕ</h4>
<div class="table effect6">
    <div class="row">
        <div class="thhead pinewsjq">Последние новости<span class="fr"><a class="red nd" href="#">Развернуть &DoubleRightArrow;</a></span></div>
    </div>
    <div class="row">
        <div class="cell pl3" id="pinewsnone">
            <?php foreach($news_new as $key => $value): ?>
            <div><a href="/news/<?=$value['id']?>">Новость за <?=$value['dt']?></a></div>
            <?php endforeach;?>
            <div><a href="/news">Все новости</a></div>
        </div>
    </div>
    
    <div class="row">
        <div class="thhead picompanyjq">Предложения от компаний<span class="fr"><a class="red nd" href="#">Развернуть &DoubleRightArrow;</a></span></div>
    </div>
    <div class="row">
        <div class="cell pl3" id="picompanynone">Страница в разработке</div>
    </div>
    
    <div class="row">
        <div class="thhead piakciijq">Новые акции от магазинов<span class="fr"><a class="red nd" href="#">Развернуть &DoubleRightArrow;</a></span></div>
    </div>
    
    <div class="row">
        <div class="cell pl3" id="piakciinone">Страница в разработке</div>
    </div>
    
        <div class="row">
            <div class="cell"><a href="#" id="pilastnews">Последние просмотренные страницы</a></div>
            <div class="cell pl3" id="pilastnewsnone">
                <?php if($last_messages['0']['id'] != ''){ ?>
                    <p>Сообщения</p>
                <?php for($i = 0; $i < count($last_messages); $i++): ?>
                    <p><a href="/messages/item/<?=$last_messages[$i]['id']?>"><?=$last_messages[$i]['time']?><span class="picontent"><?=mb_substr($last_messages[$i]['title'],0,40)?>...</span></a></p>
                <?php endfor; ?>
                <?php } ?>
                        
                <?php if(!empty($last_orders)){ ?>
                    <p>Заказы</p>
                <?php for($i = 0; $i < count($last_orders); $i++): ?>
                    <p><a href="/order/id<?=$last_orders[$i]['id']?>"><?=$last_orders[$i]['date_time']?><span><?=mb_substr($last_orders[$i]['title'],0,40)?>...</span></a></p>
                <?php endfor; ?>
                <?php } ?>
                        
                <?php if($last_parcels['0']['id'] != ''){ ?>
                    <p>Посылки</p>
                <?php for($i = 0; $i < count($last_parcels); $i++): ?>
                    <p><a href="/parcel/id<?=$last_parcels[$i]['id']?>"><?=$last_parcels[$i]['date_time']?><span class="picontent"><?=mb_substr($last_parcels[$i]['title'],0,40)?>...</span></a></p>
                <?php endfor; ?>
                <?php } ?>
                    
                <?php if(!empty($last_payments)){ ?>
                    <p>Денежные переводы</p>
                <?php for($i = 0; $i < count($last_payments); $i++): ?>
                    <p><a href="/payment/id<?=$last_payments[$i]['id']?>"><?=$last_payments[$i]['date_time']?><span><?=mb_substr($last_payments[$i]['title'],0,40)?>...</span></a></p>
                <?php endfor; ?>
                <?php } ?>
                        
                <?php if($last_files['0']['id'] != ''){ ?>
                        <p>Файлы</p>  
                <?php for($i = 0; $i < count($last_files); $i++): ?>
                    <p><a href="/files/download/<?=$last_files[$i]['id']?>"><?=mb_substr($last_files[$i]['name'],0,60)?>...</a></p>
                <?php endfor; ?>
                <?php } ?>
        </div>
</div>
</div>
</div>
<pre>
<?//print_r($last_orders)?>
</pre>
<script type="text/javascript">
$(document).ready(function() {
    $(".pimessegesjq").click(function(){
		$("#pimessegesnone").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
    $(".pizakazjq").click(function(){
		$("#pizakaznone").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
       
    $(".piposulkajq").click(function(){
		$("#piposulkanone").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
    
    $(".pipaymentjq").click(function(){
		$("#pipaymentnone").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
        
    $(".pifajlotchetajq").click(function(){
		$("#pifajlotchetanone").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
    
    $(".pinewfajljq").click(function(){
		$("#pinewfajlnone").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
        
   $(".pinewsjq").click(function(){
		$("#pinewsnone").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
   $(".picompanyjq").click(function(){
		$("#picompanynone").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
   $(".piakciijq").click(function(){
		$("#piakciinone").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
   $("#pilastnews").click(function(){
		$("#pilastnewsnone").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
   
});
</script>