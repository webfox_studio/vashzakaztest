<h3>Управление сообщениями</h3>

<div class="block effect6">
    <h4 class="blue">Вы уже подписаны на разделы:</h4>
    <ul style="margin: 0 0 0 1em;" class="pisections">
        <?php foreach ($active_cats as $value): ?>
            <li>&mdash; <?php print $value ?>;<br></li>
        <?php endforeach; ?>
    </ul>
   
<br />
<h4 class="blue">Общий список разделов информационных сообщений:</h4>

<form method="POST" action="<?php print Url::site('subscription/update'); ?>">
    <ul style="margin: 0 0 0 0.8em;">
			<?php foreach ($cats as $value): ?>
				<?php if (isset($active_cats[$value['id']])): ?>
					<li>
						<input type="checkbox" name="categories[<?php print $value['id']; ?>]" checked="checked" /><span>&nbsp;<?php print $value['name']; ?></span>
					</li>
				<?php else: ?>
					<li>
                        <input class="checkbox" type="checkbox" name="categories[<?php print $value['id']; ?>]" /><span><?php print $value['name']; ?></span>
					</li>
				<?php endif; ?>
			<?php endforeach; ?>
            </ul><br>
    <input class="button" type="submit" value="Сохранить">
</form>
</div>