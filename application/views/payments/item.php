<table>
    <tr>
        <th><?if(empty($is_parcel)):?>Перевод<?else:?>Посылка<?endif;?> №<?= $payments->id ?>: <?=$payments->title?></th>
    </tr>
    <tr>
        <td>
            <div class="fl">
                <? if($payments->readed): ?>просмотрен<? else: ?>не просмотрен<? endif; ?>
                <? if($payments->important):?><span style="color: red">важный</span><?endif?>
                <?if($payments->processed == FALSE):?>текущий (<a href="<?=Url::site('/'.(empty($is_parcel) ? 'payments' : 'payments').'/close/'.$payments->id)?>">отменить перевод</a>)<?else:?>отмененный (<a href="<?=Url::site('/'.(empty($is_parcel) ? 'payments' : 'payments').'/open/'.$payments->id)?>">открыть</a>)<?endif;?>
                
                
            </div>
            <div class="fr"><?= $payments->date_time ?></div>
        </td>
    </tr>
</table>

<? foreach($payments->messages->find_all() as $message): ?>
<table>
    <tr>
        <td>
            <div class="fl">
                <?/* if($user_id == $message->user_id): ?>--><? else: ?><--<? endif; */?>
                <?= $message->user->user_data->name ?>
                <? if($message->user->is_admin()): ?>(<span class="red">администратор</span>)<? endif; ?>
            </div>
            <div class="fr"><?= $message->date_time ?></div>
        </td>
    </tr>
    <tr>
        <td class="p4 lnk"><?=$message->message?></td>
    </tr>
</table>
<? endforeach;?>

<? if(! $payments->processed): ?>
<table>
    <tr>
        <td>
            <div>Ваше сообщение</div>
            <?=Block::form('payment_message', $form)?>
        </td>
    </tr>
    <?if(isset($pre_message)):?>
    <tr class="bgc1">
        <td class="p8">
            <h3>Предварительный просмотр:</h3>
        </td>
    </tr>
     <tr>
        <td>
                <div>
                    <span class="<?if($user->is_admin()):?>red<?endif?>">
                        <?= $user->user_data->name ?>
                        <?if($user->is_admin()):?>[Администратор]<?endif?>
                    </span>
                </div>
                <div class="ar f6 c2"><?= $pre_message['time'] ?></div>
                <div class="bg2 brd3 mt4 mb16 p4">
                    <?= $pre_message['content'] ?>
                </div>
        </td>
    </tr>
    <?endif?>
</table>
<? else: ?>
<div>Перевод закрыт</div>
<? endif ?>

