<h3><?if(empty($is_parcel)):?>Переводы<?else:?>Посылки<?endif;?></h3>

<table class="effect6">
    <tr>
        <td class="mt16" style="width: 130px;"><a class="mt16" href="<?=Url::site((empty($is_parcel) ? 'payments' : 'payments').'/unread')?>">Непрочитаные</a></td>
        <td><?=isset($count_unread) ? $count_unread : 0?></td>
    </tr>
    <tr>
        <td class="mt16"><a class="mt16" href="<?=Url::site((empty($is_parcel) ? 'payments' : 'payments').'/current')?>">Текущие</a></td>
        <td><?=isset($count_all, $count_processed) ? ($count_all - $count_processed) : 0?></td>
    </tr>
    <tr>
        <td class="mt16"><a class="mt16" href="<?=Url::site((empty($is_parcel) ? 'payments' : 'payments').'/closed')?>">Отмененные</a></td>
        <td><?=isset($count_processed) ? $count_processed : 0?></td>
    </tr>
    <tr>
        <td class="mt16"><a class="mt16" href="<?=Url::site((empty($is_parcel) ? 'payments' : 'payments').'/all')?>">Все</a></td>
        <td><?=isset($count_all) ? $count_all : 0?></td>
    </tr>
</table>

<div><?=View::factory('forms/payment_find', array('form' => $find_form))?></div>

<div><a href="<?= Url::site((empty($is_parcel) ? 'payments' : 'payments').'/all') ?>">Все <?if(empty($is_parcel)):?>переводы<?else:?>посылки<?endif;?></a></div>
<div><a class="red" href="<?= Url::site((empty($is_parcel) ? 'payments' : 'payments').'/new') ?>"><?if(empty($is_parcel)):?>Новый перевод<?else:?>Новая посылка<?endif;?></a></div>
