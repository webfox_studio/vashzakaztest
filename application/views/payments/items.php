<?switch($status) {
    case 'all': $st = 'Все'; break;
    case 'closed': $st = 'Отмененные'; break;
    case 'current': $st = 'Текущие'; break;
    case 'unread': $st = 'Непрочитаные'; break;
}?>
<h3><?=$st?> <?if(empty($is_parcel)):?>переводы<?else:?>посылки<?endif;?></h3>

<table>
    <tr>
        <th>ID</th>
        <th>Дата/Время</th>
        <th colspan="2">Название</th>
<!--        <th>-->
<!--            <span class="mp" title="непрочитан">н</span>-->
<!--            <span class="mp" title="прочитан">п</span>-->
<!--            <span class="mp" title="важный">в</span>-->
<!--            <span class="mp" title="текущий">т</span>-->
<!--            <span class="mp" title="закрытый">з</span>-->
<!--        </th>-->
    </tr>
    <? foreach ($payments as $payment): ?>
<!--    <tr class="bgc1 hover<?if($payment->readed == FALSE):?> bg1 b<?endif;?>" onclick="window.location = '<?=Url::site((empty($is_parcel) ? 'payments' : 'payments').'/id'.$payment->id)?>'">-->
    <tr class="bgc1 hover<?if($payment->readed == FALSE):?> bg1 b<?endif;?>">
        <td class="ar pr4 f7" style="width: 36px"><a style="font-size:12px;" href="<?=Url::site((empty($is_parcel) ? 'payment' : 'payment').'/id'.$payment->id)?>"><?=$payment->id?></a></td>
        <td class="ar pr4 f6" style="width: 126px;"><a style="font-size:12px;" href="<?=Url::site((empty($is_parcel) ? 'payment' : 'payment').'/id'.$payment->id)?>"><?=$payment->date_time?></a></td>
        <td class="ar pr4 f7 p4" style="text-align:left"><a style="font-size:12px;" href="<?=Url::site((empty($is_parcel) ? 'payment' : 'payment').'/id'.$payment->id)?>"><?=mb_substr($payment->title,0,60)?>...</a></td>
        <td class="ar" style="font-weight:normal !important; text-transform: none;">
            <?if($payment->processed == FALSE):?>текущий (<a href="<?=Url::site('/'.(empty($is_parcel) ? 'payments' : 'payments').'/close/'.$payment->id)?>">отменить перевод</a>)<?else:?>отмененный (<a href="<?=Url::site('/'.(empty($is_parcel) ? 'payments' : 'payments').'/open/'.$payment->id)?>">открыть</a>)<?endif;?>
        </td>
<!--        <td class="ac f6">-->
<!--            --><?// if($payment->readed == FALSE): ?><!--н--><?//else:?><!--п--><?// endif; ?>
<!--            --><?// if($payment->important): ?><!--в--><?// endif; ?>
<!--            --><?// if($payment->processed): ?><!--з--><?//else:?><!--т--><?// endif; ?>
<!--            -->
<!--        </td>-->
    </tr>
    <? endforeach; ?>
</table>

<? if (isset($pagination) AND $pagination != ''): ?>
<br />
<table class="bgc2 w100p" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18 f7">
        <th width="32"><?=$pagination?></th>
    </tr>
</table>
<? endif; ?>
