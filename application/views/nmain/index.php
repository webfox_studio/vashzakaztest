<div class="" style="padding:0;margin:3.5em 0 0 0;">
    
<div class="slider-box">
	<div class="slider">
        <img src="/media/img/slider_1.jpg" alt="Доставка под ключ" title="Доставка под ключ" />
        <img src="/media/img/slider_2.jpg" alt="Склад в безналоговом штате" title="Склад в безналоговом штате" />
        <img src="/media/img/slider_3.jpg" alt="Бесплатная регистация и консолидация товаров" title="Бесплатная регистация и консолидация товаров" />
        <img src="/media/img/slider_4.jpg" alt="Полное сопровождение" title="Полное сопровождение" />
        <img src="/media/img/slider_5.jpg" alt="8-летний опыт работы на рынке поставки товаров из США по всему миру" title="8-летний опыт работы на рынке поставки товаров из США по всему миру" />
        <img src="/media/img/slider_6.jpg" alt="Взаимовыгодное партнерство" title="Взаимовыгодное партнерство" />
        <img src="/media/img/slider_7.jpg" alt="Бесплатная подготовка всех необходимых почтово-таможенных и страховых документов" title="Бесплатная подготовка всех необходимых почтово-таможенных и страховых документов" />
        <img src="/media/img/slider_8.jpg" alt="Бонусные программы для постоянных клиентов" title="Бонусные программы для постоянных клиентов" />
	</div>
</div>

<div class="slider2 effect1">
    <div class="content_text">
        <h2>&laquo;Доставка под ключ&raquo;</h2>
        <p>Недорогая коммерческая доставка застрахованных грузов и отдельных товаров клиентам с проведением полной таможенной очистки, без участия государственной почты России и других стран, так называемая «доставка под ключ»</p>
    </div>
	<div class="content_text">
        <h2>Склад в безналоговом штате</h2>
        <p>Предоставление адреса американского склада VZ в безналоговом штате (налог отсутствует на все товары, в том числе на электронику) для совершения клиентом самостоятельных покупок с любой торговой точки на территории США (sale tax &mdash; $0.00)</p>
    </div>
    <div class="content_text">
        <h2>Бесплатная регистрация и консолидация товаров</h2>
        <p>Бесплатная регистрация самостоятельных покупок, бесплатная консолидация товаров, бесплатное удаление всего лишнего из коробок; дополнительные услуги склада по желанию клиента (фото отчет, проверка работоспособности, проверка соответствия описанию и т.д.)</p>
    </div>
    <div class="content_text">
        <h2>Полное сопровождение</h2>
        <p>Недорогие услуги &laquo;полного сопровождения&raquo; (выкуп заказов сотрудниками сервиса VZ) в рабочие и выходные дни (в том числе в &laquo;проблемных&raquo; магазинах), оперативное решение возникающих споров с продавцами и службами доставки</p>
    </div>
    <div class="content_text">
        <h2>8 лет на рынке</h2>
        <p>8-летний опыт работы на рынке поставки товаров из США по всему миру, официально зарегистрированная компания, доступная общественности информация о местонахождении, контактах, сотрудниках, расценках т.п.</p>
    </div>
    <div class="content_text">
        <h2>Взаимовыгодное партнерство</h2>
        <p>Взаимовыгодное партнерство, возможность отправки товаров непосредственно Вашим клиентам (партнерам), без предоставления им какой-либо рекламы сервиса VZ, в т.ч. контактной информации, реквизитов, рекламных буклетов и т.п.; пересылка товаров на склады других компаний по запросу клиента</p>
    </div>
    <div class="content_text">
        <h2>Бесплатная подготовка документов</h2>
        <p>Бесплатная подготовка всех необходимых почтово-таможенных и страховых документов (в том числе коммерческих инвойсов) силами сотрудников сервиса VZ, то есть приятный бонус всем клиентам в виде экономии значительного количества времени на оформление и заполнение данных</p>
    </div>
    <div class="content_text">
        <h2>Бонусные программы для постоянных клиентов</h2>
        <p>Постоянным и проверенным клиентам предоставляется возможность выкупов и отправок в кредит, длительное бесплатное содержание товаров на складе, голосовая связь (или чат) через программу Skype при первой необходимости, бесплатная помощь со звонками по США и т.п., бонусы</p>
    </div>
    <div class="clearfix"></div>
    <div class="slider-box"><ul class="bullets"></ul></div>    
</div>
</div>
<a name="news"></a>

<h3>Последние новости</h3>

<div class="block effect6">
    <?= Block::nnews(isset($news) ? $news : array()) ?>
    <div class="fr"><a href="/news/all#news">все новости</a></div>
    <br>
</div>

<h3>Новые сообщения на форуме</h3>
<div class="block effect6">
    <ul style="padding: 1em 0 0 1em;">
        <?= Block::ntopics(isset($last_topics) ? $last_topics : array()) ?>
    </ul>
    <div class="fr"><a href="http://www.vashzakaz.us/board/">перейти на форум</a></div>
    <br>
</div>

<h3>Популярные интернет-магазины</h3>

<div class="block effect6">
    <div class="list_carousel">
        <ul id="foo1">
            <?= Block::banners(isset($banners) ? $banners : array()) ?>
        </ul>
        <div class="fr"><a href="http://www.vashzakaz.us/useful/view/21/" target="_blank">Все интернет-магазины</a></div>
        <div class="clearfix"></div>
    </div>
</div>


<!--<h3>Акции</h3>

<div class="block effect6">
    <?php $a = 0; ?>
    <?php for ($i = 0; $i < count($mid_actions); $i++): ?>
        <?php $a++; ?>
        <div class="container">
            <section id="photo-wall">
                <ul>
                    <li>
                        <a href="<?= $mid_actions[$i]['link'] ?>" target="_blank">
                            <img src="http://www.vashzakaz.us/upload/actions/<?= $mid_actions[$i]['img'] ?>" alt="">
                            <h4><?= strip_tags($mid_actions[$i]['description']) ?></h4>
                        </a>
                    </li>
                </ul>
            </section>
        </div>
    <?php endfor; ?>
    <div style="clear: both"></div>
</div>
-->
<script type="text/javascript" language="javascript" src="/media/js/jquery.carouFredSel-4.4.2-packed.js"></script>
<script type="text/javascript" language="javascript">
    $(function () {

        //    Basic carousel + timer
        $('#foo1').carouFredSel({
            scroll: 1,
            auto: {
                pauseOnHover: 'resume',
                onPauseStart: function (percentage, duration) {
                    $(this).trigger('configuration', ['width', function (value) {
                            $('#timer1').stop().animate({
                                width: value
                            }, {
                                duration: duration,
                                easing: 'linear'
                            });
                        }]);
                },
                onPauseEnd: function (percentage, duration) {
                    $('#timer1').stop().width(0);
                },
                onPausePause: function (percentage, duration) {
                    $('#timer1').stop();
                }
            }
        });


    });
</script>

<script type="text/javascript">
    $(function() {

        // slider 2
        var el =  $('.slider2 .content_text'),
            indexImg = 1,
            indexMax = el.length;

        var slider = $('.slider'),
            sliderContent = slider.html(),                      // Содержимое слайдера
            slideWidth = $('.slider-box').outerWidth(),         // Ширина слайдера
            slideCount = $('.slider img').length,               // Количество слайдов
            prev = $('.slider-box .prev'),                      // Кнопка "назад"
            next = $('.slider-box .next'),                      // Кнопка "вперед"
            slideNum = 1,                                       // Номер текущего слайда
            index =0,
            clickBullets=0,
            sliderInterval = 7000,                              // Интервал смены слайдов
            animateTime = 1000,                                 // Время смены слайдов
            course = 1,                                         // Направление движения слайдера (1 или -1)
            margin = - slideWidth;                              // Первоначальное смещение слайдов

        for (var i=0; i<slideCount; i++)                      // Цикл добавляет буллеты в блок .bullets
        {
            html=$('.bullets').html() + '<li></li>';          // К текущему содержимому прибавляется один буллет
            $('.bullets').html(html);                         // и добавляется в код
        }
        var  bullets = $('.slider-box .bullets li')          // Переменная хранит набор буллитов


        $('.slider-box .bullets li:first').addClass('active');
        $('.slider img:last').clone().prependTo('.slider');   // Копия последнего слайда помещается в начало.
        $('.slider img').eq(1).clone().appendTo('.slider');   // Копия первого слайда помещается в конец.  
        $('.slider').css('margin-left', -slideWidth);         // Контейнер .slider сдвигается влево на ширину одного слайда.

        function nextSlide(){                                 // Запускается функция animation(), выполняющая смену слайдов.

            interval = window.setInterval(animate, sliderInterval);
        }

        function animate(indexA){
            if (margin==-slideCount*slideWidth-slideWidth  && course==1){     // Если слайдер дошел до конца
                slider.css({'marginLeft':-slideWidth});           // то блок .slider возвращается в начальное положение
                margin=-slideWidth*2;
            }else if(margin==0 && course==-1){                  // Если слайдер находится в начале и нажата кнопка "назад"
                slider.css({'marginLeft':-slideWidth*slideCount});// то блок .slider перемещается в конечное положение
                margin=-slideWidth*slideCount+slideWidth;
            }else{                                              // Если условия выше не сработали,
                margin = margin - slideWidth*(course);            // значение margin устанавливается для показа следующего слайда
            }
            slider.animate({'marginLeft':margin},animateTime);  // Блок .slider смещается влево на 1 слайд.

            if (clickBullets==0){                               // Если слайдер сменился не через выбор буллета
                bulletsActive();                                // Вызов функции, изменяющей активный буллет
            }else{                                              // Если слайдер выбран с помощью буллета
                slideNum=index+1;                               // Номер выбранного слайда
            }
			
            // slider 2
            el.fadeOut(1000);   
			console.log(slideNum)
            // el.eq(slideNum).fadeIn(1000);
			el.filter(':nth-child('+slideNum+')').fadeIn(1000);
        }

        function bulletsActive(){
            if (course==1 && slideNum!=slideCount){        // Если слайды скользят влево и текущий слайд не последний
                slideNum++;                                     // Редактирунтся номер текущего слайда
                $('.bullets .active').removeClass('active').next('li').addClass('active'); // Изменить активный буллет
            }else if (course==1 && slideNum==slideCount){       // Если слайды скользят влево и текущий слайд последний
                slideNum=1;                                     // Номер текущего слайда
                $('.bullets li').removeClass('active').eq(0).addClass('active'); // Активным отмечается первый буллет
                return false;
            }else if (course==-1  && slideNum!=1){              // Если слайды скользят вправо и текущий слайд не последни
                slideNum--;                                     // Редактирунтся номер текущего слайда
                $('.bullets .active').removeClass('active').prev('li').addClass('active'); // Изменить активный буллет  
                return false;
            }else if (course==-1  && slideNum==1){              // Если слайды скользят вправо и текущий слайд последни
                slideNum=slideCount;                            // Номер текущего слайда
                $('.bullets li').removeClass('active').eq(slideCount-1).addClass('active'); // Активным отмечается последний буллет
            }
        }

        function sliderStop(){                                // Функция преостанавливающая работу слайдера      
            window.clearInterval(interval);
        }

        prev.click(function() {                               // Нажата кнопка "назад"
            if (slider.is(':animated')) { return false; }       // Если не происходит анимация
            var course2 = course;                               // Временная переменная для хранения значения course
            course = -1;                                        // Устанавливается направление слайдера справа налево
            animate();                                          // Вызов функции animate()
            course = course2 ;                                  // Переменная course принимает первоначальное значение
        });
        next.click(function() {                               // Нажата кнопка "назад"
            if (slider.is(':animated')) { return false; }       // Если не происходит анимация
            var course2 = course;                               // Временная переменная для хранения значения course
            course = 1;                                         // Устанавливается направление слайдера справа налево
            animate();                                          // Вызов функции animate()
            course = course2 ;                                  // Переменная course принимает первоначальное значение
        });
        bullets.click(function() {                            // Нажат один из буллетов
            if (slider.is(':animated')) { return false; }       // Если не происходит анимация  
            sliderStop();                                       // Таймер на показ очередного слайда выключается
            index = bullets.index(this);                        // Номер нажатого буллета
			// console.log(index)
            if (course==1){                                     // Если слайды скользят влево
                margin=-slideWidth*index;                       // значение margin устанавливается для показа следующего слайда
            }else if (course==-1){                              // Если слайды скользят вправо
                margin=-slideWidth*index-2*slideWidth;
            }
            $('.bullets li').removeClass('active').eq(index).addClass('active');  // Выбранному буллету добавляется сласс .active
            clickBullets=1;                                     // Флаг информирующий о том, что слайд выбран именно буллетом
            animate(index);
            clickBullets=0
			
			//el.fadeOut(1000);
			//el.filter(':nth-child('+index+')').fadeIn(1000);
			
        });

        slider.add(next).add(prev).hover(function() {         // Если курсор мыши в пределах слайдера
            sliderStop();                                       // Вызывается функция sliderStop() для приостановки работы слайдера
        }, nextSlide);                                        // Когда курсор уходит со слайдера, анимация возобновляется.

        nextSlide();                                          // Вызов функции nextSlide()
    });
</script>