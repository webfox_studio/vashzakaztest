<div class="header">
    <img src="/<?= Kohana::config('main.path_media') ?>/img/head_left.jpg" class="noborder" /><a href="<?= Url::site('/') ?>" class="noborder"><img class="noborder" src="/<?= Kohana::config('main.path_media') ?>/img/head_logo.jpg" /></a><img src="/<?= Kohana::config('main.path_media') ?>/img/head_center.jpg" class="noborder" style="display: inline" />
    <img src="/<?= Kohana::config('main.path_media') ?>/img/head_right.jpg" class="noborder" style="position: absolute; right: 0px;" />

    <span style="position: absolute; top: 50; left: 182;">
        <a  class="nav1" href="<?= Url::site('page/about') ?>">О КОМПАНИИ</a>
    </span>

    <span style="position: absolute; top: 50; left: 315;">
        <a class='nav1' target="_blank" href="<?= Url::site('board') ?><? if (Auth::instance()->logged_in() && isset($_COOKIE['phpbb_sid'])): ?>?sid=<?=$_COOKIE['phpbb_sid']?><?endif;?>">ФОРУМ</a>
    </span>

    <? if (Auth::instance()->logged_in()): ?>
        <span  class="nav2" style="position: absolute;  right: 130px; top: 5px">
            Вы зашли, как <a href="<?= Url::site('user/data') ?>" title="Информация пользователя"><?= Auth::instance()->get_user()->user_data->name ?></a><? if (Auth::instance()->logged_in('admin') OR Auth::instance()->logged_in('redactor')): ?> [<small><a href="<?= Url::site('admin') ?>">Admin</a></small>]<? endif; ?>
        </span>
        <!-- span  class="nav2" style="background-image: url('/<?= Kohana::config('main.path_media') ?>/img/head_exit.jpg'); width: 60; height: 20px; text-align: center; position: absolute; right: 50px; top: 2px">
            <a  href="<?= Url::site('logout') ?><? if ( isset($_COOKIE['phpbb_sid'])): ?>?sid=<?=$_COOKIE['phpbb_sid']?><?endif;?>">Выход</a>
        </span -->
    <? endif; ?>

    <div class="topmenu">
        
        <ul id="topnav">

            <li>
                <a href="<?=Url::site('news')?>">Новости</a>
                <div class="sub">
                    <ul>
                        <li><a href="<?=Url::site('news#shed')?>">Расписание</a></li>
                        <li><a href="<?=Url::site('news#news')?>">Новости сайта</a></li>
                    </ul>
                </div>
            </li>

            <li>
                <a href="<?=Url::site('faq')?>/">Вопросы</a>
                <div class="sub">
                    <ul>
                        <? foreach ($faq_cats as $faq_cat):?>
                        <li style="z-index: 3;"><a href="<?=Url::site('faq')?>/#<?php echo $faq_cat['id'] ?>.1"><?=$faq_cat['name']?></a></li>
                        <? endforeach;?>
                    </ul>
                </div>
            </li>

            <li>
                <a href="<?=Url::site('useful')?>/">Полезное</a>
                <div class="sub">
                    <ul>
                        <? foreach ($useful_cats as $useful_cat):?>
                        <li><a href="<?=Url::site('/useful/cat/'.$useful_cat['id'])?>"><?=$useful_cat['name']?></a></li>
                        <? endforeach;?>
                       
                    </ul>
                </div>
            </li>

            <?foreach($menu as $_menu):?>
            <li>
                <a href="<?=Url::site('page/'.$_menu->name)?>"><?=$_menu->title?></a>
                <div class="sub">
                    <ul>
                        <?foreach($_menu->contents->find_all() as $_submenu):?>
                        <li><a href="<?=Url::site('page/'.$_menu->name.'/'.$_submenu->name)?>"><?=$_submenu->title?></a></li>
                        <?endforeach?>
                    </ul>
                </div>
            </li>
            <?endforeach?>

            <li>
                <a href="<?=Url::site('page/contacts')?>">Контакты</a>
                <div class="sub">
                    <ul>
                        <li><a href="<?=Url::site('page/contacts#contacts')?>">Контактные данные</a></li>
                        <!-- <li><a href="<?=Url::site('contacts#send')?>">Отправить нам сообщение</a></li> -->
                        <!-- <li><a title="(открывается в новом окне)" target="_blank" href="<?=Url::site('chat')?>">Онлайн чат</a></li> -->
                        <li><a title="(открывается в новом окне)" target="_blank" href="http://vkontakte.ru/club10603912">Группа ВКонтакте</a></li>
                        <li><a title="(открывается в новом окне)" href="http://www.facebook.com/pages/VashZakaz/153129784732527?v=wall" target="_blank">Страница FaceBook</a> </li>
                        <li><a title="(открывается в новом окне)" target="_blank" href="http://twitter.com/VashZakazUS">Twitter</a></li>
                    </ul>
                </div>
            </li>

        </ul>


        <form method="POST" action="<?= Url::site('search') ?>">
            <input id="search" name="search" class="search" value="<?if(isset($search)):echo $search; else:?>ПОИСК ПО САЙТУ<?endif?>" title="Введите строку поиска и нажмите Enter" style="right: 80px;"/>&nbsp;<input type="submit" class="search" value="найти" />
        </form>
    </div> <!-- .tophead -->

</div> <!-- .header -->