<div class="botcol">
    
    <table  width="100%" border="0" cellpadding="0" cellspacing="0" height="21" style="position: relative; background-color: #3E4D6A;">
        <tr>
            <td class="nav2 ac bo" valign="baseline" ><a class="nav2" href="<?= Url::site('news') ?>">НОВОСТИ</a></td>
            <td class="nav2 ac bo" valign="baseline" ><a class="nav2" href="<?= Url::site('faq') ?>">ВОПРОСЫ</a></td>
            <td class="nav2 ac bo" valign="baseline"><a class="nav2" href="<?= Url::site('useful') ?>">ПОЛЕЗНОЕ</a></td>
            <td class="nav2 ac bo" valign="baseline" ><a class="nav2" href="<?= Url::site('page/tariffs') ?>">ТАРИФЫ</a></td>
            <td class="nav2 ac bo" valign="baseline" > <a class="nav2" href="<?= Url::site('page/discounts') ?>">СКИДКИ</a></td>
            <td class="nav2 ac bo" valign="baseline" > <a class="nav2" href="<?=Url::site('page/shipping')?>">ДОСТАВКА</a></td>
            <td class="nav2 ac bo" valign="baseline" ><a class="nav2" href="<?=Url::site('/shop')?>">НАШ МАГАЗИН</a></td>
        </tr>
    </table>

    <table  width="100%" border="0" cellpadding="0" cellspacing="0" height="44" style="position: relative; background-color: #49557B; ">
        <tr>
            <td>
                <img src="/<?=Kohana::config('main.path_media')?>/img/footer_left.jpg" />
            </td>
            <td class="nav2" width="90%" align="center">
                © 2008-<?=date('Y')?> VashZakaz.US
            </td>
            <td>
                <img src="/<?=Kohana::config('main.path_media')?>/img/footer_right.jpg" />
            </td>
        </tr>
    </table>

</div> <!-- .botcol -->
