<h3>Сообщения<? if(isset($count_all_in) AND $count_all_in): ?> <small>(всего / новых)</small><? endif; ?></h3>

<div class="table effect6">
    <div class="row">
        <div class="cell mt16"><a class="mt16" href="<?=Url::site('messages/inbox')?>">Входящие<? if(isset($count_all_in) AND $count_all_in): ?> (<?=$count_all_in?> / Непрочитанных: <?=$count_new_in?>)<? endif; ?></a></div>
    </div>
    <div class="row">        
        <div class="cell mt16"><a class="mt16" href="<?=Url::site('messages/outbox')?>">Исходящие<? if(isset($count_all_out) AND $count_all_out): ?> (<?=$count_all_out?> / Непрочитанных: <?=$count_new_out?>)<? endif; ?></a></div>
    </div>
    <div class="row">        
        <div class="cell mt16"><a class="mt16" href="<?=Url::site('messages/archive')?>">Архив<? if(isset($count_all_archive) AND $count_all_archive): ?> (<?=$count_all_archive?> / Непрочитанных: <?=$count_new_archive?>)<? endif; ?></a></div>
    </div>
    <div class="row">
        <div class="cell mt16"><a class="mt16 red" href="<?= Url::site('messages/send') ?>">Новое сообщение</a></div>
    </div>
</div>