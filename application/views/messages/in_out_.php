<table>
    <tr>
        <th><? if(isset($in) AND $in): ?>Входящие<? else: ?>Исходящие<? endif; ?> сообщения</th>
    </tr>
</table>
<br />
<? foreach ($messages as $message): ?>
<table class="w80p hover" onclick="window.location='<?=Url::site('messages/item/'.$message->id)?>'">
    <tr>
        <td>
            <div class="al b fl"><?=$message->title ? $message->title : '< Без темы >'?></div>
            <? $return = 'messages/' . ((isset($in) AND $in) ? 'inbox' : 'outbox') ?>
            <?if(! empty($in)):?><div class="fr"><a href="<?=Url::site('messages/delete/'.$message->id.'?return='.$return)?>"><span class="c7">удалить</span></a></div><?endif?>
        </td>
    </tr>
    <tr>
        <td class="p4" colspan="2">
            <div class="al fl fn14">
                <? if(isset($in) AND $in): ?>входящее<? else: ?>исходящее<? endif; ?>
                <? if(! $message->readed):?><span class="green">новое</span><? endif; ?>
                <? if ($message->important): ?><span style="color: red">важное</span><? endif; ?>
            </div>
            <div class="ar fr"><?=$message->time?></div>
        </td>
    </tr>
</table>
<br />
<? endforeach; ?>

<? if ($pagination != ''): ?>
<br />
<table>
    <tr>
        <th width="32"><?=$pagination?></th>
    </tr>
</table>
<br />
<? endif; ?>