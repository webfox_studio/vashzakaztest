<h3>Архив сообщений</h3>

<? foreach ($messages as $message): ?>
<table class="w80p hover" onclick="window.location='<?=Url::site('messages/item/'.$message->id)?>'">
    <tr>
        <td>
            <div><?=$message->title ? $message->title : '< Без темы >'?></div>
            <? $return = 'messages/' . ((isset($in) AND $in) ? 'inbox' : 'outbox') ?>
            <?if(! empty($in)):?><div class="fr"><a href="<?=Url::site('messages/archiveopen/'.$message->id)?>"><span style="color: #fff;" >Восстановить</span></a></div><?endif?>
        </td>
    </tr>
    <tr>
        <td>
            <div>
                <? if(isset($userid) AND ($userid == $message->user_id)): ?>исходящее<? else: ?>входящее<? endif; ?>
                <? if(! $message->readed):?><span class="green">новое</span><? endif; ?>
                <? if ($message->important): ?><span style="color: red">важное</span><? endif; ?>
            </div>
            <div class="ar fr"><?=$message->time?></div>
        </td>
    </tr>
</table>
<br />
<? endforeach; ?>

<? if ($pagination != ''): ?>
<br />
<table>
    <tr>
        <th width="32"><?=$pagination?></th>
    </tr>
</table>
<br />
<? endif; ?>