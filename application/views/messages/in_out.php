<table class="effect6" style="margin: 3em 0 0 0;">
    <tr>
        <th><? if(isset($in) AND $in): ?>Входящие<? else: ?>Исходящие<? endif; ?> сообщения</th>
    </tr>
</table>
<br />
<? foreach ($messages as $message): ?>
<table class="w80p hover" onclick="window.location='<?=Url::site('messages/item/'.$message->id)?>'">
    <tr>
        <td class="pl4 pr4 f7">
            <div class="al b fl"><?=$message->title ? $message->title : '< Без темы >'?></div>
            <? $return = 'messages/' . ((isset($in) AND $in) ? 'inbox' : 'outbox') ?>
            <?if(! empty($in)){?><div class="fr"><a href="<?=Url::site('messages/delete/'.$message->id.'?return='.$return)?>"><span>в архив</span></a></div><?
            } else { ?>
            <div class="fr"><a href="<?=Url::site('messages/delete/new/'.$message->id)?>"><span style="color: #fff;" >в архив</span></a> <?php } ?>
        </td>
    </tr>
    <tr>
        <td class="p4" colspan="2">
            <div class="al f6 fl fn14">
                <? if(isset($in) AND $in): ?>входящее<? else: ?>исходящее<? endif; ?>
                <? if(! $message->readed):?><span class="green">новое</span><? endif; ?>
                <? if ($message->important): ?><span style="color: red">важное</span><? endif; ?>
            </div>
            <div class="ar fr"><?=$message->time?></div>
        </td>
    </tr>
</table>
<br />
<? endforeach; ?>

<? if ($pagination != ''): ?>
<br />
<table class="bgc2 w100p">
    <tr>
        <th width="32"><?=$pagination?></th>
    </tr>
</table>
<br />
<? endif; ?>