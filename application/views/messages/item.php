<table class="effect6" style="margin: 3em 0 0 0;">
    <tr>
        <th>Сообщение</th>
    </tr>
    <tr>
        <td>
            <? if($message->whom_id == $user_id): ?>входящее<? else: ?>исходящее<? endif; ?>
            <? if (! $message->readed):?>непрочитано<?else:?>прочитано<?endif;?>
            <? if($message->important):?><span style="color: red">важное</span><?endif?>
        </td>
    </tr>
    <tr>
        <td>
            <div style="margin-bottom:10px;">Время отправки сообщения : <?= $message->time ?></div>
            <div style="margin-bottom:10px;">Тема сообщения : <strong><?= $message->title ?></strong></div>
            <? foreach($messages as $_message): ?>
                <div style="margin-bottom:10px;" class="fl f6 c2"> Отправитель :
                    <span class="<?if($_message->user->is_admin()):?>red<?endif?>">
                        <?= $_message->user->user_data->name ?>
                        <?if($_message->user->is_admin()):?>[VashZakaz]<?endif?>
                    </span>
                </div>
                <!--<div class="ar f6 c2"><?= $_message->time ?></div>-->
                <div style="margin-bottom:10px;" class="bg2 brd3 mt4 mb16 p4">
                    Сообщение : <?= $_message->content ?>
                </div>
            <? endforeach ?>
        </td>
    </tr>

    <tr>
        <td>
            <? if($message->deleted2 == 1){?><div style="background-color:red; height: 30px; text-align:center; color: white"><h3>Данная переписка удалена администратором.</h3></div><? } else {?>
            <div class="ac f11 mb8 bg1 p4">Ответ</div>
            <?=Block::form('message_answer', $form)?>
             <?php } ?>
        </td>
    </tr>
    <?if(isset($pre_message)):?>
    <tr class="bgc1">
        <td class="p8">
            <h3>Предварительный просмотр:</h3>
        </td>
    </tr>
     <tr>
        <td>
                <div>
                    <span class="<?if($user->is_admin()):?>red<?endif?>">
                        <?= $user->user_data->name ?>
                        <?if($user->is_admin()):?>[Администратор]<?endif?>
                    </span>
                </div>
                <div class="ar f6 c2"><?= $pre_message['time'] ?></div>
                <div class="bg2 brd3 mt4 mb16 p4">
                    <?= $pre_message['content'] ?>
                </div>
        </td>
    </tr>
    <?endif?>
</table>

