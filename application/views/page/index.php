<h3><?=$page->title?><?if(isset($subpage)):?>: <?=$subpage->title?><?endif?></h3>

<div class="block effect6 text"> 
    <div><!--<a href="<?=Url::site('page/'.$page->name)?>"><?=$page->title?></a><?if(isset($subpage)):?>: <a href="<?=Url::site('page/'.$page->name.'/'.$subpage->name)?>"><?=$subpage->title?></a><?endif?>--></div>
    <div>
        <?if(isset($subpages)):?>
        <?foreach($subpages as $_page):?>
        <div><?=$_page->content?></div>
        <?endforeach?>
        <?elseif(isset($subpage)):?>
        <div><?=$subpage->content?></div>
        <?endif?>
    </div>
</div>