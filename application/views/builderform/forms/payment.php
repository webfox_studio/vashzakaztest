<? if (isset($errors)): ?>
    <? foreach ($errors as $error): ?>
        <div class="blue"><?= $error ?></div>
    <? endforeach; ?>
<? endif; ?>

<div class="blue">Поля, обозначеные (<span class="red">*</span>), обязательны для заполнения</div>
<form name="order" method="post" action="" enctype="multipart/form-data">
    <table id="newordertable">
        <? foreach ($sections as $section): ?>
            <tr>
                <td style="text-align: left;"><?= $section['label'] ?><? if (isset($section['required']) AND $section['required']): ?>
                        <span class="red">*</span><? endif; ?><? if (isset($section['hint'])): ?><sup
                        style="color: red; cursor: pointer;font-size: 16px;" title="<?= $section['hint'] ?>">
                            (?)</sup><? endif; ?>:
                </td>
                <td>
                    <? if (isset($section['beforetext'])): ?><?= $section['beforetext'] ?><? endif; ?>
                    <? if (isset($section['field'])): ?><?= $section['field'] ?><? endif; ?>
                    <? if (isset($section['aftertext'])): ?><?= $section['aftertext'] ?><? endif; ?>
                </td>
            </tr>
        <? endforeach; ?>
    </table>

    <div><h3>Изображения:</h3>
        <p>Размер изображения не должен превышать 3-х мегабайт</p>
        <input type="hidden" name="images_count" id="images_count" value="0"/>
        <a href="javascript:void(0)" onClick="add_image()">Прикрепить изображение</a>
        <p id="images_upload">
            <span id="error_html"></span>
            <span id="files_html"></span>
        </p>
    </div>
    <div style="height: 200px; overflow: auto"><?= View::factory('edit/agreements') ?></div>
    <div class="fl">
        <label>
            <input id="ch" class="mr4" type="checkbox" onclick="javascript:check();" name="agree"/>&nbsp;Соглашение
            принимаю</label></div>
    <div>
        <input class="button" type="submit" name="preview" value="Предварительный просмотр"/>
        <input class="button" id="sb" type="submit" name="submit" disabled="disabled" value="Отправить" onClick="start_animate_upload();"/>
        <div style="display:none" id="wait_please">Подождите пожалуйста. Идет загрузка изображений<span
                    style="width:10px" id="upload_progress">.</span></div>

</form>

<script type="text/javascript">
    <!--
    function check() {
        console.log('1');
        var ch = document.getElementById("ch");
        var sb = document.getElementById("sb");
        ch.checked ? sb.disabled = false : sb.disabled = true;
    }
    //-->
    var add_image = function () {
        var num = parseInt($('#images_count').val() * 1);
        if (num < 10) {
            num = num * 1 + 1;
            //alert(num);
            $('#images_count').val(num);
            $('#images_upload').append('<p class="mt8" id="image_' + num + '"><input type="file" name="image_' + num + '" /><a style="margin-left:8px;" href="javascript:void(0)" onClick="del_image(\'' + num + '\')">Удалить</a></p>');
        }
    }
    var del_image = function (num) {
        $('#image_' + num).html('');
    }
    function del_image_ajax(num) {
        var filename = $('#image_' + num).attr('filename');
        $('#image_' + num).html('');
        $.post("<?=URL::site('ajax/del_image_ajax')?>",
            {
                name: filename
            });
    }
    var step = 1;
    function animate_upload() {
        if (step == 3) {
            step = 1;
            $('#upload_progress').text('.');
            return true;
        }
        if (step == 1) {
            step = 2;
            $('#upload_progress').text('..');
            return true;
        }
        if (step == 2) {
            step = 3;
            $('#upload_progress').text('...');
            return true;
        }
    }
    var start_animate_upload = function () {
        $('#wait_please').css('display', 'block');
        setInterval('animate_upload()', 500);
    }
</script>