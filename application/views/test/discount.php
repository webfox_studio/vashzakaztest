<h3>Акции</h3>
<div class="text effect6">

    <div>
        <?php foreach ($content as $key => $value): ?>
            <div style="min-height: 150px; border-bottom: 1px dotted gray; margin-bottom: 20px;padding-bottom: 20px;">
                <div class="text1">
                    <h3><?= $value->title ?></h3>
                </div>
                <div>
                <span>
                    <img class="news_img" style="width:100px;float: left;margin: 0 1em 0 0;"
                         src="/upload/discount/<?= $value->img ?>">
                </span>
                    <span><?= $value->description ?></span>
                    <div class="data"><?=date('d.m.Y', strtotime($value->addDate))?></div>
                </div>

            </div>
        <?php endforeach; ?>
    </div>
</div>