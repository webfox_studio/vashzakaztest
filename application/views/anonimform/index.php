<style type="text/css">
    #anonimform label{
        font-size: 14px;
    }
    #anonimform input{
        margin-right: 5px;
    }
    .anonim_data input{
        width: 290px;
    }
</style>
<div id="anonimform">
    <div>
        <h1 style="text-decoration: underline">Опрос клиентского мнения</h1>
    </div>
    <br />
    <div>
        <h3 style="color:red">*Внимание! В данном опросе нет вообще обязательных к заполнению полей и возможен полностью анонимный вариант ответов</h3>
    </div>
    <br />
    <div style="width: 600px; margin:0 auto">
        <form method="post" action="/anonimform/index">
            <div>
                <h3>Как долго Вы не сотрудничаете с нашим сервисом?</h3>
                <div style="text-align:left; width: 480px; margin:0 auto">
                    <label><input type="radio" name="anonim[trabl]" value="Очень давно, уже не помню, когда прекратил заказывать через Вас" />Очень давно, уже не помню, когда прекратил заказывать через Вас</label><br />
                    <label><input type="radio" name="anonim[trabl]" value="Недавно, помню хорошо завершение сотрудничества" />Недавно, помню хорошо завершение сотрудничества</label><br />
                    <label><input type="radio" name="anonim[trabl]" value="Сотрудничать не прекращал, возникли временные сложности" />Сотрудничать не прекращал, возникли временные сложности</label><br />
                    <label><input type="radio" name="anonim[trabl]" value="Иной вариант, ниже объясню подробнее" />Иной вариант, ниже объясню подробнее:</label><br />
                </div>
                <br />
                <textarea name="anonim[orher]" rows="5" cols="60"></textarea>
                
            </div>
            <br /><br />
            <div>
                <h3>Если возможно, напишите более подробно, что повлияло на прекращение сотрудничества с VashZakaz</h3>
                
                <textarea name="anonim[detail]" rows="5" cols="60"></textarea>
            </div>
            <br /><br />
            <div>
                <h3>Продолжаете ли Вы активно покупать за рубежом, используя услуги других посредников или сервисов? Если да, по каким причинам (критериям) сделали новый выбор?</h3>
                <textarea name="anonim[other_buy]" rows="5" cols="60"></textarea>
            </div>
            
            <br /><br />
            <div>
                <h3>Какие минусы, на Ваш взгляд, наиболее существенные в работе нашей компании?</h3>
                <textarea name="anonim[minus]" rows="5" cols="60"></textarea>
            </div>
            
            <br /><br />
            <div>
                <h3>Что бы Вы могли порекомендовать в первую очередь изменить в нашей работе, чтобы ситуация существенно улучшилась?</h3>
                <textarea name="anonim[recommendation]" rows="5" cols="60"></textarea>
            </div>
            
            <br /><br />
            <div>
                <h3>Запомнились ли Вам какие-либо положительные моменты в работе с нами, которые Вы могли бы охарактеризовать как плюсы, которые можно ставить в пример другим посредникам? </h3>
                <textarea name="anonim[positive]" rows="5" cols="60"></textarea>
            </div>
            <br /><br />
            <div style="text-align:left; width: 480px; margin:0 auto">
                <table align="center" width="100%" class="anonim_data">
                    <tr>
                        <td width="180">Имя, фамилия</td>
                        <td><input type="text" name="anonim[fio]"></td>
                    </tr>
                    <tr>
                        <td>Анонимно (ник, только само имя, город, т.п.)</td>
                        <td><input type="text" name="anonim[login]"></td>
                    </tr>
                    <tr>
                        <td>Контакт для обратной связи (по желанию)</td>
                        <td><input type="text" name="anonim[contact]"></td>
                    </tr>
                </table>
            </div>
            <br />
            <div>
                <input style="width: 180px; height: 40px; background-color: blue; color: white; font-size: 16px; font-weight: bold" type="submit" name="anonim_submit" value="Отправить" />
            </div>
        </form>
    </div>
</div>