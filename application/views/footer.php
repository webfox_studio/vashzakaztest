<div class="botcol">
    <ul>
        <li><a href="<?= Url::site('/page/about') ?>">О компании</a></li>
        <li><a href="<?=Url::site('/page/contacts')?>">Контакты</a></li>
        <li><a href="<?=Url::site('/news/all#news')?>">Новости</a></li>
        <li><a target="_blank" href="<?= Url::site('board') ?><? if (Auth::instance()->logged_in() && isset($_COOKIE['phpbb_sid'])): ?>?sid=<?=$_COOKIE['phpbb_sid']?><?endif;?>">Форум</a></li>
    </ul>
    <ul>
        <li><a href="<?=Url::site('/faq')?>">Вопросы</a></li>
        <li><a href="<?=Url::site('/useful')?>">Полезное</a></li>
        <li><a href="<?=Url::site('/page/discounts')?>">Акции</a></li>   
    </ul>
    <ul>        
        <li><a href="<?=Url::site('/page/shipping')?>">Доставка</a></li>
        <li><a href="<?=Url::site('//page/tariffs')?>">Полное сопровождение</a></li>
        <li><a href="<?=Url::site('/page/mforwarding')?>">Виртуальный склад</a></li>
        <li><a href="<?=Url::site('/page/moneytransfer')?>">Денежные переводы</a></li>
    </ul>
    <div class="clear"></div>
    <div class="copyright">&copy; 2008 &mdash; <?=date('Y')?> VashZakaz.us. Доставка грузов из США в Россию, Европу и СНГ</div>
    <div class="clear"></div>
</div>