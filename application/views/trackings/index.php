<style>
    a.buttonAdd, div.buttonAdd {
        display: inline-block;
        padding: 0 7px 3px 7px;
        margin: 0;
        line-height: 26px;
        border: 1px solid #556697;
        border-radius: 3px;
        background: linear-gradient(to bottom, rgb(120, 147, 195) 0%, #556697 100%);
        box-shadow: inset 0 0 1px #5877b4;
        font-size: 15px;
        color: white;
        text-decoration: none;
        cursor: pointer;
    }

    .hiddenRow {
        padding: 0 !important;
    }

    .accordion-toggle {
        cursor: pointer;
    }

    .active {
        display: block;
    }

    h3 {
        font-size: 13pt;
        font-weight: 500;
        margin: 10px 0 5px 0;
        color: #ea6153;
        text-shadow: #fff 2px 2px 2px;
    }

    .main_col {
        padding-top: 45px;
    }

    .notes_admin {
        margin-left: 60px;
    }
</style>
<?php if(isset($_SESSION['add_notes'])): ?>
    <div class="alert alert-info" role="alert">
        <?=$_SESSION['add_notes']?>
    </div>
    <?php unset($_SESSION['add_notes']); ?>
<?php endif; ?>
<div>
    <?php if(isset($errors)): ?>
        <div>
            <ul class="errors has-error">
                <?php foreach($errors as $k => $v): ?>
                    <li class="control-label"><?=$v?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
</div>
<div class="table-responsive-lg">
    <table class="table table-hover" style="border-collapse:collapse;">
        <thead>
        <tr>
            <th><?= $form_column['tracking'] ?></th>
            <th><?= $form_column['departure_date'] ?></th>
            <th><?= $form_column['number'] ?></th>
            <th><?= $form_column['delivery_id'] ?></th>
            <th><?= $form_column['last_update'] ?></th>
            <th><?= $form_column['last_status'] ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($trackings as $key => $value): ?>
            <?php $lastStatuses = $value->statuses->order_by('date_add', 'desc')->limit(1)->find(); ?>
            <?php $firstStatuses = $value->statuses->order_by('date_add', 'asc')->limit(1)->find(); ?>
            <?php $tracking = 'VZ'.$value->tracking.mb_strtoupper(substr($delivery[$value->delivery_id], 0, 2)) ?>
            <tr data-toggle="collapse" data-target="#demo<?= $key ?>" class="accordion-toggle">
                <!--                <td>--><? //= $value->id ?><!--</td>-->
                <td class="text-info"><?=$tracking?></td>

                <td class="text-info"><?= Controller_Tracking::dateConvertTracking($firstStatuses->date_add) ?></td>
                <td class="text-info"><?= $value->number ?></td>
                <td class="text-info"><?= $delivery[$value->delivery_id] ?></td>
                <td class="text-success"><?= Controller_Tracking::dateConvertTracking($lastStatuses->date_add) ?></td>
                <td class="text-success"><?= $lastStatuses->status ?></td>

            </tr>
            <tr>
                <td colspan="10" class="hiddenRow">
                    <div class="accordian-body collapse" id="demo<?= $key ?>" style="padding: 5px">

                        <?php $statuses = $value->statuses->find_all(); ?>
                        <table class="table table-striped" style="width: 550px">
                            <thead>
                            <tr>
                                <th scope="col">Дата</th>
                                <th scope="col">Статус</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($statuses as $k => $v): ?>
                                <tr>
                                    <td><?= Controller_Tracking::dateConvertTracking($v->date_add) ?></td>
                                    <td><?= $v->status ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php foreach ($statuses as $key1 => $value1) {
                            $date[] = $value1->date_add;
                        }
                        $datetime1 = new DateTime(reset($date));
                        $datetime2 = new DateTime(end($date));
                        $interval = $datetime1->diff($datetime2);
                        ?>

                        <div><?= $form_column['days_delivery'] ?>: <?= $interval->format('%a дней'); ?></div>
                        <div><?= $form_column['comment'] ?>: <?= $value->comment ?></div>
                        <br/>
                        <div>Заметки:</div>
                        <?php $notes = $value->notes->find_all(); ?>
                        <?php foreach ($notes as $note): ?>
                            <div>
                                <p class="<?=($note->who == 1) ? 'notes_admin' : '';?>"><?= $note->notes ?> - (<?=Controller_Tracking::dateConvertTracking($note->date_add)?>)</p>
                            </div>
                        <?php endforeach; ?>
                        <div><b>Добавить заметку (заметки видны только Вам):</b></div>
                        <form action="<?= URL::site('trackings') ?>" method="post">
                            <div class="form-group">
                                <textarea name="notes" class="form-control"></textarea>
                            </div>

                            <input type="hidden" name="tracking_id" value="<?=$value->id?>">
                            <input type="hidden" name="tracking_number" value="<?=$tracking?>">
                            <input type="submit" name="submit" value="Добавить">
                        </form>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<? if ($pagination != ''): ?>
    <br/>
    <table class="bgc2 w100p">
        <tr>
            <th width="32"><?= $pagination ?></th>
        </tr>
    </table>
    <br/>
<? endif; ?>