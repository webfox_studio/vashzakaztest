<h2 class="pih2">Отчеты по полученным посылкам</h2>

<table width="600" style="text-align: center" align="center">
    <tr>
        <td><a class="pi_button" href="/statistics/finance">Финансы</a></td>
        <td><a class="pi_button" href="/statistics/order">Заказы</a></td>
        <td><a class="pi_button" href="/statistics/storage">Склад</a></td>
        <td><a class="pi_button" href="/statistics/dispatch">Отправки</a></td>
    </tr>
</table>

<h2>Склад</h2>
<!--<table cellspacing="1" cellpadding="1" class="bgc2 w100p">
    <tbody>
        <tr class="bg1 ac h20 f9">
            <th>Дата внесения посылки</th>
            <th>ID посылки</th>
            <th>Вес посылки</th>
            <th>Статус</th>
        </tr>
        <?php foreach($content as $key => $value): ?>
        <tr class="bgc1 ac">
            <td><?=$value->xls_sklad_0?></td>
            <td><?=$value->xls_sklad_1?></td>
            <td><?=$value->xls_sklad_8?></td>
            <td><?=$value->xls_sklad_17?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>-->



<br />
<div id="page">
    <ul class="accordion" style="border-bottom: 0px solid gray;">
    
        <li>
            <span>
                <div class="" style="height:30px">
                    <div class="pi_zagolovok" style="width:15%">Дата регистрации</div>
                    <div class="pi_zagolovok" style="width:15%">ID посылки</div>
                    <div class="pi_zagolovok" style="width:15%">Магазин (продавец)</div>
                    <div class="pi_zagolovok" style="width:15%">Количество товаров</div>
                    <div class="pi_zagolovok" style="width:15%">Вес посылки, в фунтах</div>
                    <div class="pi_zagolovok" style="width:15%">Статус</div>
                    <div class="pi_zagolovok" style="width:10%"></div>
                    <div style="clear: both"></div>
                </div>
            </span>
            
        </li>
    </ul>
    
<ul id="example4" class="accordion"  style="border-top: 0px solid gray;">
    <?php foreach($content as $key => $value): ?>
        <li>
           
                <div style="height:30px"  class="pi_stroka" data-item="<?=$value->id?>">
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_sklad_0?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_sklad_1?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_sklad_2?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_sklad_5?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_sklad_7?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_sklad_14?></div>
                    <div class="pi_zagolovok pi_open<?=$value->id?> pi_buttons">Открыть</div>
                    <div style="clear: both"></div>
                </div>
           
            <div class="panel" style="text-align: left">
                <div>
                    <div class="pi_info">
                        <p><span>Дата регистрации:</span> <?=$value->xls_sklad_0?></p>
                        <p><span>ID посылки:</span> <?=$value->xls_sklad_1?></p>
                        <p><span>Магазин (продавец):</span> <?=$value->xls_sklad_2?></p>
                        <p><span>Номер инвойса (лота):</span> <?=$value->xls_sklad_3?></p>
                        <p><span>Номер отслеживания (tracking #):</span> <?=$value->xls_sklad_4?></p>
                        <p><span>Количество товаров в посылке:</span> <?=$value->xls_sklad_5?></p>
                        <p><span>Количество еще не полученных позиций (остаток):</span> <?=$value->xls_sklad_6?></p>
                        <p><span>Вес посылки, в фунтах:</span> <?=$value->xls_sklad_7?></p>
                        <p><span>Краткое описание полученных товаров:</span> <?=$value->xls_sklad_8?></p>
                        <p><span>Регистрация и прием посылки на склад:</span> <?=$value->xls_sklad_9?></p>
                        <p><span>Фото отчет (дополнительная услуга):</span> <?=$value->xls_sklad_10?></p>
                        <p><span>Проверка (дополнительная услуга):</span> <?=$value->xls_sklad_11?></p>
                        <p><span>Дополнительная услуга 4 (см. комментарий):</span> <?=$value->xls_sklad_12?></p>
                        <p><span>Дополнительная услуга 5 (см. комментарий):</span> <?=$value->xls_sklad_13?></p>
                        <p><span>Статус:</span> <?=$value->xls_sklad_14?></p>
                        <p><span>Комментарии:</span> <?=$value->xls_sklad_15?></p>
                        
                        
                        
                        
                    </div>
                    <div></div>
                </div>        
            </div>
        </li>
        
        <?php endforeach; ?>
               
            </ul>
</div>
<?=($pagination) ? $pagination : '';?>
<script type="text/javascript" src="/media/js/statistika.js"></script>