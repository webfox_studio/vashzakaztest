<h2 class="pih2">Отчеты по отправкам</h2>

<table width="600" style="text-align: center" align="center">
    <tr>
        <td><a class="pi_button" href="/statistics/finance">Финансы</a></td>
        <td><a class="pi_button" href="/statistics/order">Заказы</a></td>
        <td><a class="pi_button" href="/statistics/storage">Склад</a></td>
        <td><a class="pi_button" href="/statistics/dispatch">Отправки</a></td>
    </tr>
</table>

<h2>Отправления</h2>
<br />
<div id="page">
    <ul class="accordion" style="border-bottom: 0px solid gray;">
    
        <li>
            <span>
                <div class="" style="height:50px">
                    <div class="pi_zagolovok" style="width:13%">Дата отправки</div>
                    <div class="pi_zagolovok" style="width:13%">ID отправления</div>
                    <div class="pi_zagolovok" style="width:15%">Вид (виды) отправки</div>
                    <div class="pi_zagolovok" style="width:15%">Номер отслеживания (tracking #)</div>
                    <div class="pi_zagolovok" style="width:15%">Вес посылки, в фунтах</div>
                    <div class="pi_zagolovok" style="width:19%">Статус</div>
                    <div class="pi_zagolovok" style="width:10%"></div>
                    <div style="clear: both"></div>
                </div>
            </span>
            
        </li>
    </ul>
    
<ul id="example4" class="accordion"  style="border-top: 0px solid gray;">
    <?php foreach($content as $key => $value): ?>
        <li>
            <div style="height:30px"  class="pi_stroka" data-item="<?=$value->id?>">
                    <div class="pi_zagolovok" style="width:13%"><?=$value->xls_otpravki_0?></div>
                    <div class="pi_zagolovok" style="width:13%"><?=$value->xls_otpravki_1?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_otpravki_4?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_otpravki_5?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_otpravki_8?></div>
                    <div class="pi_zagolovok" style="width:19%"><?=$value->xls_otpravki_18?></div>
                    <div class="pi_zagolovok pi_open<?=$value->id?> pi_buttons"  >Открыть</div>
                    <div style="clear: both"></div>
            </div>
            
            <div class="panel" style="text-align: left">
                <div>
                    <div class="pi_info">
                        <p><span>Дата отправки:</span> <?=$value->xls_otpravki_0?></p>
                        <p><span>ID отправленной посылки в отчете клиента:</span> <?=$value->xls_otpravki_1?></p>
                        <p><span>ID полученных посылок из отчета клиента, вошедших в данную отправку:</span> <?=$value->xls_otpravki_2?></p>
                        <p><span>ID полученных посылок из отчета клиента, отправленных частично:</span> <?=$value->xls_otpravki_3?></p>
                        <p><span>Виды отправки, в порядке очередности:</span> <?=$value->xls_otpravki_4?></p>
                        <p><span>Трекинг номера 1:</span> <?=$value->xls_otpravki_5?></p>
                        <p><span>Трекинг номера 2:</span> <?=$value->xls_otpravki_6?></p>
                        <p><span>Трекинг номера 3:</span> <?=$value->xls_otpravki_7?></p>
                        <p><span>Вес посылки, в фунтах:</span> <?=$value->xls_otpravki_8?></p>                     
                        <p><span>Стоимость отправления 1:</span> <?=$value->xls_otpravki_9?></p>
                        <p><span>Стоимость отправления 2:</span> <?=$value->xls_otpravki_10?></p>
                        <p><span>Стоимость отправления 3:</span> <?=$value->xls_otpravki_11?></p>
                        
                        <p><span>Усиленная упаковка:</span> <?=$value->xls_otpravki_12?></p>
                        <p><span>Специальная (сложная) упаковка:</span> <?=$value->xls_otpravki_13?></p>
                        <p><span>Дополнительная услуга 3 (см. комментарий):</span> <?=$value->xls_otpravki_14?></p>
                        <p><span>Дополнительная услуга 4 (см. комментарий):</span> <?=$value->xls_otpravki_15?></p>
                        <p><span>Дополнительная услуга 5 (см. комментарий):</span> <?=$value->xls_otpravki_16?></p>
                        <p><span>Сумма возврата на баланс по страховке:</span> <?=$value->xls_otpravki_17?></p>
                        <p><span>Статус:</span> <?=$value->xls_otpravki_18?></p>
			<p><span>Комментарий:</span> <?=$value->xls_otpravki_19?></p>		    
                    
                    </div>
                    <div></div>
                </div>        
            </div>
        </li>
        
        <?php endforeach; ?>
               
            </ul>
</div>
<?=($pagination) ? $pagination : '';?>
<script type="text/javascript" src="/media/js/statistika.js"></script>

