

<h2 class="pih2">Отчеты по заказам</h2>

<table width="600" style="text-align: center" align="center">
    <tr>
        <td><a class="pi_button" href="/statistics/finance">Финансы</a></td>
        <td><a class="pi_button" href="/statistics/order">Заказы</a></td>
        <td><a class="pi_button" href="/statistics/storage">Склад</a></td>
        <td><a class="pi_button" href="/statistics/dispatch">Отправки</a></td>
    </tr>
</table>

<h2>Заказы</h2>
<!--<table cellspacing="1" cellpadding="1" class="bgc2 w100p">
    <tbody>
        <tr class="bg1 ac h20 f9">
            <th>Дата заказа</th>
            <th>ID заказа</th>
            <th>Номер заказа на сайте</th>
            <th>Статус</th>
        </tr>
        <?php foreach($content as $key => $value): ?>
        <tr class="bgc1 ac">
            <td><?=$value->xls_zakaz_0?></td>
            <td><?=$value->xls_zakaz_1?></td>
            <td><?=$value->xls_zakaz_2?></td>
            <td><?=$value->xls_zakaz_20?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>-->
<br />
<div id="page">
    <ul class="accordion" style="border-bottom: 0px solid gray;">
    
        <li>
            <span>
                <div class="" style="height:30px">
                    <div class="pi_zagolovok" style="width:15%">Дата заказа</div>
                    <div class="pi_zagolovok" style="width:15%">ID заказа</div>
                    <div class="pi_zagolovok" style="width:15%">Магазин (аукцион)</div>
                    <div class="pi_zagolovok" style="width:15%">Номер инвойса (лота)</div>
                    <div class="pi_zagolovok" style="width:15%">Итоговая стоимость (списание)</div>
                    <div class="pi_zagolovok" style="width:15%">Статус</div>
                    <div class="pi_zagolovok" style="width:10%"></div>
                    <div style="clear: both"></div>
                </div>
            </span>
            
        </li>
    </ul>
    
<ul id="example4" class="accordion"  style="border-top: 0px solid gray;">
    <?php foreach($content as $key => $value): ?>
        <li>
           
                <div style="height:30px" class="pi_stroka" data-item="<?=$value->id?>">
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_zakaz_0?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_zakaz_1?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_zakaz_3?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_zakaz_12?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_zakaz_15?></div>
                    <div class="pi_zagolovok" style="width:15%"><?=$value->xls_zakaz_20?></div>
                    <div class="pi_zagolovok pi_open<?=$value->id?> pi_buttons">Открыть</div>
                    <div style="clear: both"></div>
                </div>
           
            <div class="panel" style="text-align: left">
                <div>
                    <div class="pi_info">
                        <p><span>Дата заказа:</span> <?=$value->xls_zakaz_0?></p>
                        <p><span>ID заказа:</span> <?=$value->xls_zakaz_1?></p>
                        <p><span>Номер заказа на сайте:</span> <?=$value->xls_zakaz_2?></p>
                        <p><span>Магазин (аукцион):</span> <?=$value->xls_zakaz_3?></p>
                        <p><span>Дополнительные данные продавца:</span> <?=$value->xls_zakaz_4?></p>
                        <p><span>Наименование товаров 1:</span> <?=$value->xls_zakaz_5?></p>
                        <p><span>Наименование товаров 2:</span> <?=$value->xls_zakaz_6?></p>
                        <p><span>Наименование товаров 3:</span> <?=$value->xls_zakaz_7?></p>
                        
                        <p><span>Общее количество выкупленных позиций:</span> <?=$value->xls_zakaz_8?></p>
                        <p><span>Сумма предоставленной VZ скидки (при наличии):</span> <?=$value->xls_zakaz_9?></p>
                        <p><span>Комиссия VZ за предоставление скидок (при наличии):</span> <?=$value->xls_zakaz_10?></p>
                        <p><span>Первоначальная стоимость заказа (сумма по инвойсу):</span> <?=$value->xls_zakaz_11?></p>
                        
                        <p><span>Номер инвойса продавца:</span> <?=$value->xls_zakaz_12?></p>
                        <p><span>Отмененные при выкупе позиции (при наличии):</span> <?=$value->xls_zakaz_13?></p>
                        <p><span>Количество позиций (с учетом отменных до списаний со счета):</span> <?=$value->xls_zakaz_14?></p>
                        <p><span>Итоговая стоимость (списание):</span> <?=$value->xls_zakaz_15?></p>
                        <p><span>% комиссии сервиса за услуги:</span> <?=$value->xls_zakaz_16?></p>
                        <p><span>Основная комиссия VZ за услуги:</span> <?=$value->xls_zakaz_17?></p>
                        <p><span>Итоговое количество  позиций (с учетом отмененных до и после списания счета):</span> <?=$value->xls_zakaz_18?></p>
                        <p><span>Сумма возврата денежных средств магазином на баланс после всех списаний:</span> <?=$value->xls_zakaz_19?></p>
                        <p><span>Статус:</span> <?=$value->xls_zakaz_20?></p>
                        <p><span>Комментарии:</span> <?=$value->xls_zakaz_21?></p>

                    </div>
                    <div></div>
                </div>        
            </div>
        </li>
        
        <?php endforeach; ?>
               
            </ul>
</div>
<?=($pagination) ? $pagination : '';?>

<script type="text/javascript" src="/media/js/statistika.js"></script>