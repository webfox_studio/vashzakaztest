



<h2 class="pih2">Финансовые отчеты</h2>

<table width="600" style="text-align: center" align="center">
    <tr>
        <td><a class="pi_button" href="/statistics/finance">Финансы</a></td>
        <td><a class="pi_button" href="/statistics/order">Заказы</a></td>
        <td><a class="pi_button" href="/statistics/storage">Склад</a></td>
        <td><a class="pi_button" href="/statistics/dispatch">Отправки</a></td>
    </tr>
</table>

<h2>Финансы</h2>
<!--<table cellspacing="1" cellpadding="1" class="bgc2 w100p">
    <tbody>
        <tr class="bg1 ac h20 f9">
            <th>Дата перевода</th>
            <th>ID перевода</th>
            <th>Вид перевода</th>
            <th>Статус</th>
            <th></th>
        </tr>
        <?php foreach($content as $key => $value): ?>
        <tr class="bgc1 ac">
            <td><?=$value->xls_finance_0?></td>
            <td><?=$value->xls_finance_1?></td>
            <td><?=$value->xls_finance_3?></td>
            <td><?=$value->xls_finance_8?></td>
            <td><a href="#">Посмотреть</a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>-->
<br />
<div id="page">
    <ul class="accordion" style="border-bottom: 0px solid gray;">
    
        <li>
            <span>
                <div class="" style="height:50px"  data-item="<?=$value->id?>">
                    <div class="pi_zagolovok">Дата перевода</div>
                    <div class="pi_zagolovok">ID перевода</div>
                    <div class="pi_zagolovok">Вид перевода</div>
                    <div class="pi_zagolovok">Итоговая сумма зачисления(списания)</div>
                    <div class="pi_zagolovok">Статус</div>
                    <div class="pi_zagolovok"></div>
                    <div style="clear: both"></div>
                </div>
            </span>
            
        </li>
    </ul>
    
<ul id="example4" class="accordion"  style="border-top: 0px solid gray;">
    <?php foreach($content as $key => $value): ?>
        <li>
            
                <div style="height:30px"  class="pi_stroka" data-item="<?=$value->id?>">
                    <div class="pi_zagolovok"><?=$value->xls_finance_0?></div>
                    <div class="pi_zagolovok"><?=$value->xls_finance_1?></div>
                    <div class="pi_zagolovok"><?=$value->xls_finance_3?></div>
                    <div class="pi_zagolovok"><?=$value->xls_finance_6?></div>
                    <div class="pi_zagolovok"><?=$value->xls_finance_8?></div>
                    <div class="pi_zagolovok pi_open<?=$value->id?> pi_buttons">Открыть</div>
                    <div style="clear: both"></div>
                </div>
            
            <div class="panel" style="text-align: left">
                <div>
                    <div class="pi_info">
                        <p><span>Дата перевода:</span> <?=$value->xls_finance_0?></p>
                        <p><span>ID перевода:</span> <?=$value->xls_finance_1?></p>
                        <p><span>Номер перевода на сайте:</span> <?=$value->xls_finance_2?></p>
                        <p><span>Вид перевода:</span> <?=$value->xls_finance_3?></p>
                        <p><span>Первоначальная сумма транзакции:</span> <?=$value->xls_finance_4?></p>
                        <p><span>Комиссия банка (комиссия на вывод):</span> <?=$value->xls_finance_5?></p>
                        <p><span>Итоговая сумма зачисления(списания):</span> <?=$value->xls_finance_6?></p>
                        <p><span>Дата зачисления (списания):</span> <?=$value->xls_finance_7?></p>
                        <p><span>Статус:</span> <?=$value->xls_finance_8?></p>
                        <p><span>Комментарии:</span> <?=$value->xls_finance_9?></p>
                    </div>
                    <div></div>
                </div>        
            </div>
        </li>
        
        <?php endforeach; ?>
               
            </ul>
</div>
<?=($pagination) ? $pagination : '';?>

<script type="text/javascript" src="/media/js/statistika.js"></script>