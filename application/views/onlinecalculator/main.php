<h3>Онлайн калькулятор доставки</h3>

<div><span class="red">*</span> &mdash; поля, обязательные для заполнения</div>
<br>
<table class="block effect6">
    <tr>
        <td width="100"></td>
        <th>Отправитель</th>
        <th>Получатель</th>
    </tr>
    <tr>
        <td><strong>Страна</strong></td>
        <td>
            <select name="country" class="pi-search country-out">
                <option>--</option>
                <?php foreach($sender as $k => $v): ?>
                    <option value="<?=$v->id?>"><?=$v->name?></option>
                <?php endforeach; ?>
            </select>
            <span class="red">*</span>
        </td>
        <td>
            <select id="bv-resipient"  class="pi-search country-in">
                <option value="0">--</option>
                <?php foreach($resipient as $k => $v): ?>
                    <option value="<?=$v->id?>"><?=$v->name?></option>
                <?php endforeach; ?>
            </select>
            <span class="red">*</span>
        </td>
    </tr>
    <tr class="pi-region">
        <td><strong>Город(регион)</strong></td>
        <td>

        </td>
        <td>
            <select id="bv-resipient-city" class="pi-search city-in">
                <option>--</option>
            </select>
            <span class="red">*</span>
        </td>
    </tr>
    <tr>
        <td><strong>Вес посылки</strong></td>
        <td>
            <input id="lb" type="radio" checked class="pi-search heft-lb" name="heft" value="lb"/> lb
            <input id="kg" type="radio" class="pi-search heft-kg" name="heft" value="kg" /> кг
        </td>
        <td>
            <input type="text" class="pi-search heft-value" name="heft_value" value=""  onkeyup="this.value = this.value.replace (/\D/, '')" /> <span class="red">*</span>
        </td>
    </tr>
    <tr>
        <td><strong>Размеры</strong></td>
        <td>
            <input type="radio" checked class="pi-search size-in"  name="size" value="in" /> in
            <input type="radio" class="pi-search size-sm" name="size"  value="sm" /> см
        </td>
        <td>
            <input placeholder="длина" title="длина" type="text" class="pi-search size-value1" name="size_value1" value="" onkeyup="this.value = this.value.replace (/\D/, '')" /> X
            <input placeholder="ширина"  title="ширина" type="text" class="pi-search size-value2" name="size_value2" value="" onkeyup="this.value = this.value.replace (/\D/, '')" /> X
            <input placeholder="высота" title="высота" type="text" class="pi-search size-value3" name="size_value3" value="" onkeyup="this.value = this.value.replace (/\D/, '')" />

            <div class="errorSize" style="color: red; display: none; margin-top: 15px; font-size: 12px">
                <?=$staticText['errorSize']?>
                <br/></div>
        </td>
    </tr>
</table>

<div id="result">
    <div id="pi-result"></div>
</div>

