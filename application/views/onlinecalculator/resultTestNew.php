<style type="text/css">
    .pi-tr-title {
        background-color: #648CB7;
        font-weight: bold;
        color: #ffffff;
    }

    .table td {
        text-align: center;
    }

    .table {
        /*border: 1px solid black;*/
        /*border-collapse: collapse;*/
    }

    .table tr {
        border: 1px solid black;

    }

    .pi-tr-res td {
        font-size: 12px;

    }

    .mosk {
        font-size: 10px;
        color: red;
    }

    .dop td {
        font-size: 11px;
    }

    .titleDelivery {
        cursor: pointer;
        display: table;
        margin-bottom: 4px;
        width: 100%;
        height: 50px;
        background: linear-gradient(to bottom, #ffffff, #d1d1d1);
    }

    .titleDelivery h3 {
        color: black;
        padding-top: 2px;
        padding-bottom: 2px;
        padding-left: 10px;
        padding-right: 10px;
        border: 2px solid #bcbcbc;
        display: table-cell;
        vertical-align: middle;
        position: relative;
        line-height: 20px;
    }

    .titleDeliveryHide {
        display: none;
    }

    .h_active {
        background: linear-gradient(to bottom, #f7f7f7, #dddddd) !important;
        border: 2px solid #d1d1d1 !important;
    }

    .arrow-up {
        width: 0px;
        height: 0px;
        border-left: 5px solid transparent; /* левый наклон стрелки */
        border-right: 5px solid transparent; /* правый наклон стрелки */
        border-bottom: 5px solid #2f2f2f; /* низ, указываем цвет стрелки */
        margin: 0 auto; /* центрируем стрелку в родительском контейнере */
    }
    .fa {
        float: right;
        position: absolute;
        right: 10px;
        top: 6px;
        text-shadow: initial;
    }
    .fa-caret-down{
        color: #c3c3c3;
    }
    .fa-caret-up{
        color: #c3c3c3;
    }
    .calcForm h3, .calcForm span, .calcForm div, .calcForm option, .calcForm th, .calcForm td, .calcForm input, .calcForm a {
        font-family: Arial !important;
    }
</style>
<br/><br/><br/>
<script>
    var errorSize = <?=($pi_empty_in == 1) ? 1 : 0;  ?>
</script>
<?php if ($pi_empty_in == 1): ?>

<?php endif; ?>
<?php
$cargoTemp = array();
$comparisonTemp = array();
?>
<div class="contentCalc calcForm block effect6">
    <div class="titleDelivery" data-title="comparision"><h3>Итоговые результаты расчета<i class="fa fa-caret-up fa-2x" aria-hidden="true"></i>
        </h3>   </div>
    <div class="titleDeliveryHide comparision">
        <?php if (isset($commission)): ?>
            <div>Общая стоимость выкупов: $<?=$commission['summTotal_1']?></div>
            <div>Общая стоимость доставки на склад: $<?=$commission['summTotal_2']?></div>
            <div>Общая комиссия на выкупы (услуга «полное сопровождение»): $<?= $commission['summTotal'] ?></div>
        <?php endif; ?>
        <br>
        <?php $m = 0; ?>
        <?php foreach ($resultTotals as $key => $value): ?>
            <div class="comparisionClick" style="cursor:pointer;" data-type="<?= $m++ ?>"><strong style="color: #526cca;text-decoration: underline;"><?= $staticText[$key]['short'] ?></strong></div>
            <?php foreach ($value as $k => $v): ?>
                <?php if($k == 'additionally') {$padding = 25; $additionally = '(приблизительно)'; } else {$padding = 0; $additionally = ''; } ?>
                <?php for ($i = 0; $i < count($v); $i++): ?>
                    <?php if($k == 'additionally' && $i == 0) { echo '<div style="margin-left: 15px">' . $staticText['comparison']['dop']['name'] . ' </div>'; }?>
                    <div style="margin-left: <?=$padding?>px"><?= $v[$i]['opt_name'].' '. $additionally ?>  - <?=$v[$i]['pr_price']?></div>
                <?php endfor; ?>

            <?php endforeach; ?> <br>
        <?php endforeach; ?>

    </div>
    <?php $n = 0; ?>
    <?php foreach ($resultTotals as $k => $v): ?>
        <?php $nNum = $n++; ?>
        <div class="titleDelivery" data-title="<?=$k?>"><h3 data-menu="<?=$nNum?>"><?= $staticText[$k]['name'] ?><i class="fa fa-caret-down fa-2x" aria-hidden="true"></i></h3></div>
        <table data-block="<?=$nNum?>" width="100%" cellpadding="0" cellspacing="0" class="table titleDeliveryHide <?=$k?>">
            <tr class="pi-tr-title">
                <td>Варианты доставки</td>
                <td>Стоимость + VZ</td>
                <td>Сроки доставки</td>
                <td>Трекинг номер</td>
                <td>Страховка</td>
                <td>Таможенная очистка</td>
                <td>Вариант получения</td>
            </tr>
            <?php foreach ($v as $k1 => $v1): ?>

                <?php if ($k1 == 'additionally'): ?>
                    <tr>
                        <td colspan="7">
                            <div style="font-weight: bold"><span
                                        style="font-size: 17px"><?= $staticText[$k]['dop']['name'] ?></span><br/><br/><span
                                        style="color: red; font-size: 12px; font-weight: normal"><?= $staticText[$k]['dop']['additionally'] ?></span><br/><br/>
                            </div>
                        </td>
                    </tr>
                    <?php if (count($v1) > 0): ?>
                        <?php for ($i = 0; $i < count($v1); $i++): ?>
                            <tr class="pi-tr-res" style=" background-color: #E1FFDC; font-size: 14px">
                                <td><b><?= $v1[$i]['opt_name'] ?></b></td>
                                <td>
                                    <span style="color: blue">+<?= $v1[$i]['pr_price']?> руб.</span>
                                    <?php if ($v1[$i]['opt_id'] == 12 AND $city == 6 AND $cargo != 1) {
                                        echo '<p class="mosk"></p>';
                                    } ?>
                                </td>
                                <td><?= $v1[$i]['opt_delivery_period'] ?></td>
                                <td><?= $v1[$i]['opt_tracing'] ?></td>
                                <td><?= $v1[$i]['opt_insurance'] ?></td>
                                <td><?= $v1[$i]['opt_customsclearance'] ?></td>
                                <td><?= $v1[$i]['opt_variant_reception'] ?></td>
                            </tr>
                        <?php endfor; ?>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if ($k1 == 'main'): ?>
                <?php if (count($v1) > 0): ?>
                    <?php for ($i = 0; $i < count($v1); $i++): ?>
                        <tr class="pi-tr-res" style=" background-color: #E1FFDC; font-size: 14px">
                            <td><b><?= $v1[$i]['opt_name'] ?></b></td>
                            <td>
                                <span style="color: blue"><?= $v1[$i]['pr_price'] ?></span>
                                <?php if ($v1[$i]['opt_id'] == 12 AND $city == 6 AND $cargo != 1) {
                                    echo '<p class="mosk"></p>';
                                } ?>
                            </td>
                            <td><?= $v1[$i]['opt_delivery_period'] ?></td>
                            <td><?= $v1[$i]['opt_tracing'] ?></td>
                            <td><?= $v1[$i]['opt_insurance'] ?></td>
                            <td><?= $v1[$i]['opt_customsclearance'] ?></td>
                            <td><?= $v1[$i]['opt_variant_reception'] ?></td>
                        </tr>
                    <?php endfor; ?>
                <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </table>

    <?php endforeach; ?>
</div>
