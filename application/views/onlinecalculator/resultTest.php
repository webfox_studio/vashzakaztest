<style type="text/css">
    .pi-tr-title {
        background-color: #648CB7;
        font-weight: bold;
        color: #ffffff;
    }

    .table td {
        text-align: center;
    }

    .table {
        /*border: 1px solid black;*/
        /*border-collapse: collapse;*/
    }

    .table tr {
        border: 1px solid black;

    }

    .pi-tr-res td {
        font-size: 12px;

    }

    .mosk {
        font-size: 10px;
        color: red;
    }

    .dop td {
        font-size: 11px;
    }

    .titleDelivery {
        cursor: pointer;
    }

    .titleDelivery h3 {
        color: black;
        background: linear-gradient(to bottom, white, darkgray);
        padding-top: 2px;
        padding-bottom: 2px;
        padding-left: 10px;
        padding-right: 10px;
        border: 2px solid lightgray;
        height: 50px;
    }

    .titleDeliveryHide {
        display: none;
    }

    .h_active {
        background: white !important;
    }

    .arrow-up {
        width: 0px;
        height: 0px;
        border-left: 5px solid transparent; /* левый наклон стрелки */
        border-right: 5px solid transparent; /* правый наклон стрелки */
        border-bottom: 5px solid #2f2f2f; /* низ, указываем цвет стрелки */
        margin: 0 auto; /* центрируем стрелку в родительском контейнере */
    }
</style>
<br/><br/><br/>
<script>
    var errorSize = <?=($pi_empty_in == 1) ? 1 : 0;  ?>
</script>
<?php if ($pi_empty_in == 1): ?>

    <!--    <div style="color: red; font-weight: bold">-->
    <!--        «Внимание! Вы не указали размеры посылки (груза). Итоговые данные в этом случае отображаются правильно только для обычных посылок, но не для негабаритных грузов».-->
    <!--        <br/></div>-->
<?php endif; ?>
<?php
//if($_SERVER['REMOTE_ADDR'] == '95.134.108.122') {
//    echo '<pre>';
//    print_r($result);
//    echo '</pre>';
//}
$cargoTemp = array();
$comparisonTemp = array();
?>
<div class="contentCalc">
    <div class="titleDelivery" data-title="usps"><h3><?= $staticText['usps']['name'] ?></h3></div>
    <table data-block="0" width="100%" cellpadding="0" cellspacing="0" class="table titleDeliveryHide usps">
        <tr class="pi-tr-title">
            <td>Варианты доставки</td>
            <td>Стоимость + VZ</td>
            <td>Сроки доставки</td>
            <td>Трекинг номер</td>
            <td>Страховка</td>
            <td>Таможенная очистка</td>
            <td>Вариант получения</td>
        </tr>
        <?php foreach ($result as $k => $v): ?>
            <?php if ($v['opt_id'] == 12) {
                $cargoTemp[] = $v;
                unset($v);
                continue;
                // echo '</table><table><tr><td colspan="7" style=""><h3>Доставка транспортной курьерской службой (VashZakaz cargo) до Санкт-Петербурга<br /> (до терминала транспортной компании)<br /> (услуги mail forwarding и таможенной очистки учтены для обычных товаров) </h3></td></tr>';
            }
            $comparisonTemp['Доставка государственной почтой США (USPS)'][] = array(
                'name' => $v['opt_name'],
                'price' => $v['pr_price']
            );
            ?>
            <tr class="pi-tr-res" style=" background-color: #E1FFDC; font-size: 14px">
                <td><b><?= $v['opt_name'] ?></b></td>
                <td>
                    <span style="color: blue"><?= $v['pr_price'] ?></span>
                    <?php if ($v['opt_id'] == 12 AND $city == 6 AND $cargo != 1) {
                        echo '<p class="mosk"></p>';
                    } ?>
                </td>
                <td><?= $v['opt_delivery_period'] ?></td>
                <td><?= $v['opt_tracing'] ?></td>
                <td><?= $v['opt_insurance'] ?></td>
                <td><?= $v['opt_customsclearance'] ?></td>
                <td><?= $v['opt_variant_reception'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>


    <div class="titleDelivery" data-title="cargo"><h3><?= $staticText['cargo']['name'] ?></h3></div>
    <table data-block="1" width="100%" cellpadding="0" cellspacing="0" class="table titleDeliveryHide cargo">
        <tr class="pi-tr-title">
            <td>Варианты доставки</td>
            <td>Стоимость + VZ</td>
            <td>Сроки доставки</td>
            <td>Трекинг номер</td>
            <td>Страховка</td>
            <td>Таможенная очистка</td>
            <td>Вариант получения</td>
        </tr>
        <?php foreach ($cargoTemp as $k => $v): ?>
            <tr class="pi-tr-res" style=" background-color: #E1FFDC; font-size: 14px">
                <td><b><?= $v['opt_name'] ?></b></td>
                <td>
                    <span style="color: blue"><?= $v['pr_price'] ?></span>
                    <?php if ($v['opt_id'] == 12 AND $city == 6 AND $cargo != 1) {
                        echo '<p class="mosk"></p>';
                    } ?>
                </td>
                <td><?= $v['opt_delivery_period'] ?></td>
                <td><?= $v['opt_tracing'] ?></td>
                <td><?= $v['opt_insurance'] ?></td>
                <td><?= $v['opt_customsclearance'] ?></td>
                <td><?= $v['opt_variant_reception'] ?></td>
            </tr>
            <?php
            $comparisonTemp[$v['opt_name']][] = array(
                'name' => $v['opt_name'],
                'price' => $v['pr_price']
            );
            ?>
        <?php endforeach; ?>
    </table>

    <table data-block="1" width="100%" cellpadding="0" cellspacing="0" class="table titleDeliveryHide cargo">

        <?php if ($cargo == 0) { ?>
            <?php if (isset($result_city)): ?>
                <tr>
                    <td colspan="7">
                        <div style="font-weight: bold"><span
                                    style="font-size: 17px"><?= $staticText['cargo']['dop']['name'] ?></span><br/><br/><span
                                    style="color: red; font-size: 12px; font-weight: normal"><?= $staticText['cargo']['dop']['additionally'] ?></span><br/><br/>
                        </div>
                    </td>
                </tr>
                <tr class="pi-tr-title">
                    <td>Варианты доставки</td>
                    <td>Стоимость + VZ</td>
                    <td>Сроки доставки</td>
                    <td>Трекинг номер</td>
                    <td>Страховка</td>
                    <td>Таможенная очистка</td>
                    <td>Вариант получения</td>
                </tr>
                <?php foreach ($result_city as $k => $v): ?>
                    <?php
                    $comparisonTemp['VashZakaz Cargo']['dop'][] = array(
                        'name' => $v->opt_name,
                        'price' => $v->pr_price . ' руб'
                    );
                    ?>
                    <tr class="pi-tr-res dop">
                        <td><b><?= $v->opt_name ?></b></td>
                        <td>
                            <?php if ($v->opt_name == 'ПЭК авиа' || $v->opt_name == 'ПЭК авто') {
                                $pek = $v->pr_price + 500;
                            } else {
                                $pek = $v->pr_price;
                            } ?>
                            <?php if ($cargo == 0) { ?>
                                <span style="color: blue">+<?= $pek ?> руб</span>
                                <?php if ($city == 6) {
                                    echo '<p class="mosk"></p>';
                                } ?>
                            <?php } else { ?>
                                --
                            <?php } ?>
                        </td>
                        <td><?= $v->opt_delivery_period ?></td>
                        <td><?= $v->opt_tracing ?></td>
                        <td><?= $v->opt_insurance ?></td>
                        <td><?= $v->opt_customsclearance ?></td>
                        <td><?= $v->opt_variant_reception ?></td>
                    </tr>

                <?php endforeach; ?>
            <?php endif; ?>
        <?php } ?>
    </table>


    <?php if (count($newDelivery) > 0): ?>
        <div class="titleDelivery" data-title="new"><h3><?= $staticText['new']['name'] ?></h3></div>
        <table data-block="2" width="100%" cellpadding="0" cellspacing="0" class="table titleDeliveryHide new">
            <tr class="pi-tr-title">
                <td>Варианты доставки</td>
                <td>Стоимость + VZ</td>
                <td>Сроки доставки</td>
                <td>Трекинг номер</td>
                <td>Страховка</td>
                <td>Таможенная очистка</td>
                <td>Вариант получения</td>
            </tr>
            <?php foreach ($newDelivery as $k => $v): ?>
                <tr class="pi-tr-res">
                    <td><b><?= $v->opt_name ?></b></td>
                    <td>
                        <?= $v->pr_price ?>
                    </td>
                    <td><?= $v->opt_delivery_period ?></td>
                    <td><?= $v->opt_tracing ?></td>
                    <td><?= $v->opt_insurance ?></td>
                    <td><?= $v->opt_customsclearance ?></td>
                    <td><?= $v->opt_variant_reception ?></td>
                </tr>

                <?php
                $comparisonTemp['Новая доставка'][] = array(
                    'name' => $v->opt_name,
                    'price' => $v->pr_price . '$'
                );
                ?>
            <?php endforeach; ?>
        </table>


    <?php endif; ?>

    <div class="titleDelivery" data-title="comparision"><h3><?= $staticText['comparison']['name'] ?></h3></div>

    <div class="titleDeliveryHide comparision">
        <?php $i = 0; ?>
        <?php foreach ($comparisonTemp as $key => $value): ?>
            <div class="comparisionClick" style="cursor:pointer" data-type="<?= $i++ ?>"><strong><?= $key ?></strong>
            </div>
            <?php foreach ($value as $k => $v): ?>

                <?php if ($k === 'dop' && is_array($v)) { ?>
                    <?php foreach ($v as $k1 => $v1): ?>
                        <?php if ($k1 == 0) : ?>
                            <?php echo '<div style="margin-left: 15px">' . $staticText['comparison']['dop']['name'] . '</div>'; ?>
                        <?php endif; ?>
                        <div style="margin-left: 30px"> + <?= $v1['name'] ?> - <?= $v1['price'] ?></div>
                    <?php endforeach; ?> <br>
                <?php } else { ?>
                    <div> <?= $v['name'] ?> - <?= $v['price'] ?></div>
                <?php } ?>
            <?php endforeach; ?> <br>
        <?php endforeach; ?>

    </div>
    <!--    <pre>-->
    <!--        --><? //=print_r($comparisonTemp)?>
    <!--    </pre>-->

</div>
