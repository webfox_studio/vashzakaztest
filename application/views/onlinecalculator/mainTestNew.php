<style>
    .buyoutHide{
        display: none;
    }
    #addBuyond{
        padding:2px;
    }
    .delBuyond{
        color:red;
        cursor: pointer;
    }
    .emptyColumnCityIn, .emptyColumnCountryIn, .emptyColumnHeftIn, .emptyColumnCountryOut{
        color: red;
        font-size: 12px;
    }
    .calcForm h3, .calcForm span, .calcForm div, .calcForm option, .calcForm th, .calcForm td, .calcForm input, .calcForm a {
        font-family: Arial !important;
    }
</style>
<div class="calcForm">
<h3>Онлайн калькулятор доставки</h3>

<div><span class="red">*</span> &mdash; поля, обязательные для заполнения</div>
<br>
<table class="block effect6">
    <tr>
        <td width="100"></td>
        <th>Отправитель</th>
        <th>Получатель</th>
    </tr>
    <tr>
        <td><strong>Страна</strong></i>
        </td>
        <td>
            <select name="country" class="country-out pi-search-select">
                <option>--</option>
                <?php foreach($sender as $k => $v): ?>
                    <option value="<?=$v->id?>"><?=$v->name?></option>
                <?php endforeach; ?>
            </select>
            <span class="red">*</span>
            <div class="emptyColumnCountryOut"></div>
        </td>
        <td>
            <select id="bv-resipient"  class="country-in pi-search-select">
                <option value="0">--</option>
                <?php foreach($resipient as $k => $v): ?>
                    <option value="<?=$v->id?>"><?=$v->name?></option>
                <?php endforeach; ?>
            </select>
            <span class="red">*</span>
            <div class="emptyColumnCountryIn"></div>
        </td>
    </tr>
    <tr class="pi-region">
        <td><strong>Город(регион)</strong></td>
        <td>

        </td>
        <td>
            <select id="bv-resipient-city" class="city-in pi-search-select">
                <option>--</option>
            </select>
            <span class="red">*</span>
            <div class="emptyColumnCityIn"></div>
        </td>
    </tr>
    <tr>
        <td>Выкуп с помощью сотрудников VashZakaz (услуга «полное сопровождение»)</td>
        <td colspan="2">
            <input id="buyout" type="checkbox" name="buyout" value="" />

            <div class="buyoutHide">
                <table class="buyoutTable">
                    <tr>
                        <td>Общая сумма выкупа с помощью VashZakaz *</td>
                        <td>Стоимость доставки от продавца к нам на склад </td>
                        <td></td>
                    </tr>
                    <tr class="totalTr">
                        <td><input class=" pi-search-input" type="text" name="totalSummBuyout" value="" onkeyup="this.value = this.value.replace (/[^0-9.\s]+/g, '')"></td>
                        <td><input class=" pi-search-input" type="text" name="totalExtraSummBuyout" value="" onkeyup="this.value = this.value.replace (/[^0-9.\s]+/g, '')"></td>
                        <td><span class="delBuyond">del</span></td>
                    </tr>
                </table>
                <button id="addBuyond">Добавить еще заказ</button>
            </div>
        </td>
    </tr>
    <tr>
        <td><strong>Вес посылки</strong></td>
        <td>
            <input id="lb" type="radio" checked class="heft-lb pi-search-radio" name="heft" value="lb"/> lb
            <input id="kg" type="radio" class="heft-kg pi-search-radio" name="heft" value="kg" /> кг
        </td>
        <td>
            <input type="text" class="heft-value pi-search-input" name="heft_value" value=""  onkeyup="this.value = this.value.replace (/\D/, '')" /> <span class="red">*</span>
            <div class="emptyColumnHeftIn"></div>
        </td>
    </tr>
    <tr>
        <td><strong>Размеры</strong></td>
        <td>
            <input type="radio" checked class="size-in pi-search-radio"  name="size" value="in" /> in
            <input type="radio" class="size-sm  pi-search-radio" name="size"  value="sm" /> см
        </td>
        <td>
            <input placeholder="длина" title="длина" type="text" class="size-value1 pi-search-input" name="size_value1" value="" onkeyup="this.value = this.value.replace (/\D/, '')" /> X
            <input placeholder="ширина"  title="ширина" type="text" class="size-value2 pi-search-input" name="size_value2" value="" onkeyup="this.value = this.value.replace (/\D/, '')" /> X
            <input placeholder="высота" title="высота" type="text" class="size-value3 pi-search-input" name="size_value3" value="" onkeyup="this.value = this.value.replace (/\D/, '')" />

            <div class="errorSize" style="color: red; display: none; margin-top: 15px; font-size: 12px">
                <?=$staticText['errorSize']?>
                <br/></div>
        </td>
    </tr>
</table>
</div>
<div id="result">
    <div id="pi-result"></div>
</div>

