<a name="news"></a>

<div>
    <? if (isset($all_news) AND $all_news): ?>
        <h3>Все новости</h3>
        <? else: ?>
                <h3>Последняя новость</h3>
    <? endif; ?>
</div>

<div class="block effect6">
    <div class="text2">
        <?= Block::news(isset($news) ? $news : array()) ?>
    </div>
    <a class="fr" href="<?= Url::site('news/all') ?>">Все новости</a>
    <br>
</div>