<div class="news">
    <div><a name="news_<?= isset($id) ? $id : '' ?>"></a></div>
    <div class="data"><?= isset($dt) ? $dt : 'News #' . $id ?></div>
</div>
<br>
<?php
$text = explode('|||', $content);
?>
<div class="p4">
    <h2 class="blue"><?= $name ?></h2>
    <div>
		<?php
            if (isset($text[1])) {
                echo $text[1];
            } else {
			    if (!empty($img)):
				    echo "<img class=\"lorry_img\" style=\"width:100px;float: left;margin: 0 1em 0 0;\" src=\"/upload/news/<?=$img?>\">";
			    endif;
			    echo $text[0];
		    }
		?>
    </div>

</div>
