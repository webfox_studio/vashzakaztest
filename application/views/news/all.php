<a name="news_<?= isset($id) ? $id : '' ?>"></a>
<h3>Новости</h3>
<div class="text effect6">
    <?php
    foreach ($news as $k => $v):
        $text = explode('|||', $v['content']);
        ?>
        <div class="text1"><a class="nd" href="/news/<?= $v['id'] ?>"><h3><?= $v['name'] ?></h3></a></div>
        <div>
			<?php if(!empty($v['img']) && file_exists($_SERVER['DOCUMENT_ROOT'].'/upload/news/'.$v['img'])): ?>
                <img class="news_img" style="width:100px;float: left;margin: 0 1em 0 0;"
                     src="/upload/news/<?= $v['img'] ?>">
            <?php endif; ?>
            <?= $text[0] ?>
        </div>
        <div class="data"><?= date('d.m.Y', strtotime($v['dt'])) ?></div>
        <div><a class="nd blue" href="/news/<?= $v['id'] ?>">подробнее &Rrightarrow;</a></div>
        <div class="dot"></div><br>
    <?php endforeach; ?>
    <?= $pagination ?>

</div>


