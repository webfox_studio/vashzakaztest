<div class="header">
    <img src="/<?=Kohana::config('main.path_media')?>/img/head_left.jpg" class="noborder" /><a href="<?=Url::site('/')?>" class="noborder"><img class="noborder" src="/<?=Kohana::config('main.path_media')?>/img/head_logo.jpg" /></a><img src="/<?=Kohana::config('main.path_media')?>/img/head_center.jpg" class="noborder" style="display: inline" />
    <img src="/<?=Kohana::config('main.path_media')?>/img/head_right.jpg" class="noborder" style="position: absolute; right: 0px;" />

    <span style="position: absolute; top: 50; left: 182;">
        <a  class="nav1" href="<?=Url::site('page/about')?>">О КОМПАНИИ</a>
    </span>

    <span style="position: absolute; top: 50; left: 315;">
        <a class='nav1' target="_blank" href="<?=Url::site('board')?>">ФОРУМ</a>
    </span>

    <? if (Auth::instance()->logged_in()):?>
    <span  class="nav2" style="position: absolute;  right: 130px; top: 5px">
        <a href="<?=Url::site('user/data')?>" title="Информация пользователя"><?=Auth::instance()->get_user()->user_data->name?></a><?if(Auth::instance()->logged_in('admin')):?> [<small><a href="<?=Url::site('admin')?>">Admin</a></small>]<?endif;?>
    </span>
    <span  class="nav2" style="background-image: url('/<?=Kohana::config('main.path_media')?>/img/head_exit.jpg'); width: 60; height: 20px; text-align: center; position: absolute; right: 50px; top: 2px">
        <a  href="<?=Url::site('logout')?>">Выход</a>
    </span>
    <? endif;?>

    <div class="topmenu">

        <ul>
            <li><a href="index.php?page=news" >Новости</a>
                <ul>
            	   <li><a href="index.php?page=news#shed">Расписание</a></li>
            	   <li><a href="index.php?page=news#news">Новости сайта</a></li>
            	</ul>
            </li>

            <li><a href="index.php?page=faq" >Вопросы</a>
                <ul><li><a href="index.php?page=faq#1.1">Регистрация на сайте. Оформление заказа</a></li>
                        <li><a href="index.php?page=faq#2.1">Денежные переводы</a></li>
                        <li><a href="index.php?page=faq#3.1">Наши услуги</a></li>
                        <li><a href="index.php?page=faq#4.1">Тарифы</a></li>
                        <li><a href="index.php?page=faq#5.1">eBay. Другие аукционы</a></li>

                        <li><a href="index.php?page=faq#6.1">Интернет-магазины</a></li>
                        <li><a href="index.php?page=faq#7.1">Почтовые службы и таможня</a></li>
                        <li><a href="index.php?page=faq#8.1">Гарантии и риски</a></li>
                        <li><a href="index.php?page=faq#9.1">Разное</a></li>
                        </ul>
            </li>
            <li><a href="index.php?page=_useful" >Полезное</a>

                <ul><li><a href="index.php?page=_useful&cat_id=6">Международная доставка товаров</a></li>
                        <li><a href="index.php?page=_useful&cat_id=7">Безопасность в сети интернет</a></li>
                        <li><a href="index.php?page=_useful&cat_id=8">Покупки на аукционе eBay</a></li>
                        <li><a href="index.php?page=_useful&cat_id=9">О наших услугах</a></li>
                        </ul>
            </li>
                        <li><a href="index.php?page=tariffs">Тарифы</a>

                        <ul><li><a href="index.php?page=tariffs&subitem=main">Основные услуги</a></li>
                               <li><a href="index.php?page=tariffs&subitem=additional">Дополнительные услуги</a></li>
                               <li><a href="index.php?page=tariffs&subitem=commission">Комиссии денежных переводов</a></li>
                               </ul>
                        <li><a href="index.php?page=discounts">Скидки</a>
                        <ul><li><a href="index.php?page=discounts&subitem=clientdiscount">Скидки клиентов</a></li>

                               <li><a href="index.php?page=discounts&subitem=forumdiscount">Скидки форумчан</a></li>
                               </ul>
                        <li><a href="index.php?page=shipping">Доставка</a>
                        <ul><li><a href="index.php?page=shipping&subitem=ship">Расценки</a></li>
                               <li><a href="index.php?page=shipping&subitem=strah">Страхование</a></li>
                               <li><a href="index.php?page=shipping&subitem=shiptype">Виды доставки</a></li>

                               </ul></li>
            <li><a href="index.php?page=contacts">Контакты</a>
                <ul>
            	   <li><a href="index.php?page=contacts#contacts">Контактные данные</a></li>
            	   <li><a href="index.php?page=contacts#send">Отправить нам сообщение</a></li>
                   <li><a title="(открывается в новом окне)" target="_blank" href="/chat/">Онлайн чат</a></li>
                   <li><a title="(открывается в новом окне)" target="_blank" href="http://vkontakte.ru/club10603912">Группа ВКонтакте</a></li>

                   <li><a title="(открывается в новом окне)" href="http://www.facebook.com/pages/VashZakaz/153129784732527?v=wall" target="_blank">Страница FaceBook</a> </li>
                   <li><a title="(открывается в новом окне)" target="_blank" href="http://twitter.com/VashZakazUS">Twitter</a></li>
            	</ul>
            </li>
        </ul>

        <form method="POST" action="<?=Url::site('search')?>">
            <input id="search" name="search" class="search" value="ПОИСК ПО САЙТУ" title="Введите строку поиска и нажмите Enter"/>
        </form>
    </div> <!-- .topmenu -->

</div> <!-- .header -->