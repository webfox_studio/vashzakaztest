<h3>Загрузка файла</h3>

<? if (isset($error)): ?>
    <div><?=$error?></div>
<? endif; ?>
    <form method="POST" action="" enctype="multipart/form-data">
        <table class="effect6" style="width: 80%;">
            <tr>
                <td>Имя файла: <input type="file" name="file_name" value="">
                    <input class="button" type="submit" name="submit" value="Загрузить файл">
                </td>
            </tr>
            <tr><td><p class="blue">Размер загружаемого файла не должен превышать 3-х мегабайт.</p></td></tr>
    </table>
</form>

<h3>Мои файлы</h3>
<? foreach ($files as $file): ?>
<table class="block effect1 text" style="width: 80%;margin-bottom:20px;position:relative;border:1px solid #5877b4;">
    <tr>
        <td>
            <div style="font-style:italic;color:#5877b4;"><?=$file->name?></div>
            <div>
                <? if(! $file->readed):?><span class="blue">Новый</span><? endif; ?>
            </div>
            <?if($file->incoming==1):?><div class="fr">Файл от администратора</div><?endif?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="fl"><a class="files_link" href="javascript:void(0)" onclick="window.location='<?=Url::site('files/download/'.$file->id)?>'">Скачать</a> | <?=date('Y-m-d H:i:s',$file->date_time)?></div>
        </td>
    </tr>
</table>
<? endforeach; ?>
<?= $pagination ?>