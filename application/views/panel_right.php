<div id="righttable">

    <?= isset($account_opt) ? $account_opt : '' ?>
    <h3 class="system_none">Калькулятор расчетов</h3>
    <div class="block effect1 center">
        <a href="/onlinecalculator/main">
            <img src="/media/img/onlinecalculator.jpg" style="" border="0">
        </a>
    </div>
    <h3 class="system_none">Платежные системы</h3>

    <div class="block effect1 center">
        <div class="p4">
            <a href="https://www.paypal.com/us/verified/pal=vash_zakaz%40yahoo%2ecom" target="_blank">
                <img border="0" src="/<?= Kohana::config('main.path_media') ?>/img/paypal.png" width="55" height="55">
            </a>
            <a href="https://www.swift.com/" target="_blank"><img src="/media/img/swift.jpg" height="55" border="0"></a>

        </div>
        <!--            <div class="p4"><a href="http://checkout.google.com/" target="_blank"><img border="0" src="/-->
        <? //=Kohana::config('main.path_media')?><!--/img/google_checkout.png" width="114" height="46"></a></div>-->

        <!--            <div class="p4"><a href="http://www.privatbank.ua/" target="_blank"><img border="0" src="/-->
        <? //=Kohana::config('main.path_media')?><!--/img/privatbank.png" width="120" height="20"></a></div>-->
        <!--            <div class="p4"><a href="http://www.monetaexpress.com/" target="_blank"><img border="0" src="/-->
        <? //=Kohana::config('main.path_media')?><!--/img/monetaexpress.png" width="120" height="17"></a></div>-->

        <div class="p4"><a href="http://www.unistream.ru/" target="_blank"><img border="0"
                                                                                src="/<?= Kohana::config('main.path_media') ?>/img/unistream.png"
                                                                                width="120" height="17"></a></div>
        <!--            <div class="p4"><a href="http://www.anelik.ru/" target="_blank"><img border="0" src="/-->
        <? //=Kohana::config('main.path_media')?><!--/img/anelik.png" width="120" height="32"></a></div>-->
        <div class="p4"><a href="http://www.contact-sys.com" target="_blank"><img border="0"
                                                                                  src="/<?= Kohana::config('main.path_media') ?>/img/contact.png"
                                                                                  width="120" height="30"></a></div>
        <div class="p4"><a href="http://webmoney.ru/" target="_blank"><img src="/media/img/webmoney.jpg" width="120"
                                                                           border="0"></a></div>
    </div>

    <!--        <h3 class="system_none">Курьерские службы</h3>-->
    <!---->
    <!--        <div class="block effect1 center">-->
    <!--            <div class="p4"><a href="http://www.usps.com" target="_blank"><img border="0" src="/-->
    <? //=Kohana::config('main.path_media')?><!--/img/usps.png" width="120" height="22"></a></div>-->
    <!--            <div class="p4"><a href="http://www.usps.com/international/expressmailinternational.htm" target="_blank"><img border="0" src="/-->
    <? //=Kohana::config('main.path_media')?><!--/img/ems.png" width="120" height="28"></a></div>-->
    <!--            <div class="p4"><a href="http://www.usps.com/international/prioritymailinternational.htm" target="_blank"><img border="0" src="/-->
    <? //=Kohana::config('main.path_media')?><!--/img/pmusps.png" width="120" height="36"></a></div>-->
    <!--            <div class="p4"><a href="http://www.meest.us" target="_blank"><img border="0" src="/-->
    <? //=Kohana::config('main.path_media')?><!--/img/meest.png" width="55" height="37"></a></div>-->
    <!--            <div class="p4"><a href="http://www.ups.com" target="_blank"><img border="0" src="/-->
    <? //=Kohana::config('main.path_media')?><!--/img/ups.png" width="55" height="65"></a></div>-->
    <!--            <div class="p4"><a href="http://www.fedex.com" target="_blank"><img border="0" src="/-->
    <? //=Kohana::config('main.path_media')?><!--/img/fedex.png" width="99" height="28"></a></div>-->
    <!--        </div>-->
    <br>
    <script type="text/javascript">
        $(document).ready(function () {
            Load();
            is_online();
            setInterval("Load();", 30000);
            setInterval("is_online();", 60000);
            $("#pac_form").submit(Send);

        });

        function Send() {
            if ($("#pac_text").val() == "") return false;
            $.post("<?=URL::site('ajax/online')?>", {
                    act: "send",
                    user_nick: "<?=$nick?>",
                    text: $("#pac_text").val()
                },
                Load());

            $("#pac_text").val("");
            $("#pac_text").focus();

            return false;
        }

        var last_message_id = 0;
        var load_in_process = false;


        function Load() {
            if (!load_in_process) {
                load_in_process = true;
                $.post("<?=URL::site('ajax/online')?>", {
                        act: "load",
                        last: last_message_id,
                        rand: (new Date()).getTime(),
                        user_nick: "<?=$nick?>"
                    },
                    function (result) {
                        eval(result);
                        /*if (result!='') {
                         $("#chat").scrollTop($("#chat").get(0).scrollHeight);
                         $("#chat_area").html(result);
                         }*/

                        load_in_process = false;
                    });
            }
        }
        ;

        function is_online() {
            $.post("<?=URL::site('ajax/online')?>", {
                    act: "online",
                    last: last_message_id,
                    rand: (new Date()).getTime(),
                    user_nick: "<?=$nick?>"
                },
                function (res) {
                    if (res == 0) {
                        $('#admin_status').removeClass('Online');
                        $('#admin_status').addClass('Offline');
                        $('#admin_status').text('Администратор оффлайн');
                    } else {
                        $('#admin_status').removeClass('Offline');
                        $('#admin_status').addClass('Online');
                        $('#admin_status').text('Администратор онлайн');
                    }
                });
        }
    </script>
</div>