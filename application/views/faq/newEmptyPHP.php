<h3>Вопросы и ответы</h3>

<div class="block effect6">
    <div>
        <? foreach ($faq_cats as $faq_cat):?>
            <div><?=$faq_cat['id']?>. <?=$faq_cat['name']?></div>
            <div><? foreach ($faq[$faq_cat['id']] as $faq_uncat_num => $faq_uncat):?>
            <div><?=$faq_cat['id']?>.<?=$faq_uncat_num?>
                <a href="#<?=$faq_cat['id']?>.<?=$faq_uncat_num?>"><?=$faq_uncat['question']?></a>
            </div>
                <? endforeach;?>
            </div>
        <? endforeach;?>
    </div>

<div>Если Вы не нашли ответ на Ваш вопрос, воспользуйтесь <a href="<?=Url::site('contacts')?>">контактами</a></div>

<div>
    <? foreach($faq as $faq_cat_id => $faq_cat):?>
    <div>
        <? foreach($faq_cat as $faq_uncat_id => $faq_uncat):?>
            <div id="<?=$faq_cat_id?>.<?=$faq_uncat_id?>">
                <div><?=$faq_cat_id?>.<?=$faq_uncat_id?> <?=$faq_uncat['question']?></div>
                <div><?=$faq_uncat['answer']?></div>
            </div>
        <? endforeach;?>
    <? endforeach;?>
    </div>
</div>
<div class="clear"></div>
</div>
