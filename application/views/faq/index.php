<h3>Вопросы и ответы</h3>

<div class="block effect6">
    <div class="text2">
        <? foreach ($faq_cats as $faq_cat):?>
            <div>
                <h4 class="red"><?=$faq_cat['id']?>. <?=$faq_cat['name']?></h4>
                <? foreach ($faq[$faq_cat['id']] as $faq_uncat_num => $faq_uncat):?>
                <div>
                    <b><?=$faq_cat['id']?>.<?=$faq_uncat_num?></b> <a href="#<?=$faq_cat['id']?>.<?=$faq_uncat_num?>"><?=$faq_uncat['question']?></a>
                </div>
                <? endforeach;?>
            </div>
        <? endforeach;?>
        <br>
        <div class="block effect1 text1">Если Вы не нашли ответ на Ваш вопрос, воспользуйтесь <a href="<?=Url::site('contacts')?>">контактами</a></div>
        <br>
        <? foreach($faq as $faq_cat_id => $faq_cat):?>
            <? foreach($faq_cat as $faq_uncat_id => $faq_uncat):?>
            <div id="<?=$faq_cat_id?>.<?=$faq_uncat_id?>">
                <h4 class="blue"><?=$faq_cat_id?>.<?=$faq_uncat_id?> <?=$faq_uncat['question']?></h4>
                <div><?=$faq_uncat['answer']?></div>
            </div>
            <? endforeach;?>
        <? endforeach;?>
    </div>
</div>
