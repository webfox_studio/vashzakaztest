<?= View::factory('admin/onlinecalculator/left') ?>

<div class="w80p fr">
    <h2>Варианты доставки в <?=$resipient->name?></h2>
    <div>
        <table >
            <tr>
                <td>id</td>
                <td>Название</td>
                <td>Доставка USPS</td>
                <td>Доставка <br />Vaszakaz Economy</td>
                <td>Доставка <br />морем</td>
                <td>Стоимость, включая услуги VZ</td>

                <td>Сроки доставки</td>

                <td>Трекинг номер</td>
                <td>Страховка</td>
                <td>Таможенная очистка</td>
                <td>Вариант получения</td>
                <td>Сортировка</td>
<!--                <td>Ширина/in</td>-->
<!--                <td>Высота/in</td>-->
<!--                <td>Длина/in</td>-->
                <td></td>
                <td width="20"></td>

            </tr>
            <?php foreach($options as $k => $v): ?>
                <tr>
                    <td><?=$v->id?></td>
                    <td><input type="text" class="ajax-option" data-column="name" data-id="<?=$v->id?>" name="" value="<?=$v->name?>" /></td>
                    <td><input type="checkbox" class="ajax-option" data-column="sort" name="sort" data-id="<?=$v->id?>" <?=($v->sort == '-1') ? 'checked' : '';?> value="<?=($v->sort == '-1') ? '0' : '-1';?>" /> </td>
                    <td><input type="checkbox" class="ajax-option" data-column="newDelivery" name="newDelivery" data-id="<?=$v->id?>" <?=($v->newDelivery == '1') ? 'checked' : '';?> value="<?=($v->newDelivery == '1') ? '0' : '1';?>" /></td>
                    <td><input type="checkbox" class="ajax-option" data-column="newDelivery" name="newDelivery" data-id="<?=$v->id?>" <?=($v->newDelivery == '2') ? 'checked' : '';?> value="<?=($v->newDelivery == '2') ? '0' : '2';?>" /></td>
                    <td><a href="/admin/onlinecalculator/price/<?=$v->id?>">Стоимость</a></td>

                    <td><input type="text" class="ajax-option" data-column="delivery_period" data-id="<?=$v->id?>" name="" value="<?=$v->delivery_period?>" /></td>
                    <td><input type="text" class="ajax-option" data-column="tracing" data-id="<?=$v->id?>" name="" value="<?=$v->tracing?>" /></td>
                    <td><input type="text" class="ajax-option" data-column="insurance" data-id="<?=$v->id?>" name="" value="<?=$v->insurance?>" /></td>
                    <td><input type="text" class="ajax-option" data-column="customsclearance" data-id="<?=$v->id?>" name="" value="<?=$v->customsclearance?>" /></td>
                    <td><input type="text" class="ajax-option" data-column="variant_reception" data-id="<?=$v->id?>" name="" value="<?=$v->variant_reception?>" /></td>
                    <td><input type="text" class="ajax-option" data-column="sort" data-id="<?=$v->id?>" name="" value="<?=$v->sort?>" /></td>
<!--                    <td><input type="text" class="ajax-option" data-column="width" data-id="--><?//=$v->id?><!--" name="" value="--><?////=$v->width?><!--" /></td>-->
<!--                    <td><input type="text" class="ajax-option" data-column="height" data-id="--><?//=$v->id?><!--" name="" value="--><?////=$v->height?><!--" /></td>-->
<!--                    <td><input type="text" class="ajax-option" data-column="length" data-id="--><?//=$v->id?><!--" name="" value="--><?////=$v->length?><!--" /></td>-->

                    <td><a  onclick="javascript :return (confirm('Удалить?'))?true:false;" href="/admin/onlinecalculator/optiondel/<?=$v->id?>">X</a></td>
                    <td><div class="pi-ok<?=$v->id?>"></div></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <hr />
    <div>
        <form action="/admin/onlinecalculator/addoption/" method="post">
            <select name="priceId">
                <?php foreach ($temp as $k => $v): ?>
                    <option value="<?=$v->id?>"><?=$v->nameCity?> / <?=$v->nameDelivery?></option>
                <?php endforeach; ?>
            </select>
            <input type="hidden" name="optionId" value="<?=$id?>">
            <input type="submit" name="submit" value="Добавить">
        </form>
    </div>
    <hr />
    <h2>Добавить</h2>

    <form action="/admin/onlinecalculator/options/<?=$id?>" method="post">
        <table>
            <tr>
                <td>Название</td>
                <td><input type="text" name="name" /></td>
            </tr>
            <tr>
                <td>Стоимость, включая услуги VZ</td>
                <td><input type="text" name="price" /></td>
            </tr>
            <tr>
                <td>Сроки доставки</td>
                <td><input type="text" name="delivery_period" /></td>
            </tr>
            <tr>
                <td>Трекинг номер</td>
                <td><input type="text" name="tracing" /></td>
            </tr>
            <tr>
                <td>Страховка</td>
                <td><input type="text" name="insurance" /></td>
            </tr>
            <tr>
                <td>Таможенная очистка</td>
                <td><input type="text" name="customsclearance" /></td>
            </tr>
            <tr>
                <td>Вариант получения</td>
                <td><input type="text" name="variant_reception" /></td>
            </tr>

<!--            <tr>-->
<!--                <td>Ширина</td>-->
<!--                <td><input type="text" name="width" /></td>-->
<!--            </tr>-->
<!---->
<!--            <tr>-->
<!--                <td>Высота</td>-->
<!--                <td><input type="text" name="height" /></td>-->
<!--            </tr>-->
<!---->
<!--            <tr>-->
<!--                <td>Длина</td>-->
<!--                <td><input type="text" name="length" /></td>-->
<!--            </tr>-->

            <tr>
                <td></td>
                <td colspan="2"><input type="submit" name="submit" value="Добавить" /> </td>
            </tr>

        </table>
    </form>
</div>