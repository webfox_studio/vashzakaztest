<?= View::factory('admin/onlinecalculator/left') ?>
<div>
    <form action="/admin/onlinecalculator/addprice/" method="post">
        <select name="priceId">
            <?php foreach ($temp as $k => $v): ?>
                <option value="<?=$v->id?>"><?=$v->nameCity?> / <?=$v->nameDelivery?></option>
            <?php endforeach; ?>
        </select>
        <input type="hidden" name="optionId" value="<?=$id?>">
        <input type="submit" name="submit" value="Обновить">
    </form>
</div>
<div class="w80p fr">
    <h1>Цены для <?=$option->name?></h1>

    <table border="0" width="500" style="text-align: center">
        <tr>
            <td>id</td>
            <td>Вес</td>
            <td>Цена</td>
            <td></td>
            <td width="20"></td>
        </tr>
        <?php foreach($price as $k => $v): ?>
            <tr>
                <td><?=$v->id?></td>

                <td><input type="text" class="ajax-price" data-column="heft" data-id="<?=$v->id?>" name="" value="<?=$v->heft?>" /> lb</td>
                <td><input type="text" class="ajax-price" data-column="price" data-id="<?=$v->id?>" name="" value="<?=$v->price?>" /> $</td>
                <td><a onclick="javascript :return (confirm('Удалить?'))?true:false;" href="/admin/onlinecalculator/pricedel/<?=$v->id?>">X</a></td>
                <td><div class="pi-ok<?=$v->id?>"></div></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <h2>Добавить</h2>
    <form action="/admin/onlinecalculator/price/<?=$option->id?>" method="post">
        <table>
            <tr>
                <td>Вес</td>
                <td><input type="text" name="heft" /></td>
            </tr>
            <tr>
                <td>Цена</td>
                <td><input type="text" name="price" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="hidden" name="option_id" value="<?=$option->id?>" />
                    <input type="submit" name="submitprice" value="Добавить" />
                </td>
            </tr>
        </table>

    </form>
</div>