<?= View::factory('admin/onlinecalculator/left') ?>

<div class="fl text2 effect1" style="margin: 0 0 0 2em;">
    <h2>Страна отправитель</h2>
    <div>
        <table>
            <tr>
                <td>id</td>
                <td>Страна</td>
                <td></td>
            </tr>
            <?php foreach($country as $k => $v): ?>
            <tr>
                <td><?=$v->id?></td>
                <td><?=$v->name?></td>
                <td><a href="/admin/onlinecalculator/senderdel/<?=$v->id?>">X</a></td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <hr />
    <h2>Добавить</h2>

    <form action="/admin/onlinecalculator/sender" method="post">
        <table>
            <tr>
                <td><input type="text" name="name" /></td>
                <td><input class="button" type="submit" name="submit" value="Добавить" /> </td>
            </tr>
        </table>
    </form>
</div>