<table>
    <tr>
        <td>
            <div class="fl"><strong class="red"><?= $message->user->user_data->name ?></strong></div>
            <div class="fr" style="margin: 0 0 0 2em;"><a href="<?= Url::site('admin/messages/client/'.$message->user->id) ?>">к сообщениям</a></div>
            <div class="fr"><a href="#nul" onclick="window.open('<?= Url::site('admin/adminfaq') ?>','','Toolbar=0,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=1,Resizable=0,Width=550,Height=400');">Вопросы и ответы</a></div>
        </td>
    </tr>
    <tr>
        <td>
            <? if($message->important or $message->admin_important):?><small class="red">важное</small><?endif?>&nbsp;
            <small><? if (! $message->readed):?>непрочитано<?else:?>прочитано<?endif;?></small>
        </td>
    </tr>
    <tr>
        <td>
            <div class="fr"><?= $message->time ?></div><br>
            <div class="cell">
                <strong class="blue"><?= $message->title;  ?></strong>
                <? if($message->important == 1 OR $message->admin_important == 1): ?>
                <a href="<?=URL::site('admin/messages/set_noimportant/'.$message->id)?>"><b class="blue">Убрать из важных</b></a>
                <?else:?>
                <a href="<?=URL::site('admin/messages/set_important/'.$message->id)?>"><b class="red">Пометить как важное</b></a><? endif; ?>
                    <a style="float:right" href="<?=URL::site('admin/messages/delete_post/'.$message->id);?>" onclick="return confirm_delete();">Удалить эту переписку</a>
            </div>

            <? foreach($messages as $_message): ?>
                <div>
                    <span class="<?if($_message->user->is_admin()):?>red<?endif?>">
                        <span class="blue"><?= $_message->user->user_data->name; $a[] = $_message->user->user_data->name; ?></span>
                        <span class="red"><?if($_message->user->is_admin()):?>[VashZakaz]<?endif?></span>
                    </span>
                    <a class="fr" href="<?=URL::site('admin/messages/delete_message/'.$_message->id);?>">Удалить сообщение</a>
                </div>
            <div class="fr"><?= $_message->time ?></div>
                <div>
                    <?= $_message->content ?>
                </div>
                            <hr>
            <? endforeach ?>
        </td>
    </tr>
<? if(isset($in) AND $in): ?>
    <tr>
        <td>
            <? if($message->deleted == 1){?><div><h3>Данная переписка удалена пользователем.</h3></div><? } else {?>
            <h2 class="blue">Ответ <?php if(!empty($a)):?> для <?php echo $a['0']; endif;?></h2>
            <?=Block::form('message_answer_mcef', $form)?>
            <?php } ?>
        </td>
    </tr>
<? endif; ?> 
    <?if(isset($pre_message)):?>
    <tr><td><h3>Предварительный просмотр:</h3></td></tr>
    <tr>
        <td>
            <div>
                    <span class="<?if($user->is_admin()):?>red<?endif?>">
                        <?= $user->user_data->name ?>
                        <?if($user->is_admin()):?>[Администратор]<?endif?>
                    </span>
                </div>
            <div class="lightgray"><?= $pre_message['time'] ?></div>
                <div>
                    <?= $pre_message['content'] ?>
                </div>
        </td>
    </tr>
    <?endif?>
</table>
