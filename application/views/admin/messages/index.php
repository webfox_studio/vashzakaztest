<?= View::factory('admin/blocks/search_table') ?>

<table id="search_table" class="bgc2" width="600" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <th width="36">#</th>
        <th>Имя Фамилия</th>
        <th width="54"><small><a href="#" title="Всего сообщений">вс</a> / <a href="#" title="Важных сообщений">важ</a></small></th>
        <th width="16"><small><a href="#" title="Новых сообщений">н</a></small></th>
        <th width="16"><small><span class="mp red" title="важный">в</span></small></th>
    </tr>
    <? foreach ($users as $user): ?>
        <? $new = $user->messages->where('readed', '=', 0)->count_all()?>
        <tr class="bgc1 ac h18 client<?if($new > 0):?> bgc1<?endif?>">
            <td><?= $user->id ?></td>
            <td class="al pl8 pr8<?= $new ? ' bg1' : '' ?>"><a href="<?=Url::site('admin/messages/user/'.$user->id)?>"><?= $user->user_data->name ?></a></td>
            <td><?= ($c = $user->messages->count_all()) ? $c : '-' ?> / <?= ($c = $user->messages->where('important', '=', 1)->count_all()) ? $c : '-' ?></td>
            <td><?= ($new ) ? '<b>'.$new.'</b>' : '-' ?></td>
            <td><? if($user->admin_important): ?><b>!</b><? endif; ?></td>
        </tr>
    <? endforeach; ?>
</table>

<? if (isset($pagination) AND $pagination != ''): ?>
<br />
<table class="bgc2 w100p" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18 f7">
        <th width="32"><?=$pagination?></th>
    </tr>
</table>
<? endif; ?>