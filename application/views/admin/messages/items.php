
<form action="#" method="post">
    <fieldset>
        Фильтр: <input type="text" name="search" value="" id="search_name" /> <span class="loading" style="display: none">Loading...</span>
    </fieldset>
</form>

<table id="search_table" class="bgc2 mt16 mb16" width="700" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <td>
            <?if(!empty($status)):?><b><?=$status?></b><?endif?>
            <?if(isset($user_name)):?>Клиент: <b><?=$user_name?></b><?endif?>
            <?if(isset($user_name)):?>
            <small>
                <?if(empty($in)):?>исходящие<?else:?>входящие<?endif?>,
                перейти к <?if(empty($in)):?>
                    <a href="<?=Url::site('admin/messages/client/'.$user_id.':in')?>">входящим</a>
                <?else:?>
                    <a href="<?=Url::site('admin/messages/client/'.$user_id.':out')?>">исходящим</a>
                <?endif?>, 
                <a href="<?=Url::site('admin/messages/send/'.$user_id)?>">написать сообщение</a>
              
            </small>
            <?endif?>
        </td>
    </tr>
</table>

<table id="search_table" align="center" class="bgc2" width="700" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <th width="32">#</th>
        <th>Сообщение</th>
        <th width="16"><small><span class="mp red" title="важный">в</span></small></th>
        <th width="16">Удалить</th>
    </tr>
    <?if(! empty($messages) AND count($messages) > 0):?>
    <? foreach ($messages as $message): ?>
        <tr class="bgc1 ac h18 client hover" >
            <td onclick="document.location.href = '<?=Url::site('admin/messages/item/'.$message->id)?>'"><?= $message->id ?></td>
            <td class="p8" onclick="document.location.href = '<?=Url::site('admin/messages/item/'.$message->id)?>'">
                <div>
                    
                    <strong><?=$message->title?></strong>
                </div>
                <div class="al f6 fl mt4">
                    <? if(! $message->readed2):?><span style="color: green">непрочитано</span><? endif; ?>
                    <? if ($message->important  or $message->admin_important): ?><span style="color: red">важное</span><? endif; ?>
                    <? if($message->user_id == $admin_id OR $message->user_id == 0): ?>исходящее<? else: ?>входящее<? endif; ?>
                </div>
                <div class="ar f6 fr mt4">
                    <?=$message->time?>
                </div>
            </td>
            <td><? if($message->admin_important): ?><a href="<?=URL::site('admin/messages/set_noimportant/'.$message->id)?>"><b>!</b></a><?else:?><a href="<?=URL::site('admin/messages/set_important/'.$message->id)?>"><b>- -</b></a><? endif; ?></td>
            <td><a style="float:right" href="<?=URL::site('admin/messages/delete_post/'.$message->id);?>" onclick="return confirm_delete();"><img border="0" src="<?=Url::site('images')?>/del.gif"></a></td>
        </tr>
    <? endforeach; ?>
    <?else:?>
        <tr>
            <td class="bgc1 ac h20" colspan="3">Сообщений нет</td>
        </tr>
    <?endif;?>
</table>

<? if(isset($pagination)): ?>
<?= Block::pagination($pagination) ?>
<? endif ?>
