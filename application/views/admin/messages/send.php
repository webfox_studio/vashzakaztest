<table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h20 f9">
        <th valign="top">Новое сообщение</th>
    </tr>
    <tr class="bgc1">
        <td class="p4">Кому: <?=$user_name?></td>
    </tr>
    <tr>
        <td class="bgc1 p8" valign="center">
            <?=Block::form('message_send_mcef', $form)?>
        </td>
    </tr>
     <?if(isset($pre_message)):?>
    <tr class="bgc1">
        <td class="p8">
            <h3>Предварительный просмотр:</h3>
        </td>
    </tr>
     <tr class="bgc1">
        <td class="p8">
            <div class="bg1 brd3 mb16 pl4 pr4"><strong><?= $pre_message['head']?></strong></div>
                <div class="fl f6 c2">
                    <span class="<?if($user->is_admin()):?>red<?endif?>">
                        <?= $user->user_data->name ?>
                        <?if($user->is_admin()):?>[Администратор]<?endif?>
                    </span>
                </div>
                <div class="ar f6 c2"><?= $pre_message['time'] ?></div>
                <div class="bg2 brd3 mt4 mb16 p4">
                    <?= $pre_message['content'] ?>
                </div>
        </td>
    </tr>
    <?endif?>
</table>
