<form action="#" method="post">
    <fieldset>
        Фильтр: <input type="text" name="search" value="" id="search_name" /> <span class="loading" style="display: none">Loading...</span>
    </fieldset>
</form>

<table id="search_table" class="bgc2 mt16 mb16" width="700" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <td>
            <b>Предложения</b>
        </td>
    </tr>
</table>

<table id="search_table" align="center" class="bgc2" width="700" cellspacing="1" cellpadding="1">
    <?if(! empty($offers) AND count($offers) > 0):?>
    <? foreach ($offers as $offer): ?>
        <tr class="bgc1 ac h18 client hover<?if(! $offer->readed):?> bg2<?endif?>" onclick="document.location.href = '<?=Url::site('admin/offers/item/'.$offer->id)?>'">
            <td><?= $offer->id ?></td>
            <td class="p8">
                <div>
                    <strong><?=$offer->title?></strong>
                </div>
                <div class="ar f6 fl mt4"><?= $offer->user_id ? $offer->user->user_data->name : '[без имени]'?></div>
                <div class="al f6 fl mt4">
                    <? if(! $offer->readed):?><span class="ml8" style="color: green">непрочитано</span><? endif; ?>
                </div>
                <div class="ar f6 fr mt4"><?=$offer->time?></div>
            </td>
        </tr>
    <? endforeach; ?>
    <?else:?>
        <tr>
            <td class="bgc1 ac h20" colspan="3">Сообщений нет</td>
        </tr>
    <?endif;?>
</table>

<? if(isset($pagination)): ?>
<?= Block::pagination($pagination) ?>
<? endif ?>