<table class="bgc2" width="800" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 h18 f7">
        <td class="pl4 pr4">
            <div class="fl">Предложение от <strong><?= $offer->user_id ? $offer->user->user_data->name : '[без имени]' ?></strong></div>
            <div class="fr"><a href="<?= Url::site('admin/offers') ?>">к предложениям</a></div>
        </td>
    </tr>
    <tr class="bgc1">
        <td class="p8">
            <div class="ar f6 mt4 c2"><?= $offer->time ?></div>
            <div class="bg1 brd3 mb16 pl4 pr4"><strong><?= $offer->title ?></strong></div>
                <div class="bg2 brd3 mt4 mb8 p4">
                    <?= $offer->message ?>
                </div>
            
        </td>
    </tr>
</table>