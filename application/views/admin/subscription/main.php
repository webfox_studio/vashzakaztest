<div><?php echo Controller_Admin_Abstract::statistics_subscription(); ?></div>
<div><?php echo Controller_Admin_Abstract::statistics_news(); ?></div>
<div>
    <select name="/subscription?cat=" onchange="document.location=this.options[this.selectedIndex].value">
        <option value="/admin/subscription">Все категории</option>
        <?php
    foreach ($subscription_cats as $value) {
        if(isset($_GET['cat']) && (int)$_GET['cat'] == $value['id']){
            $selected = 'selected';
        }
        else {
            $selected = '';
        }
        echo '<option '.$selected.' value="/admin/subscription?cat='.$value['id'].'" > '.$value['name'].'</option>';
    }
    ?>
    </select>
</div>
<div>
	<?php $marker = ''; foreach($option as $k => $v): ?>
		<?php
		
			if($v['email'] != $marker){
				echo '<div style="margin-top: 10px; margin-bottom: 5px"><a target="_blank" href="/admin/client/'.$v['userId'].'"><b style="color: black">'.$v['userId'].' - '.$v['username'].' ('.$v['email'].')</b></a></div>';
			}
			$h =($v['hidden']) ? ' <span style="color: red; font-weight: bold">[скрытый]</span>' : '';
			echo '<div style="margin-left: 30px">'.$v['name'].$h.'</div>';
			$marker = $v['email'];
		?>
	<?php endforeach; ?>
</div>
