<?= View::factory('admin/blocks/search_table') ?>

<table id="search_table" class="bgc2" width="600" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <th width="36">#</th>
        <th>Название файла</th>
        <th width="16"><small><span class="mp red" title="важный">в</span></small></th>
        <th>Ссылка на скачивание</th>
    </tr>
    <? foreach ($files as $file): ?>
        <tr class="bgc1 ac h18 hover client<?if($file->readed_admin == FALSE):?> b bg1<?endif;?>">
            <td><?= $file->id ?></td>
            <td class="al pl8 pr8"><?= $file->name ?> (<?=$file->user->user_data->name?>)<a onclick="window.location='<?=Url::site('admin/files/download'.$file->id)?>'" href="javascript:void(0)">Скачать</a></td>
            <td><? if($file->important): ?><b>!</b><? endif; ?></td>
            <td><a onclick="window.location='<?=Url::site('admin/files/download'.$file->id)?>'" href="javascript:void(0)">Скачать</a></td>
        </tr>
    <? endforeach; ?>
</table>

<? if ($pagination != ''): ?>
<br />
<table class="bgc2 w100p" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18 f7">
        <th width="32"><?=$pagination?></th>
    </tr>
</table>
<? endif; ?>