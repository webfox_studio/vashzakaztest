<fieldset>
    <form action="" method="post">
    <div class="fl">
            Поиск по id или по названию: <input type="text" name="search_by_name" value="<?=$search?>" id="search_order" class="admin_search"/>
            <span class="loading" style="display: none">Загрузка...</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

    </div>
    </form>
</fieldset>
<?if(! empty($client_name)):?>
<table class="bgc2 mt16 mb16" width="900" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h20 f9">
        <th>Загрузить файл для клиента <?=$client_name?></th>
    </tr>
</table>
<? if (isset($error)): ?>
    <div align="center" class="mb8" style="color: red"><?=$error?></div>
<? endif; ?>
    <form method="POST" action="" enctype="multipart/form-data">
        <table class="w80p mt16" cellspacing="1" cellpadding="1" align="center">
            <tr class="ac ">

                <td>Имя файла: <input type="file"  name="file_name" value="">
                    <input class="b  b_1" style="width: 100px" type="submit" name="submit" value="Загрузить файл">
                </td>
        </tr><tr class="ac ">

                <td><em class="red">Размер загружаемого файла не должен превышать 3-х мегабайт.</em>
                </td>
        </tr>
    </table>
</form>
<?endif?>
<table id="search_table" class="bgc2 mt16 mb16" width="900" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <td>
            <?if(! empty($client_name)):?>Клиент: <b><?=$client_name?></b><?endif?>
            <?if(! empty($status)):?><b><?=$status?></b><?endif?>
        </td>
    </tr>
</table>

<table id="search_table" class="bgc2" width="900" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <th width="36">Заказ №</th>
        <th>№ Клиента, имя клиента, Название файла</th>
        <th width="16"><small><span class="mp red" title="важный">в</span></small></th>
        <th>Ссылка на скачаивание</th>
        <th>Удалить</th>
    </tr>
    <? foreach ($files as $file): ?>
        <tr class="bgc1 ac h18 hover client<?if($file->readed_admin == FALSE ):?> bg1<?endif;?>" >
            <td><?= $file->id ?></td>
            <td class="al pl8 pr8">
                <div class="fl">#<?=$file->user->id?> <a href="<?=URL::site('admin/files/client/'.$file->user->id)?>"><?=$file->user->user_data->name?></a> [<?= $file->name ?>]</div>
                <small class="fr">
                    <?if($file->important):?><span style="color: red">важный</span><?endif?>
                    <?if(! $file->readed_admin):?><span style="color: blue">непросмотрен</span><?endif?>
                    <?if($file->incoming):?><span>От администратора</span><?endif?>
                </small>
            </td>
            <td><? if($file->important): ?><b><a title="Сделать обычным" href="<?=Url::site('admin/files/set_notimportant/'.$file->id)?>">!!!</a></b><?else:?><a title="Сделать важным" href="<?=Url::site('admin/files/set_important/'.$file->id)?>">---</a><? endif; ?></td>
            <td><a onclick="window.location='<?=Url::site('admin/files/download/'.$file->id)?>'" href="javascript:void(0)">Скачать</a></td>
            <td><a onclick="window.location='<?=Url::site('admin/files/delete/'.$file->id)?>'" href="javascript:void(0)">удалить</a></td>
        </tr>
    <? endforeach; ?>
</table>

<? if(isset($pagination)): ?>
<?= Block::pagination($pagination) ?>
<? endif ?>