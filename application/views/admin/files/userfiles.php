<table class="bgc2 w100p mt16" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h20 f9">
        <th>Загрузка файла</th>
    </tr>
</table>
<form method="POST" action="" enctype="multipart/form-data">
    <table class="w80p mt16" cellspacing="1" cellpadding="1" align="center">
        <tr class="ac ">

            <td>Имя файла: <input type="file" name="file_name" value="">
                <input class="b  b_1" style="width: 100px" type="submit" name="submit" value="Загрузить файл">
            </td>
        </tr>
    </table>
</form>
<table class="bgc2 w100p mt16" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h20 f9">
        <th>Мои файлы</th>
    </tr>
</table>
<br/>
<? foreach ($files as $file): ?>
    <table align="center" class="w80p hover" cellspacing="1" cellpadding="1">
        <tr class="bg1">
            <td class="pl4 pr4 f7">
                <div class="al b fl"><?= $file->name ?></div>
                <div class="fl pl8">
                    <? if (!$file->readed): ?><span class="green">Новый</span><? endif; ?>
                </div>
                <? if ($file->incoming == 1): ?>
                    <div class="fr">Файл от администратора</div><? endif ?>
            </td>
        </tr>
        <tr class="bgc1 f6 h16 c1">
            <td class="p4" colspan="2">
                <div class="al f6 fl">
                    <a href="javascript:void(0)"
                       onclick="window.location='<?= Url::site('files/download/' . $file->id) ?>'">Скачать</a>
                </div>
                <div class="ar fr"><?= date('Y-m-d H:i:s', $file->date_time) ?></div>
            </td>
        </tr>
    </table>
    <br/>
<? endforeach; ?>
