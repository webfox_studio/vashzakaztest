<div><?= View::factory('admin/blocks/search_table') ?></div>

<table id="search_table" align="center" class="bgc2" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <th width="60">#</th>
        <th width="300">Имя Фамилия</th>
        <th width="40"><small><a href="#" title="Всего заказов">вс</a></small></th>
        <th width="40"><small><a href="#" title="Важных заказов">важ</a></small></th>
        <th width="40"><small><a href="#" title="Напрочитаных заказов">непр</a></small></th>

    </tr>
    <? foreach ($clients as $client): ?>
        <tr class="bgc1 ac h18 client hover<?if( $client->count_unread):?> bg1<?endif?>" onclick="window.location='<?=Url::site('admin/files/client/'.$client->id)?>'">
            <td><?= $client->id ?></td>
            <td ><?= $client->name ?></td>
            <td><?= $client->count_total ?></td>
            <td class="<?if($client->count_important):?> b<?endif?>"><?= $client->count_important ?></td>
            <td class="<?if($client->count_unread):?> b<?endif?>"><?= $client->count_unread ?></td>
        </tr>
    <? endforeach; ?>
</table>

<? if(isset($pagination)): ?>
<?= Block::pagination($pagination) ?>
<? endif ?>