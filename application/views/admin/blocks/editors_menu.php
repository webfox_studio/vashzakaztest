<ul id="editors_menu">
<?foreach($menu as $_menu_item):?>
    <li class="<?if(Request::detect_uri() == $_menu_item['url']):?>active<?endif?>">
        <a href="<?=$_menu_item['url']?>"><?=$_menu_item['title']?>
            <?if(isset($_menu_item['order'])):?> <sup title="Вес пункта"><?=$_menu_item['order']?></sup><?endif?>
        </a>
    </li>
<?endforeach?>
<br>
    <li>Добавить категорию:<br />
        <form method="post" action="<?=URL::site('admin/editors/add_page/')?>">
            <input name="new_cat" type="text"/> <input class="button" type="submit" value="Add"/>
        </form>
    </li>
</ul>