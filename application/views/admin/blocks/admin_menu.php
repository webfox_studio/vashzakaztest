<? if(Auth::instance()->logged_in('admin')) { ?>
<br>
<div class="topmenu p4">
    <ul class="nav">
        <li class="btn"><a href="<?= Url::site('admin/editors') ?>">Редакторы</a></li>
        <li class="btn"><a href="<?= Url::site('admin/clients') ?>" class="c2">Клиенты<?if(isset($changes) AND $changes > 0):?> (<?=$changes?>)<?endif?></a>
            <div class="sub">
            <ul>
                <li class="btn"><a href="<?= Url::site('admin/clients') ?>">Все</a></li>
                <li class="btn"><a href="<?= Url::site('admin/clients/new') ?>">Новые за месяц</a></li>
                <li class="btn"><a href="<?= Url::site('admin/clients/changes') ?>">Изменения данных<?if(!empty($changes)):?>(<?=$changes?>)<?endif?></a></li>
            </ul>
            </div>
        </li>
        <li class="btn"><a href="<?= Url::site('admin/messages') ?>">Сообщения<? if (isset($messages) AND $messages > 0): ?> (<?= $messages ?>)<? endif; ?></a>
            <div class="sub">
            <ul>
                <li class="btn"><a href="<?= Url::site('admin/messages/all') ?>">Все</a></li>
                <li class="btn"><a href="<?= Url::site('admin/messages/clients') ?>">По клиентам</a></li>
                <li class="btn"><a href="<?= Url::site('admin/messages/unreaded') ?>">Непрочитанные<? if (isset($messages) AND $messages > 0): ?> (<?= $messages ?>)<? endif; ?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/messages/readed') ?>">Прочитанные</a></li>
                <li class="btn"><a href="<?= Url::site('admin/messages/important') ?>">Важные<?if($msg_important):?>(<?=$msg_important?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/messages/deleted') ?>">Удаленные</a></li>
                <li class="btn"><a href="<?= Url::site('admin/messages/send') ?>">Новое сообщение</a></li>
            </ul>
            </div>
        </li>
        <li class="btn"><a href="<?= Url::site('admin/orders') ?>">Заказы<? if (isset($orders_unreaded) AND $orders_unreaded > 0): ?> (<?= $orders_unreaded ?>)<? endif; ?></a>
            <div class="sub">
            <ul>
                <li class="btn"><a href="<?=Url::site('admin/orders/clients')?>">По клиентам</a></li>
                <li class="btn"><a href="<?= Url::site('admin/orders/all') ?>">Все</a></li>
                <li class="btn"><a href="<?= Url::site('admin/orders/new') ?>">Новые<?if($orders_new):?>(<?=$orders_new?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/orders/unreaded') ?>">Непрочитанные<?if($orders_unreaded):?>(<?=$orders_unreaded?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/orders/readed') ?>">Прочитаные</a></li>
                <li class="btn"><a href="<?= Url::site('admin/orders/important') ?>">Важные<?if($orders_important):?>(<?=$orders_important?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/orders/opened') ?>">Текущие</a></li>
                <li class="btn"><a href="<?= Url::site('admin/orders/closed') ?>">Закрытые</a></li>
                <li class="btn"><a href="<?= Url::site('admin/orders/deleted') ?>">Удаленные</a></li>
            </ul>
            </div>
        </li>

        <li class="btn"><a href="<?= Url::site('admin/parcels') ?>">Посылки<? if (isset($parcels_unreaded) AND $parcels_unreaded > 0): ?> (<?= $parcels_unreaded ?>)<? endif; ?></a>
            <div class="sub">
            <ul>
                <li class="btn"><a href="<?=Url::site('admin/parcels/clients')?>">По клиентам</a></li>
                <li class="btn"><a href="<?= Url::site('admin/parcels/all') ?>">Все</a></li>
                <li class="btn"><a href="<?= Url::site('admin/parcels/new') ?>">Новые<?if($parcels_new):?>(<?=$parcels_new?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/parcels/unreaded') ?>">Непрочитанные<?if($parcels_unreaded):?>(<?=$parcels_unreaded?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/parcels/readed') ?>">Прочитаные</a></li>
                <li class="btn"><a href="<?= Url::site('admin/parcels/important') ?>">Важные<?if($parcels_important):?>(<?=$parcels_important?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/parcels/opened') ?>">Текущие</a></li>
                <li class="btn"><a href="<?= Url::site('admin/parcels/closed') ?>">Закрытые</a></li>
                <li class="btn"><a href="<?= Url::site('admin/parcels/deleted') ?>">Удаленные</a></li>
            </ul>
            </div>
        </li>
        <li class="btn"><a href="<?= Url::site('admin/files/all') ?>">Файлы<? if (isset($files_unreaded) AND $files_unreaded > 0): ?> (<?= $files_unreaded ?>)<? endif; ?></a>
            <div class="sub">
            <ul>
                <li class="btn"><a href="<?=Url::site('admin/files/clients')?>">По клиентам</a></li>
                <li class="btn"><a href="<?= Url::site('admin/files/all') ?>">Все</a></li>
                <li class="btn"><a href="<?= Url::site('admin/files/unreaded') ?>">Непросмотренные<?if($files_unreaded):?>(<?=$files_unreaded?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/files/readed') ?>">Просмотренные</a></li>
                <li class="btn"><a href="<?= Url::site('admin/files/important') ?>">Важные<?if($files_important):?>(<?=$files_important?>)<?endif?></a></li>
            </ul>
            </div>
        </li>

        <li class="btn"><a href="<?= Url::site('admin/payments') ?>">Платежи<? if (isset($payments_unreaded) AND $payments_unreaded > 0): ?> (<?= $payments_unreaded ?>)<? endif; ?></a>
             <div class="sub">
            <ul>
                <li class="btn"><a href="<?=Url::site('admin/payments/clients')?>">По клиентам</a></li>
                <li class="btn"><a href="<?= Url::site('admin/payments/all') ?>">Все</a></li>
                <li class="btn"><a href="<?= Url::site('admin/payments/new') ?>">Новые<?if($orders_new):?>(<?=$payments_new?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/payments/unreaded') ?>">Непрочитанные<?if($payments_unreaded):?>(<?=$payments_unreaded?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/payments/readed') ?>">Прочитаные</a></li>
                <li class="btn"><a href="<?= Url::site('admin/payments/important') ?>">Важные<?if($payments_important):?>(<?=$payments_important?>)<?endif?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/payments/opened') ?>">Текущие</a></li>
                <li class="btn"><a href="<?= Url::site('admin/payments/closed') ?>">Закрытые</a></li>
                <li class="btn"><a href="<?= Url::site('admin/payments/deleted') ?>">Удаленные</a></li>
            </ul>
            </div>
        </li>
        
        <li class="btn"><a href="<?= Url::site('admin/adminfiles') ?>">Админ файлы<? if (isset($adminfiles_important) AND $adminfiles_important > 0): ?> (<?= $adminfiles_important ?>)<? endif; ?></a>
             <div class="sub">
            <ul>
                <li class="btn"><a href="<?=Url::site('admin/adminfiles')?>">Все<? if (isset($adminfiles_all) AND $adminfiles_all > 0): ?> (<?= $adminfiles_all ?>)<? endif; ?></a></li>
                <li class="btn"><a href="<?= Url::site('admin/adminfiles/important') ?>">Важные<? if (isset($adminfiles_important) AND $adminfiles_important > 0): ?> (<?= $adminfiles_important ?>)<? endif; ?></a></li>
                
            </ul>
            </div>
        </li>
        
        <li class="btn"><a href="<?= Url::site('admin/offers') ?>">Предложения<? if (isset($offers) AND $offers > 0): ?> (<?= $offers ?>)<? endif; ?></a>
            <div class="sub">
            <ul>
                <li class="btn"><a href="<?= Url::site('admin/offers') ?>">Предложения</a></li>
            </ul>
            </div>
        </li>

        <li class="btn"><a href="#">Сервис</a>
            <div class="sub">
            <ul>
                <li class="btn"><a href="<?=Url::site('admin/set/registration')?>">Управление регистрацией</a></li>
                <li class="btn"><a href="<?=Url::site('admin/set/admins')?>">Администрация</a></li>
                <li class="btn"><a href="<?=Url::site('admin/set/report')?>">Xls-отчеты</a></li>
            </ul>
            </div>
        </li>
        <li class="btn"><a href="#">Расчет<?php if($requestform_new): ?> (<?=$requestform_new; ?>) <?php endif; ?></a>
            <div class="sub">
            <ul>
                <li class="btn"><a href="<?=Url::site('admin/requestform')?>">Все<?php if($requestform_all): ?> (<?=$requestform_all; ?>) <?php endif; ?></a></li>
                <li class="btn"><a href="<?=Url::site('admin/requestform/new')?>">Непрочитанные<?php if($requestform_new): ?> (<?=$requestform_new; ?>) <?php endif; ?></a></li>
                <li class="btn"><a href="<?=Url::site('admin/requestform/readed')?>">Прочитанные<?php if($requestform_old): ?> (<?=$requestform_old; ?>) <?php endif; ?></a></li>
                <li class="btn"><a href="<?=Url::site('admin/requestform/report')?>">Важные<?php if($requestform_important): ?> (<?=$requestform_important; ?>) <?php endif; ?></a></li>
                <li class="btn"><a href="<?=Url::site('admin/requestform/arhiv')?>">Удаленные<?php if($requestform_deleted): ?> (<?=$requestform_deleted; ?>) <?php endif; ?></a></li>
            </ul>
            </div>
        </li>
        <li class="btn"><a href="<?=Url::site('admin/allinclusive')?>">Товары</a>
            <div class="sub">
            <ul>
                <li class="btn"><a href="<?=Url::site('admin/allinclusive/category/add')?>">Добавить категорию</li>
                <li class="btn"><a href="<?=Url::site('admin/allinclusive/add')?>">Добавить товар</li>
                
            </ul>
            </div>
        </li>
        <li class="btn"><a href="<?=Url::site('admin/onlinecalculator')?>">Калькулятор</a>
<!--            <div class="sub">-->
<!--                <ul>-->
<!--                    <li class="btn"><a href="--><?//=Url::site('admin/allinclusive/category/add')?><!--">Добавить категорию</li>-->
<!--                    <li class="btn"><a href="--><?//=Url::site('admin/allinclusive/add')?><!--">Добавить товар</li>-->
<!---->
<!--                </ul>-->
<!--            </div>-->
        </li>
		<li class="btn"><a href="<?=Url::site('admin/seo')?>">SEO</a></li>
		<li class="btn"><a href="<?=Url::site('admin/subscription')?>">Подписка</a></li>
		<li class="btn"><a href="<?=Url::site('admin/discount')?>">Discount</a></li>
		<li class="btn"><a href="<?=Url::site('admin/tracking')?>">Tracking-номера</a></li>
    </ul>
</div>
<?} elseif(Auth::instance()->logged_in('redactor') && !Auth::instance()->logged_in('admin')) {?>
<br>
<div class="topmenu p4">
    <ul class="nav">
        <li class="btn"><a href="<?=Url::site('admin/editors')?>">Редакторы</a></li>
        <li class="btn"><a href="<?=Url::site('admin/onlinecalculator')?>">Калькулятор</a></li>
        <li class="btn"><a href="<?=Url::site('admin/seo')?>">SEO</a></li>
        <li class="btn"><a href="<?=Url::site('admin/subscription')?>">Подписка</a></li>
        <li class="btn"><a href="<?=Url::site('admin/discount')?>">Discount</a></li>
    </ul>
</div>
<?} else { }?>
<br><br>