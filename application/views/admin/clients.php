<?= View::factory('admin/blocks/search_table') ?>

<table id="search_table">
    <tr>
        <th width="32">#</th>
        <th width="160">Имя Фамилия</th>
        <th><a title="Написать сообщение" href="#">M</a></th>
        <th>$</th>
        <th>XLS</th>
        <th>Страна</th>
        <th>Город</th>
        <th>E-mail</th>
        <th>ICQ</th>
        <th>Skype</th>
        <!--<th>Mail.Ru</th>-->
        <th>Другие</th>
        <th>Перешел на сайт с</th>
        <th>Дата регистрации</th>

    </tr>

    <? foreach ($clients as $client): ?>
    <tr class="client hover" >
            <td><?= $client->id ?></td>
            <td onclick="window.location='<?=Url::site('admin/client/'.$client->id)?>'"><?= $client->name ?></td>
            <td><a title="Написать сообщение" href="<?=Url::site('admin/messages/send/'.$client->id)?>">M</a></td>
            <td><?= isset($client->value) ? $client->value : 0 ?></td>
            <td><? if (isset($xls[$client->id])): ?><a href="<?= Url::site('user/report/' . $client->id) ?>">Скачать</a><? else: ?>-<? endif; ?></td>
            <td><?= $client->country ?></td>
            <td><?= $client->city ?></td>
            <td><?= $client->email ?></td>
            <td><?= $client->icq ? $client->icq : '' ?></td>
            <td><?= $client->skype ?></td>
            <!--<td><?= $client->mail_ru ?></td>-->
            <td><?= $client->another ?></td>
            <td><?= $client->referer ?></td>
        <td><?=$client->registered?></td>
        </tr>
    <? endforeach; ?>
</table>

<? if(isset($pagination)): ?>
<?= Block::pagination($pagination) ?>
<? endif ?>