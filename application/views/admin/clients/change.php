<div class="mt16" align="center" style="font-size: medium; color: #800000;">Клиент: <?=$client->username?></div>
<br />
<script>
    var del_confirm = function(){
        if(confirm('Вы уверены, что хотите удлаить данного пользователя?')){
            return true;
        }
        return false;
    }
</script>
<table class="bgc2 w80p" align="center" cellspacing="1" cellpadding="1">
    <tr class="bgc1">
        <td class="bg1 p4 ar"></td>
        <td class="p4 ar">Старые данные</td>
        <td class="p4 ar">Новые данные</td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Имя:</td>
        <td class="p4"><?= $client->user_data->name ?></td>
        <td class="p4"><?= $client->user_data_changes->name ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Email:</td>
        <td class="p4"><?= $client->email ?></td>
        <td class="p4"><?= $client->user_data_changes->email ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Страна:</td>
        <td class="p4"><?= $client->user_data->country ?></td>
        <td class="p4"><?= $client->user_data_changes->country ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Почтовый индекс:</td>
        <td class="p4"><?= $client->user_data->zip ?></td>
        <td class="p4"><?= $client->user_data_changes->zip ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Область / регион:</td>
        <td class="p4"><?= $client->user_data->region ?></td>
        <td class="p4"><?= $client->user_data_changes->region ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Населенный пункт:</td>
        <td class="p4"><?= $client->user_data->city ?></td>
        <td class="p4"><?= $client->user_data_changes->city ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Остальной адрес:</td>
        <td class="p4"><?= $client->user_data->address ?></td>
        <td class="p4"><?= $client->user_data_changes->address ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Контактный телефон(ы):</td>
        <td class="p4"><?= $client->user_data->phones ?></td>
        <td class="p4"><?= $client->user_data_changes->phones ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">ICQ:</td>
        <td class="p4"><?= $client->user_data->icq ? $client->user_data->icq : '' ?></td>
        <td class="p4"><?= $client->user_data_changes->icq ? $client->user_data_changes->icq : '' ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Skype:</td>
        <td class="p4"><?= $client->user_data->skype ?></td>
        <td class="p4"><?= $client->user_data_changes->skype ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Агент Mail.ru:</td>
        <td class="p4"><?= $client->user_data->mail_ru ?></td>
        <td class="p4"><?= $client->user_data_changes->mail_ru ?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Другой контакт:</td>
        <td class="p4"><?= $client->user_data->another ?></td>
        <td class="p4"><?= $client->user_data_changes->another ?></td>
    </tr>
    <?if($client->user_data_changes->delete == '1'):?>
    <tr class="bgc1 ar">
        <td class="bg1 p4 red" colspan="3">Пользователь отправил запрос на удаление аккаунта. <a href="<?=Url::site('admin/clients/delete/'.$client->id)?>" onClick="return del_confirm()">Удалить пользователя</a></td>
    </tr>
    <?endif?>
    <tr class="bgc1 ar">
        <td class="bg1 p4">Причина изменений:</td>
        <td class="p4" colspan="2"><?=$client->user_data_changes->cause?></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="pr4" colspan="3"><a href="<?=Url::site('admin/clients/approve/'.$client->id)?>">подтвердить</a></td>
    </tr>
    <form action="<?=Url::site('admin/clients/changes_denied/'.$client->id)?>" method="post" id="changes_form">
    <tr class="bgc1 ar">
        <td class="bg1 p4">Причина отказа:</td>
        <td class="p4" colspan="2"><textarea name="changes_denied"></textarea></td>
    </tr>
    <tr class="bgc1 ar">
        <td class="pr4" colspan="3"><a href="javascript:void(0)" onClick="$('#changes_form').submit();">отказать в изменениях</a></td>
    </tr>
    </form>
</table>
