<div class="text effect1">Клиент #<?=$client->id?>: <b class="red"><?=$client->user_data->name?></b>     
<?
if(empty($role))
{
    echo '<a href="'.Url::site('admin/set/set_admin/'.$client->id).'">Назначить администратором</a> || <a href="'.Url::site('admin/set/set_redactor/'.$client->id).'">Назначить редактором</a>';
} elseif ($role[0]['role_id'] == 2) {
    echo '<a href="'.Url::site('admin/set/unset_admin/'.$client->id).'">Убрать из администраторов</a>';
} elseif ($role[0]['role_id'] == 4) {
    echo '<a href="'.Url::site('admin/set/unset_redactor/'.$client->id).'">Убрать из редакторов</a>';
}
?>

</div>

<div class="">
    <h3>Данные клиента</h3>
    <a href="<?=Url::site('admin/clients/delete/'.$client->id)?>" onClick="return del_confirm()">Удалить пользователя</a>
    <br>   
    <h2>Редактирование информации</h2>
    <table class="fl" style="width: 80%;">
    <tr>
        <td><?=Block::form('Changeinfo_User', $form)?></td>
    </tr>   
    <tr>
        <th colspan="2">Дополнительная информация</th>
    </tr>
    <tr>
            <td>Как узнали про нас:</td>
            <td><?=$client->user_data->where?></td>
    </tr>
   <tr>
            <td>Перешел на сайт с:</td>
            <td><?=$client->user_data->referer?></td>
    </tr>
</table>
</div>
<div class="clearfix"></div>
<pre>
   <!-- <?=print_r($client)?>-->
</pre>
<div>
    <h3>Баланс</h3>
    $ <?=$client->user_data->value?>
</div>

<div class="fl">
<div>
    <h3><a href="<?=Url::site('admin/messages/client/'.$client->id)?>">Сообщения</a></h3>
    <?=$client->messages->where('deleted2', '=', 0)->count_all()?>
</div>

<div>
    <h3><a href="<?=Url::site('admin/orders/client/'.$client->id)?>">Заказы</a></h3>
    <?=$client->orders->where('deleted', '=', 0)->count_all()?>
</div>

<div>
    <h3><a href="<?=Url::site('admin/parcels/client/'.$client->id)?>">Посылки</a></h3>
    <?=$client->parcels->where('deleted', '=', 0)->count_all()?>
</div>

<div>
    <h3><a href="<?=Url::site('admin/files/client/'.$client->id)?>">Файлы</a></h3>
    <?=$client->files->count_all()?>
</div>
    <div>
        <h3><a href="<?=Url::site('admin/payments/client/'.$client->id)?>">Переводы</a></h3>
		<?=ORM::factory('payments')->where('user_id','=',$client->id)->count_all()?>
    </div>
    <div>
        <h3>Файл отчета</h3>
		<?=$xlsUrl?>
    </div>
    <br />
</div>
<script>
    var del_confirm = function(){
        if(confirm('Вы уверены, что хотите удлаить данного пользователя?')){
            return true;
        }
        return false;
    }
</script>
<hr>
<div>
    <h2>Новая рассылка</h2>
    <h3>Темы, на которые подписан клиент</h3>
    <div>
        <div id="mailCategoryActive">
        <?php foreach($active_cats as $k =>$v): ?>
            <div title="Удалить" class="deleteMailCategory" data-id="<?=$v['id']?>"><?=$v['name']?> <?=($v['hiddenMailing']) ? '<b>[скрытый]</b>' : '';?></div>
        <?php endforeach; ?>
        </div>
    </div>
    <h3>Все темы</h3>
    <input type="hidden" id="userId" value="<?=$client->id?>">
    <select id="mailCategory" name="cat">
        <option selected>Выбрать категорию</option>
        <?php foreach($cats as $k => $v): ?>
            <option value="<?=$v['id']?>"><?=$v['name']?></option>
        <?php endforeach; ?>
    </select>
    <div>
        <button class="button btn saveMailCategory">Добавить тему в рассылку</button>
    </div>
</div>
<hr>
