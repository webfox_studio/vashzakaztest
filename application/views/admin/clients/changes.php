<?= View::factory('admin/blocks/search_table') ?>

<?if(isset($clients) AND count($clients) > 0):?>
<table id="search_table" class="bgc2" align="center" width="600" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <th width="32">#</th>
        <th width="">Имя Фамилия</th>
        <th width="160">E-mail</th>

    </tr>
    <? foreach ($clients as $client): ?>
        <tr class="bgc1 ac h18 client hover" onclick="window.location = '<?=Url::site('admin/clients/change/'.$client->id)?>'">
            <td><?= $client->user_id ?></td>
            <td><?= $client->name ?></td>
            <td><?= $client->email ?></td>
        </tr>
    <? endforeach; ?>
</table>
<?else:?>
<div class="mt32 f11" align="center" style="color: red">Список пуст</div>
<div class="f6 mt8" align="center">перейти на <a href="<?=Url::site('admin')?>">главную</a></div>
<?endif?>