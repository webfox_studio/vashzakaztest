<div>
    <table>
        <tr>
            <td>ID</td>
            <td>TITLE</td>
            <td>IMG</td>
            <td>DATE</td>
            <td>ACTION</td>
        </tr>
        <?php foreach($content as $key => $value): ?>
            <tr>
                <td><?=$value->id?></td>
                <td><?=$value->title?></td>
                <td><img style="width: 50px" src="/upload/discount/<?=$value->img?>"></td>
                <td><?=$value->addDate?></td>
                <td><a href="/admin/discount/delete/<?=$value->id?>">del</a><br><a href="/admin/discount/edit/<?=$value->id?>">edit</a></td>
            </tr>
        <?php endforeach ?>
    </table>
</div>
<br /><br />
<div>ADD DISCOUNT</div>
<br />
<div>
    <form enctype="multipart/form-data" method="POST" action="<?=Url::site('admin/discount/add')?>">
        TITLE:
        <input type="text" name="title" />
        <br />
        <br />
        DESCRIPTION
        <textarea id="tinyMCE_f" class="w100p" rows="10" name="description"></textarea>
        <br />
        IMAGE
        <input type="file" name="img" />
        <br />

        <div class="mt8">
            <input id="addbtn" class="bg1 b" type="submit" value="Добавить" />
        </div>
    </form>
</div>