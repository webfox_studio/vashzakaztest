<?= View::factory('admin/blocks/log_days_menu') ?>

<table id="search_table" class="bgc2 w70p" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <th width="50">Время</th>
        <th width="500">Событие</th>       
    </tr>
    <? foreach ($day_log as $v): ?>
    <tr class="bgc1 ac h18 client hover" >
        <td>
            <?echo date('H:i:s', $v['dt']);?> </td><td class="fl">
            <?$to = ($v['to'])? ('to '.$v['to']):'';
            if ($v['type'] >3 &&  $v['type'] <=6)
                echo " <b>".$v['who']." сообщение из чата ".$to." : </b>".$v['what'];
            else
                echo " <b>".$v['who']."</b>: ".$v['what'];
            ?>
        </td>
    </tr>
    <? endforeach; ?>
</table>
