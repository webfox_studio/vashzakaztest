<div class="fl mr"><?php print View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#edit_button').click(function() {
			document.location.href = '<?php print Url::site('admin/editors/sub_edit_form').'/'.$subscription['id']; ?>';
		});
		$('#delete_button').click(function() {
			if (confirm('Вы действительно хотите удалить данную рассылку?'))
				document.location.href = '<?php print Url::site('admin/editors/sub_delete').'/'.$subscription['id']; ?>';
		});
		return false;
	});
</script>

<div class="w80p">
	<fieldset>
		<legend><h3><?php print $subscription['title'] ?></h3></legend>
		<?php print $subscription['content'] ?>
	</fieldset><br /><br />
    <input class="button" type="button" value="Изменить" id="edit_button" />
    <input class="button" type="button" value="Удалить" id="delete_button" />
</div>