<div class="fl mr"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<div class="block table fl">
    <div class="row">
        <form method="POST" action="<?=Url::site('admin/editors/cat_add')?>" >
            <div class="cell">Номер раздела: <input name="cat_id" size="2" maxsize="2" value="" /></div>
            <div class="cell">Добавить раздел: <input name="name" max_size="100" value=""> <input id="addbtn" class="button" type="submit" value="Добавить" /></div>
        </form>
    </div>
</div>

    <div class="block table fl">
        <div class="row">
            <div class="cell"><h3>Новый Вопрос и Ответ</h3>
                <form method="POST" action="<?=Url::site('admin/editors/faq_add')?>">
                    <input name="cat" size="2" value="" /><b>.</b><input name="num" size="2" value="" /><b>.</b> <input size="90" name="question" value="" />
                    <textarea id="tinyMCE_f" rows="10" name="answer"></textarea>
                    <div class="fr mt8"><input id="addbtn" class="button" type="submit" value="Добавить" /></div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="block table fl">
        <div class="row">
            <div class="cell">
                <h3>Вопросы и ответы</h3>
                        <? foreach ($faq_cats as $faq_cat):?>
                        <div class="table">
                            <div class="row">
                                <div class="cell">
<?=$faq_cat['id']?>. <?=$faq_cat['name']?><a href="<?=Url::site('admin/editors/cat_edit')?>/<?=$faq_cat['id']?>"><img border="0" src="<?=Url::site('images')?>/edit.png"></a><a href="<?=Url::site('admin/editors/cat_delete')?>/<?=$faq_cat['id']?>"><img border="0" src="<?=Url::site('images')?>/del.gif"></a>
                                </div>
                            </div>
                            <?if(isset($faq[$faq_cat['id']])):?>
                            <? foreach ($faq[$faq_cat['id']] as $faq_uncat_num => $faq_uncat):?>
                            <div class="row">
                                <div class="cell"><b><?=$faq_cat['id']?>.<?=$faq_uncat_num?></b> <a href="#<?=$faq_cat['id']?>.<?=$faq_uncat_num?>"><?=$faq_uncat['question']?></a></div>
                                <div class="cell"><a href="<?=Url::site('admin/editors/faq_delete')?>/<?=$faq_cat['id']?>/<?=$faq_uncat_num?>"><img border="0" src="<?=Url::site('images')?>/del.gif"></a></div>
                            </div>
                            <? endforeach;?>
                            <?endif;?>
                        </div>
                        <? endforeach;?>

                        <div class="block effect1">Если Вы не нашли ответ на Ваш вопрос, воспользуйтесь <a href="<?=Url::site('contacts')?>">контактами</a></div>

                        <? foreach($faq as $faq_cat_id => $faq_cat):?>
                            <? foreach($faq_cat as $faq_uncat_id => $faq_uncat):?>
                        <div id="<?=$faq_cat_id?>.<?=$faq_uncat_id?>" class="table">
                            <div class="row">
                                <div class="cell"><?=$faq_cat_id?>.<?=$faq_uncat_id?> <?=$faq_uncat['question']?></div>
                                <div class="cell"><a href="<?=Url::site('admin/editors/faq_edit')?>/<?=$faq_cat_id?>/<?=$faq_uncat_id?>"><img border="0" src="<?=Url::site('images')?>/edit.png"></a></div>
                            </div>
                            <div class="row">
                                <div class="cell"><?=$faq_uncat['answer']?></div>
                            </div>
                        </div>
                            <? endforeach;?>
                    <? endforeach;?>
                </div>
            </div>
    </div>
    
</div>
