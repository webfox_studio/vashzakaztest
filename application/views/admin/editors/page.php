<div class="fl mr"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>


 <script type="text/javascript">
    function clickdelcat() {
        res = confirm ("Вы действительно хотите удалить Категорию? \n Все находящиеся в ней подразделы будут удалены. \n при необходимости можете перед удалением перенести подразделы в другую категорию");
        if (res)
            location.href="<?=URL::site('admin/editors/delete_page/')?>/<?=$category->name?>";
    }
</script>

<div class="block effect6 fl">
    
    <form method="post" action="<?=URL::site('admin/editors/save_page/')?>/<?=$category->name?>">
        <input type="hidden" name="edit_cat_id" value="<?=$category->id?>"><img src="/images/del.gif" class="cr" onclick="clickdelcat()" />
            Название категории: <input style="width: 30%" name="edit_cat_title" type="text" class="bg1 ac" value="<?=$category->title?>" />&nbsp;
            Ссылка: <input title="Короткое слово на латинице, которое будет в ссылке (например для новостей - news, для тарифов - tariffs) Убедитесь, что это слово уникально для страниц, то есть не должно быть двух страниц одинаковой ссылкой" style="width: 15%"  type="text"  name="edit_cat_chpu"  class="bg1 ac" value="<?=$category->name?>" />&nbsp;
            Вес: <input title="Чем больше вес, тем правее или ниже пункт в меню" style="width: 10%"  type="text" class="bg1 ac" name="edit_cat_weight" value="<?=$category->order?>" /> <input class="button" type="submit" class="bgc1 ac" value="сохранить" />
    </form>
    
    <form method="POST" action="<?=URL::site('admin/editors/save_subpages/')?>/<?=$page?>">
        <?$i=0;?>
            <?foreach($sub_pages as $k => $sub_page):?>
                <div>
                    <input type="hidden" name="item_<?=$sub_page->id?>" value="<?=$sub_page->id?>">
                    <h4 class="blue">Вес пункта:</h4><input type="text" name="weight_<?=$sub_page->id?>" value="<?=$sub_page->order?>" maxlength="2" style="width: 30px;"/>
                    <h4 class="blue">Отображать в родительской категории:</h4><!--					<input type="checkbox" name="display_on_parent_<?=$sub_page->id?>" value="1" checked="<?= $sub_page->display_on_parent ? "checked" : "" ?>" maxlength="2" style="width: 30px;"/><br />--><input type="checkbox" name="display_on_parent_<?=$sub_page->id?>" value="1" maxlength="2" style="width: 30px;"/>
                    <h4 class="blue">Категория:</h4>
                        <select name="cat_<?=$sub_page->id?>">
                            <?foreach ($cats as $k => $cat):?>
                                <option value="<?=$cat->id?>" <?if($cat->id==$sub_page->page_id):?>selected="selected"<?endif?>><?=$cat->title?></option>
                            <?endforeach?>
                        </select>
                    <h4 class="blue">Название пункта меню:</h4>Если оставить пустым, то подраздел будет отображаться только на странице раздела. <input class="button center" type="text" name="title_<?=$sub_page->id?>" value="<?=$sub_page->title?>" />
                    <h4 class="blue">Cсылка:</h4>(одно слово латиницей, уникальное для всего меню) <input type="text" name="link_<?=$sub_page->id?>" value="<?=$sub_page->name?>" />
                    <h4 class="blue">Содержание:</h4>(для удаления подраздела оставьте пустым)<br><br>
                    <textarea id="tinyMCE_on_<?echo $i;$i++;?>" class="w100p" rows="10" name="content_<?=$sub_page->id?>"><?=$sub_page->content?></textarea>
                </div>
        <?endforeach?>
    <div class=""><input class="button" type="submit" value="Сохранить изменения" /></div>
</form>

<h3>Добавить новый подраздел:</h3>
        <form method="POST" action="<?=URL::site('admin/editors/add_subpage/')?>/<?=$page?>">
            <input type="hidden" name="page_id" value="<?=$category->id?>">
            <h4 class="blue">Вес пункта:</h4><input type="text" name="new_subitem_weight" maxlength="2" style="width: 30px;"/> - чем больше "вес", тем ниже в меню и в списке будет этот подраздел
            <h4 class="blue">Отображать в родительской категории:</h4><input type="checkbox" name="new_display_on_parent" value="1" checked="checked" maxlength="2" style="width: 30px;"/>
            <h4 class="blue">Название пункта меню:</h4>Если оставить пустым, то подраздел будет отображаться только на странице раздела <input type="text" name="new_subitem_title" />
            <h4 class="blue">Cсылка:</h4>(одно слово латиницей, уникальное для всего меню)<input type="text" name="new_subitem_chpu" />
            <h4 class="blue">Содержание:</h4>
            <textarea class="w100p"  id="tinyMCE_on_<?echo $i;$i++;?>" rows="10" name="new_subitem_content"></textarea><br>
            <div class="fl"><input class="button" type="submit" value="Добавить" /></div>
        </form>
</div>    
<script>
    function tinyMCEInit(id){
        tinyMCE.execCommand( 'mceAddControl', true, 'tinyMCE_on_'+id);
            };
            document.ready= function(){
                for(i=0;i<<?=$i?>;i=i+1){
                    tinyMCEInit(i);
                       }
                    };
</script>
