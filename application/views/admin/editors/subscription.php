<div class="fl mr"><?php print View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#category_control').click(function() {
			if ($('#category_block').css('display') == 'none')
				$('#category_block').show(300);
			else
				$('#category_block').hide(300);
			return false;
		});
		$('#sub_add_control').click(function() {
			if ($('#sub_add_block').css('display') == 'none')
				$('#sub_add_block').show(300);
			else
				$('#sub_add_block').hide(300);
			return false;
		});
		$('.del_cat_button').click(function() {
			if (!confirm('Вы действительно хотите удалить данную категорию?'))
				return false;
		});
		$('#send_new_sub').click(function() {
			$('#send_email').val('1');
			$(this).parent().submit();
		});
	});
</script>

<div class="fl">
	<fieldset>
		<legend><a href="#" id="category_control">Управление категориями</a></legend>
		<div id="category_block" style="display: none">
			<form method="POST" action="<?php print Url::site('admin/editors/subs_cat_add'); ?>">
				<label>Имя категории:</label><br />
				<input type="text" name="category_name" maxlength="255" style="width:50%" /><br /><br />
                <input class="button" type="submit" value="Добавить" />
			</form>
			<div id="category_list" style="padding: 20px 0;">
			<?php
            // print_r($subscription_cats);
				if (count($subscription_cats) == 0)
					print '<span style="font-size: 14px; font-weight: bold;">На данный момент нет созданных категорий</span>';
				else {
					print '<table>';
					print '<h3>Список категорий:</h3>';
					foreach ($subscription_cats as $value):
			?>
						<tr>
							<td><?php print $value['count'] ?></td>
							<td><?php print $value['name'] ?></td>
                            <td>
                                <a href="<?php echo Url::site('admin/editors/subs_update_active').'/'.$value['id']; ?>">
                                    <?=($value['active']) ? 'активно' : 'неактивно'; ?>
                                </a>
                            </td>
                            <td><a class="button" href="<?php echo Url::site('admin/editors/subs_cat_edit_form').'/'.$value['id']; ?>">Изменить</a></td>
							<td><a href="<?php echo Url::site('admin/editors/subs_cat_delete').'/'.$value['id']; ?>" class="del_cat_button button">Удалить</a></td>
						</tr>
			<?php
					endforeach;
					print '</table>';
				}
			?>			</div>
		</div>
	</fieldset>
    
	<fieldset style="margin-top: 30px;">
		<legend><a href="#" id="sub_add_control">Добавление рассылки</a></legend>
		<div id="sub_add_block">
			<form method="POST" action="<?php print Url::site('admin/editors/subs_add'); ?>">
                <label><h4>Заголовок рассылки:</h4></label>
				<input type="text" name="sub_title">
				<label><h4>Категория:</h4></label>
				<?php
					foreach ($subscription_cats as $value) {
						echo '<input type="checkbox" name="cat_id[]" value="'.$value['id'].'" /> '.$value['name'].'<br />';
					}
				?>
				<label><h4>Содержимое рассылки:</h4></label>
				<textarea name="sub_content" class="w100p" id="tinyMCE_f"></textarea><br /><br />
				<input type="hidden" name="send" value="0" id="send_email" />
                <input class="button" type="submit" value="Разослать" />
<!--				<input type="button" value="Разослать по E-Mail" id="send_new_sub" />-->
			</form>
		</div>
	</fieldset>
    
	<fieldset>
		<legend><h3>Список рассылок:</h3></legend>
		<div id="subscription_list">
			<?php
				if (count($subscriptions) > 0) foreach ($subscriptions as $key_1 => $value_1) {
					print '<h3>'.$key_1.'</h3>';
					print '<ul>';
					foreach ($value_1 as $key_2 => $value_2) {
						echo '<li><a href="'.Url::site('admin/editors/sub_show').'/'.$value_2['id'].'">'.$value_2['title'].'</a></li>';
					}
					print '</ul>';
				} else
					print '<h3>На данный момент нет созданных рассылок.</h3>';
			?>
		</div>
	</fieldset>
</div>