<div class="fl mr"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<div class="w80p fl">
    <table>
        <tr>
            <th>Новость #<?=$news['id']?>, <?=$news['dt']?></th>
        </tr>
        <tr>
            <td>
                <form enctype="multipart/form-data" method="POST" action="" onsubmit="addbtn.disabled=true; addbtn.value='Идет рассылка...'">
					Картинка <input type="file" name="img" /><br /><br />
					Название <input type="text" name="name" value="<?=$news['name']?>" /><br /><br />
					<?php if($news['img']): ?>
					<img src="/upload/news/<?=$news['img']?>" />
					<?php endif; ?>
                    <textarea id="tinyMCE_f" class="w100p" rows="10" name="content"><?=$news['content']?></textarea>
                    <div class="fl mt8"><label><input type="checkbox" name="to_emails" /> Разослать на e-mail</label></div>
                    <div class="fr mt8"><input id="addbtn" class="button" type="submit" value="Сохранить" /></div>
                </form>
            </td>
        </tr>
    </table>
</div>