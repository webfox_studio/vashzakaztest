<div class="fl mr"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<div class="fl">
   <h4 class="blue">Новый Комментарий</h4> 
    <table>
        <tr>
            <td>
                <form enctype="multipart/form-data" method="POST" action="<?=Url::site('admin/editors/reviews_add')?>" onsubmit="addbtn.disabled=true;'">
                    Имя: <input type="text" name="name" /><br /><br />
                    Url: <input type="text" name="link" /><br /><br />
					Tracking #:
                    <input type="text" name="tracking" /><br /><br />
                    <textarea id="tinyMCE_f" class="w100p" rows="10" name="content"></textarea><br />
                    Картинка <input type="file" name="img" /><br /><br />
                    <small>Размер картинки не меньше чем 100 пикселей по ширине.</small><br /><br />
                    <div><input id="addbtn" class="button" type="submit" value="Добавить" /></div>
                </form>
            </td>
        </tr>
    </table>

    <div>
        <?foreach($news as $_news):?>
        <table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
            <tr class="bg1">
                <td width="1%">
                    <a href="<?=Url::site('admin/editors/reviews_edit/'.$_news['id'])?>"><img border="0" src="/<?=Kohana::config('main.path_media')?>/img/edit.png"></a>
                </td>

                <td width="1%">
                    <a href="<?=Url::site('admin/editors/reviews_delete/'.$_news['id'])?>"><img border="0" src="/<?=Kohana::config('main.path_media')?>/img/del.gif"></a>
                </td>
            </tr>
            <tr class="bgc1">
                <td class="p4" colspan="3">
				<strong>Name: <?=$_news['name']?></strong><br />
				<strong>Url: <?=$_news['link']?></strong><br />
                <strong>Tracking#: <?=$_news['tracking']?></strong><br />
                <?if(isset($_news['img'])):?>
                <img src="/upload/reviews/<?=$_news['img']?>" width="100" /><br />
                <?endif?>
                <?=$_news['description']?><br />
                
                </td>
            </tr>
        </table>
        <?endforeach?>
    </div>
</div>