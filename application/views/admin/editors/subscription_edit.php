<div class="fl mr"><?php print View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<script type="text/javascript">
	$(document).ready(function() {

		$('#send_sub').click(function() {
            var result = confirm("Разослать сообщения?");
            if (result) {
                $('#send_email').val('1');
                $(this).parent().submit();
            }
			
		});
        // $('#tinyMCE_f').val("<?php // print $subscription['content'] ?>");
	});
</script>

<div class="w80p">
    <div><?php echo Controller_Admin_Abstract::statistics_subscription(); ?></div>
    <div><?php echo Controller_Admin_Abstract::statistics_news(); ?></div>
	<fieldset>
		<legend><h3>Редактирование рассылки</h3></legend>
		<div id="sub_edit_block">
			<form method="POST" action="<?php print Url::site('admin/editors/sub_edit'); ?>" style="padding: 15px 0">
				<input type="hidden" name="sub_id" value="<?php print $subscription['id'] ?>" />
				<label>Заголовок рассылки:</label><br />
				<input type="text" name="sub_title" style="margin-bottom: 15px; width: 50%" value="<?php print $subscription['title'] ?>" /><br />
				<label>Категория:</label><br />
				<?php
					foreach ($subscription_cats as $value) {
						if (in_array($value['id'], json_decode($subscription['category_id'])))
							echo '<input type="checkbox" name="cat_id[]" value="'.$value['id'].'" checked="checked" /> '.$value['name'].'<br />';
						else
							echo '<input type="checkbox" name="cat_id[]" value="'.$value['id'].'" /> '.$value['name'].'<br />';
					}
				?>
				<br />
				<label>Содержимое рассылки:</label><br />
				<textarea name="sub_content" class="w100p" id="tinyMCE_f"><?php print $subscription['content'] ?></textarea><br /><br />
				<input type="hidden" name="send" value="0" id="send_email" />
                <input class="button" type="submit" value="Сохранить" />
                <input class="button" type="button" value="Разослать по E-Mail" id="send_sub" style="" />
			</form>
		</div>
	</fieldset>
</div>