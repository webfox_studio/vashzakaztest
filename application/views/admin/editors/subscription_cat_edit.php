<div class="fl mr"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?><</div>

<div class="w80p">
	<fieldset style="padding: 20px;">
		<legend>Редактирование категории</legend>
		<form method="POST" action="<?php print Url::site('admin/editors/subs_cat_edit'); ?>">
			<input type="hidden" name="cat_id" value="<?php print $subscription_cat['id']; ?>" />
			<label>Имя категории:</label><br />
			<input type="text" name="category_name" maxlength="40" value="<?php print $subscription_cat['name'] ?>" /><br /><br />
			<input type="submit" value="Изменить" />
		</form>
	</fieldset>
</div>