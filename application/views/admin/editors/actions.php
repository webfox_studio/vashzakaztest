<div class="fl mr"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

    <div class="w80p">
        <table>
            <tr class="bg1 ac h18">
                <th>Новая Акция</th>
            </tr>
            <tr class="bgc1 ac h18">

                <td class="p8">
                    <form enctype="multipart/form-data" method="POST" action="<?=Url::site('admin/editors/actions_add')?>" onsubmit="addbtn.disabled=true;'">
                        Название:
                        <input type="text" name="title" />
                        <br />
                        <br />
                        <textarea id="tinyMCE_f" class="w100p" rows="10" name="content"></textarea>
                        <br /> Ссылка:
                        <input type="text" name="link" />
                        <br />
                        <br /> Картинка
                        <input type="file" name="img" />
                        <br />
                        <br />
                        <small>
                    <ul style="text-align: left;">Размер не меньше чем: 
                        <li>Большие 470 пикселей по ширине</li>
                        <li>Средние 220 пикселей по ширине</li>
                        <li>Маленькие 170 пикселей по ширине</li>
                    </ul>                    
                    </small>
                        <br />
                        <br />
                        <select name="status">
                            <option value="0">-Неактивна-</option>
                            <option value="1">Большая</option>
                            <option value="2">Средняя</option>
                            <option value="3">Маленькая</option>
                        </select>
                        <div class="fr mt8">
                            <input id="addbtn" class="bg1 b" type="submit" value="Добавить" />
                        </div>
                    </form>
                </td>
            </tr>
        </table>

        <div>
            <?foreach($news as $_news):?>
                <table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
                    <tr class="bg1">
                        <td class="ar pr4 f7 b">
                            <?php
                    if($_news['status']=='0') echo "Неактивна";
                    elseif($_news['status']=='1') echo "Большая";
                    elseif($_news['status']=='2') echo "Средняя";
                    elseif($_news['status']=='3') echo "Маленькая";
                ?>
                        </td>
                        <td width="1%">
                            <a href="<?=Url::site('admin/editors/actions_edit/'.$_news['id'])?>"><img border="0" src="/<?=Kohana::config('main.path_media')?>/img/edit.png"></a>
                        </td>

                        <td width="1%">
                            <a href="<?=Url::site('admin/editors/actions_delete/'.$_news['id'])?>"><img border="0" src="/<?=Kohana::config('main.path_media')?>/img/del.gif"></a>
                        </td>
                    </tr>
                    <tr class="bgc1">
                        <td class="p4" colspan="3">
                            <?=$_news['title']?>
                                <br />
                                <?if(isset($_news['img'])):?>
                                    <img src="http://www.vashzakaz.us/upload/actions/<?=$_news['img']?>" />
                                    <?endif?>
                                        <?=$_news['description']?>
                                            <br />

                        </td>
                    </tr>
                </table>
                <?endforeach?>
        </div>
    </div>