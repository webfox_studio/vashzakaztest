<div class="fl mr"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<div class="w80p">
    <table>
        <tr>
            <th>Акция #<?=$news['id']?></th>
        </tr>
        <tr>
            <td class="p8">
                <form enctype="multipart/form-data" method="POST" action="" onsubmit="addbtn.disabled=true;'">
                    Название: 
                    <input type="text" name="title" value="<?=$news['title']?>" /><br /><br />
                    <textarea id="tinyMCE_f" class="w100p" rows="10" name="content"><?=$news['description']?></textarea><br />
                    Ссылка: <input type="text" name="link" value="<?=$news['link']?>" /><br /><br />
                    Картинка <input type="file" name="img" /><br /><br />
                    <small>
                    <ul style="text-align: left;">Размер не меньше чем: 
                        <li>Большие 470 пикселей по ширине</li>
                        <li>Средние 220 пикселей по ширине</li>
                        <li>Маленькие 170 пикселей по ширине</li>
                    </ul>  
                    <select name="status">
                        <option value="0">-Неактивна-</option>
                        <option value="1">Большая</option>
                        <option value="2">Средняя</option>
                        <option value="3">Маленькая</option>
                    </select>
                    <div class="fr mt8"><input id="addbtn" class="bg1 b" type="submit" value="Сохранить" /></div>
                </form>
            </td>
        </tr>
    </table>
</div>