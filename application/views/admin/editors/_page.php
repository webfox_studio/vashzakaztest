<?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?>
 <script type="text/javascript">
    function clickdelcat() {
        res = confirm ("Вы действительно хотите удалить Категорию? \n Все находящиеся в ней подразделы будут удалены. \n при необходимости можете перед удалением перенести подразделы в другую категорию");
        if (res)
            location.href="<?=URL::site('admin/editors/delete_page/')?>/<?=$category->name?>";
    }
</script>
<div class="w80p fr">
   <table class="bgc2 w100p" cellspacing="1" cellpadding="1">
        <tr class="bg1 ac h18">
                    <th>
                    <form method="post" action="<?=URL::site('admin/editors/save_page/')?>/<?=$category->name?>">
                    <input type="hidden" name="edit_cat_id" value="<?=$category->id?>">
                    <img src="/images/del.gif" class="cr" onclick="clickdelcat()" />&nbsp;&nbsp;
                        Название категории:
                    <input style="width: 30%" name="edit_cat_title" type="text" class="bg1 ac" value="<?=$category->title?>" />
                        Ссылка
                    <input title="Короткое слово на латинице, которое будет в ссылке (например для новостей - news, для тарифов - tariffs) Убедитесь, что это слово уникально для страниц, то есть не должно быть двух страниц одинаковой ссылкой"
                     style="width: 15%"  type="text"  name="edit_cat_chpu"  class="bg1 ac" value="<?=$category->name?>" />
                        Вес
                    <input title="Чем больше вес, тем правее или ниже пункт в меню" style="width: 10%"  type="text" class="bg1 ac" name="edit_cat_weight" value="<?=$category->order?>" />
                    <input style="width: 10%"   type="submit" class="bgc1 ac" value="сохранить" />
                    </form>
                    </th>
                    <form method="POST" action="<?=URL::site('admin/editors/save_subpages/')?>/<?=$page?>">
                    <?$i=0;?>
                    <?foreach($sub_pages as $k => $sub_page):?>
                        <tr class="bgc1 ac h18">
                        <td class="p8 al">
                            <input type="hidden" name="item_<?=$sub_page->id?>" value="<?=$sub_page->id?>">
                            <b>Вес пункта:</b> <br />
                            <input type="text" name="weight_<?=$sub_page->id?>" value="<?=$sub_page->order?>" maxlength="2" style="width: 30px;"/> <br />
                            <b>Категория</b><br />
                            <select name="cat_<?=$sub_page->id?>">
                            <?foreach ($cats as $k => $cat):?>
                                <option value="<?=$cat->id?>" <?if($cat->id==$sub_page->page_id):?>selected="selected"<?endif?>><?=$cat->title?></option>
                            <?endforeach?>
                            </select><br />
                            <b>Название пункта меню:</b> Если оставить пустым, то подраздел будет отображаться только на странице раздела<br />
                            <input type="text" name="title_<?=$sub_page->id?>" value="<?=$sub_page->title?>" /><br />
                            <b>Cсылка:</b> (одно слово латиницей, уникальное для всего меню)<br />
                            <input type="text" name="link_<?=$sub_page->id?>" value="<?=$sub_page->name?>" /><br />
                            <b>Содержание:</b> (для удаления подраздела оставьте пустым)
                            <textarea id="tinyMCE_on_<?echo $i;$i++;?>" class="w100p" rows="10" name="content_<?=$sub_page->id?>"><?=$sub_page->content?></textarea>
                        </td>
                        </tr>
                    <?endforeach?>
                <tr class="bgc1 ac h18">
               
                <td><input class="bg1 b mt16" type="submit" value="Сохранить изменения" />
                </td></tr>
                </form>             
                <tr class="bgc1 ac h18">
                    <td class="p8 al">
                    <h2>Добавить новый подраздел:</h2>
                      <form method="POST" action="<?=URL::site('admin/editors/add_subpage/')?>/<?=$page?>">
                        <input type="hidden" name="page_id" value="<?=$category->id?>">
                        <b>Вес пункта:</b> <br />
                        <input type="text" name="new_subitem_weight" maxlength="2" style="width: 30px;"/> - чем больше "вес", тем ниже в меню и в списке будет этот подраздел<br />
                        <b>Название пункта меню:</b> Если оставить пустым, то подраздел будет отображаться только на странице раздела<br />
                        <input type="text" name="new_subitem_title" /><br />
                        <b>Cсылка:</b> (одно слово латиницей, уникальное для всего меню)<br />
                        <input type="text" name="new_subitem_chpu" /><br />
                        <b>Содержание:</b>
                        <textarea class="w100p"  id="tinyMCE_on_<?echo $i;$i++;?>" rows="10" name="new_subitem_content"></textarea>
                        <div align="right"><input class="bg1 b mt16" type="submit" value="Добавить" /></div>
                      </form>
                    </td>
                  </tr>
                </table>
                <script>
                    function tinyMCEInit(id){
                        tinyMCE.execCommand( 'mceAddControl', true, 'tinyMCE_on_'+id);

                    };
                    document.ready= function(){
                        
                        for(i=0;i<<?=$i?>;i=i+1){
                            tinyMCEInit(i);

                        }
                    };
                  </script>
</div>