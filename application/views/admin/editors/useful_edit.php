<div class="fl mr"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<div class="fl">
   <form method="POST" action="<?=Url::site('admin/editors/useful_edit')?>/<?=$useful['id']?>">
    <table>
        <tr><td><h3>Редактирование статьи</h3></td></tr>
        <tr>
            <td>
                <select name="cat" size="1" >
                <?foreach($useful_cats as $k => $cat):?>
                <option value="<?=$cat['id']?>" <?if($useful['cat']==$cat['id']) echo "selected=\"selected\"";?>><?=$cat['name']?></option>
                <?endforeach?>
                </select>
                <h4>Заголовок:</h4>
                <input type="text" size="60" maxsize="255" name="title" value="<?=$useful['title']?>">
            </td>
        </tr>
       <tr><th>Краткое описание</th></tr>
        <tr><td class="p8"><textarea id="tinyMCE_f" class="w100p" rows="10" name="short"><?=$useful['short']?></textarea></td></tr>
    </table>

    <table>
       <tr><th>Полный текст</th></tr>
        <tr><td class="p8"><textarea id="tinyMCE_f1" class="w100p" rows="10" name="content"><?=$useful['content']?></textarea><div class="fr mt8"><input id="addbtn" class="button" type="submit" value="Сохранить" /></div></td></tr>
    </table>
 </form>
              
</div>