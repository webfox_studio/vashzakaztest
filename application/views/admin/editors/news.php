<div class="fl mr"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<h3>Новая новость</h3>
<table style="width: 50%;">
    <tr>
        <td>
             <form method="POST" enctype="multipart/form-data" action="<?=Url::site('admin/editors/news_add')?>" onsubmit="addbtn.disabled=true; addbtn.value='Идет рассылка...'">
                 Картинка <input type="file" name="img" /><br /><br />
				Название <input type="text" name="name" />
                <textarea id="tinyMCE_f" class="w100p" rows="10" name="content"></textarea><br>
                <div class="fl"><label><input class="checkbox" type="checkbox" name="to_emails" />Разослать на e-mail</label></div><br>
                    <div class="fl"><input id="addbtn" class="button" type="submit" value="Добавить" /></div><br><br>
                    <div class="fl"><span class="red">Примечание:</span> для вывода анонса последней новости на заглавную страницу сайта используйте в тексте разделитель |||</div>
                    <div class="fl"><span class="red">Пример:</span> Уважаемые посетители, поздравляем Вас||| c Праздником Нептуна! (текст до ||| будет выведен на заглавной странице)</div>
                </form>
            </td>
        </tr>
    </table>

    <div>
        <?foreach($news as $_news):?>
        <table style="width: 90%;margin: 0 3em;">
            <tr>
                <td><?=$_news['dt']?></td>
                <td><?=$_news['img']?></td>
                <td><a href="<?=Url::site('admin/editors/news_edit/'.$_news['id'])?>"><img src="/<?=Kohana::config('main.path_media')?>/img/edit.png"></a></td>
                <td><a href="<?=Url::site('admin/editors/news_delete/'.$_news['id'])?>"><img src="/<?=Kohana::config('main.path_media')?>/img/del.gif"></a></td>
            </tr>
            <tr><td colspan="3"><?=$_news['content']?></td></tr>
        </table>
        <?endforeach?>
    </div>
</div>