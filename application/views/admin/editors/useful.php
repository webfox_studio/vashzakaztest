<div class="fl mr"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<div class="fl">
    <table><tr><td colspan=3><form method="POST" action="<?=Url::site('admin/editors/useful_category_add')?>">Новая категория <input type="text" name="add_category" class="w50p" /> <input class="button" type="submit" value="Добавить" /></form></td></tr></table>
    <br />
    <h3>Новая статья</h3>
    <form method="POST" action="<?=Url::site('admin/editors/useful_add')?>">
    <table>
        <tr>
            <td>
                <select name="cat" size="1" multiply="no">
                <?foreach($useful_cats as $k => $cat):?>    
                <option value="<?=$cat['id']?>"><?=$cat['name']?></option>
                <?endforeach?>
                </select>
                Заголовок:
                <input type="text" size="60" maxsize="255" name="title">
            </td>
        </tr>
        <tr><th class="blue fl"><h2>Краткое описание</h2></th></tr>
        <tr><td><textarea id="tinyMCE_f" class="w100p" rows="10" name="short"></textarea></td></tr>
    </table>

    <table>
        <tr><th class="blue fl"><h2>Полный текст</h2></th></tr>
        <tr class="bgc1 ac h18">
           <td class="p8">
                <textarea id="tinyMCE_f1" class="w100p" rows="10" name="content"></textarea>
                <div class="fr mt8"><input id="addbtn" class="bg1 b" type="submit" value="Добавить" /></div>
            </td>
        </tr>
    </table>
 </form>
<table>
    <tr><th>Статьи&rArr;Разделы</th></tr>
    <tr>
        <td>
           <?php if (!empty($useful_cats)): ?>
            <table>
                <?php foreach ($useful_cats as $cat) : ?>
                 <tr class="bgc3">
                 	<th class="b aс p4" title=""><a href="/useful/cat/<?php echo $cat['id']?>/"><?php echo $cat['name']?></a></th>
                        <td class="p8 aj" width="50"><a href="<?=Url::site('admin/editors/useful_cat_edit')?>/<?=$cat['id']?>"><img border="0" src="<?=Url::site('images')?>/edit.png"></a> &nbsp; <a href="<?=Url::site('admin/editors/useful_cat_delete')?>/<?=$cat['id']?>"><img border="0" src="<?=Url::site('images')?>/del.gif"></a></td>
                 </tr>
                    <?php $data = Model_Useful::instance()->get_by_cat($cat['id'])?>
                	<?php if (!empty($data)):?>
                		<tr class="bgc1"><td class="p8 aj" colspan="2" ><ul>
                		  <?php foreach ($data as $cats) : ?>
                    		<li><a href="/useful/view/<?php echo $cats['id'] ?>/"><?php echo $cats['title'] ?></a>
                                <a href="<?=Url::site('admin/editors/useful_edit')?>/<?=$cats['id']?>">
                                    <img border="0" src="<?=Url::site('images')?>/edit.png"></a>
                                <a href="<?=Url::site('admin/editors/useful_delete')?>/<?=$cats['id']?>">
                                    <img border="0" src="<?=Url::site('images')?>/del.gif"></a>
                            </li>
                   		  <?php endforeach ?>
                   		  </ul>
                                    </td>
                                </tr>
                	<?php endif ?>
                <?php endforeach ?>
            </table>
            <?php endif ?>
        </td>
    </tr>
</table>

    </div>