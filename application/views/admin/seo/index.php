<style>
	.editContent{
		display: none;
	}
	.active{
		display: table-row;
	}
	.editPage{
		cursor: pointer;
	}
</style>


<table width="100%" border="1" align="center" 
  cellpadding="4" cellspacing="0">
  <tr>
	<td colspan="8" style="text-align: center">Список страниц</td>
  </tr>
	<tr class="bgc1">
		<td><b>id</b></td>
		<td><b>url</b></td>
		<td><b>title</b></td>
		<td><b>description</b></td>
		<td><b>keywords</b></td>
		<td><b>addDate</b></td>
		<td><b>editDate</b></td>
		<td><b>func</b></td>
	</tr>
	<?php foreach($pages as $k => $v): ?>
			<tr>
				<td><?=$v->id?></td>
				<td><a href="<?=$v->url?>" target="_blank"><?=$v->url?></a></td>
				<td><?=$v->title?></td>
				<td><?=$v->desc?></td>
				<td><?=$v->key?></td>
				<td><?=($v->addDate != '0000-00-00 00:00:00') ? $v->addDate : '-'; ?></td>
				<td><?=($v->editDate != '0000-00-00 00:00:00') ? $v->editDate : '-'; ?></td>
				<td>
					<div class="editPage" data-id="<?=$v->id?>">edit</div>
					
				</td>
			</tr>
			<tr class="editContent" data-id="<?=$v->id?>">
				<td colspan="8">
					<form method="POST" action="<?=Url::site('admin/seo/edit')?>">
                        <table border="0">
                            <tr>
                                <td>url</td>
                                <td ><label><input type="text" name="url" size="100" value="<?=$v->url?>"/></label></td>
                            </tr>
                            <tr>
                                <td>title</td>
                                <td><div class="fl mt8"><label><input type="text" name="title" size="100" value="<?=$v->title?>"/></label></div></td>
                            </tr>
							<tr>
                                <td>description</td>
                                <td><div class="fl mt8"><label><input type="text" name="desc" size="100" value="<?=$v->desc?>" /></label></div></td>
                            </tr>
							<tr>
                                <td>keywords</td>
                                <td><input type="text" name="key" size="100" value="<?=$v->key?>" /></td>
                            </tr>
                        </table>
						<input type="hidden" name="id" value="<?=$v->id?>" />
                        <br />
                        <div><input class="bg1 b" type="submit" value="Сохранить" /></div>
                    </form>
				</td>
			</tr>
		<?php endforeach; ?>
</table>

<h2>Добавить страницу</h2>
					<form method="POST" action="<?=Url::site('admin/seo/add')?>">
						<table border="0">
                            <tr>
                                <td>url</td>
                                <td ><label><input type="text" name="url" size="100" value=""/></label></td>
                            </tr>
                            <tr>
                                <td>title</td>
                                <td><div class="fl mt8"><label><input type="text" name="title" size="100" value=""/></label></div></td>
                            </tr>
							<tr>
                                <td>description</td>
                                <td><div class="fl mt8"><label><input type="text" name="desc" size="100" value="" /></label></div></td>
                            </tr>
							<tr>
                                <td>keywords</td>
                                <td><input type="text" name="key" size="100" value="" /></td>
                            </tr>
                        </table>
                        <br />
                        <div><input class="bg1 b" type="submit" value="Добавить" /></div>
                    </form>

<script>
	$(document).ready(function(){
		$(".editPage").click(function(){
			$(".editContent").removeClass("active");
			var id = $(this).data("id");
			$(".editContent[data-id="+id+"]").toggleClass("active");
		})
	});
</script>


