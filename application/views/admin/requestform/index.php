

<table id="search_table" class="bgc2" width="700" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <th width="36">Заказ №</th>
        <th>№  заказа, клиент</th>
        <th width="16"><small><span class="mp red" title="важный">в</span></small></th>
        <th width="40"><small><span class="mp" title="Статус">Статус</span></small></th>
        <th width="40"><small><span class="mp" title="Статус">Удаление</span></small></th>
    </tr>
    <? foreach ($zakaz as $order): ?>
<!--        <tr class="bgc1 ac h18 hover client<?if($order->readed_admin == FALSE OR $order->status):?> bg1<?endif;?>" onclick="window.location='<?=Url::site('admin/'.(empty($is_parcel) ? 'order' : 'parcel').'/'.$order->id)?>'">-->
           <tr class="bgc1 ac h18 hover client<?if($order->status == FALSE):?> bg1<?endif;?>" >    
    <td><a href="<?=Url::site('admin/requestform/z'.$order->id)?>"><?= $order->id ?></a></td>
            <td class="al pl8 pr8">
                <a href="<?=Url::site('admin/requestform/z'.$order->id)?>"><div class="fl<?if($order->status):?> b<?endif?>"><?=$order->fio?> [<?= $order->country.' '.$order->gorod ?>]</div></a>
                <small class="fr">
                    <?if($order->important):?><span style="color: red">важный</span><?endif?>
                    <?if(!$order->status):?><span style="color: blue">непрочитан</span><?endif?>
                    <?if(!$order->status):?><span style="color: green">новый</span><?endif?>
                    
                    <?if($order->processed):?>закрыт<?endif?>
                    <?if($order->deleted):?><span style="color: red">удален</span><?endif?>
                </small>
            </td>
            <td><? if($order->important): ?><b><a title="Сделать обычным" href="<?=Url::site('admin/'.(empty($is_parcel) ? 'orders' : 'parcels').'/set_notimportant/'.$order->id)?>">!!!</a></b><?else:?><a title="Сделать важным" href="<?=Url::site('admin/'.(empty($is_parcel) ? 'orders' : 'parcels').'/set_important/'.$order->id)?>">---</a><? endif; ?></td>
            <td><?if($order->processed == FALSE):?>текущий (<a href="<?=Url::site('admin/requestform/close/'.$order->id)?>">закрыть</a>)<?else:?>закрытый (<a href="<?=Url::site('admin/requestform/open/'.$order->id)?>">открыть</a>)<?endif;?></td>
            <td><?php if($zakaz->deleted == 0){ ?> <a href="/admin/requestform/arhiv/<?= $order->id ?>">В архив</a><?php } else { ?><a href="/admin/requestform/arhiv/<?= $order->id ?>">Восстановить</a> <?php } ?></td>
        </tr>
    <? endforeach; ?>
</table>

<? if(isset($pagination)): ?>
<?= Block::pagination($pagination) ?>
<? endif ?>
