<table class="bgc2 w80p" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18 f7">
        <th>Заказ №<?= $zakaz->id ?> - <?=$zakaz->servise?>
            
        </th>
    </tr>
    <tr class="bgc1 h16 c1">
        <td class="pl8 pr8 f6">
            <div class="fl"><?=$zakaz->fio?></div>
            <div class="fr">
                <? if($zakaz->status == 0):?>непрочитано<?else:?>прочитано<?endif;?>
                <? if($zakaz->important):?><a title="сделать обычным" href="<?=Url::site('admin/requestform/set_notimportant/'.$zakaz->id)?>"><span style="color: red">важное</span></a>
                <?else:?><a title="сделать важным" href="<?=Url::site('admin/requestform/set_important/'.$zakaz->id)?>">сделать важным</a><?endif?>
            </div>
        </td>
    </tr>
    <tr class="bgc1">
        <td class="pl8 pr8 c1">
            <div class="fl al f6">статус заказа: <?if($zakaz->processed == FALSE):?>текущий (<a href="<?=Url::site('admin/requestform/close/'.$zakaz->id)?>">закрыть</a>)<?else:?>закрытый (<a href="<?=Url::site('admin/requestform/open/'.$zakaz->id)?>">открыть</a>)<?endif;?></div>
            <div class="ar f6">время заказа: <?= $zakaz->date_time ?></div>
        </td>
    </tr>
    <tr  class="bgc1">
        <td>
            <p class="b">Вид услуги</p>
            <p><?=$zakaz->servise?></p>
            <br>
            <p class="b">Пожелания относительно доставки</p>
            <p><?=$zakaz->pozhelaniya?></p>
            <br>
            <p class="b">Ссылка на товар и иная важная информацияи</p>
            <p><?=$zakaz->vazh_info?></p>
            <br>
            <p class="b">Стоимость покупки</p>
            <p><?=$zakaz->price?>$</p>
            <br>
            <p class="b">Вес</p>
            <p><?=$zakaz->ves_kol.' '.$zakaz->ves?></p>
            <br>
            <p class="b">Габариты</p>
            <p><?=$zakaz->ves_kol_1.'*'.$zakaz->ves_kol_2.'*'.$zakaz->ves_kol_3.' '.$zakaz->gabarit?></p>
            <br>
            <p class="b">Данные получателя</p>
            
            <p class="b"></p>
            <p>Страна: <?=$zakaz->country?><br />
                Город: <?=$zakaz->gorod?><br />
                Почтовый индекс: <?=$zakaz->index?></p>
            <br>
            <p class="b">Дополнительная информация</p>
            <p><?=$zakaz->dop_info?></p>
            <br>
            <p class="b">Контактные данные</p>
            <p>ФИО: 		&emsp;<?=$zakaz->fio?></p>
            <p>E-mail: 		&emsp;<?=$zakaz->email?></p>
            <p>Контакт в соц сети: 		&emsp;<a href="<?=$zakaz->contact?>" target="_blank"><?=$zakaz->contact?></p>
            
        </td>
    </tr>
</table>

