<?= View::factory('admin/blocks/search_table') ?>

<table id="search_table" class="bgc2" width="600" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <th width="36">#</th>
        <th>Имя Фамилия</th>
        <th width="54"><small><a href="#" title="Всего заказов">вс</a> / <a href="#" title="Важных сообщений">важ</a></small></th>
        <th width="16"><small><a href="#" title="Новых заказов">н</a></small></th>
    </tr>
    <? foreach ($users as $user): ?>
        <tr class="bgc1 ac h18 client">
            <? $new = $user->orders->where('readed', '=', 0)->count_all()?>
            <td><?= $user->id ?></td>
            <td class="al pl8 pr8<?= $new ? ' bg1' : '' ?>"><a href="<?=Url::site('admin/orders/user/'.$user->id)?>"><?= $user->user_data->name ?></a></td>
            <td><?= ($c = $user->orders->count_all()) ? $c : '-' ?> / <?= ($c = $user->orders->where('important', '=', 1)->count_all()) ? $c : '-' ?></td>
            <td><?= ($new ) ? '<b>'.$new.'</b>' : '-' ?></td>
        </tr>
    <? endforeach; ?>
</table>

<? if ($pagination != ''): ?>
<br />
<table class="bgc2 w100p" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18 f7">
        <th width="32"><?=$pagination?></th>
    </tr>
</table>
<? endif; ?>