<fieldset>
    <form action="" method="post">
    <div class="fl">Поиск по id или по заказу: <input type="text" name="search_by_name" value="<?=$search?>" id="search_order" class="admin_search"/><span class="loading" style="display: none">Загрузка...</span></div>
    </form>
</fieldset>


<table>
    <tr>
        <td>
            <?if(! empty($client_name)):?>Клиент: <b><?=$client_name?></b><?endif?>
            <?if(! empty($status)):?><b><?=$status?></b><?endif?>
        </td>
    </tr>
</table>

<table>
    <tr>
        <th>Заказ №</th>
        <th>№ Клиента, имя клиента, <?if(empty($is_parcel)):?>Название заказа<?else:?>Название посылки<?endif?></th>
        <th><small><span class="mp red" title="важный">в</span></small></th>
        <th><small><span class="mp" title="Статус">Статус</span></small></th>
    </tr>
    <? foreach ($orders as $order): ?>
<!--        <tr class="bgc1 ac h18 hover client<?if($order->readed_admin == FALSE OR $order->new):?> bg1<?endif;?>" onclick="window.location='<?=Url::site('admin/'.(empty($is_parcel) ? 'order' : 'parcel').'/'.$order->id)?>'">-->
           <tr class="<?if($order->readed_admin == FALSE OR $order->new):?> bg1<?endif;?>" >    
    <td><a href="<?=Url::site('admin/'.(empty($is_parcel) ? 'order' : 'parcel').'/'.$order->id)?>"><?= $order->id ?></a></td>
            <td class="">
                <a href="<?=Url::site('admin/'.(empty($is_parcel) ? 'order' : 'parcel').'/'.$order->id)?>"><div class="fl<?if($order->new):?> b<?endif?>">#<?=$order->user->id?> <?=$order->user->user_data->name?> [<?= $order->title ?>]</div></a>
                <small class="fr">
                    <?if($order->important):?><span style="color: red">важный</span><?endif?>
                    <?if(! $order->readed_admin):?><span style="color: blue">непрочитан</span><?endif?>
                    <?if($order->new):?><span style="color: green">новый</span><?endif?>
                    <?if($order->processed):?>закрыт<?endif?>
                    <?if($order->deleted):?><span style="color: red">удален</span><?endif?>
                </small>
            </td>
            <td><? if($order->important): ?><b><a title="Сделать обычным" href="<?=Url::site('admin/'.(empty($is_parcel) ? 'orders' : 'parcels').'/set_notimportant/'.$order->id)?>">!!!</a></b><?else:?><a title="Сделать важным" href="<?=Url::site('admin/'.(empty($is_parcel) ? 'orders' : 'parcels').'/set_important/'.$order->id)?>">---</a><? endif; ?></td>
            <td><?if($order->processed == FALSE):?>текущий (<a href="<?=Url::site('admin/'.(empty($is_parcel) ? 'orders' : 'parcels').'/close/'.$order->id)?>">закрыть</a>)<?else:?>закрытый (<a href="<?=Url::site('admin/'.(empty($is_parcel) ? 'orders' : 'parcels').'/open/'.$order->id)?>">открыть</a>)<?endif;?></td>
        </tr>
    <? endforeach; ?>
</table>

<? if(isset($pagination)): ?>
<?= Block::pagination($pagination) ?>
<? endif ?>