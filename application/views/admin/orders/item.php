<table class="text2 effect6 w80">
    <tr>
        <th>Заказ №<?= $order->id ?> - <?=$order->title?>
            <div class="fr pr8"><a href="#nul" onclick="window.open('<?= Url::site('admin/adminfaq') ?>','','Toolbar=0,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=1,Resizable=0,Width=550,Height=400');">Вопросы и ответы</a></div>
        </th>
    </tr>
    <tr>
        <td>
            <div class="fl"><b class="blue"><?=$order->user->user_data->name?></b></div>
            <div class="fr">
                <? if($order->readed == 0):?>непрочитано<?else:?>прочитано<?endif;?>
                <? if($order->important):?><a title="сделать обычным" href="<?=Url::site('admin/'.(empty($is_parcel) ? 'orders' : 'parcels').'/set_notimportant/'.$order->id)?>"><span class="red">важное</span></a>
                <?else:?><a title="сделать важным" href="<?=Url::site('admin/'.(empty($is_parcel) ? 'orders' : 'parcels').'/set_important/'.$order->id)?>">сделать важным</a><?endif?>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="fl al f6">статус заказа: <?if($order->processed == FALSE):?>
                <span class="red">текущий</span> (<a href="<?=Url::site('admin/'.(empty($is_parcel) ? 'orders' : 'parcels').'/close/'.$order->id)?>">закрыть</a>)
                <?else:?>
                <span class="blue">закрытый</span> (<a href="<?=Url::site('admin/'.(empty($is_parcel) ? 'orders' : 'parcels').'/open/'.$order->id)?>">открыть</a>)
                <?endif;?>
            </div>
            <div class="fr">время заказа: <?= $order->date_time ?></div>
        </td>
    </tr>
</table>

<? foreach($order->messages->find_all() as $message): ?>
<table>
    <tr>
        <td>
            <div class="fl">
                <h2 class="blue"><?/* if($user_id == $message->user_id): ?>--><? else: ?><--<? endif; */?></h2>
                <b class="blue"><?= $message->user->user_data->name ?></b>
                <span class="red"><? if($message->user->is_admin()): ?>(администратор)<? endif; ?></span>
            </div>
            <? if ($message->user->is_admin()): ?>
            <div class="fr ml4">
            	<!-- <a href="<?=Url::site('admin/'.(empty($is_parcel) ? 'order' : 'parcel').'/'.$order->id.'.'.$message->id.':edit')?>"><img border="0" src="/<?=Kohana::config('main.path_media')?>/img/edit.png"></a> -->&nbsp;
            	<a onclick="javascript: if (confirm('Вы уверены, что хотите удалить сообщение?')) window.location = '<?=Url::site('admin/'.(empty($is_parcel) ? 'order' : 'parcel').'/'.$order->id.'.'.$message->id.':del')?>'" href="#"><img border="0" src="/<?=Kohana::config('main.path_media')?>/img/del.gif"></a>
            </div>
            <? endif; ?>
            <div class="fr"><?= $message->date_time ?></div>
        </td>
    </tr>
    <tr><td class="p4"><?=$message->message?></td></tr>
</table>
<? endforeach;?>

<table>
    <tr>
        <td>
            <div>Ваше сообщение</div>
            <?=Block::form('order_message_mcef', $form)?>
        </td>
    </tr>
    <?if(isset($pre_message)):?>
    <tr>
        <td><h3>Предварительный просмотр:</h3></td>
    </tr>
     <tr>
        <td>
                <div>
                    <span class="<?if($user->is_admin()):?>red<?endif?>">
                        <?= $user->user_data->name ?>
                        <?if($user->is_admin()):?>[Администратор]<?endif?>
                    </span>
                </div>
                <div><?= $pre_message['time'] ?></div>
                <div>
                    <?= $pre_message['content'] ?>
                </div>
        </td>
    </tr>
    <?endif?>
</table>