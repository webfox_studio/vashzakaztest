<?= View::factory('admin/blocks/search_table') ?>

<table id="search_table" class="bgc2" width="600" align="center" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18">
        <th width="36">#</th>
        <th>Название заказа</th>
        <th width="16"><small><span class="mp red" title="важный">в</span></small></th>
    </tr>
    <? foreach ($orders as $order): ?>
        <tr class="bgc1 ac h18 hover client<?if($order->readed_admin == FALSE):?> b bg1<?endif;?>" onclick="window.location='<?=Url::site('admin/order/'.$order->id)?>'">
            <td><?= $order->id ?></td>
            <td class="al pl8 pr8"><?= $order->title ?> (<?=$order->user->user_data->name?>)</td>
            <td><? if($order->important): ?><b>!</b><? endif; ?></td>
        </tr>
    <? endforeach; ?>
</table>

<? if ($pagination != ''): ?>
<br />
<table class="bgc2 w100p" cellspacing="1" cellpadding="1">
    <tr class="bg1 ac h18 f7">
        <th width="32"><?=$pagination?></th>
    </tr>
</table>
<? endif; ?>