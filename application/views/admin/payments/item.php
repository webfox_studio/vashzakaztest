<table>
    <tr>
        <th>Перевод №<?= $order->id ?> - <?=$order->title?>
            <div class="fr pr8"><a href="#nul" onclick="window.open('<?= Url::site('admin/adminfaq') ?>','','Toolbar=0,Location=0,Directories=0,Status=0,Menubar=0,Scrollbars=1,Resizable=0,Width=550,Height=400');">Вопросы и ответы</a></div>
        </th>
    </tr>
    <tr class="bgc1 h16 c1">
        <td class="pl8 pr8 f6">
            <div class="fl"><?=$order->user->user_data->name?></div>
            <div class="fr">
                <? if($order->readed == 0):?>непрочитано<?else:?>прочитано<?endif;?>
                <? if($order->important):?><a title="сделать обычным" href="<?=Url::site('admin/'.(empty($is_parcel) ? 'payments' : 'parcels').'/set_notimportant/'.$order->id)?>"><span style="color: red">важное</span></a>
                <?else:?><a title="сделать важным" href="<?=Url::site('admin/'.(empty($is_parcel) ? 'payments' : 'parcels').'/set_important/'.$order->id)?>">сделать важным</a><?endif?>
            </div>
        </td>
    </tr>
    <tr class="bgc1">
        <td class="pl8 pr8 c1">
            <div class="fl al f6">статус заказа: <?if($order->processed == FALSE):?>текущий (<a href="<?=Url::site('admin/'.(empty($is_parcel) ? 'payments' : 'parcels').'/close/'.$order->id)?>">закрыть</a>)<?else:?>закрытый (<a href="<?=Url::site('admin/'.(empty($is_parcel) ? 'payments' : 'parcels').'/open/'.$order->id)?>">открыть</a>)<?endif;?></div>
            <div class="ar f6">время перевода: <?= $order->date_time ?></div>
        </td>
    </tr>
</table>

<? foreach($order->messages->find_all() as $message): ?>
<table>
    <tr class="bgc1 h16 c1">
        <td class="pl8 pr8 h14 f6">
            <div class="fl">
                <?/* if($user_id == $message->user_id): ?>--><? else: ?><--<? endif; */?>
                <?= $message->user->user_data->name ?>
                <? if($message->user->is_admin()): ?>(<span class="red">администратор</span>)<? endif; ?>
            </div>
            <? if ($message->user->is_admin()): ?>
            <div class="fr ml4">
            	<!-- <a href="<?=Url::site('admin/'.(empty($is_parcel) ? 'payment' : 'parcel').'/'.$order->id.'.'.$message->id.':edit')?>"><img border="0" src="/<?=Kohana::config('main.path_media')?>/img/edit.png"></a> -->
            	<a onclick="javascript: if (confirm('Вы уверены, что хотите удалить сообщение?')) window.location = '<?=Url::site('admin/'.(empty($is_parcel) ? 'payment' : 'parcel').'/'.$order->id.'.'.$message->id.':del')?>'" href="#"><img border="0" src="/<?=Kohana::config('main.path_media')?>/img/del.gif"></a>
            </div>
            <? endif; ?>
            <div class="fr"><?= $message->date_time ?></div>
        </td>
    </tr>
    <tr class="bgc1 al f7">
        <td class="p4"><?=$message->message?></td>
    </tr>
</table>
<? endforeach;?>

<table>
    <tr>
        <td class="bgc1 p8" valign="center">
            <div class="ac f8 bg1 mb4 brd1">Ваше сообщение</div>
            <?=Block::form('order_message_mcef', $form)?>
        </td>
    </tr>
    <?if(isset($pre_message)):?>
    <tr class="bgc1">
        <td class="p8">
            <h3>Предварительный просмотр:</h3>
        </td>
    </tr>
     <tr class="bgc1">
        <td class="p8">
                <div class="fl f6 c2">
                    <span class="<?if($user->is_admin()):?>red<?endif?>">
                        <?= $user->user_data->name ?>
                        <?if($user->is_admin()):?>[Администратор]<?endif?>
                    </span>
                </div>
                <div class="ar f6 c2"><?= $pre_message['time'] ?></div>
                <div class="bg2 brd3 mt4 mb16 p4">
                    <?= $pre_message['content'] ?>
                </div>
        </td>
    </tr>
    <?endif?>
</table>