<div class="fr mr" style="float: left"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>
<div class="f1 w80p fr">
    <table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
            <tr class="bg1 ac h18">
                <th>Новая страница</th>
            </tr>
            <tr class="bgc1 ac h18">

                <td class="p8">
                    <form method="POST" action="<?=Url::site('admin/editors/additionalpage/add')?>" onsubmit="addbtn.disabled=true; addbtn.value='Идет рассылка...'">
                        <table border="0">
                            <tr>
                                <td>Название</td>
                                <td ><label><input type="text" name="title" size="100" /></label></td>
                            </tr>
                            <tr>
                                <td>Ссылка</td>
                                <td><div class="fl mt8"><label><input type="text" name="links" size="100" /></label></div></td>
                            </tr>
                            <tr>
                                <td>Категория</td>
                                <td>
                                    <select name="sort">
                                        <option value="">Выбрать</option>
                                        <?php foreach($type as $k => $v): ?>
                                            <option value="<?=$v->id?>"><?=$v->name?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <br />
                           
                        <textarea id="tinyMCE_f" class="w100p" rows="10" name="text"></textarea>
                        
                        
                        <div class="fr mt8"><input class="bg1 b" type="submit" value="Добавить" /></div>
                        <div style="float: left; width: 100%;"></div>
                        
                    </form>
                </td>
            </tr>
    </table>
</div>