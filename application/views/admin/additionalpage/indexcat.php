<div style="float:left"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<?php
$type = array(
'1'=>'Тарифы',
'2'=>'Доставка',
'3'=>'Ограничения',
'4'=>'Другое',
);
?>

<div  style="float:left" class="w80p fr">
    <div>
        <table border="0" width="600">
            <tr>
				<td><p style="font-weight: bold; font-size: 18px;"><a href="<?=Url::site('admin/editors/additionalpage')?>">Список страниц</a></p></td>
				<td><p><a href="/admin/editors/additionalpage/add">Добавить страницу</a></p></td>
				<td><p><a href="/admin/editors/additionalpage/addcat">Добавить категорию</a></p></td>
			</tr>
        </table><br />
        <table width="100%">
            <?php foreach($cats as $page): ?>
            <tr>
                <td><a href="<?=Url::site('admin/editors/additionalpage/editcat/'.$page->id)?>"><?=$page->name;?></a></td>
                
                <td> </td>
                <td><a href="<?=Url::site('admin/editors/additionalpage/delcat/'.$page->id)?>">Удалить</a></td>
            </tr>
            <?php endforeach; ?>
                            
        </table> 
        </div>
</div>