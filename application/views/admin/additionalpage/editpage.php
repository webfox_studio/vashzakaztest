<div class="fl mr" style="float: left"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<div class="f1" style="float: left">
    <table>
            <tr><th>Новая страница</th></tr>
            <tr><td>
                    <form method="POST" action="<?=Url::site('admin/editors/additionalpage/edit/'.$pages->id)?>" onsubmit="addbtn.disabled=true; addbtn.value='Идет рассылка...'">
                        <table>
                            <tr>
                                <td>Название</td>
                                <td><label><input type="text" name="title" value="<?= $pages->title;?>" size="100" /></td>
                            </tr>
                            <tr>
                                <td>Ссылка</td>
                                <td><label><input type="text" name="links" value="<?= $pages->links;?>" size="100" /></td>
                            </tr>
                            <tr>
                                <td>Категория</td>
                                <td>
                                    <select name="sort">
                                        <option value="">Выбрать</option>
                                        <?php foreach($type as $k => $v): ?>
                                            <?php if($pages->sort == $v->id){ ?>
                                                <option selected value="<?=$v->id?>"><?=$v->name?></option>
                                            <?php } else { ?>
                                                <option value="<?=$v->id?>"><?=$v->name?></option>
                                            <?php } ?>

                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
           
                        <textarea id="tinyMCE_f" class="w100p" rows="10" name="text"><?= $pages->text;?></textarea>
                        
                        
                        <div class="fr mt8"><input class="button" type="submit" value="Изменить" /></div>
                       
                    </form>
                </td>
            </tr>
    </table>
</div>