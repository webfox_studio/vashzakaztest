<div class="fl mr" style="float: left"><?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?></div>

<div class="fl" style="float: left; width: 70%;">

    <table width="600" border="0">
        <tbody><tr>
            <td><p style="font-weight: bold; font-size: 18px;">Список страниц</p></td>
            <td><p><a href="/admin/editors/additionalpage/add">Добавить страницу</a></p></td>
            <td>
                <select onchange="document.location=this.options[this.selectedIndex].value">
                    <option value="additionalpage">Все категории</option>
                    <?php foreach($type as $k => $v): ?>
                        <?php if((int)$_GET['sort'] == $v->id){ ?>
                            <option selected value="additionalpage?sort=<?=$v->id?>"><?=$v->name?></option>
                        <?php } else { ?>
                            <option value="additionalpage?sort=<?=$v->id?>"><?=$v->name?></option>
                        <?php } ?>

                    <?php endforeach; ?>

                </select>
            </td>
            <td><p><a href="/admin/editors/additionalpage/cat">Категории</a></p></td>
        </tr>
        </tbody></table>
    <div>
    <ul>
        <li><a href="<?=Url::site('admin/editors/additionalpage')?>">Все</a></li>
        <li><a href="<?=Url::site('admin/editors/additionalpage?show=act')?>">Актуальные</a></li>
        <li><a href="<?=Url::site('admin/editors/additionalpage?show=arch')?>">Архив</a></li>
    </ul>
    </div>
    <br />
    <p><a href="/admin/editors/additionalpage/add">Добавить страницу</a></p><br />
        <table>
            <tr>
                <td>ID</td>
                <td>TITLE</td>
                <td>LINK</td>
                <td>ARCHIVE</td>
                <td>DELETE</td>
            </tr>
            <?php foreach($pages as $page): ?>
            <tr>
                <td><a href="<?=Url::site('admin/editors/additionalpage/edit/'.$page->id)?>"><?=$page->id;?></a></td>
                <td><a href="<?=Url::site('admin/editors/additionalpage/edit/'.$page->id)?>"><?=$page->title;?></a></td>
                <td width="500"><?=$_SERVER['HTTP_HOST'].Url::site('additionalpage/'.$page->links)?> --><a href="<?=Url::site('additionalpage/'.$page->links)?>" target="_blank">Перейти</a></td>
                <td><a href="<?=Url::site('admin/editors/additionalpage/archive/'.$page->id)?>"><?=($page->archive == 0) ? 'В архив' : 'Из архива' ?></a></td>
                <td><a href="<?=Url::site('admin/editors/additionalpage/del/'.$page->id)?>">Удалить</a></td>
            </tr>
            <?php endforeach; ?>
        </table> 
</div>