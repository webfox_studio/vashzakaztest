<style>
    a.buttonAdd, div.buttonAdd {
        display: inline-block;
        padding: 0 7px 3px 7px;
        margin: 0;
        line-height: 26px;
        border: 1px solid #556697;
        border-radius: 3px;
        background: linear-gradient(to bottom, rgb(120, 147, 195) 0%, #556697 100%);
        box-shadow: inset 0 0 1px #5877b4;
        font-size: 15px;
        color: white;
        text-decoration: none;
        cursor: pointer;
    }

    .hiddenRow {
        padding: 0 !important;
    }

    .accordion-toggle {
        cursor: pointer;
    }

    .active {
        display: block;
    }
    .activeBackground {
        background: #ffffe6 !important;
    }
</style>

<div>
    <a href="<?= URL::site('admin/tracking/add') ?>" class="buttonAdd">Добавить трекинг</a>
</div>
<br>
<div class="table-responsive-lg">
    <table class="table table-hover" style="border-collapse:collapse;">
        <thead>
        <tr>
            <th><?=$form_column['id']?></th>
            <th><?=$form_column['tracking']?></th>
            <th><?=$form_column['number']?></th>
            <th><?=$form_column['delivery_id']?></th>
            <th><?=$form_column['user_id']?></th>
            <th><?=$form_column['user_name']?></th>
            <th><?=$form_column['user_surname']?></th>
            <th><?=$form_column['first_update']?></th>
            <th><?=$form_column['last_update']?></th>
            <th><?=$form_column['last_status']?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($trackings as $key => $value): ?>
            <?php $firstStatuses = $value->statuses->order_by('date_add', 'asc')->limit(1)->find(); ?>
            <?php $lastStatuses = $value->statuses->order_by('date_add', 'desc')->limit(1)->find(); ?>
            <tr data-toggle="collapse" data-target="#demo<?= $key ?>" class="accordion-toggle">
                <td><?= $value->id ?></td>
                <td class="text-info">VZ<?= $value->tracking ?><?=mb_strtoupper(substr($delivery[$value->delivery_id], 0, 2))?></td>
                <td class="text-info"><?=$value->number?></td>
                <td class="text-info"><?= $delivery[$value->delivery_id] ?></td>
                <td><?= $value->user_id ?></td>
                <td><?= $value->user_name ?> </td>
                <td class="text-success"><?= $value->user_surname ?></td>
                <td class="text-success"><?= Controller_Admin_Abstract::dateConvertTracking($firstStatuses->date_add) ?></td>
                <td class="text-success"><?= Controller_Admin_Abstract::dateConvertTracking($lastStatuses->date_add) ?></td>
                <td class="text-success"><?= $lastStatuses->status ?></td>
                <td><a href="<?=URL::site('/admin/tracking/delTracking/'.$value->id)?>">del</a></td>
            </tr>
            <tr>
                <td colspan="11" class="hiddenRow">
                    <div class="accordian-body collapse" id="demo<?= $key ?>" style="padding: 5px">
                        <?php $statuses = $value->statuses->find_all(); ?>
                        <?php foreach($statuses as $key1 => $value1){
                            $date[] = $value1->date_add;
                        }
                        $datetime1 = new DateTime(reset($date));
                        $datetime2 = new DateTime(end($date));
                        $interval = $datetime1->diff($datetime2);
                        ?>
                        <br />
                        <div>Создание посылки: <b><?= Controller_Admin_Abstract::dateConvertTracking($value->date_add) ?></b></div>
                        <div><?=$form_column['days_delivery']?>: <b><?= $interval->format('%a дней');?></b></div>
                        <div><?=$form_column['comment']?>: <b><?= $value->comment?></b></div>
                        <div><?=$form_column['comment_admin']?>: <b><?= $value->comment_admin?></b></div>
                        <br />
                        <table class="table table-striped" style="width: 550px">
                            <thead>
                            <tr>
                                <th scope="col">Дата</th>
                                <th scope="col">Статус</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($statuses as $k => $v): ?>
                                <tr>
                                    <td><?= Controller_Admin_Abstract::dateConvertTracking($v->date_add) ?></td>
                                    <td><?= $v->status ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div>
                            <a href="<?= URL::site('admin/tracking/edit/' . $value->id) ?>" class="buttonAdd">Редактировать</a>
                            <a href="<?= URL::site('admin/tracking/delTracking/' . $value->id) ?>" class="buttonAdd">Удалить трекинг</a>
                        </div>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<? if ($pagination != ''): ?>
    <br/>
    <table class="bgc2 w100p">
        <tr>
            <th width="32"><?= $pagination ?></th>
        </tr>
    </table>
    <br/>
<? endif; ?>