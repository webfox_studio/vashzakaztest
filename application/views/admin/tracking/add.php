<style>
    .trTracking td {
        background: #eff0f1 !important;
    }
    #trackingSearchUser div{
        cursor: pointer;
    }
</style>
<div style="margin: 0 auto; width: 800px">
    <div>
        <?php if(isset($errors)): ?>
            <div>
                <ul class="errors has-error">
                    <?php foreach($errors as $k => $v): ?>
                        <li class="control-label"><?=$v?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
    <h2>Добавить посылку</h2>
    <form action="<?= URL::site('admin/tracking/add') ?>" method="post">
        <table class="table" style="width: 800px">
            <tbody>
            <tr class="trTracking">
                <td><?=$form_column['user_id']?></td>
                <td>
                    <input class="form-control user_id" name="user_id" placeholder="Выберите пользователя" value="<?=(isset($post['user_id'])) ? $post['user_id'] : ''?>">
                    <div id="trackingSearchUser"></div>
                </td>
            </tr>
            <tr class="trTracking">
                <td><?=$form_column['user_name']?></td>
                <td><input class="form-control" name="user_name" placeholder="Укажите имя пользователя" value="<?=(isset($post['user_name'])) ? $post['user_name'] : ''?>"></td>
            </tr>
            <tr class="trTracking">
                <td><?=$form_column['user_surname']?></td>
                <td><input class="form-control" name="user_surname" placeholder="Укажите фамилию пользователя" value="<?=(isset($post['user_surname'])) ? $post['user_surname'] : ''?>"></td>
            </tr>
            <tr class="trTracking">
                <td><?=$form_column['delivery_id']?></td>
                <td>
                    <select class="form-control" name="delivery_id">
                        <option value="0">--</option>
                        <?php foreach ($delivery as $key => $value): ?>
                            <option <?=(isset($post['delivery_id']) && $post['delivery_id'] == $key) ? 'selected' : ''; ?> value="<?= $key ?>"><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr class="trTracking">
                <td><?=$form_column['number']?></td>
                <td><input class="form-control" name="number" placeholder="Номер посылки" value="<?=(isset($post['number'])) ? $post['number'] : ''?>"></td>
            </tr>
            <tr class="trTracking">
                <td><?=$form_column['city_delivery']?></td>
                <td><input class="form-control" name="city_delivery" placeholder="Город доставки" value="<?=(isset($post['city_delivery'])) ? $post['city_delivery'] : ''?>"></td>
            </tr>
            <tr>
                <td><?=$form_column['date_add']?></td>
                <td><input class="date_add" name="date_add" type='text' class='datepicker-here' value="<?=(isset($post['date_add'])) ? $post['date_add'] : ''?>"/></td>
            </tr>
            <tr>
                <td><?=$form_column['status']?></td>
                <td><input class="form-control" name="status" placeholder="Статус посылки" value="<?=(isset($post['status'])) ? $post['status'] : ''?>"></td>
            </tr>
            <tr>
                <td><?=$form_column['comment']?></td>
                <td><textarea name="comment"><?=(isset($post['comment'])) ? $post['comment'] : ''?></textarea></td>
            </tr>
            <tr>
                <td><?=$form_column['comment_admin']?></td>
                <td><textarea name="comment_admin"><?=(isset($post['comment_admin'])) ? $post['comment_admin'] : ''?></textarea></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="submit" value="Сохранить">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>