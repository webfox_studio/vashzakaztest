<style>
    .trTracking td {
        background: #eff0f1 !important;
    }

    #trackingSearchUser div {
        cursor: pointer;
    }
    caption {
        text-align: center;
        color: green;
    }
    .notes_admin {
        margin-left: 60px;
    }
</style>
<div style="margin: 0 auto; width: 800px">
    <div>
        <?php if (isset($errors)): ?>
            <div>
                <ul class="errors has-error">
                    <?php foreach ($errors as $k => $v): ?>
                        <li class="control-label"><?= $v ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
    <h2>Редактировать посылку <?= $tracking->tracking ?></h2>
    <form action="<?= URL::site('admin/tracking/edit/' . $tracking->id) ?>" method="post">
        <table class="table" style="width: 800px">
            <caption>Основная информация</caption>
            <tbody>
            <tr class="trTracking">
                <td><?= $form_column['user_id'] ?></td>
                <td>
                    <input disabled class="form-control user_id" name="user_id" placeholder=""
                           value="<?= $tracking->user_id ?>">
                    <div id="trackingSearchUser"></div>
                </td>
            </tr>
            <tr class="trTracking">
                <td><?= $form_column['user_name'] ?></td>
                <td><input class="form-control" name="user_name" placeholder="Укажите имя пользователя"
                           value="<?= $tracking->user_name ?>"></td>
            </tr>
            <tr class="trTracking">
                <td><?= $form_column['user_surname'] ?></td>
                <td><input class="form-control" name="user_surname" placeholder="Укажите фамилию пользователя"
                           value="<?= $tracking->user_surname ?>"></td>
            </tr>
            <tr class="trTracking">
                <td><?= $form_column['delivery_id'] ?></td>
                <td>
                    <select class="form-control" name="delivery_id">
                        <option value="0">--</option>
                        <?php foreach ($delivery as $key => $value): ?>
                            <option <?= ($key == $tracking->delivery_id) ? 'selected' : '' ?>
                                    value="<?= $key ?>"><?= $value ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr class="trTracking">
                <td><?= $form_column['number'] ?></td>
                <td><input class="form-control" name="number" placeholder="Номер посылки"
                           value="<?= $tracking->number ?>"></td>
            </tr>
            <tr class="trTracking">
                <td><?= $form_column['city_delivery'] ?></td>
                <td><input class="form-control" name="city_delivery" placeholder="Город доставки"
                           value="<?= $tracking->city_delivery ?>"></td>
            </tr>
            <tr>
                <td><?= $form_column['comment'] ?></td>
                <td><textarea name="comment"><?= $tracking->comment ?></textarea></td>
            </tr>
            <tr>
                <td><?=$form_column['comment_admin']?></td>
                <td><textarea name="comment_admin"><?= $tracking->comment_admin ?></textarea></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="hidden" name="user_id" value="<?= $tracking->user_id ?>">
                    <input type="submit" name="submit" value="Сохранить"></td>
            </tr>
            </tbody>
        </table>
    </form>
    <br/>
    <form action="<?= URL::site('admin/tracking/editStatus/' . $tracking->id) ?>" method="post">
        <table class="table" style="width: 800px">
            <caption>Список статусов</caption>
            <thead>
            <tr>
                <th><?= $form_column['date_add'] ?></th>
                <th><?= $form_column['status'] ?></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($tracking->statuses->find_all() as $key => $value): ?>
                <tr>
                    <td><input class="date_add" name="date_add[<?= $value->id ?>]" type='text' class='datepicker-here'
                               value="<?= $value->date_add ?>"/></td>
                    <td><input class="form-control" name="status[<?= $value->id ?>]" placeholder="Статус посылки"
                               value="<?= $value->status ?>"></td>
                    <td class="text-center"><a href="<?= URL::site('admin/tracking/delStatus/' . $value->id) ?>">del</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="3">
                    <input type="hidden" name="tracking_id" value="<?= $tracking->id ?>">
                    <input type="submit" name="submit" value="Сохранить">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
    <br>
    <form action="<?= URL::site('admin/tracking/addStatus/' . $tracking->id) ?>" method="post">
        <table class="table" style="width: 800px">
            <thead>
            <tr>
                <th colspan="2">Добавить статус</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><input class="date_add" name="date_add" type='text' class='datepicker-here' placeholder="Дата"
                           value=""/></td>
                <td><input class="form-control" name="status" placeholder="Статус посылки" value=""></td>
            </tr>
            <tr>
                <td colspan="3">
                    <input type="hidden" name="tracking_id" value="<?= $tracking->id ?>"/>
                    <input type="submit" name="submit" value="Добавить">
                </td>
            </tr>
            </tbody>
        </table>
    </form>


<!--        <table class="table" style="width: 800px">-->
<!--            <caption>Заметки по посылке</caption>-->
<!--            <tbody>-->
<!--            <tr>-->
<!--                <td>-->
<!--                    --><?php //foreach ($tracking->notes->order_by('date_add', 'desc')->find_all() as $note): ?>
<!--                        <p class="--><?//=($note->who == 1) ? 'notes_admin' : '';?><!--">--><?//=$note->notes?><!-- (--><?//=$note->date_add?><!--)</p>-->
<!--                    --><?php //endforeach; ?>
<!--                </td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td>-->
<!--                    <form action="--><?//= URL::site('admin/tracking/addNotes') ?><!--" method="post">-->
<!--                        <div class="form-group">-->
<!--                            <textarea name="notes" class="form-control"></textarea>-->
<!--                        </div>-->
<!--                        <input type="hidden" name="tracking_id" value="--><?//=$tracking->id?><!--">-->
<!--                        <input type="hidden" name="tracking_number" value="--><?//= $tracking->tracking ?><!--">-->
<!--                        <input type="submit" name="submit" value="Добавить">-->
<!--                    </form>-->
<!--                </td>-->
<!--            </tr>-->
<!--            </tbody>-->
<!--        </table>-->

</div>