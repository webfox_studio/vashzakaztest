
<?= View::factory('admin/blocks/search_adminfiles') ?>
<table border="1" width="100%">
    <tr class="bg1 ac h18">
        <td>№</td>
        <td>Название магазина</td>
        <td>Важность</td>
        <td>Оператор: Ярослав</td>
        <td>Изменение</td>
        <td>Оператор: Оксана</td>
        <td>Изменение</td>
        <td>Оператор: Алёна</td>
        <td>Изменение</td>
        <td>Оператор: Наталья</td>
        <td>Изменение</td>
    </tr>
    
    <?php foreach($clients as $client): ?>

    <tr>
        <td><?=$client->id?></td>
        <td><span><?=$client->shop?></span></td>
        <td><?php if($client->important == 1){ ?><span><a href="/admin/adminfile/close/<?=$client->id?>" style="color:red">Убрать из важных</a></span> <?php }else{ ?><span><a href="/admin/adminfile/open/<?=$client->id?>" style="color:green">Пометить как важное</a></span> <?php } ?></td>
        <td style="background-color: #DDE9FF; text-align: center;"><? if (isset($xls_yaroslav[$client->id])): ?><a href="<?= Url::site('admin/adminfiles/yaroslav/' . $client->id) ?>">Скачать</a><? else: ?>-<? endif; ?></td>
        <td style="background-color: #DDE9FF; text-align: center;"><? if (isset($xls_yaroslav[$client->id])):?> <?=$xls_yaroslav[$client->id]?><?else:?> - <? endif;?></td>
        <td style=" text-align: center;"><? if (isset($xls_oksana[$client->id])): ?><a href="<?= Url::site('admin/adminfiles/oksana/' . $client->id) ?>">Скачать</a><? else: ?>-<? endif; ?></td>
        <td style=" text-align: center;"><? if (isset($xls_oksana[$client->id])):?> <?=$xls_oksana[$client->id]?><?else:?> - <? endif;?></td>
        <td style="background-color: #DDE9FF; text-align: center;"><? if (isset($xls_alena[$client->id])): ?><a href="<?= Url::site('admin/adminfiles/alena/' . $client->id) ?>">Скачать</a><? else: ?>-<? endif; ?></td>
        <td style="background-color: #DDE9FF; text-align: center;"><? if (isset($xls_alena[$client->id])):?> <?=$xls_alena[$client->id]?><?else:?> - <? endif;?></td>
        <td style="background-color: #DDE9FF; text-align: center;"><? if (isset($xls_natalya[$client->id])): ?><a href="<?= Url::site('admin/adminfiles/natalya/' . $client->id) ?>">Скачать</a><? else: ?>-<? endif; ?></td>
        <td style="background-color: #DDE9FF; text-align: center;"><? if (isset($xls_natalya[$client->id])):?> <?=$xls_natalya[$client->id]?><?else:?> - <? endif;?></td>
    </tr>
    <?php endforeach; ?>

</table>

<? if(isset($pagination)): ?>
<?= Block::pagination($pagination) ?>
<? endif ?>