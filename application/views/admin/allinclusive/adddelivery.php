<?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?>
<div class="w80p fr">
    <?php if($errors):?>
        <?php foreach($errors as $key =>$value): ?>
            <p style="color:red" class="b"><?=$value?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
            <tr class="bg1 ac h18">
                <th>Добавление страны доставки</th>
            </tr>
            <tr class="bgc1 ac h18">

                <td class="p8">
                    <form enctype="multipart/form-data" method="POST" action="<?=Url::site('admin/allinclusive/delivery/add/'.$id)?>" onsubmit="addbtn.disabled=true; addbtn.value='Идет загрузка...'">
                        <table border="0">
                            <tr>
                                <td>Страна</td>
                                <td ><label><input type="text" name="name" size="100" value="<?=($_POST) ? $_POST['name'] : ''?>"/></label></td>
                            </tr>
                            <tr>
                                <td>Цена</td>
                                <td><label><input type="text" name="price" size="100" value="<?=($_POST) ? $_POST['price'] : ''?>"/></label></td>
                            </tr>
                        </table>
                        <br />
                        <p style="text-align: left">Описание</p>   
                        <textarea id="tinyMCE_f" class="w100p" rows="10" name="description"><?=($_POST) ? $_POST['description'] : ''?></textarea>
                        
                        <input type="hidden" name="allinclusives_id" value="<?=$id?>" />
                        
                        <div class="fr mt8"><input class="bg1 b" type="submit" value="Добавить" /></div>
                        <div style="float: left; width: 100%;"></div>
                        
                    </form>
                </td>
            </tr>
    </table>
</div>