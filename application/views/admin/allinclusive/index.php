<?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?>
<div class="w80p fr">
    
    <span class="b" style="font-size:16px">Список категорий</span> / <span><a href="<?=Url::site('admin/allinclusive/category/add')?>">Добавить категорию</a></span>
    <br /><br />
    <table width="600">
        <tr class="bg1 b ac">
            <td width="80">id категории</td>
            <td>название категории</td>
            <td></td>
        </tr>
    
    <?php foreach ($category as $key => $value) : ?> 
    <tr class="ac">
        <td><?=$value->id?></td>
        <td><a href="<?=Url::site('admin/allinclusive/category/edit/'.$value->id)?>"><?=$value->category?></a></td>
        <td><a href="<?=Url::site('admin/allinclusive/category/delete/'.$value->id)?>">Удалить</a></td>
    </tr>
               

        <?php endforeach; ?>
    </table>
    
    
    
    <span class="b" style="font-size:16px">Список товаров</span> / <span><a href="<?=Url::site('admin/allinclusive/add')?>">Добавить товар</a></span>
    <br /><br />
    <table width="600">
        <tr class="bg1 b ac">
            <td width="80">id товара</td>
            <td>название товара</td>
            <td></td>
        </tr>
    
    <?php foreach ($productsnew as $key => $value) : ?> 
    <tr class="ac">
        <td><?=$key?></td>
        <td><a href="<?=Url::site('admin/allinclusive/edit/'.$key)?>"><?=$value['name']?></a></td>
        <td><a href="<?=Url::site('admin/allinclusive/delete/'.$key)?>">Удалить</a></td>
    </tr>
               

        <?php endforeach; ?>
    </table>
</div>