<?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?>
<div class="w80p fr">
    <?php if($errors):?>
        <?php foreach($errors as $key =>$value): ?>
            <p style="color:red" class="b"><?=$value?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
            <tr class="bg1 ac h18">
                <th>Новая страница</th>
            </tr>
            <tr class="bgc1 ac h18">

                <td class="p8">
                    <form enctype="multipart/form-data" method="POST" action="<?=Url::site('admin/allinclusive/category/add')?>" onsubmit="addbtn.disabled=true; addbtn.value='Идет загрузка...'">
                        <table border="0">
                            <tr>
                                <td>Название категории</td>
                                <td ><label><input type="text" name="category" size="100" value="<?=($_POST) ? $_POST['category'] : ''?>"/></label></td>
                                <td><div class="fr"><input class="bg1 b" type="submit" value="Добавить" /></div></td>
                            </tr>
                            
                        </table>
                        <br />
                        
                        
                        
                        <div style="float: left; width: 100%;"></div>
                        
                    </form>
                </td>
            </tr>
    </table>
</div>