<?= View::factory('admin/blocks/editors_menu', array('menu' => $editors_menu)) ?>
<div class="w80p fr">
    <?php if($errors):?>
        <?php foreach($errors as $key =>$value): ?>
            <p style="color:red" class="b"><?=$value?></p>
        <?php endforeach; ?>
    <?php endif; ?>
    <table class="bgc2 w100p mb16" cellspacing="1" cellpadding="1">
            <tr class="bg1 ac h18">
                <th>Редактор</th>
            </tr>
            <tr class="bgc1 ac h18">

                <td class="p8">
                    <form method="POST"  enctype="multipart/form-data" action="<?=Url::site('admin/allinclusive/edit/'.$pages->id)?>" onsubmit="addbtn.disabled=true; addbtn.value='Идет рассылка...'">
                        <table border="0">
                            <tr>
                                <td>Название</td>
                                <td ><label><input type="text" name="name" value="<?=($_POST) ? $_POST['name'] : $pages->name?>" size="100" /></div></td>
                            </tr>
                            <tr>
                                <td>Ссылка</td>
                                <td><input type="file" name="img"/></td>
                            </tr>
                            <tr>
                                <td>Категория</td>
                                <td>
                                    <select name="cat">
                                        <option>--</option>
                                        <?php foreach($category as $key => $value): ?>
                                        <?php if($pages->cat != $value->id){ ?>
                                            <option  value="<?=$value->id?>"><?=$value->category?></option>
                                        <?php } else { ?>
                                            <option selected value="<?=$value->id?>"><?=$value->category?></option>
                                        <?php } ?>
                                        <?php endforeach; ?>
                                    </select>
                                    
                                </td>
                            </tr>
                        </table>
                        <br />
                        
                       
                        <textarea id="tinyMCE_f" class="w100p" rows="10" name="description"><?=($_POST) ? $_POST['name'] : $pages->description?></textarea>
                        
                        
                        <div class="fr mt8"><input class="bg1 b" type="submit" value="Изменить" /></div>
                        <div style="float: left; width: 100%;"></div>
                        
                    </form>
                </td>
            </tr>
    </table>
    
            <span class="b" style="font-size:16px">Список доставки</span> / <span><a href="<?=Url::site('admin/allinclusive/delivery/add/'.$pages->id)?>">Добавить</a></span>
            <br><br>
            <table width="600">
                <tr class="bg1 b ac">
                    <td width="80">id доставка</td>
                    <td>страна</td>
                    <td>цена</td>
                    <td></td>
                    <td></td>
                </tr>
                <?php foreach($delivery as $key => $value): ?>
                <tr  class="ac">
                    <td><?=$value->id?></td>
                    <td><?=$value->name?></td>
                    <td><?=$value->price?></td>
                    <td><a href="<?=Url::site('admin/allinclusive/delivery/edit/'.$value->id)?>">редактировать</a></td>
                    <td><a href="<?=Url::site('admin/allinclusive/delivery/delete/'.$value->id)?>">удалить</a></td>
                </tr>
                <?php endforeach; ?>
            </table>
</div>