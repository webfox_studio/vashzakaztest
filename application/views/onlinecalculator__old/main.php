<style type="text/css">
    .calc-main{

    }
    .calc-main option, input{
        padding: 4px;
    }
    .calc-main input:hover{
        border: 1px solid #0000ff;
    }
    .calc-main select:hover{
        border: 1px solid #0000ff;
    }
    #pi-result td{
        padding: 4px;
       
    }

    #pi-result .pi-tr-res:hover{
        background-color: #F5F5F5;
    }
    .pi-empty{
        color: red;
        font-size: 20px;
    }

</style>

<h3>Онлайн калькулятор доставки</h3>
<div><span class="pi-empty">*</span> - поля, обязательные для заполнения</div>
<table class="block effect6 calc-main">
    <tr>
        <td width="100"></td>
        <td><h2>Отправитель</h2></td>
        <td><h2>Получатель</h2></td>
    </tr>
    <tr>
        <td class="b">Страна</td>
        <td>
            <select name="country" class="pi-search country-out">
                <option>--</option>
                <?php foreach($sender as $k => $v): ?>
                    <option value="<?=$v->id?>"><?=$v->name?></option>
                <?php endforeach; ?>
            </select>
            <span class="pi-empty">*</span>
        </td>
        <td>
            <select id="bv-resipient"  class="pi-search country-in">
                <option value="0">--</option>
                <?php foreach($resipient as $k => $v): ?>
                    <option value="<?=$v->id?>"><?=$v->name?></option>
                <?php endforeach; ?>
            </select>
            <span class="pi-empty">*</span>
        </td>
    </tr>
    <tr class="pi-region">
        <td class="b">Город(регион)</td>
        <td>

        </td>
        <td>
            <select id="bv-resipient-city" class="pi-search city-in">
                <option>--</option>
            </select>
            <span class="pi-empty">*</span>
        </td>
    </tr>
    <tr>
        <td class="b">Вес посылки</td>
        <td>
            <input id="lb" type="radio" checked class="pi-search heft-lb" name="heft" value="lb"/>lb
            <input id="kg" type="radio" class="pi-search heft-kg" name="heft" value="kg" />кг
        </td>
        <td>
            <input type="text" class="pi-search heft-value" name="heft_value" value=""  onkeyup="this.value = this.value.replace (/\D/, '')" /><span class="pi-empty">*</span>
        </td>
    </tr>
    <tr>
        <td class="b">Размеры</td>
        <td>
            <input type="radio" checked class="pi-search size-in"  name="size" value="in" />in
            <input type="radio" class="pi-search size-sm" name="size"  value="sm" />см
        </td>
        <td>
            <input placeholder="длина" title="длина" type="text" class="pi-search size-value1" name="size_value1" value="" onkeyup="this.value = this.value.replace (/\D/, '')" /> X
            <input placeholder="ширина"  title="ширина" type="text" class="pi-search size-value2" name="size_value2" value="" onkeyup="this.value = this.value.replace (/\D/, '')" /> X
            <input placeholder="высота" title="высота" type="text" class="pi-search size-value3" name="size_value3" value="" onkeyup="this.value = this.value.replace (/\D/, '')" />
        </td>
    </tr>
</table>

<div id="result">
    <div id="pi-result"></div>
</div>
