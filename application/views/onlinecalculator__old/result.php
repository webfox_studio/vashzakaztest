<style type="text/css">
    .pi-tr-title{
        background-color: #648CB7;
        font-weight: bold;
        color: #ffffff;
    }
    .table td{
        text-align: center;
    }
    .table{
        border: 1px solid black;
        border-collapse: collapse;
    }
    .table tr {
        border: 1px solid black;

    }
    .pi-tr-res td{
        font-size: 12px;

    }
    .mosk{
        font-size: 10px;
        color:red;
    }
    .dop td{
        font-size: 11px;
    }
</style>
<br/><br/><br/>
<?php if($pi_empty_in == 1): ?>
<div style="color: red; font-weight: bold">
    «Внимание! Вы не указали размеры посылки (груза). Итоговые данные в этом случае отображаются правильно только для обычных посылок, но не для негабаритных грузов»
        <br/></div>
<?php endif; ?>

<h2>Доставка государственной почтой США (USPS)<br /> на дом получателю или до ближайшего почтового отделения</h2>
<table width="100%" cellpadding="0" cellspacing="0" class="table">
    <tr class="pi-tr-title">
        <td>Варианты доставки</td>
        <td>Стоимость + VZ</td>
        <td>Сроки доставки</td>
        <td>Трекинг номер</td>
        <td>Страховка</td>
        <td>Таможенная очистка</td>
        <td>Вариант получения</td>
    </tr>
    <?php foreach($result as $k => $v): ?>
        <?php if($v['opt_id'] ==12){ echo '<tr><td colspan="7" style=""><br /><br /><br/><br/><h2>Доставка транспортной курьерской службой (VashZakaz cargo) до Санкт-Петербурга и Москвы<br /> (до терминала транспортной компании)<br /> (услуги mail forwarding и таможенной очистки учтены для обычных товаров) </h2></td></tr>'; } ?>
        <tr class="pi-tr-res" style=" background-color: #E1FFDC; font-size: 14px">
            <td><b><?=$v['opt_name']?></b></td>
            <td >
                <span style="color: blue"><?=$v['pr_price']?></span>
                <?php if($v['opt_id'] == 12 AND $city == 6 AND $cargo !=1){echo '<p class="mosk">При получении в терминале</p>';} ?>
            </td>
            <td><?=$v['opt_delivery_period']?></td>
            <td><?=$v['opt_tracing']?></td>
            <td><?=$v['opt_insurance']?></td>
            <td><?=$v['opt_customsclearance']?></td>
            <td><?=$v['opt_variant_reception']?></td>
        </tr>
    <?php endforeach; ?>
    <?php if($cargo == 0){ ?>
    <?php if(isset($result_city)): ?>
        <tr><td  colspan="7"><br /><b>Стоимость дополнительной доставки в Ваш город (регион)</b></td></tr>
        <?php foreach($result_city as $k => $v): ?>
            <tr class="pi-tr-res dop">
                <td><b><?=$v->opt_name?></b></td>
                <td>
                    <?php if($cargo == 0){ ?>
                    <span style="color: blue">+<?=$v->pr_price?> $</span>
                        <?php if($city == 6  ){echo '<p class="mosk">При доставке на дом</p>';} ?>
                    <?php } else { ?>
                        --
                    <?php } ?>
                </td>
                <td><?=$v->opt_delivery_period?></td>
                <td><?=$v->opt_tracing?></td>
                <td><?=$v->opt_insurance?></td>
                <td><?=$v->opt_customsclearance?></td>
                <td><?=$v->opt_variant_reception?></td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php } ?>

</table>