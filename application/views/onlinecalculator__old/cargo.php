<style type="text/css">
    .pi-tr-title{
        background-color: #648CB7;
        font-weight: bold;
        color: #ffffff;
    }
    .table td{
        text-align: center;
    }
    .table{
        border: 1px solid black;
        border-collapse: collapse;
    }
    .table tr {
        border: 1px solid black;

    }
    .pi-tr-res td{
        font-size: 12px;
    }
</style>
<br/><br/><br/>
<?php if($pi_empty_in == 1): ?>
    <br /><div style="color: red; font-weight: bold">
        «Внимание! Вы не указали размеры посылки (груза). Итоговые данные в этом случае отображаются правильно только для обычных посылок, но не для негабаритных грузов»
        <br/><br /></div><br />
<?php endif; ?>

<table width="100%" cellspacing="0" cellpadding="0" class="table">
    <tbody>
    <tr><td style="font-size: 16px;" colspan="7"><b>Доставка транспортной курьерской службой (VashZakaz cargo) до Санкт-Петербурга и Москвы <br />(до терминала транспортной компании)<br />
                (услуги mail forwarding и таможенной очистки учтены для обычных товаров)<br />



            </b></td></tr>


    <tr class="pi-tr-title">
        <td>Варианты доставки</td>
        <td>Стоимость + VZ</td>
        <td>Сроки доставки</td>
        <td>Трекинг номер</td>
        <td>Страховка</td>
        <td>Таможенная очистка</td>
        <td>Вариант получения</td>
    </tr>

    <?php if($country_in == 1){ ?>
           <tr style="" class="pi-tr-res">
        <td><b>VashZakaz Cargo</b></td>
        <td><?=$price;?> </td>
        <td>10-20 дней в зависимости от конкретных условий доставки, информация может предоставляться заранее</td>
        <td>Не предоставляется, но информация о передвижении доступна через поддержку сервиса</td>
        <td>Доступна по желанию клиента</td>
        <td>Включена в стоимость  доставки, контролируется на всех этапах</td>
        <td>По выбору клиента, в руки получателю либо до ближайшего терминала</td>
    </tr>
    <?php } else { ?>
<tr>
    <td colspan="8"><br /><br /><span style="color: blue">Негабаритный груз. Стоимость доставки уточняйте у сотрудников сервиса <a href="http://vashzakaz.us/page/contacts" target="_blank">VashZakaz</a> </span><br /><br /></td>
</tr>
    <?php } ?>
    <?php if($city != 7): ?>
<!--    <tr><td style="font-size: 16px; color: red" colspan="7"><br /><br /><b>-->
<!--                Стоимость услуг доставки по регионам в данном случае – уточняйте у сотрудников сервиса <a href="http://vashzakaz.us/page/contacts" target="_blank">VashZakaz</a>-->
<!---->
<!---->
<!--            </b></td></tr>-->
    <?php endif; ?>
    </tbody></table>
<?=$city_in?>