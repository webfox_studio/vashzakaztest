<h3>Отзывы наших клиентов</h3>
<?php
for($i=0; $i<sizeof($reviews); $i++)
{
	
	if($reviews[$i]['count']>1 AND !(($reviews[$i]['count']-1) % 3)) {
		echo '';
	}
	?>
    <div class="review block effect1 all" style="padding-left: 5px; padding-right: 5px; position: relative; " data-id="<?=$reviews[$i]['id']?>">
        <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].'/upload/reviews/'.$reviews[$i]['img'])): ?>
        <div class="center"><img src="/upload/reviews/<?=$reviews[$i]['img']?>" width="80" height="100"></div>
    <?php endif; ?>
        <blockquote class="tooltip"> <?=mb_strimwidth(strip_tags($reviews[$i]['description']), 0, 50, "...")?>
            <span class="tooltiptext"><?=strip_tags($reviews[$i]['description'])?></span>
        </blockquote>
<!--        <blockquote class="all-more " data-id="--><?//=$reviews[$i]['id']?><!--" style=""> --><?//=strip_tags($reviews[$i]['description'])?><!-- </blockquote>-->
        
        <div class="name lightgray">&mdash; <?=$reviews[$i]['name']?></div>
        <div class="track-all-block" style="position: absolute; top: 270px">
            <div class="track p4" style="text-align: left !important"><a target="_blank" href="<?=$reviews[$i]['link']?>">отследить отправление</a></div>
            <div class="p1">Tracking №: <?=$reviews[$i]['tracking']?></div>
        </div>
        
    </div>
	<?php
	if($reviews[$i]['count'] % 3) {
		echo '';
	}
	
}
?>