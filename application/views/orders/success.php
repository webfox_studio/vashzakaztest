<div class="block effect6">
<div class="mt32 ac c2 f9"><?if(empty($is_parcel)):?>Заказ принят и будет рассмотрен<?else:?>Посылка принята и будет рассмотрена<?endif;?> в ближайшее время, благодарим вас</div>
<div class="mt8 ac c2 f7">перейти к <a href="<?=Url::site(empty($is_parcel) ? 'orders' : 'parcels')?>"><?if(empty($is_parcel)):?>заказам<?else:?>посылкам<?endif;?></a>, на <a href="<?=Url::site('/')?>">главную</a></div>
</div>