<div class="block effect6">
    <div class="mt32 ac c2 f9">Сообщение к <?if(empty($is_parcel)):?>заказу<?else:?>посылке<?endif;?> отправлено</div>
    <div class="mt8 ac c2 f7">перейти к <a href="<?=Url::site(empty($is_parcel) ? 'orders' : 'parcels')?>"><?if(empty($is_parcel)):?>заказам<?else:?>посылкам<?endif;?></a>, на <a href="<?=Url::site('/')?>">главную</a></div>
</div>