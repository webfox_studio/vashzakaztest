<h3>&nbsp;</h3>
<table class="effect6 text2">
    <tr>
        <th><?if(empty($is_parcel)):?>Заказ<?else:?>Посылка<?endif;?> №<?= $order->id ?>: <?=$order->title?></th>
    </tr>
    <tr>
        <td>
            <div class="fl">
                <? if($order->readed): ?>Просмотрен<? else: ?>не просмотрен<? endif; ?>
                <? if($order->important):?><span style="color: red;">Важный</span><?endif?>
                <?php $namePage = (empty($is_parcel) ? 'заказ' : 'посылку') ?>
                <?if($order->processed == FALSE):?><span class="blue">"Текущий"</span> (<a href="<?=Url::site('/'.(empty($is_parcel) ? 'orders' : 'parcels').'/close/'.$order->id)?>">отменить <?=$namePage?></a>)<?else:?><span class="blue">"Отмененный"</span> (<a href="<?=Url::site('/'.(empty($is_parcel) ? 'orders' : 'parcels').'/open/'.$order->id)?>">открыть <?=$namePage?></a>)<?endif;?>
                
                
            </div>
            <div class="fr"><?= $order->date_time ?></div>
        </td>
    </tr>
</table>

<? foreach($order->messages->find_all() as $message): ?>
<table class="effect6 text2">
    <tr>
        <td>
            <div class="fl">
                <?/* if($user_id == $message->user_id): ?>--><? else: ?><--<? endif; */?>
                <?= $message->user->user_data->name ?>
                <? if($message->user->is_admin()): ?>(<span class="red">администратор</span>)<? endif; ?>
            </div>
            <div class="fr"><?= $message->date_time ?></div>
        </td>
    </tr>
    <tr>
        <td class="p4 lnk"><?=$message->message?></td>
    </tr>
</table>
<? endforeach;?>

<? if(! $order->processed): ?>

<h3>Ваше сообщение</h3>
    <?=Block::form('order_message', $form)?>
    <?if(isset($pre_message)):?>
<h3>Предварительный просмотр:</h3>
<span class="<?if($user->is_admin()):?>red<?endif?>">
        <?= $user->user_data->name ?>
    <?if($user->is_admin()):?>[Администратор]<?endif?>
</span>
<div class=""><?= $pre_message['time'] ?></div>
<div class=""><?= $pre_message['content'] ?></div>
<?endif?>

<? else: ?>
<h2>Заказ закрыт</h2>
<? endif ?>