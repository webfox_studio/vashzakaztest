<div class="block effect6">
<div class="mt32 ac f11" style="color: red"><?if(empty($is_parcel)):?>Запрошенный заказ<?else:?>Запрошенная посылка<?endif;?> не существует</div>
<div class="mt8 ac c2 f7">перейти к <a href="<?=Url::site(empty($is_parcel) ? 'orders' : 'parcels')?>"><?if(empty($is_parcel)):?>заказам<?else:?>посылкам<?endif;?></a>, на <a href="<?=Url::site('/')?>">главную</a></div>
</div>