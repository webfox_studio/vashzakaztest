<style>
    .w80 {
        width: 85%;
    }

    .cell h3 {
        margin: 0;
        padding: 0;
    }
</style>

<? if (empty($is_parcel)) { ?>
    <?php
    if (!$id_zakaz) {
        ?>

        <h3>Оформить заказ</h3>

        <div class="table">
            <div class="row">
                <div class="cell w80"><h3>Заказ в магазине</h3>
                    <div>данная форма предназначена для стандартного заказа из интернет-магазина, который оформляется и
                        оплачивается нашими сотрудниками (<a href="/page/tariffs" target="_blank">услуга «полного
                            сопровождения»</a>)
                    </div>
                </div>
                <div class="cell"><a class="red nd b" href="/orders/new1">Оформить &DoubleRightArrow;</a></div>
            </div>

            <div class="row">
                <div class="cell w80"><h3>Аукцион (торги)</h3>
                    <div>данная форма подразумевает участие наших сотрудников в торгах, где окончательная цена покупки
                        чаще всего заранее неизвестна и требуется участие в розыграше лота либо переговорах с продавцом;
                        также здесь предусмотрен вариант обычной покупки по фиксированной цене на аукционной площадке
                        (<a href="/page/tariffs" target="_blank">услуга «полного сопровождения»</a>)
                    </div>
                </div>
                <div class="cell"><a class="red nd b" href="/orders/new2">Оформить &DoubleRightArrow;</a></div>
            </div>
            <!--            <div class="row">-->
            <!--                <div class="cell w80"><h3>Самостоятельная покупка</h3>-->
            <!--                    <div>данная форма заполняется в случае, если товары приобретены самостоятельно и необходимы только-->
            <!--                        услуги склада по их приему, хранению, обработке и отправке (<a href="/page/mforwarding"-->
            <!--                                                                                       target="_blank">услуга-->
            <!--                            «виртуального склада»</a>)-->
            <!--                    </div>-->
            <!--                </div>-->
            <!---->
            <!--                <div class="cell"><a class="red nd b" href="/orders/new3">Оформить &DoubleRightArrow;</a></div>-->
            <!--            </div>-->
            <!--            <div class="row">-->
            <!--                <div class="cell w80"><h3>Альтернативный заказ</h3>-->
            <!--                    <div>данная форма может быть выгодна для некоторых заказов по схеме "продавец (магазин) –-->
            <!--                        негосударственная транспортная служба доставки – клиент", который оформляется и оплачивается-->
            <!--                        нашими сотрудниками. Обратите внимание на более подробную информацию по данному виду доставки на-->
            <!--                        нашем сайте (<a href="/page/tariffs/alterorder" target="_blank">услуга «альтернативный-->
            <!--                            заказ»</a>)-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--                <div class="cell"><a class="red nd b" href="/orders/new4">Оформить &DoubleRightArrow;</a></div>-->
            <!--            </div>-->
        </div>

    <?php } else { ?>

        <h3><? if (empty($is_parcel)): ?>Новый заказ<? else: ?>Новая посылка<? endif; ?></h3>
        <p class="stretchTable">Расширить форму</p>
        <p class="stretchTable" style="display:none">Убрать расширение</p>
        <br/>
        <table class="text2 effect6">
            <tr>
                <div style="display:none;">
                    <span id="insert_error_html"><?= $error_html ?></span>
                    <span id="insert_files_html"><?= $files_html ?></span>
                    <input type="hidden" name="image_counter" id="image_counter" value="<?= $images_count ?>">
                </div>
                <td><?= isset($form) ? $form : 'Форма заказа недоступна' ?></td>
            </tr>
            <tr>
                <td>
                    <? if (isset($preview)): ?>
                        <p><h3>Предварительный просмотр заказа:</h3></p>
                        <div class="al mt16 mb16 ml8"><?= $preview ?></div>
                    <? endif ?>
                </td>
            </tr>

        </table>
    <?php } ?>
<? } else { ?>
    <!--    <h3>--><? // if (empty($is_parcel)): ?><!--Новый заказ--><? // else: ?><!--Новая посылка--><? // endif; ?><!--</h3>-->

    <?php
    if (!$id_zakaz) {
        ?>

        <h3>Посылку к отправке</h3>

        <div class="table">
            <div class="row">
                <div class="cell w80"><h3>VashZakaz USPS – быстрая доставка при помощи государственной почты США</h3>
                    <div>данная форма предназначена для оформления доставки различных товаров посредством одного из
                        вариантов госпочты США (Priority Express mail, Priority mail, First class mail); при соблюдении
                        отправителем и получателем условий беспошлинных лимитов по стоимости, а также лимитов по
                        максимальному весу и габаритам, посылки с товарами для личного пользования проходят таможенное
                        оформление автоматически
                    </div>
                </div>
                <div class="cell"><a class="red nd b" href="/parcels/new1">Оформить &DoubleRightArrow;</a></div>
            </div>

            <div class="row">
                <div class="cell w80"><h3>VashZakaz Сargo – коммерческая доставка партий грузов при помощи транспортных
                        компаний</h3>
                    <div>данная форма предназначена для оформления поэтапной доставки коммерческих, а также и
                        персональных заказов, в страны России и СНГ без участия государственной почты; доставка карго
                        включает услуги таможенной очистки грузов и региональную рассылку товаров со склада в г.
                        Санкт-Петербург по адресам получателей
                    </div>
                </div>
                <div class="cell"><a class="red nd b" href="/parcels/new2">Оформить &DoubleRightArrow;</a></div>
            </div>
            <div class="row">
                <div class="cell w80"><h3>VashZakaz Economy – недорогая доставка персональных заказов при помощи
                        курьерской и брокерской службы</h3>
                    <div>данная форма предназначена для оформления пересылки товаров личного пользования в Россию без
                        участия государственной почты; экономичная доставка включает услуги таможенной очистки товаров и
                        их рассылку по регионам из г. Москвы при помощи Boxberry; данное таможенное оформление требует
                        предоставления паспортных данных получателя и детальной информации о содержимом посылок
                    </div>
                </div>

                <div class="cell"><a class="red nd b" href="/parcels/new3">Оформить &DoubleRightArrow;</a></div>
            </div>
            <div class="row">
                <div class="cell w80"><h3>VashZakaz Sea – бюджетная доставка товаров и грузов морем</h3>
                    <div>данная форма предназначена для оформления отправки товаров и грузов в Россию; относительно недорогая доставка, которая включают отправления морским путем, прохождение таможни, а также региональную рассылку до местного отделения почты России или непосредственно в руки клиенту посредством курьера ЕМС; характерной чертой являются более длительные сроки и возможность доставки авиаопасных товаров</div>
                </div>

                <div class="cell"><a class="red nd b" href="/parcels/new4">Оформить &DoubleRightArrow;</a></div>
            </div>
        </div>
    <?php } else { ?>
        <h3><? if (empty($is_parcel)): ?>Новый заказ<? else: ?>Новая посылка<? endif; ?></h3>
        <p class="stretchTable">Расширить форму</p>
        <p class="stretchTable" style="display:none">Убрать расширение</p>
        <br/>
        <table class="text2 effect6">
            <tr>
                <div style="display:none;">
                    <span id="insert_error_html"><?= $error_html ?></span>
                    <span id="insert_files_html"><?= $files_html ?></span>
                    <input type="hidden" name="image_counter" id="image_counter" value="<?= $images_count ?>">

                </div>
                <td><?= isset($form) ? $form : 'Форма заказа недоступна' ?></td>
            </tr>
            <tr>
                <td>
                    <form action="" method=""></form>
                </td>
            </tr>
            <tr>
                <td>
                    <? if (isset($preview)): ?>
                        <p><h3>Предварительный просмотр заказа:</h3></p>
                        <div class="al mt16 mb16 ml8"><?= $preview ?></div>
                    <? endif ?>
                </td>
            </tr>

        </table>


    <? } ?>
<? } ?>

