<h3><?if(empty($is_parcel)):?>Заказы<?else:?>Посылки<?endif;?></h3>

<table class="effect6">
    <tr>
        <td class="mt16" style="width: 130px;"><a class="mt16" href="<?=Url::site((empty($is_parcel) ? 'orders' : 'parcels').'/unread')?>">Непрочитаные</a></td>
        <td><?=isset($count_unread) ? $count_unread : 0?></td>
    </tr>
    <tr>
        <td class="mt16"><a class="mt16" href="<?=Url::site((empty($is_parcel) ? 'orders' : 'parcels').'/current')?>">Текущие</a></td>
        <td><?=isset($count_all, $count_processed) ? ($count_all - $count_processed) : 0?></td>
    </tr>
    <tr>
        <td class="mt16"><a class="mt16" href="<?=Url::site((empty($is_parcel) ? 'orders' : 'parcels').'/closed')?>">Отмененные</a></td>
        <td><?=isset($count_processed) ? $count_processed : 0?></td>
    </tr>
    <tr>
        <td class="mt16"><a class="mt16" href="<?=Url::site((empty($is_parcel) ? 'orders' : 'parcels').'/all')?>">Все</a></td>
        <td><?=isset($count_all) ? $count_all : 0?></td>
    </tr>
</table>
<h4><?=View::factory('forms/order_find', array('form' => $find_form))?></h4>

<div><a href="<?= Url::site((empty($is_parcel) ? 'orders' : 'parcels').'/all') ?>">Все <?if(empty($is_parcel)):?>заказы<?else:?>посылки<?endif;?></a></div>
<div><a class="red" href="<?= Url::site((empty($is_parcel) ? 'orders' : 'parcels').'/new') ?>"><?if(empty($is_parcel)):?>Новый заказ<?else:?>Новая посылка<?endif;?></a></div>