<?switch($status) {
    case 'all': $st = 'Все'; break;
    case 'closed': $st = 'Отмененные'; break;
    case 'current': $st = 'Текущие'; break;
    case 'unread': $st = 'Непрочитаные'; break;
}?>
<h3><?=$st?> <?if(empty($is_parcel)):?>заказы<?else:?>посылки<?endif;?></h3>

<table class="block effect6">
    <tr class="">
        <th>ID</th>
        <th>Дата Время</th>
        <th colspan="2">Название</th>
        <!--<th class="f6 red" width="50">
            <span class="mp" title="непрочитан">н</span>
            <span class="mp" title="прочитан">п</span>
            <span class="mp" title="важный">в</span>
            <span class="mp" title="текущий">т</span>
            <span class="mp" title="закрытый">з</span>
        </th>-->
    </tr>
    <? foreach ($orders as $order): ?>
<!--    <tr class="bgc1 hover<?if($order->readed == FALSE):?> bg1 b<?endif;?>" onclick="window.location = '<?=Url::site((empty($is_parcel) ? 'order' : 'parcel').'/id'.$order->id)?>'">-->
    <tr class="<?if($order->readed == FALSE):?>noReadMessage<?endif;?>">
        <td class=""><a href="<?=Url::site((empty($is_parcel) ? 'order' : 'parcel').'/id'.$order->id)?>"><?=$order->id?></a></td>
        <td class=""><a href="<?=Url::site((empty($is_parcel) ? 'order' : 'parcel').'/id'.$order->id)?>"><?=$order->date_time?></a></td>
        <td class=""><a href="<?=Url::site((empty($is_parcel) ? 'order' : 'parcel').'/id'.$order->id)?>"><?=mb_substr($order->title,0,60)?>...</a></td>
        <td class="" style="font-weight:normal !important; text-transform: none;">
            <?php $namePage = (empty($is_parcel) ? 'заказ' : 'посылку') ?>
            <?if($order->processed == FALSE):?>текущий (<a href="<?=Url::site('/'.(empty($is_parcel) ? 'orders' : 'parcels').'/close/'.$order->id)?>">отменить <?=$namePage?></a>)<?else:?>отмененный (<a href="<?=Url::site('/'.(empty($is_parcel) ? 'orders' : 'parcels').'/open/'.$order->id)?>">открыть <?=$namePage?></a>)<?endif;?>
        </td>
       <!-- <td class="">
            <? if($order->readed == FALSE): ?>н<?else:?>п<? endif; ?>
            <? if($order->important): ?>в<? endif; ?>
            <? if($order->processed): ?>з<?else:?>т<? endif; ?>
            
        </td>-->
    </tr>
    <? endforeach; ?>
</table>

<? if (isset($pagination) AND $pagination != ''): ?>

<br />
<?=$pagination?>

<? endif; ?>