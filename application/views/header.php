<div class="header">
    <div class="logo"><a href="/"><img src="/<?= Kohana::config('main.path_media') ?>/img/logo.png"/></a></div>
    <div class="head-buttons clearfix">
        <div class="head">
            <a href="<?= Url::site('page/about') ?>">О КОМПАНИИ</a><a
                    href="<?= Url::site('page/contacts') ?>">КОНТАКТЫ</a>
        </div>
        <div class="skype">
            <?php
            $query = "SELECT status FROM kh_skype_status LIMIT 5";
            $skype_status = DB::query(Database::SELECT, $query)->execute()->as_array();
            $skype_status_0 = (@$skype_status[0]['status']) ? 'online' : 'offline';
            $skype_status_2 = (@$skype_status[2]['status']) ? 'online' : 'offline';
            $skype_status_4 = (@$skype_status[4]['status']) ? 'online' : 'offline';
            ?>
            <script type="text/javascript" src="http://download.skype.com/share/skypebuttons/js/skypeCheck.js"></script>
            <div>
                <a href="/page/contacts">
                    <img style="padding: 0; <?=(@$skype_status[0]['status']) ? '' : ' opacity: 0.3;'; ?>" src="/images/Telegram-icon.png" width="16" height="16" alt="My status"/>
                    <img style="padding: 0 0.5em 0 0;" src="/images/skype_status_<?= $skype_status_0 ?>.png" width="16" height="16" alt="My status"/>ЯРОСЛАВ
                </a>
            </div>
            <div>
                <a href="/page/contacts">
                    <img style="padding: 0; <?=(@$skype_status[4]['status']) ? '' : ' opacity: 0.3;'; ?>" src="/images/Telegram-icon.png" width="16" height="16" alt="My status"/>
                    <img style="padding: 0 0.5em 0 0;" src="/images/skype_status_<?= $skype_status_4 ?>.png" width="16" height="16" alt="My status"/>ОКСАНА
                </a>
            </div>
            <div style="margin-bottom:25px;">
                <a href="/page/contacts">
                    <img style="padding:0; <?=(@$skype_status[2]['status']) ? '' : ' opacity: 0.3;'; ?>" src="/images/Telegram-icon.png" width="16" height="16" alt="My status"/>
                    <img style="padding: 0 0.5em 0 0;" src="/images/skype_status_<?= $skype_status_2 ?>.png" width="16" height="16" alt="My status"/>АЛЕНА
                </a>
            </div>
            <div class="calc">
                <a class="link math" href="<?= Url::site('/onlinecalculator/main') ?>"><img
                            src="/<?= Kohana::config('main.path_media') ?>/img/calculator.png" border="0"/>РАССЧИТАТЬ
                    СТОИМОСТЬ<br>ДОСТАВКИ ГРУЗА ИЗ США</a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="b_search">
            <form class="frm-search" method="POST" action="<?= Url::site('search') ?>">
                <input id="search" name="search" class="tbx-search" value="<?php if (isset($search)) {
                    echo $search;
                } ?>" placeholder="Поиск по сайту" title="Введите строку поиска и нажмите Enter"/>
                <input type="submit" class="button btn-search" value="Найти"/>
            </form>
        </div>
        <div class="clear"></div>
    </div>

</div>

<div class="head-menu">
    <div class="topmenu">
        <ul class="nav">
            <li class="btn">
                <a href="<?= Url::site('/news/all#news') ?>/">Новости</a>
                <div class="sub">
                    <ul>
                        <? foreach ($news_cats as $news_cat): ?>
                            <li>
                                <a href="<?= Url::site('/news/all#news') ?>/#<?php echo $news_cat['id'] ?>">
                                    <?= $news_cat['name'] ?>
                                </a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
            </li>
            <li class="btn">
                <a href="<?= Url::site('faq') ?>/">Вопросы</a>
                <div class="sub">
                    <ul>
                        <? foreach ($faq_cats as $faq_cat): ?>
                            <li>
                                <a href="<?= Url::site('faq') ?>/#<?php echo $faq_cat['id'] ?>.1">
                                    <?= $faq_cat['name'] ?>
                                </a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
            </li>
            <li class="btn">
                <a href="<?= Url::site('useful') ?>/">Полезное</a>
                <div class="sub">
                    <ul>
                        <? foreach ($useful_cats as $useful_cat): ?>
                            <li>
                                <a href="<?= Url::site('/useful/cat/' . $useful_cat['id']) ?>">
                                    <?= $useful_cat['name'] ?>
                                </a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </div>
            </li>
            <? foreach ($menu as $_menu): ?>
                <?php if ($_menu->name): ?>
                    <li class="btn">
                        <a href="<?= Url::site('page/' . $_menu->name) ?>"><?= $_menu->title ?></a>
                        <div class="sub">
                            <ul>
                                <? foreach ($_menu->contents->order_by('order', 'ASC')->find_all() as $_submenu): ?>
                                    <?php if (!empty($_submenu->title)): ?>
                                        <li>
                                            <a href="<?= Url::site('page/' . $_menu->name . '/' . $_submenu->name) ?>"><?= $_submenu->title ?></a>
                                        </li>
                                    <?php endif; ?>
                                <? endforeach ?>
                            </ul>
                        </div>
                    </li>
                <?php endif; ?>
            <? endforeach ?>
            <li class="btn"><a target="_blank"
                               href="<?= Url::site('board') ?><? if (Auth::instance()->logged_in() && isset($_COOKIE['phpbb_sid'])): ?>?sid=<?= $_COOKIE['phpbb_sid'] ?><? endif; ?>">Форум</a>
            </li>
        </ul>
    </div>
</div>
