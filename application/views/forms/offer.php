<? if ($form->errors()): ?>
<? foreach ($form->errors() as $error): ?>
        <div align="center" class="mb4"><span class="red"><?= $error ?></span></div>
<? endforeach; ?>
<? endif; ?>

<form action="" method="POST">
    <div><?=$form->head->label()?>:</div>
    <div>
        <? if($form->head->get('editable')): ?>
        <input type="text" name="<?=$form->head->name()?>" value="<?=$form->head->val()?>">
        <? else: ?>
        <span><?=$form->head->val()?></span>
        <? endif;?>
    </div>
    <div class="bgc1">
        <div><?=$form->message->label()?>:</div>
        <divide><textarea id="tinyMCE_m" name="<?=$form->message->name()?>" class="w100p" rows="20"><?=$form->message->val()?></textarea></div><br>
    <div><input class="button" name="<?=$form->submit->name()?>" type="submit" value="<?=$form->submit->label()?>"></div>
</form>