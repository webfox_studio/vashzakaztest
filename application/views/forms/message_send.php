<? if ($form->errors()): ?>
<? foreach ($form->errors() as $error): ?>
        <div align="center" class="mb4"><span color="red"><?= $error ?></span></div>
<? endforeach; ?>
<? endif; ?>

<form action="" method="POST">
    <div class="table effect6">
        <div class="row">
            <div class="cell"><?=$form->head->label()?>:</div>
            <div class="cell f7">
                <? if($form->head->get('editable')): ?>
                <input type="text" class="w100p" name="<?=$form->head->name()?>" value="<?=$form->head->val()?>">
                <? else: ?>
                <span class="p4"><?=$form->head->val()?></span>
                <? endif;?>
            </div>
        </div>
        
        <div class="row">
            <div class="cell bgc3"><?=$form->message->label()?>:</div>
            <div class="cell"><textarea id="tinyMCE_m" name="<?=$form->message->name()?>" class="w100p" rows="20"><?=$form->message->val()?></textarea></div>
        </div>
        <div class="row">
            <div class="cell"><!--                <label><input class="mt16" type="checkbox" name="<?=$form->important->name()?>"<? if($form->important->val()): ?> checked="checked"<? endif;?> /> <?=$form->important->label()?></label>-->
            </div>
            <div class="cell ar pr8">
                <input class="button bg1 b mt16" name="preview" type="submit" value="Предварительный просмотр">
                <input class="button bg1 b mt16" name="<?=$form->submit->name()?>" type="submit" value="<?=$form->submit->label()?>">
            </div>
        </div>
    </div>
</form>