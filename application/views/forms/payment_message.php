<? if ($form->errors()): ?>
<? foreach ($form->errors() as $error): ?>
        <div align="center" class="mb4"><span class="red"><?= $error ?></span></div>
<? endforeach; ?>
<? endif; ?>
<form name="<?=$form->name()?>" action="" method="POST">
    <div>
        <div>
            <div colspan="2">
                <div>
                    <div>
                        <div><?=$form->message->label()?>:</div>
                        <div>
                                <textarea id="tinyMCE_m" name="<?=$form->message->name()?>" class="w100p" rows="20" cols="100"><?=$form->message->val()?></textarea>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div>
                <input class="button" name="preview" type="submit" value="Предварительный просмотр">
                <input class="button" name="<?=$form->submit->name()?>" type="submit" value="<?=$form->submit->label()?>">
            </div>
        </div>
    </div>
</form>