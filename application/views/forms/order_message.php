<? if ($form->errors()): ?>
<? foreach ($form->errors() as $error): ?>
        <div align="center" class="mb4"><font color="red"><?= $error ?></font></div>
<? endforeach; ?>
<? endif; ?>
<form name="<?=$form->name()?>" action="" method="POST">
    <table>
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <th><?=$form->message->label()?>:</th>
                        <td>
                                <textarea id="tinyMCE_m" name="<?=$form->message->name()?>" class="w100p" rows="20" cols="100"><?=$form->message->val()?></textarea>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="ar pr8">
                <input class="button" name="preview" type="submit" value="Предварительный просмотр">
                <input class="button" name="<?=$form->submit->name()?>" type="submit" value="<?=$form->submit->label()?>">
            </td>
        </tr>
    </table>
</form>