<? if ($form->errors()): ?>
    <? foreach ($form->errors() as $error): ?>
        <div align="center" class="mb8" style="color: red"><?= $error ?></div>
    <? endforeach; ?>
<? endif; ?>

<form method="POST" action="">

    <div>

        <div>E-mail или Логин<? //=$form->username->label()?></div>
        <div><input type="text" class="w100p" name="<?= $form->username->name() ?>"
                    value="<?= $form->username->val() ?>"></div>
        <div><?= $form->password->label() ?></div>
        <div><input type="password" class="w100p" name="<?= $form->password->name() ?>"/></div>
    </div>

    <div>
        <div><a href="/remind">Забыли пароль?</a></div>
        <label>
            <input type="checkbox" name="<?= $form->remember->name() ?>"
                   <? if ($form->remember->checked()): ?>checked="checked"<? endif; ?> value="1"/>
            <?= $form->remember->label() ?>
        </label>
        <br>
        <input class="button" type="submit" name="<?= $form->submit->name() ?>" value="Войти">
    </div>

</form>