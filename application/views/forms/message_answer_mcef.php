<? if ($form->errors()): ?>
<? foreach ($form->errors() as $error): ?>
        <div><font color="red"><?= $error ?></font></div>
<? endforeach; ?>
<? endif; ?>
<form action="" method="POST">
    <table>
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <th class="bgc3"><?=$form->message->label()?>:</th>
                        <td>
                            <textarea id="tinyMCE_f" name="<?=$form->message->name()?>" class="w100p" rows="20"><?=$form->message->val()?></textarea>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <label><input class="mt16" type="checkbox" name="<?=$form->important->name()?>"<? if($form->important->val()): ?> checked="checked"<? endif;?> /> <?=$form->important->label()?></label>
            </td>
            <td>
                <input class="button" name="preview" type="submit" value="Предварительный просмотр">
                <input class="button" name="<?=$form->submit->name()?>" type="submit" value="<?=$form->submit->label()?>">
            </td>
        </tr>
    </table>
</form>