<form name="<?= $form->name() ?>" method="post">
<? if($form->errors()):?>
<? foreach($form->errors() as $error): ?>
<div align="center" class="mb4 red"><?= $error ?></div>
<? endforeach; ?>
<? endif; ?>
<label title="Зная номер заказа, вы можете просмотреть его">
        <?= $form->order_num->label() ?>
    <input style="width: 100px" type="text" name="<?= $form->order_num->name() ?>" value="<?= $form->order_num->val() ?>" />
    <input class="button" type="submit" name="<?= $form->submit->name() ?>" value="<?= $form->submit->label() ?>" />
</label>
</form>