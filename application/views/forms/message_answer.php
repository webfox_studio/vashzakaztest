<? if ($form->errors()): ?>
<? foreach ($form->errors() as $error): ?>
        <div align="center" class="mb4"><font color="red"><?= $error ?></font></div>
<? endforeach; ?>
<? endif; ?>
<form action="" method="POST">
    <table>
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <th><?=$form->message->label()?>:</th>
                        <td><textarea id="tinyMCE_m" name="<?=$form->message->name()?>" class="w100p" rows="20"><?=$form->message->val()?></textarea></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><!--                <label><input class="mt16" type="checkbox" name="<?=$form->important->name()?>"<? if($form->important->val()): ?> checked="checked"<? endif;?> /> <?=$form->important->label()?>111111</label>-->
            </td>
            <td>
                <input class="button" name="preview" type="submit" value="Предварительный просмотр">
                <input class="button" name="<?=$form->submit->name()?>" type="submit" value="<?=$form->submit->label()?>">
            </td>
        </tr>
    </table>
</form>