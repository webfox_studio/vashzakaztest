<? if ($form->errors()):?>
    <? foreach ($form->errors() as $error):?>
<div align="center" class="mb4"><font color="red"><?=$error?></font></div>
    <? endforeach;?>
<? endif;?>
<form action="" method="post">
    <div align="center" class="mb4" style="color: blue">* заполняйте поля латиницей (a,b,c,d...x,y,z)</div>
    <input type="hidden" name="reg_step" value="4">
    <input type="hidden" name="<?= $form->email->name() ?>" value="<?= $form->email->val() ?>">
    <table align="center">
        <tr>
            <td>
                <table align="center" class="bgc2" cellspacing="1" cellpadding="1">
                    <tr class="bg1 ac">
                        <td class="ar pr4" width="160">
                            E-mail <font color="blue">*</font>:
                        </td>
                        <th class="h18" width="240">
                            <?= $form->email->value() ?>
                        </th>
                    </tr>
                    <tr class="bg1 ac">
                        <td class="ar pr4" width="160">
                            <?=$form->username->label()?> <font color="blue">*</font><span id="login_exists" style="color: red; display: none"> (занят)</span><span id="login_not_exists" style="color: green; display: none"> (свободен)</span>:
                        </td>
                        <th class="h18" width="240">
                            <input id="username" class="w100p" type="text" name="<?=$form->username->name()?>" onblur="login_exists('<?=Url::site('ajax/username_exists')?>', this.value)" value="<?= $form->username->value() ?>" />
                        </th>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Пароль <font color="blue">*</font>:
                        </td>
                        <td width="240">
                            <input class="w100p" type="password" name="<?= $form->password->name() ?>" value="" maxlength="20">
                        </td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Повтор пароля <font color="blue">*</font>:
                        </td>
                        <td width="240">
                            <input class="w100p" type="password" name="<?= $form->password2->name() ?>" value="" maxlength="20">
                        </td>
                    </tr>
                    <tr class="bgc1 ac h8">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Имя Фамилия <font color="blue">*</font>:
                        </td>
                        <td width="240">
                            <input class="w100p onlylatin" type="text" name="<?= $form->name->name() ?>" value="<?= $form->name->value() ?>">
                        </td>
                    </tr>
                    <tr class="bgc1 ac h8">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Страна:
                        </td>
                        <td width="240">
                            <? $form->country->attr('class', 'w100p')?>
                            <?=$form->country?>
                        </td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Почтовый индекс <font color="blue">*</font>:
                        </td>
                        <td width="240">
                            <input class="w100p" type="text" name="<?= $form->zip->name() ?>" value="<?= $form->zip->value() ?>" maxlength="6">
                        </td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Область / Регион <font color="blue">*</font>:
                        </td>
                        <td width="240">
                            <input class="w100p onlylatin" type="text" name="<?= $form->region->name() ?>" value="<?= $form->region->value() ?>">
                        </td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Населенный пункт <font color="blue">*</font>:
                        </td>
                        <td width="240">
                            <input class="w100p onlylatin" type="text" name="<?= $form->city->name() ?>" value="<?= $form->city->value() ?>">
                        </td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Остальной адрес <font color="blue">*</font>:
                        </td>
                        <td width="240">
                            <input class="w100p onlylatin" type="text" name="<?= $form->address->name() ?>" value="<?= $form->address->value() ?>">
                        </td>
                    </tr>
                    <tr class="bgc1 ac h8">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Контактный телефон(ы)<font color="blue">*</font>:
                        </td>
                        <td width="240">
                            <input class="w100p" type="text" name="<?= $form->phones->name() ?>" value="<?= $form->phones->value() ?>">
                        </td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            ICQ:
                        </td>
                        <td width="240">
                            <input class="w100p" type="text" name="<?= $form->icq->name() ?>" value="<?= $form->icq->value() ?>">
                        </td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Skype:
                        </td>
                        <td width="240">
                            <input class="w100p" type="text" name="<?= $form->skype->name() ?>" value="<?= $form->skype->value() ?>">
                        </td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Агент Mail.ru:
                        </td>
                        <td width="240">
                            <input class="w100p" type="text" name="<?= $form->mail_ru->name() ?>" value="<?= $form->mail_ru->value() ?>">
                        </td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Другой контакт:
                        </td>
                        <td width="240">
                            <input class="w100p" type="text" name="<?= $form->another->name() ?>" value="<?= $form->another->value() ?>">
                        </td>
                    </tr>
                    <tr class="bgc1 ac h8">
                        <td colspan="2"></td>
                    </tr>
                    <tr class="bgc1 ac">
                        <td class="bg1 ar pr4" width="160">
                            Откуда узнали о нас:
                        </td>
                        <td width="240">
                            <input class="w100p" type="text" name="<?= $form->where->name() ?>" value="<?= $form->where->value() ?>">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="right">
                <input class="bg1 b mt4" type="submit" name="<?=$form->submit->name()?>" value="Продолжить">
            </td>
        </tr>
    </table>
</form>