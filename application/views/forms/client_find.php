<form name="<?= $form->name() ?>" action="<?= $form->get('action') ?>" method="<?=$form->get('method')?>">
<? if($form->errors()):?>
<? foreach($form->errors() as $error): ?>
<div align="center" class="mb4 red"><?= $error ?></div>
<? endforeach; ?>
<? endif; ?>
<label title="Имя клиента">
    <?= $form->name->label() ?>:
    <input style="width: 400px" type="text" name="<?= $form->name->name() ?>" value="<?= $form->name->val() ?>" />
    <input type="submit" name="<?= $form->submit->name() ?>" value="<?= $form->submit->label() ?>" />
</label>
</form>