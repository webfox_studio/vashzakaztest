<? if ($form->errors()): ?>
    <? foreach ($form->errors() as $error): ?>
        <h4 class="red"><?= $error ?></h4>
    <? endforeach; ?>
<? endif; ?>

<div class="red" style="display:none;" id="email_error">Данный e-mail уже зарегистрирован</div>
<script>
    var check_email_confirm = function () {
        <?if(isset($admin_submit)):?>
        $('#form_submit').click();
        <?else:?>
        if ($('#email_confirm').val() != $('#email_confirm').attr('old_email')) {
            $.ajax({
                url: '/ajax/is_confirmed',
                data: 'email=' + $('#email_confirm').val(),
                type: "POST",
                success: function (msg) {
                    ms = eval('(' + msg + ')');
                    if (ms == true) {
//                        $('#change_info_form').submit();
                        $('#form_submit').click();
                        return true;
                    } else {
                        $('#email_error').show();
                        return false;
                    }
                }
            });
        } else {
//            $('#change_info_form').submit();
            $('#form_submit').click();
            return true;
        }
        return false;
        <?endif?>
    }
</script>
<form action="" method="post" id="change_info_form">
    <input type="hidden" name="<?= $form->username->name() ?>" value="<?= $form->username->val() ?>"/>
    <div class="t-wrapper">
        <div class="table">
            <div class="thhead c100"><h4><span class="red">*</span> заполняйте поля латиницей (a,b,c,d...x,y,z)</h4>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->email->label() ?> <span class="red">*</span>:</div>
                <div class="cell"><input class="w100p onlylatin" type="text" old_email="<?= $form->email->value() ?>"
                                         id="email_confirm" type="text" name="<?= $form->email->name() ?>"
                                         value="<?= $form->email->value() ?>"></div>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->username->label() ?>:</div>
                <div class="cell"><b class="red"><?= $form->username->val() ?></b></div>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->name->label() ?> <span class="red">*</span>:</div>
                <div class="cell"><input class="w100p onlylatin" type="text" name="<?= $form->name->name() ?>"
                                         value="<?= $form->name->value() ?>"></div>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->country->label() ?>:</div>
                <div class="cell">
                    <? $form->country->attr('class', 'w100p') ?>
                    <?= $form->country ?>
                </div>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->zip->label() ?>:</div>
                <div class="cell"><input class="w100p" type="text" name="<?= $form->zip->name() ?>"
                                         value="<?= $form->zip->value() ?>" maxlength="6"></div>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->region->label() ?> <span class="red">*</span>:</div>
                <div class="cell"><input class="w100p onlylatin" type="text" name="<?= $form->region->name() ?>"
                                         value="<?= $form->region->value() ?>"></div>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->city->label() ?> <span class="red">*</span>:</div>
                <div class="cell"><input class="w100p onlylatin" type="text" name="<?= $form->city->name() ?>"
                                         value="<?= $form->city->value() ?>"></div>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->address->label() ?> <span class="red">*</span>:</div>
                <div class="cell"><input class="w100p onlylatin" type="text" name="<?= $form->address->name() ?>"
                                         value="<?= $form->address->value() ?>"></div>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->phones->label() ?>:</div>
                <div class="cell"><input class="w100p" type="text" name="<?= $form->phones->name() ?>"
                                         value="<?= $form->phones->value() ?>"></div>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->icq->label() ?>:</div>
                <div class="cell"><input class="w100p" type="text" name="<?= $form->icq->name() ?>"
                                         value="<?= $form->icq->value() ?>"></div>
            </div>
            <div class="row">
                <div class="cell c50"><?= $form->skype->label() ?>:</div>
                <div class="cell"><input class="w100p" type="text" name="<?= $form->skype->name() ?>"
                                         value="<?= $form->skype->value() ?>"></div>
            </div>
<!--            <div class="row">-->
<!--                <div class="cell c50">--><?//= $form->mail_ru->label() ?><!--:</div>-->
<!--                <div class="cell"><input class="w100p" type="text" name="--><?//= $form->mail_ru->name() ?><!--"-->
<!--                                         value="--><?//= $form->mail_ru->value() ?><!--"></div>-->
<!--            </div>-->
            <div class="row">
                <div class="cell c50"><?= $form->another->label() ?>:</div>
                <div class="cell"><input class="w100p" type="text" name="<?= $form->another->name() ?>"
                                         value="<?= $form->another->value() ?>"></div>
            </div>
            <div class="row <? if (isset($form->admin_mode)): ?>hide<? endif ?>">
                <div class="cell c50"><span class="red"><?= $form->delete->label() ?>:</span></div>
                <div class="cell"><input class="w100p" type="checkbox" name="<?= $form->delete->name() ?>"
                                         value="<?= $form->delete->value() ?>"></div>
            </div>
            <div class="row <? if (isset($form->admin_mode)): ?>hide<? endif ?>">
                <div class="cell c50" style="vertical-align: top;"><?= $form->cause->label() ?>:</div>
                <div class="cell"><textarea class="w100p"
                                            name="<?= $form->cause->name() ?>"><?= $form->cause->value() ?></textarea>
                </div>
            </div>
        </div>
        <div class="fr">
            <input class="bg1 b mt4" type="submit" id="form_submit" style="display:none;"
                   name="<?= $form->submit->name() ?>" value="<?= $form->submit->label() ?>">
            <input class="button" type="button" onClick="return check_email_confirm();"
                   name="<?= $form->submit->name() ?>" value="<?= $form->submit->label() ?>"></div>

    </div>
</form>

