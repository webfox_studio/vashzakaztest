<? if ($form->errors()): ?>
<? foreach ($form->errors() as $error): ?>
        <div align="center" class="mb4"><font color="red"><?= $error ?></font></div>
<? endforeach; ?>
<? endif; ?>
<form action="" method="POST">
    <table class="w100p" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="2">
                <table align="center" class="bgc2 w100p" cellspacing="1" cellpadding="1">
                    <tr class="bgc1">
                        <th class="bg1" width="120"><?=$form->head->label()?>:</th>
                        <td class="f7">
                            <? if($form->head->get('editable')): ?>
                            <input type="text" class="w100p" name="<?=$form->head->name()?>" value="<?=$form->head->val()?>">
                            <? else: ?>
                            <span class="p4"><?=$form->head->val()?></span>
                            <? endif;?>
                        </td>
                    </tr>
                    <tr class="bgc1">
                        <th class="bgc3"><?=$form->message->label()?>:</th>
                        <td>
                                <textarea id="tinyMCE_f" name="<?=$form->message->name()?>" class="w100p" rows="20"><?=$form->message->val()?></textarea>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="al pl8">
                <label><input class="mt16" type="checkbox" name="<?=$form->important->name()?>"<? if($form->important->val()): ?> checked="checked"<? endif;?> /> <?=$form->important->label()?></label>
            </td>
            <td class="ar pr8">
                <input class="bg1 b mt16" name="preview" type="submit" value="Предварительный просмотр">
                <input class="bg1 b mt16" name="<?=$form->submit->name()?>" type="submit" value="<?=$form->submit->label()?>">
            </td>
        </tr>
    </table>
</form>