<? if ($form->errors()): ?>
<? foreach ($form->errors() as $error): ?>
        <div><?= $error ?></div>
<? endforeach; ?>
<? endif; ?>
<form action="" method="POST">
    <label>
        <input class="mt16" type="checkbox" name="<?= $form->allow->name() ?>"<? if ($form->allow->val()): ?> checked="checked"<? endif; ?> />
        <?= $form->allow->label() ?>
    </label><br />
    <input class="bg1 b mt16" name="<?= $form->submit->name() ?>" type="submit" value="<?= $form->submit->label() ?>">
</form>