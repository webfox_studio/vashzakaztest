<style type="text/css">
	.carousel_wrap  {
margin: 20px auto;
width:700px;
height:400px;
position:relative;
}
.visual_block {
margin: 0 auto;
height:100%;
position: relative;
overflow: hidden;
}
.visual_block ul {
position: relative;
}
.visual_block ul, .visual_block li {
height:100%;
float: left;
position: relative;
}
.visual_block li {
width:640px;
}

.carousel_wrap span.next, .carousel_wrap span.prev {
margin-top:-20px;
width:15px;
height:26px;
display:block;
text-indent:-9999px;
overflow:hidden;
cursor:pointer;
background:url(/upload/slider2_arrow.png) no-repeat;
position:absolute;
top:50%;
}
.carousel_wrap span.next {
right:0;
background-position:-15px 0;
}
.carousel_wrap span.next:hover {
background-position:-15px -26px;
}
.carousel_wrap span.prev:hover {
background-position:0 -26px;
}

.text {
padding:20px 10px;
width:97%;
font-size:14px;
color:#ff0000;
position:absolute;
bottom:0;
left:0;
background-color:#ccc;
opacity:0.7;
filter: alpha(opacity=70);
}




.slider_wrap {
	margin:100px auto 0;
	width:640px;
	height:400px;
	position:relative;
	overflow:hidden;
}
.slider_wrap img {
	width:100%;
	height:auto;
	display:none;
	position:absolute;
	top:0;
	left:0;	
}
.slider_wrap img:first-child {
	display:block;
}
</style>

<div>
	<div class="carousel_wrap">	
		<!-- <span class="prev">prev</span>
		<span class="next">next</span>	 -->	
		<div class="visual_block">
			<ul>
				<li>
					<img src="/upload/1.jpg" alt="" />
					<div class="text">Подпись к фото 1</div>
				</li>
				<li>
					<img src="/upload/2.jpg" alt="" />
					<div class="text">Подпись к фото 2</div>
				</li>
				<li>
					<img src="/upload/3.jpg" alt="" />
					<div class="text">Подпись к фото 3</div>
				</li>
				<li>
					<img src="/upload/4.jpg" alt="" />
					<div class="text">Подпись к фото 4</div>
				</li>
			</ul>
		</div>		
	</div>	
	
	<div id="slider" class="slider_wrap">
		<img src="/upload/1.jpg" alt="" />
		<img src="/upload/2.jpg" alt="" />
		<img src="/upload/3.jpg" alt="" />
		<img src="/upload/4.jpg" alt="" />
	</div>
	<div id="sliderPagination">
	
	</div>
</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){
		var elWrap = $('.carousel_wrap');
		var	visual = $('.visual_block');
		var	carousel = visual.children('ul');
		var	visible = 1;
			itemWidth = carousel.children().outerWidth(),
			itemsTotal = carousel.children().length,
			autoChange = 1000,
			btnNext = $('.next'),
			btnPrev = $('.prev');

		visual.css({'width': visible * itemWidth + 'px'});
		
		carousel.css({'width': itemsTotal * itemWidth,	'left': 0});
		
		function chengeLeft () {
			var item = carousel.children().eq(0);
			// btnNext.off('click', chengeLeft);	
			carousel.animate({left: -itemWidth}, 500, function() {
				item.appendTo(carousel);		
				carousel.css({"left": 0 });	
				// btnNext.on('click', chengeLeft);
			});
		}	
		
		function chengeRigth () {
			var item = $(carousel).children().eq(-1);
			item.prependTo(carousel);
			carousel.css({"left": -itemWidth});		
			// btnPrev.off('click', chengeRigth);		
			carousel.animate({left: 0}, 500, function() {
				// btnPrev.on('click', chengeRigth);
			});
		}	
		
		var interval = setInterval(chengeLeft, autoChange);

		// btnNext.on('click', chengeLeft);	
		// btnPrev.on('click', chengeRigth);	
		
		elWrap.mouseover(function() {
			clearInterval(interval);
		});
		
		elWrap.mouseout(function() {
			interval = setInterval(chengeLeft, autoChange);
		});	
		
		
		
		var el =  $('#slider img'),
			indexImg = 1,
			indexMax = el.length;
			
		function autoCange () {	
			indexImg++;
			if(indexImg > indexMax) {
				indexImg = 1;
			}			
			el.fadeOut(500);
			el.filter(':nth-child('+indexImg+')').fadeIn(500);
		}	
		setInterval(autoCange, autoChange);

	});
	$(function () {
			
	});
</script>