<style type="text/css">
	.slider  {
		margin: 20px auto;
		width:700px;
		height:400px;
		position:relative;
	}
	.slider1 {
		margin: 0 auto;
		height:100%;
		position: relative;
		overflow: hidden;
	}
	.slider1 ul {
		position: relative;
	}
	.slider1 ul, .slider1 li {
		height:100%;
		float: left;
		position: relative;
	}
	.slider1 li {
		width:640px;
	}
	
	.slider2 {
		margin:100px auto 0;
		width:640px;
		height:400px;
		position:relative;
		overflow:hidden;
	}
	.slider2 .content_text {
		width:100%;
		height:auto;
		display:none;
		position:absolute;
		top:0;
		left:0;	
	}
	.slider2 .content_text:first-child {
		display:block;
	}
	.paginationSlider p{
		float: left;
		padding: 0px 5px 0px 5px;
		border: 1px solid gray;
		margin: 3px;
	}
</style>

<div>
	<div class="slider">
		<div class="slider1">
			<ul>
				<li>
					<img src="/upload/1.jpg" alt="" />
				</li>
				<li>
					<img src="/upload/2.jpg" alt="" />
				</li>
				<li>
					<img src="/upload/3.jpg" alt="" />
				</li>
				<li>
					<img src="/upload/4.jpg" alt="" />
				</li>
			</ul>
		</div>
		<div class="slider2">
			<div class="content_text">11111</div>
			<div class="content_text">22222</div>
			<div class="content_text">33333</div>
			<div class="content_text">44444</div>
		</div>
		<div class="paginationSlider"></div>
	</div>
</div>



<script type="text/javascript">
	$(document).ready(function(){
		var elWrap = $('.slider');
		var time = 2000;
		var timeSlide = 1000;
		// slider 1
		var	visible = 1;
		var	visual = $('.slider1');
		$('.slider1').children('ul')
		var	carousel = visual.children('ul');
		itemWidth = carousel.children().outerWidth();
		itemsTotal = carousel.children().length;
		
		visual.css({'width': visible * itemWidth + 'px'});
		carousel.css({'width': itemsTotal * itemWidth,	'left': 0});
		
		// slider 2
		var el =  $('.slider2 .content_text'),
			indexImg = 1,
			indexMax = el.length;
			
		function activeSlider(key){
			// console.log('func-'+key); 
			var item = carousel.children().eq(0);	
			// slider 1
			//item.appendTo(carousel);
			
			carousel.animate({left: -itemWidth}, timeSlide, function() {
				item.appendTo(carousel);		
				carousel.css({"left": 0 });	
			});
			
			// slider 2
			indexImg++;
			
			if(indexImg > indexMax) {
				indexImg = 1;
			}			
			console.log('indexImg-'+indexImg);
			el.fadeOut(timeSlide);
			if(key){
				indexImg = key;
			}
			el.filter(':nth-child('+indexImg+')').fadeIn(timeSlide);
		}
		
		
		var interval = setInterval(activeSlider, time);
		
		elWrap.mouseover(function() {
			clearInterval(interval);
		});
		
		elWrap.mouseout(function() {
			interval = setInterval(activeSlider, time);
		});	
		var pag = $(".paginationSlider");
		$(".slider2 .content_text").each(function(key, val){
			// $(this).attr("data-number", key);
			key++;
			pag.append("<p data-num="+key+">"+key+"</p>")
			
		})
		// $(".paginationSlider").append(pag)
		
		$(".paginationSlider p").click(function(){
			activeSlider($(this).attr("data-num"))
			// console.log($(this).attr("data-num"))
		})
			
		
		
	});
	$(function () {
			
	});
</script>