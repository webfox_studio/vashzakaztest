<style type="text/css">
    .slider-box{
        position:relative;
        width:640px;
        height:400px;
        overflow:hidden;
    }
    .slider{
        position:relative;
        width:10000px;
        height:210px;
    }
    .slider img{
        float:left;
		width: 640px;
		height: 400px;
    }
    .slider-box .prev, .slider-box .next{
        position:absolute;
        top:100px;
        display:block;
        width:29px;
        height:29px;
        cursor:pointer;
    }
    .slider-box .prev{
        left:10px;
        background:url(/upload/slider_controls.png) no-repeat 0 0;
    }
    .slider-box .next{
        right:10px;
        background:url(/upload/slider_controls.png) no-repeat -29px 0;
    }
    .bullets{
        /* position:absolute; */
        bottom:10px;
        left:130px;
        height:11px;
        padding:0;
        margin:0;
        list-style:none;
    }
    .bullets li{
        width:11px;
        height:11px;
        margin:0 3px 0 0;
        padding:0;
        float:left;
        background:url(/upload/slider_controls.png) no-repeat 0 -30px;
    }
    .bullets li.active{
        background-position:0 -42px;
    }
	.bullet:hover{
		cursor: pointer;
	}

    .slider2 {
        width:640px;
        height:400px;
        position:relative;
        overflow:hidden;
    }
    .slider2 .content_text {
        width:100%;
        height:auto;
        display:none;
        position:absolute;
        top:0;
        left:0;
    }
    .slider2 .content_text:first-child {
        display:block;
    }

</style>


<div class="slider-box">
    <div class="slider">
        <img src="/upload/1.jpg" alt="">
        <img src="/upload/2.jpg" alt="">
        <img src="/upload/3.jpg" alt="">
        <img src="/upload/4.jpg" alt="">
    </div>

    <!-- <div class="prev"></div>
    <div class="next"></div> -->
</div>
<div class="slider2">
    <div class="content_text"><p>зеленый зеленый зеленый зеленый зеленый зеленый зеленый зеленый </p><p>зеленый зеленый зеленый зеленый зеленый зеленый зеленый зеленый </p><p>зеленый зеленый зеленый зеленый зеленый зеленый зеленый зеленый </p><p>зеленый зеленый зеленый зеленый зеленый зеленый зеленый зеленый </p><p>зеленый зеленый зеленый зеленый зеленый зеленый зеленый зеленый </p></div>
    <div class="content_text"><p>желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый </p><p>желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый </p><p>желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый </p><p>желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый </p><p>желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый </p><p>желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый желтый </p></div>
    <div class="content_text"><p>синий синий синий синий синий синий синий синий синий синий синий синий синий </p><p>синий синий синий синий синий синий синий синий синий синий синий синий синий </p><p>синий синий синий синий синий синий синий синий синий синий синий синий синий </p><p>синий синий синий синий синий синий синий синий синий синий синий синий синий </p><p>синий синий синий синий синий синий синий синий синий синий синий синий синий </p></div>
    <div class="content_text"><p>красный красный красный красный красный красный красный красный красный красный </p><p>красный красный красный красный красный красный красный красный красный красный </p><p>красный красный красный красный красный красный красный красный красный красный </p><p>красный красный красный красный красный красный красный красный красный красный </p><p>красный красный красный красный красный красный красный красный красный красный </p><p>красный красный красный красный красный красный красный красный красный красный </p></div>
</div>

<div class="slider-box">
    <ul class="bullets"></ul>
</div>

<script type="text/javascript">
    $(function() {

        // slider 2
        var el =  $('.slider2 .content_text'),
            indexImg = 1,
            indexMax = el.length;

        var slider = $('.slider'),
            sliderContent = slider.html(),                      // Содержимое слайдера
            slideWidth = $('.slider-box').outerWidth(),         // Ширина слайдера
            slideCount = $('.slider img').length,               // Количество слайдов
            prev = $('.slider-box .prev'),                      // Кнопка "назад"
            next = $('.slider-box .next'),                      // Кнопка "вперед"
            slideNum = 1,                                       // Номер текущего слайда
            index =0,
            clickBullets=0,
            sliderInterval = 3300,                              // Интервал смены слайдов
            animateTime = 1000,                                 // Время смены слайдов
            course = 1,                                         // Направление движения слайдера (1 или -1)
            margin = - slideWidth;                              // Первоначальное смещение слайдов

        for (var i=0; i<slideCount; i++)                      // Цикл добавляет буллеты в блок .bullets
        {
            html=$('.bullets').html() + '<li></li>';          // К текущему содержимому прибавляется один буллет
            $('.bullets').html(html);                         // и добавляется в код
        }
        var  bullets = $('.slider-box .bullets li')          // Переменная хранит набор буллитов


        $('.slider-box .bullets li:first').addClass('active');
        $('.slider img:last').clone().prependTo('.slider');   // Копия последнего слайда помещается в начало.
        $('.slider img').eq(1).clone().appendTo('.slider');   // Копия первого слайда помещается в конец.  
        $('.slider').css('margin-left', -slideWidth);         // Контейнер .slider сдвигается влево на ширину одного слайда.

        function nextSlide(){                                 // Запускается функция animation(), выполняющая смену слайдов.

            interval = window.setInterval(animate, sliderInterval);
        }

        function animate(indexA){
            if (margin==-slideCount*slideWidth-slideWidth  && course==1){     // Если слайдер дошел до конца
                slider.css({'marginLeft':-slideWidth});           // то блок .slider возвращается в начальное положение
                margin=-slideWidth*2;
            }else if(margin==0 && course==-1){                  // Если слайдер находится в начале и нажата кнопка "назад"
                slider.css({'marginLeft':-slideWidth*slideCount});// то блок .slider перемещается в конечное положение
                margin=-slideWidth*slideCount+slideWidth;
            }else{                                              // Если условия выше не сработали,
                margin = margin - slideWidth*(course);            // значение margin устанавливается для показа следующего слайда
            }
            slider.animate({'marginLeft':margin},animateTime);  // Блок .slider смещается влево на 1 слайд.

            if (clickBullets==0){                               // Если слайдер сменился не через выбор буллета
                bulletsActive();                                // Вызов функции, изменяющей активный буллет
            }else{                                              // Если слайдер выбран с помощью буллета
                slideNum=index+1;                               // Номер выбранного слайда
            }
			
            // slider 2
            el.fadeOut(1000);   
			console.log(slideNum)
            // el.eq(slideNum).fadeIn(1000);
			el.filter(':nth-child('+slideNum+')').fadeIn(1000);
        }

        function bulletsActive(){
            if (course==1 && slideNum!=slideCount){        // Если слайды скользят влево и текущий слайд не последний
                slideNum++;                                     // Редактирунтся номер текущего слайда
                $('.bullets .active').removeClass('active').next('li').addClass('active'); // Изменить активный буллет
            }else if (course==1 && slideNum==slideCount){       // Если слайды скользят влево и текущий слайд последний
                slideNum=1;                                     // Номер текущего слайда
                $('.bullets li').removeClass('active').eq(0).addClass('active'); // Активным отмечается первый буллет
                return false;
            }else if (course==-1  && slideNum!=1){              // Если слайды скользят вправо и текущий слайд не последни
                slideNum--;                                     // Редактирунтся номер текущего слайда
                $('.bullets .active').removeClass('active').prev('li').addClass('active'); // Изменить активный буллет  
                return false;
            }else if (course==-1  && slideNum==1){              // Если слайды скользят вправо и текущий слайд последни
                slideNum=slideCount;                            // Номер текущего слайда
                $('.bullets li').removeClass('active').eq(slideCount-1).addClass('active'); // Активным отмечается последний буллет
            }
        }

        function sliderStop(){                                // Функция преостанавливающая работу слайдера      
            window.clearInterval(interval);
        }

        prev.click(function() {                               // Нажата кнопка "назад"
            if (slider.is(':animated')) { return false; }       // Если не происходит анимация
            var course2 = course;                               // Временная переменная для хранения значения course
            course = -1;                                        // Устанавливается направление слайдера справа налево
            animate();                                          // Вызов функции animate()
            course = course2 ;                                  // Переменная course принимает первоначальное значение
        });
        next.click(function() {                               // Нажата кнопка "назад"
            if (slider.is(':animated')) { return false; }       // Если не происходит анимация
            var course2 = course;                               // Временная переменная для хранения значения course
            course = 1;                                         // Устанавливается направление слайдера справа налево
            animate();                                          // Вызов функции animate()
            course = course2 ;                                  // Переменная course принимает первоначальное значение
        });
        bullets.click(function() {                            // Нажат один из буллетов
            if (slider.is(':animated')) { return false; }       // Если не происходит анимация  
            sliderStop();                                       // Таймер на показ очередного слайда выключается
            index = bullets.index(this);                        // Номер нажатого буллета
			// console.log(index)
            if (course==1){                                     // Если слайды скользят влево
                margin=-slideWidth*index;                       // значение margin устанавливается для показа следующего слайда
            }else if (course==-1){                              // Если слайды скользят вправо
                margin=-slideWidth*index-2*slideWidth;
            }
            $('.bullets li').removeClass('active').eq(index).addClass('active');  // Выбранному буллету добавляется сласс .active
            clickBullets=1;                                     // Флаг информирующий о том, что слайд выбран именно буллетом
            animate(index);
            clickBullets=0
			
			//el.fadeOut(1000);
			//el.filter(':nth-child('+index+')').fadeIn(1000);
			
        });

        slider.add(next).add(prev).hover(function() {         // Если курсор мыши в пределах слайдера
            sliderStop();                                       // Вызывается функция sliderStop() для приостановки работы слайдера
        }, nextSlide);                                        // Когда курсор уходит со слайдера, анимация возобновляется.

        nextSlide();                                          // Вызов функции nextSlide()
    });
</script>