<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Email from VashZakaz.US</title>
</head>

<body bgcolor="747474" >
<table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" align="center" >

    <tr>
        <td>
            <center>
                <table id="top-message" cellpadding="10" cellspacing="2" width="600" align="center" border="0" bgcolor="7587AF" >
    <tr height = "100" bgcolor="F4F4F4" >
        <td colspan="2" bgcolor="F4F4F4">
            <table id = "head"  cellpadding="5" cellspacing="0" align="center" width="100%" border="0" style="background-image: url(http://vashzakaz.us/media/img/bg_city.png);">
                <tr>
                    <td style="padding-left: 10px;">
                        <a href="http://vashzakaz.us"><img src="http://vashzakaz.us/media/img/logo.png" width="160px" style="float: left; display:block;"></a>
                    </td>
                    <td style="background-image: url(http://vashzakaz.us/media/img/header.png); background-repeat: no-repeat; background-size: 386px; background-position: bottom;">
                        <h1 align="center" style="color: #BF1813; font-size: 24px; padding-bottom: 17px; font-family: Verdana, sans-serif; font-weight: bold;">Мечта с доставкой на дом!</h1>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td width="220px" style="text-align: center; vertical-align: top; border: 0; padding-left: 10px;" bgcolor="7587AF" ><a name="contents"></a>
            <table border="0">
                <tr>
                    <td>
                        <hr color="556697" size="0.5" />
                        <p align="center"><a href="#text-1" style="text-decoration: none; color: #ffffff; font-size: 16px; text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif;">Вход в клиентский кабинет</a></p>

                        <hr color="556697" size="0.5" />
                        <p align="center"><a href="#text-2" style="text-decoration: none; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif;">Запрос в службу поддержки</a></p>

                        <hr color="556697" size="0.5" />
                        <p align="center"><a href="#text-3" style="text-decoration: none; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif;">Выкуп заказов через сотрудников сервиса</a></p>

                        <hr color="556697" size="0.5" />
                        <p align="center"><a href="#text-4" style="text-decoration: none; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif;">Внесение предоплаты или оплаты</a></p>

                        <hr color="556697" size="0.5" />
                        <p align="center"><a href="#text-5" style="text-decoration: none; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif;">Получение адреса для самостоятельных покупок</a></p>

                        <hr color="556697" size="0.5" />
                        <p align="center"><a href="#text-6" style="text-decoration: none; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif;">Оформление отправки товаров</a></p>

                        <hr color="556697" size="0.5" />
                        <p align="center"><a href="#text-7" style="text-decoration: none; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif;">Предварительные расчеты стоимости</a></p>

                        <hr color="556697" size="0.5" />
                        <p align="center"><a href="#text-8" style="text-decoration: none; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif;">Просмотр отчетности по сотрудничеству</a></p>

                        <hr color="556697" size="0.5" />
                        <p align="center"><a href="#text-9" style="text-decoration: none; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif;">Другие важные ссылки</a></p>

                        <hr color="556697" size="1" />


                        <p align="center" style="color: #000; padding-top: 5px; font-size: 20px; text-shadow: none; font-weight: bold; text-shadow: 1px 1px 2px #afaeae; font-family: 'Times New Roman', Times, serif;">Мы в социальных сетях</p>
                        <table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="padding-left: 15px;" >
                            <tr>
                                <td>
                                    <a href="https://vk.com/club10603912"><img src="http://vashzakaz.us/images/25-01.png" height="60px" align="left" style="display:block;"></a>
                                </td>
                                <td>
                                    <a href="https://www.facebook.com/VashZakaz-153129784732527/"><img src="http://vashzakaz.us/images/25-03.png" height="60px" align="left" style="display:block;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="https://twitter.com/VashZakazUS"><img src="http://vashzakaz.us/images/25-02.png" height="60px" align="left" style="padding-top: 20px; vertical-align: middle; display: block;"></a>
                                </td>
                                <td>
                                    <a href="http://vashzakaz.livejournal.com/382.html"><img src="http://vashzakaz.us/images/25-04.png" height="60px" align="left" style="padding-top: 20px; vertical-align: middle; display: block;"></a>
                                </td>
                            </tr>
                        </table>



                        <hr color="556697" size="1" />
                        <p align="center" style="color: #000; padding-top: 15px; font-size: 20px; font-weight: bold; text-shadow: 1px 1px 2px #afaeae; font-family: 'Times New Roman', Times, serif;">График работы<br/> (по московскому времени):</p>

                        <p style="text-align: left; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif; line-height: 20px;">Звонки и час в Skype:</p>
                        <p style="text-align: right; color: #000; font-size: 13px;  text-shadow: none; font-family: 'Times New Roman', Times, serif; font-weight: bold; line-height: 25px;">Пн-Пт с 19:00 до 24:00</p>

                        <p style="text-align: left; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif; line-height: 20px;">Ответы на сайте:</p>
                        <p style="text-align: right; color: #000; font-size: 13px;  text-shadow: none; font-family: 'Times New Roman', Times, serif;  font-weight: bold; line-height: 25px;">Пн-Сб с 17:00 до 8:00</p>

                        <p style="text-align: left; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif; line-height: 20px;">Ответы по почте:</p>
                        <p style="text-align: right; color: #000; font-size: 13px;  text-shadow: none; font-family: 'Times New Roman', Times, serif; font-weight: bold; line-height: 25px;">Пн-Сб с 17:00 до 8:00</p>

                        <p style="text-align: left; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif; line-height: 20px;">Ответы на форумах:</p>
                        <p style="text-align: right; color: #000; font-size: 13px;  text-shadow: none; font-family: 'Times New Roman', Times, serif; font-weight: bold; line-height: 25px;">Пн-Сб с 17:00 до 8:00</p>

                        <p style="text-align: left; color: #ffffff; font-size: 16px;  text-shadow: 1px 1px 2px black; font-family: 'Times New Roman', Times, serif; line-height: 20px;">Ответы в соц. сетях:</p>
                        <p style="text-align: right; color: #000; font-size: 13px;  text-shadow: none; font-family: 'Times New Roman', Times, serif; font-weight: bold; line-height: 25px;">Пн-Сб с 17:00 до 8:00</p>

                        <table border="0" width="100%" cellpadding="0" cellspacing="1" align="center" bgcolor="3e527c">
                            <tr>
                                <td align = "center" bgcolor="#556697" style="padding-top: 7px; padding-bottom: 7px; padding-left: 15px; padding-right: 15px;">
                                    <a href="http://vashzakaz.us/page/contacts" style="text-align: center; color: #000; font-size: 17px;  text-shadow: none; font-family: 'Times New Roman', Times, serif; font-weight: bold;">  Cвязаться  </a>
                                </td>
                            </tr>
                        </table>

                        <hr color="556697" size="1" />
                    </td>
                </tr>
            </table>

        </td>

        <td width="370px" bgcolor="#F4F4F4" >
            <table border="0" cellspacing="10" cellpadding="0">
                <tr>
                    <td>
                        <p style="font-size: 18px; text-align: center; font-family: 'Times New Roman', Times, serif; color: #000;"><b>Здравствуйте<?=(isset($username)) ? ', ' . $username : ''; ?></b></p>
                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Рады приветствовать Вас с успешным завершением регистрации в компании VashZakaz.us!</p>
                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">С момента регистрации на сайте <a href="http://vashzakaz.us" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us</a> Вам доступен личный клиентский кабинет и другие инструменты для длительного успешного сотрудничества по покупкам и доставке товаров из США. </p>
                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Данное письмо поможет Вам сориентироваться в начале работы, потому настоятельно рекомендуем на первых порах сохранить его и при необходимости использовать в качестве небольшого путеводителя по личному кабинету на сайте.</p>
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td><a name="text-1" id="text-1"></a>
                        <h3 align="center" style="color: #000000; font-size: 18px; font-family: 'Times New Roman', Times, serif; font-weight: bold;" ><b>Вход в клиентский кабинет</b></h3>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Для входа в сам кабинет необходимо использовать Вашу электронную почту (ту же, что и при регистрации на сайте) в качестве логина и созданный при регистрации пароль, который рекомендуем изначально сохранить в надежном месте.</p>

                        <p style="text-align: right; font-size: 14px; font-family: 'Times New Roman', Times, serif;"><a href="#contents" style="text-decoration: none; color: #BF1813; text-decoration: underline; font-weight: bold; font-size: 14px;">К оглавлению...</a></p>
                    </td>
                </tr>

                <tr>
                    <td><a name="text-2" id="text-2"></a>
                        <h3 align="center" style="color: #000000; font-size: 18px; font-family: 'Times New Roman', Times, serif; font-weight: bold;"><b>Запрос в службу поддержки</b></h3>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Для выяснения любых возникших вопросов Вы можете использовать личные сообщения на сайте – пункт меню в кабинете  <b>«Отправить сообщение»</b> либо прямая ссылка ниже: <a href="http://vashzakaz.us/messages/send" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/messages/send.</a> Также Вы можете воспользоваться для этой цели контактами на странице службы поддержки: <a href="http://vashzakaz.us/page/contacts" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/page/contacts.</a></p>

                        <p style="text-align: right; font-size: 14px; font-family: 'Times New Roman', Times, serif;"><a href="#contents" style="text-decoration: none; color: #BF1813; text-decoration: underline; font-weight: bold; font-size: 14px;">К оглавлению...</a></p>
                    </td>
                </tr>

                <tr>
                    <td><a name="text-3" id="text-3"></a>
                        <h3 align="center" style="color: #000000; font-size: 18px; font-family: 'Times New Roman', Times, serif; font-weight: bold;"><b>Выкуп заказов через сотрудников сервиса</b></h3>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Для оформления заказа на выкуп через наших сотрудников (услуга <b>«Полного сопровождения»</b>) желательно вначале ознакомиться с расценками:
                            <a href="http://vashzakaz.us/page/tariffs" style="text-decoration: underline; color: #092a84;" >http://vashzakaz.us/page/tariffs.</a> И только затем оформлять заказ в личном клиентском кабинете - пункт меню в кабинете <b> «Оформить заказ»</b> либо прямая ссылка ниже:
                            <a href="http://vashzakaz.us/orders/new" style="text-decoration: underline; color: #092a84;" >http://vashzakaz.us/orders/new.</a></p>

                        <p style="text-align: right;"><a href="#contents" style="text-decoration: none; color: #BF1813;text-decoration: underline; font-weight: bold; font-family: 'Times New Roman', Times, serif; font-size: 14px;">К оглавлению... </a></p>
                    </td>
                </tr>

                <tr>
                    <td><a name="text-4" id="text-4"></a>
                        <h3 align="center" style="color: #000000; font-size: 18px; font-family: 'Times New Roman', Times, serif; font-weight: bold;"><b>Внесение предоплаты или оплаты</b></h3>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Для внесения нужной суммы предоплаты на выкуп заказа желательно вначале ознакомиться с видами принимаемых денежных переводов и их комиссиями:</p>
                        <p style="font-size: 14px; text-align: center; color: #000; font-family: 'Times New Roman', Times, serif;">
                            <a href="http://vashzakaz.us/page/moneytransfer" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/page/moneytransfer.</a></p>
                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Для запроса счёта к оплате через Paypal - отпишите в отдельном сообщении на сайте, либо просто оформляйте сам заказ и сотрудники выставят Вам необходимый к предоплате инвойс после просмотра заказа.</p>
                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Для всех остальных видов оплат никаких предварительных инвойсов не требуется. Вы просто отправляете нам желательную или рекомендованную сотрудником сумму депозита на свой личный счет в нашем сервисе и затем заполняете форму денежного перевода на сайте - пункт меню в кабинете  <b>«Сообщить о переводе»</b> либо прямая ссылка ниже:
                            <a href=" http://vashzakaz.us/payments/new" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/payments/new.</a></p>

                        <p style="text-align: right;"><a href="#contents" style="text-decoration: none; color: #BF1813; text-decoration: underline; font-weight: bold; font-family: 'Times New Roman', Times, serif; font-size: 14px;">К оглавлению...</a></p>
                    </td>
                </tr>

                <tr>
                    <td><a name="text-5" id="text-5"></a>
                        <h3 align="center" style="color: #000000; font-size: 18px; font-family: 'Times New Roman', Times, serif; font-weight: bold;"><b>Получение адреса для самостоятельных покупок</b></h3>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Для предоставления Вам услуги виртуального склада и адреса для отправки самостоятельно выкупленных заказов вначале рекомендуем детально ознакомиться со следующей информацией на сайте: </p>

                        <p style="font-size: 14px; text-align:center; color: #000; font-family: 'Times New Roman', Times, serif;"><a href="http://vashzakaz.us/page/mforwarding/mainmailf" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/page/mforwarding/mainmailf.</a>


                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Информация по запрещенным к доставке товарам и другим ограничениям международной пересылки размещена по нижеследующим ссылкам: </p>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">USPS:<br/>
                            <a href="http://vashzakaz.us/additionalpage/uspsrestrictions" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/additionalpage/uspsrestrictions</a> </p>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Cargo:<br/>
                            <a href="http://vashzakaz.us/additionalpage/cargorestrictions" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/additionalpage/cargorestrictions</a> </p>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Econom:<br/>
                            <a href="http://vashzakaz.us/additionalpage/economyrestrictions" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/additionalpage/economyrestrictions</a> </p>


                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Условия прохождения верификации документов описаны на следующей странице: </p><p style="font-size: 14px; text-align: center; color: #000; font-family: 'Times New Roman', Times, serif;">
                            <a href="http://vashzakaz.us/page/mforwarding/rulesmailf" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/page/mforwarding/rulesmailf.</a> </p>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">До начала прохождения верификации документов рекомендуем Вам связаться с нашей службой поддержки для получения необходимой помощи и непосредственной организации процесса получения адреса виртуального склада.
                        </p>

                        <p style="text-align: right; font-family: 'Times New Roman', Times, serif;"><a href="#contents" style="text-decoration: none; color: #BF1813; text-decoration: underline; font-weight: bold; font-size: 14px;">К оглавлению...</a></p>
                    </td>
                </tr>

                <tr>
                    <td><a name="text-6" id="text-6"></a>
                        <h3 align="center" style="color: #000000; font-size: 18px; font-family: 'Times New Roman', Times, serif; font-weight: bold;"><b>Оформление отправки товаров</b></h3>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Для оформления отправки товаров с нашего склада желательно вначале ознакомиться с видами международной доставки от нашего сервиса в разделе <b> «Доставка»</b>:
                            <a href="http://vashzakaz.us/page/shipping" style="text-decoration: underline; color: #092a84; text-align:center; ">http://vashzakaz.us/page/shipping.</a><br/>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Более детальная информация по пересылке, в том числе тарифы разных видов отправлений, ограничения, запреты, страхование и т.п. доступны на страницах и по внутренним ссылкам того же раздела.<br/><br/> К примеру, страница выхода на информацию по недорогой доставке товаров личного пользования в Россию: <br/>
                            <a href="http://vashzakaz.us/page/shipping/shipeconomy" style="text-decoration: underline; color: #092a84; text-align:center;">http://vashzakaz.us/page/shipping/shipeconomy.</a><br/>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Оформлять отправку посылки необходимо в личном клиентском кабинете - пункт меню в кабинете <b>«Посылку к отправке»</b> либо прямая ссылка ниже:<br/>
                            <a href="http://vashzakaz.us/parcels/new" style="text-decoration: underline; color: #092a84; text-align:center;">http://vashzakaz.us/parcels/new.</a>


                        <p style="text-align: right; font-family: 'Times New Roman', Times, serif;"><a href="#contents" style="text-decoration: none; color: #BF1813; text-decoration: underline; font-weight: bold; font-size: 14px;">К оглавлению...</a></p>
                    </td>
                </tr>

                <tr  >
                    <td><a name="text-7" id="text-7"></a>
                        <h3 align="center" style="color: #000000; font-size: 18px; font-family: 'Times New Roman', Times, serif; font-weight: bold;"><b>Предварительные расчеты стоимости</b></h3>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Для просмотра тарифов доставки самостоятельно или отправки запроса сотрудникам Вы можете воспользоваться <b>«Онлайн калькулятором»</b> либо <b>«Формой предварительного расчета»</b> – перейти к выбору нужного инструмента Вы можете с главной страницы сайта, кликнув по иконке <b>«Калькулятор расчетов»</b> сразу под клиентским кабинетом либо же по прямой ссылке ниже:
                            <a href="http://vashzakaz.us/onlinecalculator/main" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/onlinecalculator/main.</a></p>

                        <p style="text-align: right; font-family: 'Times New Roman', Times, serif;"><a href="#contents" style="text-decoration: none; color: #BF1813; text-decoration: underline; font-weight: bold; font-size: 14px;">К оглавлению...</a></p>
                    </td>
                </tr>

                <tr>
                    <td><a name="text-8" id="text-8"></a>
                        <h3 align="center" style="color: #000000; font-size: 18px; font-family: 'Times New Roman', Times, serif; font-weight: bold;"><b>Просмотр отчетности по сотрудничеству</b></h3>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Для просмотра отчетности по текущему сотрудничеству, в том числе денежных начислений и списаний, выкупленных заказов, полученных и отправленных посылок и другой важной информации, имеются следующие инструменты:<br/><br/>

                            <b>а)</b> текущий баланс на Вашем счету – отображается в виде цифры с символом доллара в верхней части клиентского кабинета и выглядит, к примеру, вот так: </p>

                        <p style="color: red; font-weight: bold; font-size: 14px; text-align: center; font-family: 'Times New Roman', Times, serif;">Ваш баланс: $ 150.95</p>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;"><b>б)</b> клиентский отчет													– это главный документ в формате Excel, просмотреть (скачать) который Вы можете, выбрав пункт меню <b>«Скачать отчет»</b> в клиентском кабинете (Вы можете запросить сотрудника службы поддержки для получения дополнительных разъяснений по данному документу)<br/><br/>

                            <b>в)</b> файлы с регистрацией самостоятельных покупок, файлы со списком отправленных посылок (при наличии среди них самостоятельных выкупов), фотографии товаров (при запросе дополнительных услуг) и т.п. - пункт меню в кабинете <b>«Файлы»</b> либо прямая ссылка ниже:
                            <a href="http://vashzakaz.us/files" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/files.</a> <br/><br/>Кстати, по этой же ссылке Вы также можете выгружать нужные файлы на сервер для просмотра нашими сотрудниками.</p>
                        <p style="text-align: right;"><a href="#contents" style="text-decoration: none; color: #BF1813; text-decoration: underline; font-weight: bold; font-family: 'Times New Roman', Times, serif; font-size: 14px;">К оглавлению...</a></p>
                    </td>
                </tr>

                <tr >
                    <td><a name="text-9" id="text-9"></a>
                        <h3 align="center" style="color: #000000; font-size: 18px; font-family: 'Times New Roman', Times, serif; font-weight: bold;"><b>Другие важные ссылки</b></h3>

                        <p style="font-size: 14px; text-align: justify; color: #000; font-family: 'Times New Roman', Times, serif;">Другие важные ссылки, которые могут быть полезными в Вашей работе:<br/><br/>

                            <b>а)</b> Акции и скидки от магазинов: <br/><a href="http://vashzakaz.us/page/discounts/Discounts_of_the_company" style="text-decoration: underline; color: #092a84; text-align: center;">http://vashzakaz.us/page/discounts/Discounts_of_the_company;</a><br/><br/>

                            <b>б)</b> Каталог интернет-магазинов, проверенных временем и нашими клиентами, распределённых по разделам <a href="http://vashzakaz.us/useful/view/21/" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/useful/view/21/;</a><br/><br/>

                            <b>в)</b> Подписка на получение электронной рассылки по выбранным темам <a href="http://vashzakaz.us/subscription" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/subscription;</a><br/><br/>

                            <b>г)</b> Внесение предложений и замечаний по работе сайта и сервиса <a href="http://vashzakaz.us/offers" style="text-decoration: underline; color: #092a84;">http://vashzakaz.us/offers.</a></p>

                        <p style="text-align: right;"><a href="#contents" style="text-decoration: none; color: #BF1813; text-decoration: underline; font-weight: bold; font-family: 'Times New Roman', Times, serif; font-size: 14px;">К оглавлению...</a></p>
                    </td>
                </tr>

            </table>
        </td>
    </tr>


</table>
</center>
</td>
</tr>
</table>

</body>
</html>