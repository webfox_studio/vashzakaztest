<?php defined('SYSPATH') or die('No direct script access.');

return array(
    EV_LOGIN => 'Зашел в систему',
    EV_LOGOUT => 'Вышел из системы',
    EV_LOGIN_ERROR => 'Ошибка авторизации',

    EV_CHAT_MESSAGE => ':',
    EV_CHAT_MESSAGE_FROM_CLIENT => '<b>(Клиент): </b>',
    EV_CHAT_MESSAGE_TO_CLIENT => 'Ответ клиенту:',
    EV_ADMIN_RENAME => 'Изменен логин на:',
    EV_ADMIN_CHAT_ON => 'Включил статус Online',
    EV_ADMIN_CHAT_OFF => 'Отключил чат',
    EV_ADMIN_SET_PARTNER => 'Установил партнера ',
    EV_ADMIN_CHAT_AWAY => 'Установил статус Away',

    EV_MESSAGES_UPDATE_IMPORTANT_ON => 'Сообщение отмечено как важное',
    EV_MESSAGES_UPDATE_IMPORTANT_OFF => 'Сообщение отмечено как обычное',
    EV_MESSAGES_REPLY => 'Ответил на сообщение',
    EV_MESSAGES_NEW => 'Написал новое сообщение',
    EV_MESSAGES_READ => 'Читает сообщение',
    EV_MESSAGES_DELETE => 'Удалил сообщение',
    EV_MESSAGES_SKIP => 'Отметил сообщение непрочтенным',

    EV_POSTS_DELETE => 'Удалено сообщение из посылок',
    EV_POSTS_VIEW_POST => 'Просматривает посылку',
    EV_POSTS_MAKE_UNREAD => 'Отметил посылку непрочитанным',
    EV_POSTS_MAKE_CLOSED => 'Отметил посылку закрытым',
    EV_POSTS_ADD_MESSAGE => 'Написал ответ на посылку',
    EV_POSTS_UPDATE_IMPORTANT_OFF => 'Отметил посылку обычной',
    EV_POSTS_UPDATE_IMPORTANT_ON => 'Отметил посылку важной',

    EV_ORDERS_DELETE => 'Удалено сообщение из заказов',
    EV_ORDERS_VIEW_ORDER => 'Просматривает заказ',
    EV_ORDERS_MAKE_UNREAD => 'Отметил заказ непрочитанным',
    EV_ORDERS_MAKE_CLOSED => 'Отметил заказ закрытым',
    EV_ORDERS_ADD_MESSAGE => 'Написал ответ на заказ',
    EV_ORDERS_UPDATE_IMPORTANT_OFF => 'Отметил заказ обычным',
    EV_ORDERS_UPDATE_IMPORTANT_ON => 'Отметил заказ важным',

    EV_NEWS_ADD => 'Добавлена новость',
    EV_NEWS_EDIT => 'Отредактирована новость',

    EV_USEFUL_CATS_ADD => 'Добавлена категория в Статьи',
    EV_USEFUL_CATS_DEL => 'Удалена категория из Статей',
    EV_USEFUL_CATS_EDIT => 'Отредактирована категория в Статьи',

    EV_USEFUL_CATS_CHANGE => 'Назначена другая категория Статье',
    EV_USEFUL_ADD => 'Добавлена статья',
    EV_USEFUL_DEL => 'Удалена статья',
    EV_USEFUL_EDIT => 'Отредактирована статья',

    EV_SHEDULLE_EDIT => 'Отредактировано расписание',
    EV_TARIFFS_EDIT => 'Отредактированы тарифы',
    EV_CONTACTS_EDIT => 'Отредактированы контакты',
    EV_ABOUT_EDIT => 'Отредактировано О НАС ',
    EV_SHIPPING_EDIT => 'Отредактировано ПОСЫЛКИ (shipping)',
    EV_FAQ_EDIT => 'Отредактирован/добавлен вопрос в FAQ',
    EV_FAQ_DELETE => 'Удален вопрос в FAQ',
    EV_PREDLOG_DELETE => 'Удалено предложение клиента',
    EV_PREDLOG_VIEW => 'Прочитано предложение клиента',
    EV_XLS_DOWNLOAD => 'Скачен файл xls',

    EV_CACHE_MENU => 'Меню сохранено в кеш',
    EV_CACHE_POS => 'Форма посылки сохранена в кеш',
    EV_CACHE_ORDER => 'Форма заказа сохранена в кеш',
    EV_CACHE_TWITTER => 'сообщения Twitter сохранены в кеш',
);