<?php defined('SYSPATH') or die('No direct script access.');

return array
(
    'main' => 'VashZakaz',

    'index' => 'Главная',
    
    'user' => array(
        'index' => 'Пользователь',
        'signup' => 'Регистрация',
        'account' => 'Аккаунт',
        'data' => 'Данные',
        'remind' => 'Восстановление пароля',
    ),

    'messages' => array(
        'index' => 'Сообщения',
        'send' => 'Новое сообщение',
        'inbox' => 'Входящие сообщения',
        'outbox' => 'Исходящие сообщения',
    ),

    'offers' => array(
        'index' => 'Предложения',
    ),

    'admin' => 'Администрирование',
);