<?php defined('SYSPATH') or die('No direct access allowed.');

return array
(
    'for_code' => array
    (
        'subject' => 'VashZakaz.us, подтверждение e-mail',
        'message' => 'Your confirmation code: :code',
        'from'    => 'VashZakaz.us',
    ),
    
    'restore_code' => array
    (
        'subject' => 'VashZakaz.us, восстановление забытого пароля',
        'message' => "Вами был отправлен запрос на восстановление забытого пароля.  Чтобы восстановить пароль, перейдите по ссылке: :link" . "\n\r" .
 	                 "(Если Вы нажали на ссылку и ничего не произошло, то попробуйте скопировать ее и вставить в адресную строку вашего браузера и нажать Enter.)" . "\n\r" .
 	                 "Если Вы не запрашивали восстановление пароля, то просто проигнорируйте данное сообщение.",
        'from'    => 'VashZakaz.us',
    ),
);