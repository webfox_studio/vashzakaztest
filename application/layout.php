<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords"  content="доставка из сша, покупки на ebay, посреднические услуги, доставка с зарубежных интернет-магазинов, покупки на амазон, доставка в Россию, доставка в Украину, пересылка товаров, доставка посылок, шоппинг, шопинг, shopping, посредник в США, посылки в Россию "/>
        <meta name="description"  content=" Посреднические услуги по покупке и доставке товаров с eBay, Amazon, интернет-магазинов США, Европы"/>
        <title><?= isset($title) ? 'Доставка товаров из США' : 'Доставка товаров из США' ?></title>
        <?=isset($css_js) ? $css_js : ''?>
        <!--[if lt IE 7]>
        <![if gte IE 5.5]>
            <script type="text/javascript" src="/fixpng.js"></script>
            <style type="text/css">
        .iePNG, IMG { filter:expression(fixPNG(this)); }
        </style>
        <![endif]>
        <![endif]-->
<!--
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-18999141-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
-->
    </head>

    <body>
        <script type="text/javascript">
            var is_online_in_process=false;
            $(document).ready(function() {
                $('#search').focus(
                function() {
                    $(this).css('background-color', '#ffffff');
                    if($(this).val()=='ПОИСК ПО САЙТУ') $(this).val('');
                }
            );

                $('#search').blur(
                function() {
                    $(this).css('background-color', '#3E4D6A');
                    if($(this).val()=='') $(this).val('ПОИСК ПО САЙТУ');
                }
            );
                

            });
       


        </script>

        <?=isset($header) ? $header : ''?>

        <table style="width: 100%;" cellpadding="0px" cellspacing="0px">
            <tr>
                <td style="width: 220; text-align: center; border: 1px solid black; padding-top: 20px;" valign="top">
                    <?=isset($panel_left) ? $panel_left : ''?>
                </td>

                <td style="text-align: center; border-top: 1px solid black; border-bottom: 1px solid black; padding: 5px;" valign="top">
                    <div id="content">
                        <?=isset($content) ? $content : ''?>
                    </div> <!-- #content -->
                </td>

                <td style="width: 240; text-align: center; border: 1px solid black; padding-top: 10px;" valign="top">
                    <?=isset($panel_right) ? $panel_right : ''?>
                </td>
            </tr>
        </table>

        <?=isset($footer) ? $footer : ''?>
    </body>
</html>
