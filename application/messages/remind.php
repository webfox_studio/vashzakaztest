<?php defined('SYSPATH') or die('No direct script access.');

return array(

    'not_empty'  => 'Пароль не должен быть пустым',
	'min_length' => 'Пароль должен быть не меньше 5 символов',
	'max_length' => 'Пароль должен быть не более 42 символов',
    'matches' 	 => 'Введенные пароли не совпадают',
);