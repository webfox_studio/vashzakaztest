<?php defined('SYSPATH') or die('No direct access allowed.');
class Email {
	public static function send_log($log) {
		$log .= '\r\n\r\n';
		$stream = fopen('./mail.log', 'a');
		fputs($stream, $log);
		fclose($stream);
	}
	public static function send($address, $subject = NULL, $msg = NULL) {
		$msg .= "<br /><br /><b>Это автоматическое уведомление с сайта VashZakaz.US.<br />Отвечать не нужно на это письмо, любые ответы оформляйте на самом сайте http://www.vashzakaz.us</b><br /><br />Администрация VashZakaz.US<br />http://www.vashzakaz.us/page/contacts";
	
		$mail =  new Kohana_Email($subject, $msg);
		
		$mail->to($address)
			->from(Kohana::config('email.username'), Kohana::config('email.from'))
			->send();
	
		return;
	
		$msg .= "<br /><br /><b>Это автоматическое уведомление с сайта VashZakaz.US.<br />Отвечать не нужно на это письмо, любые ответы оформляйте на самом сайте http://www.vashzakaz.us</b><br /><br />Администрация VashZakaz.US<br />http://www.vashzakaz.us/page/contacts";
		$send =   "Date: ".date("D, d M Y H:i:s") . " UT\r\n";
		$send .=   'Subject: =?UTF-8?B?'.base64_encode($subject)."=?=\r\n";
		$send .= "MIME-Version: 1.0\r\n";
		$send .= "Content-Type: text/html; charset=\"UTF-8\"\r\n";
		$send .= "Content-Transfer-Encoding: 8bit\r\n";
		$send .= "From: \"".Kohana::config('email.from')."\" <".Kohana::config('email.username').">\r\n";
		$send .= "To: $address\r\n";
		$send .= "X-Priority: 3\r\n\r\n";
		$send .=  $msg."\r\n";
		if(!$socket = fsockopen(Kohana::config('email.host'), Kohana::config('email.port'), $errno, $errstr, 20)) {
			$log = 'Не могу соединится с почтовым сервером.';
			Email::send_log($log);
			return false;
		}
		
		$server_response = null;
		$log = null;
		while (substr($server_response, 3, 1) != ' ') {
			if (!($server_response = fgets($socket, 256))) 
				return false;
		}
		$log .= $server_response;
		if (!(substr($server_response, 0, 3) == '220')) {
			Email::send_log($log);
			return false;
		}
		fputs($socket, "EHLO ".Kohana::config('email.host')."\r\n");
		$server_response = null;
		while (substr($server_response, 3, 1) != ' ') {
			if (!($server_response = fgets($socket, 256)))
				return false;
		}
		$log .= $server_response;
		if (!(substr($server_response, 0, 3) == '250')) {
			Email::send_log($log);
			return false;
		}
		fputs($socket, "AUTH LOGIN\r\n");
		$server_response = null;
		while (substr($server_response, 3, 1) != ' ') {
			if (!($server_response = fgets($socket, 256)))
				return false;
		}
		$log .= $server_response;
		if (!(substr($server_response, 0, 3) == '334')) {
			Email::send_log($log);
			return false;
		}
		fputs($socket, base64_encode(Kohana::config('email.username')) . "\r\n");
		echo base64_encode(Kohana::config('email.username')).'<br />';
		$server_response = null;
		while (substr($server_response, 3, 1) != ' ') {
			if (!($server_response = fgets($socket, 256)))
				return false;
		}
		$log .= $server_response;
		if (!(substr($server_response, 0, 3) == '334')) {
			Email::send_log($log);
			return false;
		}
		fputs($socket, base64_encode(Kohana::config('email.password')) . "\r\n");
		echo base64_encode(Kohana::config('email.password'));
		$server_response = null;
		while (substr($server_response, 3, 1) != ' ') {
			if (!($server_response .= fgets($socket, 256)))
				return false;
		}
		$log .= $server_response;
		if (!(substr($server_response, 0, 3) == '235')) {
			Email::send_log($log);
			return false;
		}
		fputs($socket, "MAIL FROM: <".Kohana::config('email.username').">\r\n");
		$server_response = null;
		while (substr($server_response, 3, 1) != ' ') {
			if (!($server_response = fgets($socket, 256)))
				return false;
		}
		$log .= $server_response;
		if (!(substr($server_response, 0, 3) == '250')) {
			Email::send_log($log);
			return false;
		}
		fputs($socket, "RCPT TO: <" . $address . ">\r\n");
		$server_response = null;
		while (substr($server_response, 3, 1) != ' ') {
			if (!($server_response = fgets($socket, 256)))
				return false;
		}
		$log .= $server_response;
		if (!(substr($server_response, 0, 3) == '250')) {
			Email::send_log($log);
			return false;
		}
		fputs($socket, "DATA\r\n");
		$server_response = null;
		while (substr($server_response, 3, 1) != ' ') {
			if (!($server_response = fgets($socket, 256)))
				return false;
		}
		$log .= $server_response;
		if (!(substr($server_response, 0, 3) == '354')) {
			Email::send_log($log);
			return false;
		}
		fputs($socket, $send."\r\n.\r\n");
		$server_response = null;
		while (substr($server_response, 3, 1) != ' ') {
			if (!($server_response = fgets($socket, 256)))
				return false;
		}
		$log .= $server_response;
		if (!(substr($server_response, 0, 3) == '250')) {
			Email::send_log($log);
			return false;
		}
		fputs($socket, "QUIT\r\n");
		fclose($socket);
		return true;
    }
    public static function send_admin($subject = NULL, $msg = NULL, $from = NULL, $as_html = FALSE)
    {
        $address = Kohana::config('email.admin');

        return Email::send($address, $subject, $msg);
    }
} // End Email