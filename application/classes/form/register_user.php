<?php defined('SYSPATH') or die('No direct access allowed.');

class Form_Register_User extends Formo_Form {

    public function __construct()
    {
        parent::__construct();

        $this->init();
    }

    protected function init()
    {
        $this->alias('Register_User');

        $this->add('email', 'text')
            ->rule('email', 'Некорректный e-mail')
            ->label('E-mail');

        $this->add('username', 'text')
            ->rule('not_empty', 'Логин обязателен')
            ->rule('regex', 'Логин должен состоять только из латинских символов', array('/[a-zA-Z]/'))
            ->label('Логин');

        $this->add('password', 'password')
            ->rule('not_empty', 'Пароль не должен быть пустым')
            ->label('Пароль');

        $this->add('password2', 'password')
            ->rule('not_empty', 'Подтвердите пароль не должен быть пустым')
            ->label('Повтор пароля');

        $this->add('name', 'text')
            ->rule('not_empty', 'Имя не заполнено')
            ->rule('regex', 'Имя должно состоять только из латинских символов', array('/[a-zA-Z]/'))
            ->label('Имя Фамилия');

        $countries = array(
            "Armenia", "Azerbaijan", "Belarus",
            "Estonia", "Georgia", "Israel",
            "Kazakhstan", "Kyrgyzstan", "Latvia",
            "Lithuania", "Moldova", "Russia", "Tajikistan",
            "Turkmenistan", "Ukraine", "Uzbekistan"
        );

        for ($i = 0; $i < count($countries); $i++)
        {
            $countries[$countries[$i]] = $countries[$i];
            unset($countries[$i]);
        }

        $this->add_group('country', 'select', $countries, array(3));

        $this->add('zip', 'text')
            ->rule('digit', 'Почтовый индекс должен состоять из цифр')
            ->label('Почтовый индекс');

        $this->add('region', 'text')
            ->rule('not_empty', 'Регион не заполнен')
            ->rule('regex', 'Регион должен состоять только из латинских символов', array('/[a-zA-Z]/'))
            ->label('Область / Регион');

        $this->add('city', 'text')
            ->rule('not_empty', 'Населенный пункт не заполнен')
            ->rule('regex', 'Населенный пункт должен состоять только из латинских символов', array('/[a-zA-Z]/'))
            ->label('Населенный пункт *');

        $this->add('address', 'text')
            ->rule('not_empty', 'Остальной адрес не заполнен')
            ->rule('regex', 'Адрес должен состоять только из латинских символов', array('/[a-zA-Z]/'))
            ->label('Остальной адрес');

        $this->add('phones', 'text')
            ->label('Контактный телефон(ы)');
        
        $this->add('icq', 'text')
            ->rule('digit', 'Номер ICQ должно содержать только цифры')
            ->label('ICQ');

        $this->add('skype', 'text')
            ->label('Skype');

        $this->add('mail_ru', 'text')
            ->label('Агент Mail.ru');

        $this->add('another', 'text')
            ->label('Другой контакт');

        $this->add('where', 'text')
            ->label('Откуда узнали о нас');

        $this->add('submit', 'submit');

        
        if (Arr::path($_POST, 'Register_User.submit'))
        {
            $this->load()->validate();
        }
    }
}