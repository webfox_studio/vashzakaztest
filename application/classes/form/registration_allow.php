<?php defined('SYSPATH') or die('No direct script access.');

class Form_Registration_Allow extends Formo_Form {

    public function  __construct() {
        parent::__construct();

        $this->init();
    }

    protected function init()
    {
        $this->alias('Registration_Allow');

        $this->add('allow', 'text')
            ->label('Разрешить регистрацию клиентов');

        $this->add('submit', 'submit')
            ->label('Сохранить');

        if (Arr::path($_POST, $this->alias().'.'.$this->submit->alias()))
        {
            $this->load();
        }

    }
}