<?php defined('SYSPATH') or die('No direct script access.');

class Form_Login_User extends Formo_Form {

    public function  __construct() {
        parent::__construct();

        $this->init();
    }

    protected function init()
    {
        $this->alias('Login_User');

        $this->add('username', 'text')
            ->label('E-mail')
            ->rule('not_empty', 'Введите имя пользователя');
//            ->rule('not_empty', 'Введите e-mail')
//            ->rule('email', 'Неверный e-mail');

        $this->add('password', 'password')
            ->label('Пароль')
            ->rule('not_empty', 'Введите пароль');

        $this->add('remember', 'bool')
            ->label('Запомнить');

        $this->add('submit', 'submit')
            ->label('Войти');

        if (Arr::path($_POST, 'Login_User.submit'))
        {
            $this->load();
        }

    }
}