<?php defined('SYSPATH') or die('No direct script access.');

class Form_Payment_Find extends Formo_Form {

    public function  __construct() {
        parent::__construct();

        $this->init();
    }

    protected function init()
    {
        $this->alias('Payment_Find');

        $this->add('order_num', 'text')
            ->label('Заказ №')
            ->rule('not_empty', 'Введите номер заказа')
            ->rule('regex', 'Номер заказа должен быть целым числом', array('/^\d+$/'));
        
        $this->add('submit', 'submit')
            ->label('Найти');

        if (Arr::path($_POST, $this->alias().'.'.$this->submit->alias()))
        {
            $this->load();
        }

    }
}