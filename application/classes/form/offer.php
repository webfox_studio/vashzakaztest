<?php defined('SYSPATH') or die('No direct script access.');

class Form_Offer extends Formo_Form {

    public function  __construct() {
        parent::__construct();

        $this->init();
    }

    protected function init()
    {
        $this->alias('Offer');

        $this->add('head', 'text')
            ->rule('not_empty', 'Введите тему')
            ->label('Заголовок');

        $this->add('message', 'textarea')
            ->rule('not_empty', 'Введите текст')
            ->label('Текст');

        $this->add('submit', 'submit')
            ->label('Отправить');

        if (Arr::path($_POST, $this->alias().'.'.$this->submit->alias()))
        {
            $this->load();
        }

    }
}