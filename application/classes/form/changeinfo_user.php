<?php defined('SYSPATH') or die('No direct access allowed.');

class Form_Changeinfo_User extends Formo_Form {

    public function __construct()
    {
        parent::__construct();

        $this->init();
    }

    protected function init()
    {
        $this->alias('Changeinfo_User');
        $this->add('email', 'text')
            ->rule('email', 'Некорректный e-mail')
            ->label('Email');

        $this->add('username', 'text')
            ->rule('not_empty', 'Имя пользователя обязательно')
            ->rule('regex', 'Логин должен состоять только из латинских символов', array('/[a-zA-Z]/'))
            ->label('Логин');

        $this->add('name', 'text')
            ->rule('not_empty', 'Имя не заполнено')
            ->label('Имя Фамилия');

        $this->add('delete', 'checkbox')
            ->label('Удалить аккаунт');

        $countries = array(
            "Armenia", "Azerbaijan", "Belarus",
            "Estonia", "Georgia", "Israel",
            "Kazakhstan", "Kyrgyzstan", "Latvia",
            "Lithuania", "Moldova", "Russia", "Tajikistan",
            "Turkmenistan", "Ukraine", "Uzbekistan"
        );

        for ($i = 0; $i < count($countries); $i++)
        {
            $countries[$countries[$i]] = $countries[$i];
            unset($countries[$i]);
        }

        $this->add_group('country', 'select', $countries)
            ->label('Страна');

        
        $this->add('zip', 'text')
            ->rule('digit', 'Почтовый индекс должен состоять из цифр')
            ->label('Почтовый индекс');

        $this->add('region', 'text')
            ->rule('not_empty', 'Регион не заполнен')
            ->label('Область / Регион');

        $this->add('city', 'text')
            ->rule('not_empty', 'Населенный пункт не заполнен')
            ->label('Населенный пункт');

        $this->add('address', 'text')
            ->rule('not_empty', 'Остальной адрес не заполнен')
            ->label('Остальной адрес');

        $this->add('phones', 'text')
            ->label('Контактный телефон(ы)');

        $this->add('icq', 'text')
            ->rule('digit', 'Номер ICQ должно содержать только цифры')
            ->label('ICQ');

        $this->add('skype', 'text')
            ->label('Skype');

        $this->add('mail_ru', 'text')
            ->label('Агент Mail.ru');

        $this->add('another', 'text')
            ->label('Другой контакт');

        $this->add('cause', 'textarea')
            ->rule('not_empty', 'Укажите причину изменения информации')
            ->label('Причина изменения');

        $this->add('submit', 'submit')
            ->label('Сохранить');


        if (Arr::path($_POST, 'Changeinfo_User.submit'))
        {
            $this->load();
        }
    }
}