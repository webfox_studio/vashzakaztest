<?php defined('SYSPATH') or die('No direct script access.');

class Form_Message_Answer extends Formo_Form {

    public function  __construct() {
        parent::__construct();

        $this->init();
    }

    protected function init()
    {
        $this->alias('Message_Answer');
        
        $this->add('message', 'textarea')
            ->rule('not_empty', 'Введите текст сообщения')
            ->label('Текст');

        $this->add('important', 'bool')
            ->label('Важное');

        $this->add('submit', 'submit')
            ->label('Отправить');
        
        if (Arr::path($_POST, $this->alias().'.'.$this->submit->alias()))
        {
            $this->load();
        }

    }
}