<?php defined('SYSPATH') or die('No direct script access.');

class Form_Client_Find extends Formo_Form {

    public function  __construct() {
        parent::__construct();

        $this->init();
    }

    protected function init()
    {
        $this->alias('Client_Find');

        $this->set('action', Url::site('admin/clients/find'));

        $this->set('method', 'post');

        $this->add('name', 'text')
            ->label('Имя или ID клиента')
            ->rule('not_empty', 'Введите имя клиента');

        $this->add('submit', 'submit')
            ->label('Найти');

        if (Arr::path(($_POST + $_GET), $this->alias().'.'.$this->submit->alias()))
        {
            $this->load();
        }

    }
}