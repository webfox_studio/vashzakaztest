<?php defined('SYSPATH') or die('No direct script access.');

class Event {

    protected static $_instance;

    public static function instance()
    {
        if (! Event::$_instance)
        {
            Event::$_instance = new Event();
        }

        return Event::$_instance;
    }

    public static function factory()
    {
        return new Event();
    }

    public function add($type)
    {
        
    }

} // End Event