<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Requestform extends Controller_Abstract
{

    protected $user_id;
    protected $is_parcel = FALSE;

    protected $_model_orders;
    protected $_model_messages;


    public function action_index()
    {
        $this->set_content('requestform/index');
        $captcha = Captcha::instance();
        $this->content->captcha = $captcha;
        if ($_POST) {
            $post = Validate::factory($_POST);
            $post->rule('servise', 'not_empty')
                //     ->rule('links', 'not_empty')
                ->rule('email', 'not_empty')
                ->rule('vazh_info', 'not_empty')
                ->rule('price', 'not_empty')
                ->rule('country', 'not_empty')
                ->rule('gorod', 'not_empty')
                ->rule('fio', 'not_empty')
                ->rule('email', 'not_empty')
                ->rule('ves', 'not_empty')
                ->rule('email', 'email')
                ->rule('site_captcha', 'not_empty')
                ->rule('site_captcha', 'Captcha::valid');
            if ($post->check()  AND Captcha::valid($_POST['site_captcha'])) {
                // Все хорошо
                $_POST['date_time'] = Date::formatted_time();
                $_POST['pozhelaniya'] = '';
                if (isset($_POST['pozhelaniyaa'])) {
                    $_POST['pozhelaniya'] = $_POST['pozhelaniyaa'] . '; ';
                }
                $a = ORM::factory('requestform')
                    ->values($_POST)
                    ->save();
                $this->request->redirect('requestform/good');
            } else {
                $this->content->error = $post->errors();
                // $this->request->redirect(Request::$referrer);
            }
        }
    }

    public function action_good()
    {
        $this->set_content('requestform/good');
    }


} // End Order