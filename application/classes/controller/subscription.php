<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Subscription extends Controller_Abstract {

	public function action_index() {
		$this->set_content('subscription/index');
		$m_subscription = Model_Subscription::factory('subscription');
        $active_cats = $m_subscription->my_active_cats();
        $my_active_cats = array();
        foreach ($active_cats as $value) {
        	$my_active_cats[$value['id']] = $value['name'];
        }
        $cats = $m_subscription->get_cats();
        $this->content->active_cats = $my_active_cats;
        $this->content->cats = $cats;
	}
	public function action_update() {
            
            
            
		$m_subscription = Model_Subscription::factory('subscription');
		$subscription_cats = $m_subscription->subscription_update($_POST['categories']);
                
                $user_id = Auth::instance()->get_user()->id;
                $_POST['date'] = date('Y-m-d');
                $cat_users = $m_subscription->subscription_add_cat($_POST, $user_id);
               
		$this->request->redirect('subscription/index');
	}
}