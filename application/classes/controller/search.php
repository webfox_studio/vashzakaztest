﻿<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Search extends Controller_Abstract {

    public $content = 'search';

    public function before()
    {
        parent::before();        
    }

    public function action_index()
    {
        setlocale(LC_ALL, 'ru_RU.CP1251', 'rus_RUS.CP1251', 'Russian_Russia.1251');
        if (isset($_REQUEST['search'])){
            $items_per_page = 30;
            $search = trim(strip_tags($_REQUEST['search']));

            $res = Model_search::instance()->find($search);
            $count = 1;
            foreach($res as $k => $v){
                if (isset($v['cp1251'])){
                    $res[$k]['content'] = $this->make_bold_search($v['cp1251'], $search, true);
                }else{
                    $res[$k]['content'] = $this->make_bold_search($v['content'], $search);
                }
                $res[$k]['count'] = $count;
                if (!isset($res[$k]['category']))
                    $res[$k]['category'] = 'info';
                if ($res[$k]['category'] == 'page')
                {
                    $res[$k]['content'] = $res[$k]['content'];
                }
                $count++;
            }
             $pagination = Pagination::factory(array(
                    'total_items'    => count($res),
                    'items_per_page' => $items_per_page,
                ));
            //var_dump($res);
            //die();
            if (isset($_GET['page'])){
                $res = array_slice($res,($_GET['page']-1)*$items_per_page,$items_per_page);
            }else{
                $res = array_slice($res,0,$items_per_page);
            }

           
            //var_dump($pagination);
            //die();
            $this->content->pagination = $pagination->render();
            $this->content->search = $search;
        }else{
            $res = array();
        }
        
        $this->content->result = $res;
        
    }

    private function make_bold_search($content,$search, $cp1251=false)
    {
        $char_count =80;
        $search = $this->strtolower(iconv ('UTF-8', 'WINDOWS-1251//IGNORE', $search));
        $content = strip_tags($content);
        $content = preg_replace("/&#?[a-z0-9]+;/i", '', $content);
        $tmp_content = $this->strtolower($content);
        $content = iconv ('UTF-8', 'WINDOWS-1251//IGNORE//TRANSLIT', trim($content));
        $tmp_content = iconv ('UTF-8', 'WINDOWS-1251//IGNORE//TRANSLIT', trim($tmp_content));
        $pos = stripos($tmp_content, $search);
        if ($pos !==false) {
            $content_new = '';
            $start = ($pos < $char_count) ? 0 : $pos - $char_count;
            $end = ($pos + $char_count > strlen($content)) ? strlen($content) : $pos + $char_count;
            for($i=$start; $i<$end; $i++)
            {
                $content_new .= $content[$i];
            }
            
            $part = preg_replace('/'.$search.'/si', '<b class="oreange">'.$search.'</b>', $content_new);
            return iconv('WINDOWS-1251','UTF-8',$part);
        }     
        return iconv('WINDOWS-1251','UTF-8',$content);
    }
    
    public function action_all()
    {
        $this->set_content('news/all');
        
        $this->action_index($all = TRUE);
    }

    public function action_item($id)
    {
        $news_item = Model_News::instance()->get($id);

        if (Request::$is_ajax)
        {
            $this->content = View::factory('news/news', $news_item);
//            echo $result;
        }
        else
        {
            $this->set_content('news/item');

            $this->content->news = array($news_item);
        }
    }

    public function action_last()
    {
        
    }
    
    private function strtolower($string)
    {
        $small = array('а','б','в','г','д','е','ё','ж','з','и','й',
                   'к','л','м','н','о','п','р','с','т','у','ф',
                   'х','ч','ц','ш','щ','э','ю','я','ы','ъ','ь',
                   'э', 'ю', 'я',
                   'a','b','c','d','e','f','g','h','i','j','k',
                   'l','m','n','o','p','q','r','s','t','u','v',
                   'w','x','y','z'
                   );
        $large = array('А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й',
                   'К','Л','М','Н','О','П','Р','С','Т','У','Ф',
                   'Х','Ч','Ц','Ш','Щ','Э','Ю','Я','Ы','Ъ','Ь',
                   'Э', 'Ю', 'Я',
                   'A','B','C','D','E','F','G','H','I','J','K',
                   'L','M','N','O','P','Q','R','S','T','U','V',
                   'W','X','Y','Z');
        return str_replace($large, $small, $string); 
    }
    
    private function detect_utf($s)
    {
        //$s=urlencode($s); // в некоторых случаях - лишняя операция (закоментируйте)
        $res='0';
        $j=strlen($s);

        $s2=strtoupper($s);
        $s2=str_replace("%D0",'',$s2);
        $s2=str_replace("%D1",'',$s2);
        $k=strlen($s2);

        $m=1;
        if ($k>0)
        {
            $m=$j/$k;
            if (($m>1.2)&&($m<2.2)){ $res='1'; }
        }
        return $res;
    }

} // End News