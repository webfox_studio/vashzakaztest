<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller {

    protected $response;

    public function before()
    {
        parent::before();

        if (! Request::$is_ajax)
        {
            return;
        }
    }

    public function after()
    {
        $this->request->response = $this->response;

        parent::after();
    }

    public function action_username_exists($username = NULL)
    {
        if (! $username)
        {
            if (isset($_POST['username']))
            {
                $username = $_POST['username'];
            }
            else if (isset($_GET['username']))
            {
                $username = $_GET['username'];
            }
            else
            {
                $this->response = -1;
                return;
            }
        }

        if (! $username)
        {
            $this->response = -1;
            return;
        }

        if (! Validate::regex($username, '/[a-zA-Z]/'))
        {
            $this->response -2;
            return;
        }

        $result = ORM::factory('user', array('username' => $username))->id ? TRUE : FALSE;

        $this->response = $result ? 1 : 0;

        return;
    }

    public function action_del_image_ajax()
    {
//        if (Auth::instance()->logged_in()) {
//            $m_user = ORM::factory('user', Auth::instance()->get_user()->id);
//            {
            if(isset($_POST['name'])){
                if(file_exists(DOCROOT.'order_images/tmp/'.$_POST['name'])){                    
                    unlink(DOCROOT.'order_images/tmp/'.$_POST['name']);
                }
            }

//            }
//        }
        return;
    }

    public function action_is_confirmed(){
        if (isset($_REQUEST['email'])){
            if (Model_Email::instance()->is_confirmed($_REQUEST['email'])){
                echo "false";
            }else{
                echo "true";
            }
        }else{
            echo "false";
        }
    }

    public function action_online(){
         if (isset($_REQUEST['user_nick']) && $_REQUEST['user_nick'] != ''){
                switch($_REQUEST['act']){
                    case "send":
                            Model_event::instance()->add('EV_CHAT_MESSAGE_FROM_CLIENT',$_POST['text'],'','admin');
                        break;
                    case "load":
                        $m_event = Model_event::instance();

                        if (!isset($_POST['last']) || $_POST['last']==0){
                            $last_message_id = "";
                        }else{
                            $last_message_id = $_POST['last'];
                        }
                        
                        $log = $m_event->get_from_last_id($last_message_id);
                        $new_last_id = $m_event->get_last(1);

                        $js = "var chat_area = $('#chat_area');chat_area.empty();last_message_id = ".$new_last_id[0]['id'].";";
                        $chat = Model_event::instance()->get_chat($_POST['user_nick'],30,$new_last_id[0]['id']);
                        
                        //echo ($new_last_id[0]['id']);
                        $chat = array_reverse($chat);
                        foreach ($chat as $k => $v){
                            if ($v['type']== 5)
                                $js .= "chat_area.append('<span><b>Вы: </b>".$v['what']."</span><br/>');";
                            else
                                $js .= "chat_area.append('<span><b>Admin: </b>".$v['what']."</span><br/>');";

                        }
                        if(count($log)>0)
                            $js.= '$("#chat").scrollTop($("#chat").get(0).scrollHeight);';
                        
                        echo $js;
                    break;
                    case "online";
                        if (Auth::instance()->logged_in()) {
                            $m_user = ORM::factory('user', Auth::instance()->get_user()->id);
                            //$m_user->last_activites = time();
                            //$m_user->save();
                            if (file_exists($_SERVER['DOCUMENT_ROOT'].'/online_array.php')){
                                include $_SERVER['DOCUMENT_ROOT'].'/online_array.php';
                                $online_guest_array[$m_user->user_data->name] = array('last_act'=>time(),'nick' => $m_user->username);
                                if (isset($admin_online))
                                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/online_array.php', '<?php $online_guest_array = '. var_export($online_guest_array, true). '; $admin_online = '. var_export($admin_online, true).';?>');
                                else
                                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/online_array.php', '<?php $online_guest_array = '. var_export($online_guest_array, true). '; $admin_online = array();?>');
                                $online = false;
                                foreach($admin_online as $adm){
                                    if($adm['status']==1){
                                        $online = true;
                                        break;
                                    }
                                }
								echo($online ? '1' : '0');
                            }else echo "0";
                        }
                    break;
                }
            }
    }

    public function action_admin_online()
    {
                if (isset($_REQUEST['nick']) && $_REQUEST['nick'] != ''){
                switch($_REQUEST['act']){
                    case "send":
                            if (strstr($_POST['text'],':::'))
                                Model_event::instance()->add('EV_CHAT_SIGNAL_MESSAGE',substr($_POST['text'],3),'',$_POST['to']);
                            else
                                Model_event::instance()->add('EV_CHAT_MESSAGE_TO_CLIENT',$_POST['text'],'',$_POST['to']);
                        break;
                    case "load":
                        $m_event = Model_event::instance();
                        
                        if (!isset($_POST['last']) || $_POST['last']==0){
                            $last_message_id = "";
                        }else{
                            $last_message_id = $_POST['last'];
                        }
                        $log = $m_event->get_from_last_id($last_message_id);
                        $new_last_id = $m_event->get_last(1);
                        $js = "var chat_area_adm = $('#chat_area_adm');last_message_id = ".$new_last_id[0]['id'].";";
                        if(count($log)>0)
                            $js.= '$("#log_adm").scrollTop($("#log_adm").get(0).scrollHeight);';//'playSound("/sound1");$("#log_adm").scrollTop($("#log_adm").get(0).scrollHeight);';
                        $log = array_reverse($log);
                        
                        foreach ($log as $k => $v){
                            $to = ($v['to'])? ('to '.$v['to']):'';
                            
                            $pattern = '/<br>/';
                            
                            $replacement = '';
                            
                            if ($v['type'] >3 &&  $v['type'] <=6)
                            {
                                
                                
                                $new_pars = preg_replace($pattern, $replacement, $v['what']);
                                $order   = array("\r\n", "\n", "\r");
                                $new_pars = str_replace($order, '', $new_pars);
                                $js .= "chat_area_adm.append('<span>".date('H-i-s',$v['dt'])." <b><a href=\"javascript:void(0)\" onClick=\"set_nick(\'".addslashes($v['who'])."\');\">".addslashes($v['who'])."</a> сообщение из чата ".$to." : </b>".addslashes($new_pars)."</span>');
                                    ";
                            }
                            else
                            {
                                
                                
                                $new_pars = preg_replace($pattern, $replacement, $v['what']);
                                $order   = array("\r\n", "\n", "\r");
                                $new_pars = str_replace($order, '', $new_pars);
                                $js .= "chat_area_adm.append('<span>".date('H-i-s',$v['dt'])." <b>".addslashes($v['who'])."</b>: ".addslashes($new_pars)."</span>');
                                    ";
                            }
                            if ($v['type'] == 97 && $v['who']!=(Auth::instance()->get_user()->user_data->name)){
                                $js .= 'alert("От:'.$v['who'].':\r\n'.$v['what'].'");';
                            }
                        }
                        echo $js;
                    break;
                    case "online":
                        if (Auth::instance()->logged_in()) {
                            
                            $js = 'var online_admins_div = $("#online_admins");
                                        $("#online_admins").empty();
                                    var online_users_div = $("#online_users");
                                        $("#online_users").empty();';
                         
                            $m_user = ORM::factory('user', Auth::instance()->get_user()->id);
                            //$m_user->last_activites = time();
                            //$m_user->save();
                            if (file_exists($_SERVER['DOCUMENT_ROOT'].'/online_array.php')){
                                include $_SERVER['DOCUMENT_ROOT'].'/online_array.php';
                                $time = time();
                                foreach($online_guest_array as $k => $v){
                                    if ($v['last_act']< ($time-70)){
                                        unset ($online_guest_array[$k]);
                                    } else {
                                        $js .= 'online_users_div.append("<span class=\'onlineusers\' onClick=\"set_nick(\''.$k.'\');\">'.$k.' </span>");';
                                        
                                    }
                                }
                                if (isset($admin_online)){
                                   $key = key_exists($m_user->user_data->name,$admin_online);
                                   if ($key===false)
                                       $admin_online[$m_user->user_data->name] = array('last_act'=>$time,'status'=>'0');
                                   else
                                       $admin_online[$m_user->user_data->name]['last_act'] = $time;
                                   $chat_online = false;
                                   foreach($admin_online as $k => $v){
                                        if ($v['last_act']< ($time-70)){
                                            unset ($admin_online[$k]);
                                        }else{
                                            $online = "Offline";
                                            switch ($v['status']){
                                                case 1:$online = "Online";$chat_online = true; break;
                                                case 2:$online = "oreange";break;
                                            }
                                            $js .= 'online_admins_div.append("<span class=\'onlineusers '.$online.'\' onClick=\"set_nick(\''.$k.'\')\">'.$k.'</span>;");';
                                        }
                                    }
                                    if ($chat_online)
                                        $js .= "$('#admin_status_adm').removeClass('Offline').addClass('Online').text('Online');";

                                }else{
                                   $admin_online = array();
                                   $admin_online[$m_user->user_data->name] = array('last_act'=>$time,'status'=>'0');
                                }
                                file_put_contents($_SERVER['DOCUMENT_ROOT'].'/online_array.php', '<?php $online_guest_array = '. var_export($online_guest_array, true). '; $admin_online = '. var_export($admin_online, true).';?>');
                            }
                            echo $js;
                        }
                    break;
                }
            }
    }

    public function action_hidden_email(){
        // $session = Session::instance();
        // print_r($session);
        //print_r($_POST);

        $id = (int)$_POST['id'];
        $cat = (int)$_POST['cat'];
        $userId = (int)$_POST['userId'];

        $result = DB::select('*')
            ->from('subscription_subscribers')
            ->where('uid', '=', $userId)
            ->and_where('cat_id', '=', $cat)
            ->execute()
            ->as_array();
        if (count($result) > 0)
            return false;

        if($id != 0 && $cat != 0){
            $query = DB::insert('subscription_subscribers', array('uid', 'cat_id', 'hiddenMailing'))
                ->values(array($id, $cat, 1))->execute();
            echo json_encode(array('cat'=>$cat));
        }

    }
    public function action_del_email(){


        $id = (int)$_POST['id'];
        $userId = (int)$_POST['userId'];

        if($id != 0 && $userId != 0){
            $result = DB::delete('subscription_subscribers')
                ->where('cat_id', '=', $id)
                ->and_where('uid', '=', $userId)
                ->execute();
            echo json_encode(array('id'=>$id));
        }

    }


    // admin

    public function action_searchUser(){
        $post = Validate::factory($_POST);
        $post->rule('text', 'not_empty');
        $post->rule('text', 'min_length', array(':value', 2));
        if ($post->check()) {
            $usersData = array();
            $usersData['status'] = false;
            $users = ORM::factory('user_data')->where('user_id', 'like', $post['text'].'%')->or_where('name', 'like', '%'.$post['text'].'%')->find_all();
            foreach ($users as $key => $value):
                $usersData['users'][] = array(
                    'id' => $value->user_id,
                    'name' => $value->name,
                    'country' => $value->country
                );
            endforeach;
            if(count($usersData['users']) > 0){
                $usersData['status'] = true;
            }

            // if($users->loaded()){
                echo json_encode($usersData);
            // }
        }
        else{
            echo 'error';
        }
    }

} // End Controller_Ajax