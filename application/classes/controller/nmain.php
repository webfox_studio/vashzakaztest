<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Nmain extends Controller_Abstract
{

    public $content = 'nmain/index';

    public function before()
    {
        parent::before();
        $this->content->services = @file_get_contents(Kohana::config('main.services'));
    }

    public function action_index($all = FALSE)
    {
        $this->piTest();
        $this->add_js('media/lib/fancybox/jquery.fancybox-1.3.4.pack', TRUE);
        $this->add_css('media/lib/fancybox/jquery.fancybox-1.3.4', TRUE);

        $m_news = Model_News::instance();
        $last_news_id = $m_news->get_last_id();

        $user = Auth::instance();

        if ($user->logged_in() AND $user->get_user()->user_data->last_news_id < $last_news_id) {
            $user = $user->get_user();
            $user->user_data->last_news_id = $last_news_id;
            $user->user_data->save();
        }

        if (!$all) {
            $news = $m_news->get_all(3, FALSE, 'DESC');
        } else {
            $pagination = Pagination::factory(array(
                'total_items' => $m_news->count(),
                'items_per_page' => 15,
            ));

            $news = $m_news->get_all($pagination->items_per_page, $pagination->offset, 'DESC');

            $this->content->pagination = $pagination->render();
            $this->content->all_news = TRUE;
        }
        $this->content->news = $news;

        $topics = Model_Topics::instance();
        $last_topics = $topics->get_all();
        $this->content->last_topics = $last_topics;

        $mbanners = Model_Banners::instance();
        $banners = $mbanners->get_all(99, 0);
        $this->content->banners = $banners;

        $actions = Model_Actions::instance();
        $big_actions = $actions->get_where("1", 10);
        $mid_actions = $actions->get_where("2", 12);
        $tiny_actions = $actions->get_where("3", 20);
        $this->content->big_actions = $big_actions;
        $this->content->mid_actions = $mid_actions;
        $this->content->tiny_actions = $tiny_actions;

        $mreviews = Model_Reviews::instance();
        $reviews = $mreviews->get_all(8);
        if (is_array($reviews)) {
            for ($i = 0; $i < sizeof($reviews); $i++) {
                $reviews[$i]['count'] = $i + 1;
            }
        }
        $this->content->reviews = $reviews;
    }

    public function action_all()
    {
        $this->set_content('nmain/all');

        $this->action_index($all = TRUE);
    }

    public function action_item($id)
    {
        $news_item = Model_News::instance()->get($id);

        if (Request::$is_ajax) {
            $this->content = View::factory('nmain/news', $news_item);
//            echo $result;
        } else {
            $this->set_content('nmain/item');

            $this->content->news = array($news_item);
        }
    }

    public function action_last()
    {

    }

} // End Nmain