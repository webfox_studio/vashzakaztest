<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Kohana extends Controller {

    public function action_index()
    {
        $this->request->redirect('/');
    }

} // End Controller_Kohana