<?php defined('SYSPATH') or die('No direct script access.');

class Controller_User extends Controller_Webpage
{

    protected static $libs_included = FALSE;
    protected $user = null;

    public function before()
    {

        parent::before();
        $this->title(Lang::get('title.user.index'));

        $actions_en = array('login', 'logout', 'register', 'exists', 'remind');

        if (!Auth::instance()->logged_in() AND
            !in_array($this->request->action, $actions_en)) {
            $this->request->redirect('login');
        }
    }

    public function after()
    {

        // delete errors
        Session::instance()->delete('remind_error');

        // delete post data
        Session::instance()->delete('remind_post');

        parent::after();
    }

    public function action_index()
    {

        return $this->request->redirect('user/data');
    }


    public function action_login()
    {
        $this->piTest();
//
       //  $this->piTest();
        // var_dump($this->errorLoginForm);
        if(isset($_SESSION['errorLoginForm']) && $_SESSION['errorLoginForm'] >= 5){
            $this->set_content('user/errorLoginForm');
            return;
        }

        $this->set_content('user/login');

        $form = Formo::get('Login_User');

        $username = $form->username->val();
        $password = $form->password->val();
        $remember = $form->remember->val() ? TRUE : FALSE;

        $result = Auth::instance()->login($username, $password, $remember);

        if ($result) {
            $cur_user = Auth::instance();
            if ($cur_user->logged_in()) {
                $cur_user = $cur_user->get_user();
                $email = $cur_user->email;
                $m_phpbb_user = Model_phpbb::instance();
                $phpbb_user = $m_phpbb_user->get_by_email($email);
                $this->phpbb_login($phpbb_user['user_id']);

            }

            if (!empty($_GET['ret'])) {
                $this->request->redirect($_GET['ret']);
            } else if (Auth::instance()->logged_in('admin')) {

                $this->request->redirect('admin');
            } else {

                $this->request->redirect('user/latestupdates');
            }
        } else {
            $form->username->error('Ошибка авторизации');
        }

        return $result;
    }

    public static function phpbb_instance()
    {

        if (self::$libs_included)
            return TRUE;
        define('IN_PHPBB', true);
        define('PHPBB_DB_NEW_LINK', 1);
        $phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : $_SERVER['DOCUMENT_ROOT'] . '/board/';
        $phpEx = substr(strrchr(__FILE__, '.'), 1);
        $GLOBALS['phpbb_root_path'] = $phpbb_root_path;
        $GLOBALS['phpEx'] = $phpEx;
        require_once($phpbb_root_path . 'common_kohana.' . $phpEx);
        require_once($phpbb_root_path . 'includes/functions_user.' . $phpEx);
        require_once($phpbb_root_path . 'includes/acp/auth_kohana.' . $phpEx);
        setcookie('phpbb_sid', '', (time() - 3600));
        // Start session management
        $user->session_begin();

        self::$libs_included = TRUE;
        return TRUE;

    }

    public static function phpbb_login($user_id, $persist_login = FALSE)
    {
        global $user;

        self::phpbb_instance();
        $user->session_create($user_id, false, $persist_login, true);
    }

    public static function logout()
    {

        global $user;
        self::phpbb_instance();

        $user->session_kill(FALSE);
        // var_dump($user);
        //die();

        return TRUE;
    }

    public function action_logout()
    {

        $m_event = Model_event::instance()->add('EV_LOGOUT', 'Вышел из системы');
        Auth::instance()->logout();
        $this->logout();
        $this->request->redirect(Url::site('/'));
    }

    public function action_register()
    {


        if (Auth::instance()->logged_in()) {

            $this->request->redirect('/');
        }

        if (!Settings::instance()->get('registration_allow')) {

            $this->set_content('register/disallow');
            return FALSE;
        }

        $this->add_js('login_exists');

        $this->title(Lang::get('title.user.signup'));

        if (!isset($_POST['reg_step'])) {

            $this->set_content('register/agreement');
        } else if ($_POST['reg_step'] == 1) {

            $this->set_content('register/email');
        } else if ($_POST['reg_step'] == 2) {

            $email = trim($_POST['email']);

            $this->content->email = $email;

            if (!Validate::email($email)) {

                $this->set_content('register/email');
                $this->content->email_error = TRUE;
            } else if (Model_Email::instance()->is_confirmed($email)) {

                $this->set_content('register/email');
                $this->content->is_confirmed = TRUE;
            } else {

                if (!isset($_POST['code'])) {
                    $this->set_content('register/email_confirm');

                    // Отсылка/сохранение кода подтверждения и e-mail пользователя.
                    $this->add_email($email);
                }
            }
        } else if ($_POST['reg_step'] == 3) {

            $code = trim($_POST['code']);
            $email = trim($_POST['email']);

            $this->content->email = $email;

            if (!Model_Email::instance()->is_code_valid($email, $code)) {

                $this->set_content('register/email_confirm');
                $this->content->code_error = TRUE;
                $this->content->code = $code;
            } else {

                $this->set_content('register/index');

                $form = Formo::get('Register_User');

                $this->content->form = $form;
                $form->email->val($email);
            }
        } else if ($_POST['reg_step'] == 4) {

            $this->set_content('register/index');

            $form = Formo::get('Register_User');

            $this->content->form = $form;

            if (ORM::factory('user', array('username' => $form->username->val()))->id > 0) {

                $form->username->error('Этот логин уже занят');
            }

            if ($form->password->val() != $form->password2->val()) {

                $form->password->error('Пароли не совпадают');
            }

            if ($form->validate()) {

                Model_Email::instance()->confirm($form->email->val());
                $this->register_user();
            }
        }
    }

    protected function register_user()
    {

        $form = Formo::get('Register_User');

        $email = $form->email->val();
        $username = $form->username->val();
        $password = $form->password->val();
        $referer = isset($_COOKIE['http_referer']) ? $_COOKIE['http_referer'] : (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
        $ip = $_SERVER['REMOTE_ADDR'];

        $data = array(
            'registered' => date('Y-m-d H:i:s'),
            'referer' => $referer,
            'name' => $form->name->val(),
            'ip' => $ip,
            'country' => $form->country->val(),
            'zip' => $form->zip->val(),
            'region' => $form->region->val(),
            'city' => $form->city->val(),
            'address' => $form->address->val(),
            'phones' => $form->phones->val(),
            'icq' => $form->icq->val(),
            'skype' => $form->skype->val(),
            'mail_ru' => $form->mail_ru->val(),
            'another' => $form->another->val(),
            'where' => $form->where->val(),
            'value' => NULL,
        );

        $m_user = Model::factory('user');

        $m_user->add_user($username, $email, $password, array('login', 'client'), $data);

        Auth::instance()->login($username, $password);

        Request::instance()->redirect('temp/registration_success');

        //$this->set_content('register/registered');
    }

    protected function add_email($email)
    {

        $code = rand(11111, 99999);

        Model_Email::instance()->update($email, $code);

        $subject = Lang::get('email.for_code.subject');
        $message = Lang::get('email.for_code.message', array(':code' => $code));
        $from = Lang::get('email.for_code.from');

//        EmailHelper::send($email, Lang::get('email.from'), $subject, $message);
        EmailHelper::send($email, $subject, $message, $from);
    }

    public function action_data()
    {

        $this->set_content('user/data');

        $user = Auth::instance()->get_user();
        $user_changes = ORM::factory('user', $user->id)->user_data_changes;

        if ($user_changes->user_id == $this->user->id AND $user_changes->approved == 0) {

            $this->content->not_approved = TRUE;
            $this->content->user = $user_changes;
        } elseif ($user_changes->approved == 2) {
            $this->content->changes_denied = TRUE;
            $this->content->user = $user_changes;
            $user_changes->approved = 1;
            $user_changes->save();
        } else {
            $this->content->user = $user->user_data;
        }

        $this->content->username = $user->username;
        $this->content->email = $user->email;
    }

    public function action_changedata()
    {

        $this->set_content('user/change');

        $user = Auth::instance()->get_user();

        $user_changes = ORM::factory('user', $this->user->id)->user_data_changes;

        if ($user_changes->user_id == $this->user->id AND $user_changes->approved == 0) {

            $user_data = $user_changes;
        } else {

            $user_data = $user->user_data;
        }

        $form = Formo::get('Changeinfo_User');

        if (!$form->submit->val()) {

            $form->username->val($user->username);
            $form->delete->val(1);
            $form->name->val($user_data->name);
            $form->country->val($user_data->country);
            $form->email->val($user->email);
            $form->zip->val($user_data->zip);
            $form->region->val($user_data->region);
            $form->city->val($user_data->city);
            $form->address->val($user_data->address);
            $form->phones->val($user_data->phones);
            $form->icq->val($user_data->icq ? $user_data->icq : '');
            $form->skype->val($user_data->skype);
            $form->mail_ru->val($user_data->mail_ru);
            $form->another->val($user_data->another);
        }

        $this->content->form = $form;

        if ($form->validate()) {

            if (!$user_changes->user_id) {

                $user_changes->user_id = $user->id;
            }
//            echo $form->email->val();
//            die();
            $user_changes->email = $form->email->val();
            $user_changes->name = $form->name->val();
            $user_changes->country = $form->country->val();
            $user_changes->region = $form->region->val();
            $user_changes->city = $form->city->val();
            $user_changes->zip = $form->zip->val();
            $user_changes->address = $form->address->val();
            $user_changes->phones = $form->phones->val();
            $user_changes->icq = $form->icq->val();
            $user_changes->skype = $form->skype->val();
            $user_changes->mail_ru = $form->mail_ru->val();
            $user_changes->another = $form->another->val();
            $user_changes->cause = $form->cause->val();

            if ($form->delete->val() == 1) {
                $user_changes->delete = $form->delete->val();
            } else {
                $user_changes->delete = 0;
            }
            $user_changes->approved = 0;

            $user_changes->save();
            $this->set_content('user/info_changed');
        }
    }

    public function action_account()
    {

        $this->set_content('user/account');

        $user = Auth::instance()->get_user();

        $this->content->user = $user;
    }

    public function action_remind()
    {
        $template = 'user/remind';
    // unset($_SESSION['remind_error_update']);
            if(!isset($_SESSION['remind_error_update'])){
                $_SESSION['remind_error_update'] = 0;
            }

        $link = $code = '';
        $restore_time = $this->config['restore_link_time'];
        $IsRestore = false;

        // email and code for restore the password
        $g_email = (string)urldecode(Arr::get($_GET, 'email'));
        $g_code = (string)Arr::get($_GET, 'code');

        // get errors and post data (if exist)
        $error = Session::instance()->get('remind_error');

        // password restoration
        if ($g_code && $g_email) {

            // check email and code and restore_time
            if (Model_Email::instance()->is_restorecode_valid($g_email, $g_code, $restore_time)) {

                $IsRestore = true;

                // если нажали кнопку "Изменить пароль"
                if (isset($_POST['restore'])) {

                    // get new passwords
                    $post = Validate::factory($_POST);

                    $post->rule('password', 'not_empty')
                        ->rule('password', 'min_length', array('5'))
                        ->rule('password', 'max_length', array('42'))
                        ->rule('confirm_password', 'matches', array('password'));

                    // if success
                    if ($post->check()) {

                        // change the password
                        $user = Model::factory('user')->find_by_email($g_email);
                        $user->password = (string)Arr::get($_POST, 'password');
                        $user->save();
                        // del restore code
                        Model_Email::instance()->del_restore_code($g_email, $g_code);

                        //$this->request->redirect('remind/success');
                        $this->request->redirect('/');

                    } //  errors
                    else {

                        Session::instance()->set('remind_error', $post->errors('remind'));
                        $this->request->redirect('remind' . URL::query(array('email' => $g_email, 'code' => $g_code)));
                    }
                }
            } // incorrect restore link
            else {

                Session::instance()->set('remind_error', 'Неверная ссылка для восстановления пароля!');
                $this->request->redirect('/remind');
            }
        }

        // request for password recovery
        if (isset($_POST['remind'])) {
            $_SESSION['remind_error_update'] += 1;
            if($_SESSION['remind_error_update'] > 3){
                $template = 'user/errorUpdate';
                $this->content = View::factory($template);
                return;
            }
            // get email
            $email = (string)Arr::get($_POST, 'email');

            // check email
            if ($email && Model_Email::instance()->exists($email)) {

                // generate a code
                $code = md5(time() . $email . $this->config['restore_salt']);
                // write the code to database
                Model_Email::instance()->add_restore_code($email, $code);
                // generate a restore link
                $link = URL::base(false, true) . 'remind' . Url::query(array('email' => $email, 'code' => $code));

                // send the restore link
                $subject = Lang::get('email.restore_code.subject');
                $message = Lang::get('email.restore_code.message', array(':link' => $link));
                $from = Lang::get('email.restore_code.from');

                EmailHelper::send($email, $subject, $message, $from);

                // TODO  сообщение об успехе
                //$this->request->redirect('remind/success');
                $this->request->redirect('/');

            } else {

                Session::instance()->set('remind_error', 'Введенный адрес электронной почты не зарегистрирован!');
                $this->request->redirect('remind');
            }
        }


        $this->content = View::factory($template)
            ->set('error', $error)
            ->set('email', $g_email)
            ->set('IsRestore', $IsRestore);

    }

    public function action_access_denied()
    {
        $this->set_content('access_denied');
    }

    public function action_report_unactive($id = NULL)
    {
        $this->set_content('user/report_unactive');
    }

    public function action_report($id = NULL)
    {
        if (Settings::instance()->get('report'))
            if (!$id) {

                $id = $this->user->id;
            }

        $filename = Kohana::config('main.dir.reports') . '/' . $id . '.xls';

        if (!file_exists($filename)) {

            $this->set_content('user/report_not_find');
        } else {
            Request::instance()->send_file($filename, 'Отчет клиента #' . $id . ' от ' . date('d.m.Y H:i:s') . '.xls');
            Model_event::instance()->add('EV_XLS_DOWNLOAD', 'скачал xls-отчет');
        }
        // {
        // $this->set_content('user/report_unactive');
        // }
    }

    public function action_report_demand()
    {

        $this->set_content('user/report_demand');
    }

    public function action_latestupdates()
    {

        //$this->set_content('user/latestupdates');

        $user_id = Auth::instance()->get_user()->id;

        /*$count_new_messages = ORM::factory('messages')
            ->where('whom_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->where('deleted', '=', 0)
            ->count_all();*/
        $messages = ORM::factory('message');

        $count_new_in = $messages
            ->where('whom_id', '=', $user_id)
            ->where('deleted', '=', 0)
            ->where('readed', '=', 0)
            ->count_all();
        $count_new_out = $messages
            ->where('user_id', '=', $user_id)
            ->where('deleted', '=', 0)
            ->where('readed', '=', 0)
            ->count_all();

        //------------------------
        //$messages_new[] = $messages->where('whom_id', '=', $user_id)->where('deleted', '=', 0)->where('readed', '=', 0)->as_array();

        $pilatestupdates = Model_Pilatestupdates::instance();
        $news_new = $pilatestupdates->get_all_three();
        $messages_new_in = $pilatestupdates->messages_new_in($user_id);
        $messages_new_out = $pilatestupdates->messages_new_out($user_id);
        $orders_new = $pilatestupdates->orders_new($user_id);
        $parcels_new = $pilatestupdates->parcels_new($user_id);
        $files_new = $pilatestupdates->files_new($user_id);

        $payments_new = $pilatestupdates->payments_new($user_id);


        //------------------------

        //--Последние просмотренные объявления
        $last_messages = $pilatestupdates->messages($user_id);
        $last_orders = $pilatestupdates->orders($user_id);
        $last_parcels = $pilatestupdates->parcels($user_id);
        $last_files = $pilatestupdates->files($user_id);

        $last_payments = $pilatestupdates->payments($user_id);

        //-------------------------------
        $count_new_orders = ORM::factory('orders')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->count_all();

        $count_new_parcels = ORM::factory('parcels')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->count_all();

        $count_new_files = ORM::factory('files')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->where('incoming', '=', 1)
            ->count_all();

        $count_new_payments = ORM::factory('payments')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->count_all();

//        if ($this->user->user_data->value != 0)
//        {
//            $this->template->account_opt->balans = $this->user->user_data->value;
//        }
//        $allow = Settings::instance()->get('report') ? TRUE : FALSE;
//        
//        if ($allow){
//            $this->template->show_report = Url::site('user/report');
//        }else{
//            $this->template->show_report = Url::site('user/report_unactive');
//        }

        $this->template->user_id = $this->user->id;
        $this->template->count_new_orders = $count_new_orders;
        $this->template->count_new_files = $count_new_files;
        $this->template->count_new_parcels = $count_new_parcels;
        $this->template->count_new_messages = $count_new_in + $count_new_out;
        $this->template->count_new_payments = $count_new_payments;

        //$file_otcheta = $this->action_report($user_id);

        $filename = Kohana::config('main.dir.reports') . '/' . $user_id . '.xls';

        if (!file_exists($filename)) {

            $file_otcheta['error'] = '<div class="mt32 f11" style="color: red">Отчет отсутствует на сервере</div>';
        } else {
            //Request::instance()->send_file($filename, 'Отчет клиента #'.$id.' от '.date('d.m.Y H:i:s').'.xls');
            //Model_event::instance()->add('EV_XLS_DOWNLOAD','скачал xls-отчет');
            $time_old = stat($filename);
            $time_new = time();
            $time = $time_new - $time_old['mtime'];
            $file_otcheta['time'] = $this->sectime($time);
            //print_r($file_otcheta) ;

        }

        $this->content = View::factory('user/latestupdates')
            ->set('user_id', $this->template->user_id)
            ->set('count_new_orders', $this->template->count_new_orders)
            ->set('count_new_files', $this->template->count_new_files)
            ->set('count_new_parcels', $this->template->count_new_parcels)
            ->set('count_new_messages', $this->template->count_new_messages)
            ->set('count_new_payments', $this->template->count_new_payments)
            ->set('messages_new_in', $messages_new_in)
            ->set('messages_new_out', $messages_new_out)
            ->set('orders_new', $orders_new)
            ->set('parcels_new', $parcels_new)
            ->set('news_new', $news_new)
            ->set('files_new', $files_new)
            ->set('last_messages', $last_messages)
            ->set('last_orders', $last_orders)
            ->set('last_parcels', $last_parcels)
            ->set('last_files', $last_files)
            ->set('file_otcheta', $file_otcheta)
            ->set('payments_new', $payments_new)
            ->set('last_payments', $last_payments)
            ->set('count_new_payments', $count_new_payments);

    }

    public function sectime($secs)
    {
        $output = '';
        if ($secs >= 86400) {
            $days = floor($secs / 86400);
            $secs = $secs % 86400;
            $output = $days . ' дней';
            if ($days != 1) $output .= '/я';
            if ($secs > 0) $output .= ', ';
        }
        if ($secs >= 3600) {
            $hours = floor($secs / 3600);
            $secs = $secs % 3600;
            $output .= $hours . ' часов';
            if ($hours != 1) $output .= '/a';
            if ($secs > 0) $output .= ', ';
        }
        if ($secs >= 60) {
            $minutes = floor($secs / 60);
            $secs = $secs % 60;
            $output .= $minutes . ' минут';
            if ($minutes != 1) $output .= 'ы';
            if ($secs > 0) $output .= ', ';
        }
        $output .= $secs . ' секунд';
        if ($secs != 1) $output .= 'ы';
        return $output;
    }

} // End User