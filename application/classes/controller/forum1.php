<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Forum1 extends Controller_Abstract {

    public $content = 'forum/index';
    private static $libs_included=FALSE;
    
    public function action_index()
    {
        $this->auto_render = false;
        //$this->template = View::factory($this->template);
        //$this->request->response = $this->template;

        //$this->request->response = View::factory('forum/index');
        // Два раза повторять не надо
        if (self::$libs_included)
		return TRUE;
        define('IN_PHPBB', true);
	define('PHPBB_DB_NEW_LINK', 1);
	$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : $_SERVER['DOCUMENT_ROOT'].'board/';
        $phpEx = substr(strrchr(__FILE__, '.'), 1);
	$GLOBALS['phpbb_root_path'] = $phpbb_root_path;
	$GLOBALS['phpEx'] = $phpEx;
	require_once($phpbb_root_path . 'common_kohana.' . $phpEx);
	require_once($phpbb_root_path . 'includes/functions_user.' . $phpEx);
	require_once($phpbb_root_path . 'includes/acp/auth.' . $phpEx);
        
	// Start session management
	$user->session_begin();
        
	self::$libs_included = TRUE;
	return TRUE;
        
    }

    public function action_columns($table = NULL)
    {
//        $table = 'messages';

        $columns_a = Database::instance()->list_columns($table);

        $columns = array();

        foreach ($columns_a as $name => $data)
        {
            echo "'$name' => array('type' => '{$data['type']}'),<br />\n";
        }
    }

    public function action_test()
    {
        $result = ORM::factory('user', ORM::factory('role', array('name' => 'admin')))->as_array();
echo View::factory('profiler/stats');
       
    }

} // End Index
