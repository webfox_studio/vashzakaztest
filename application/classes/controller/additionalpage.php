<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Additionalpage extends Controller_Abstract {
    
   
    
    public function action_index()
    {
        //получаем переменные
        $idpage = $this->request->param('idpage');
        

                $pages = ORM::factory('additionalpage')
                        ->where('links', '=', $idpage)
                        ->find();
                $this->content->pages = $pages;

                $this->set_content('additionalpage/index');

        
    }
} // Admin Editor