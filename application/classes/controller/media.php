<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Media extends Kohana_Controller{

    public function action_index( $file)
    {
	$filename = DOCROOT.'/order_images/'.$file;
        if (! file_exists($filename)){
        		// Или ошибку 404, если файл не найден
            $this->request->status = 404;
        }
        else{
            Request::instance()->send_file($filename, $file);
        }
    }
}
?>