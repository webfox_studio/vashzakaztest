<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Messages extends Controller_Abstract {

    var $email = 'yaroslav25mt@gmail.com';
    
    public function before()
    {
        parent::before();

        if (! Auth::instance()->logged_in())
        {
            $this->request->redirect('access_denied?ret='.$this->request->uri);
        }
    }

    public function action_index()
    {
        $this->title(Lang::get('title.messages.index'));

        $this->set_content('messages/index');

        $messages = ORM::factory('message');
        
        $this->content->count_all_in = $messages->where('whom_id', '=', $this->user->id)->where('deleted', '=', 0)->count_all();
        $this->content->count_new_in = $messages->where('whom_id', '=', $this->user->id)->where('deleted', '=', 0)->where('readed', '=', 0)->count_all();
        $this->content->count_all_out = $messages->where('user_id', '=', $this->user->id)->where('deleted', '=', 0)->count_all();
        $this->content->count_new_out = $messages->where('user_id', '=', $this->user->id)->where('deleted', '=', 0)->where('readed', '=', 0)->count_all();
        
        $this->content->count_all_archive = $messages->where('deleted', '=', 1)->where_open()->where('whom_id', '=', $this->user->id)->or_where('user_id', '=', $this->user->id)->where_close()->count_all();
        $this->content->count_new_archive = $messages->where('whom_id', '=', $this->user->id)->where('deleted', '=', 1)->where('readed', '=', 0)->count_all();
    }

    public function in_out($in = TRUE)
    {
        $this->title(Lang::get('title.messages.inbox'));

        $this->set_content('messages/in_out');

        $messages = ORM::factory('message');

        $total_messages = $messages
            ->where($in ? 'whom_id' : 'user_id', '=', $this->user->id)
            ->where('deleted', '=', 0)
            ->count_all();

        $pagination = Pagination::factory(array(
            'total_items'    => $total_messages,
            'items_per_page' => 50,
        ));

        $messages = $messages
            ->where($in ? 'whom_id' : 'user_id', '=', $this->user->id)
            ->where('deleted', '=', 0)
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('time', 'DESC')
            ->order_by('id', 'DESC')
            ->find_all();

        $this->content->in = $in;
        $this->content->messages = $messages;
        $this->content->pagination = $pagination;
    }

    public function action_inbox()
    {
        $this->in_out(TRUE);
    }

    public function action_outbox()
    {
        $this->in_out(FALSE);
    }

    public function action_item($id)
    {
        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_m');

        $this->set_content('messages/item');

        $message = ORM::factory('message', $id);

        if ($this->user->id != $message->user->id
            AND $this->user->id != $message->whom_id)
        {
            $this->set_content('messages/not_access');
            return;
        }

        $in = ($this->user->id == $message->whom_id) ? TRUE : FALSE;

        if (! $message->readed)
        {
            $message->readed = TRUE;
            $message->save();
        }

        $count = $message->messages
            ->count_all();

        $pagination = Pagination::factory(array(
            'total_items' => $count,
            'items_per_page' => 50,
        ));

        $messages = $message->messages
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->find_all();

       //var_dump($_POST);
        //die();
        $form = Formo::get('message_answer');
        //var_dump($form);die();
        if (isset($_POST['preview'])){
                $this->content->pre_message = array(
                    'time' => Date::formatted_time(),
                    'content' => $_POST["Message_Answer"]['message']
                );
                $form->message->val( $_POST["Message_Answer"]['message']);
        }

        if ($form->sent())
        {
            $whom_id = $message->user_id;

            
                if ($form->validate() )
                {
                    $message_message = ORM::factory('message_message');
                    $message_message->message_id = $message->id;
                    $message_message->user_id = $this->user->id;
                    $message_message->content = $form->message->val();
                    $message_message->time = Date::formatted_time();

                    $message->readed = 1;
                    $message->readed2 = 0;
                    if($message->important == 1){
                        $message->admin_important = $message->important;
                    }
                    $message->important = $form->important->val() ? 1 : 0;
                    
                    //$message->admin_important = $message->admin_important;
    //                $message->deleted = 0;
    //                $message->deleted2 = 0;
                    $message->time = Date::formatted_time();
                    $message->save();
                    $message_message->save();
                    Model_event::instance()->add('EV_MESSAGES_REPLY','новый пост в сообщении <a href="/admin/messages/item/'.$message->id.'">'.$message->title.'('.$message->user->user_data->name.')</a>');
                    $head_email = 'Новый пост в сообщении #'.$message->id.' от клиента #'.$this->user->id.' '.$this->user->user_data->name;
                    $text_email = 'Содержание:'."\n\n".$message_message->content;

                    //Email::send_admin($head_email, $text_email);
                    EmailHelper::send($this->email, $head_email, $text_email, FALSE, TRUE);
                    

                    $this->request->redirect('messages/item/'.$message->id);

                    $this->set_content('messages/sended');
                }
              



        }


        $this->content->message = $message;
        $this->content->messages = $messages;
        $this->content->user_id = $this->user->id;
        $this->content->form = $form;
        $this->content->in = $in;
        $this->content->user = $this->user;
        Model_event::instance()->add('EV_MESSAGES_READ','читает сообщение <a href="/admin/messages/item/'.$message->id.'">'.$message->title.'('.$message->user->user_data->name.')</a>');
        if (Request::$is_ajax)
        {
            echo $this->content->render();
        }
    }

    public function action_chat_send()
    {
        if (Request::$is_ajax)
        {
            echo $result;
        }
    }

    public function action_chat_get()
    {
        if (Request::$is_ajax)
        {
            echo $result;
        }
    }

    public function action_send()
    {
        //$this->set_content('not_allow');
        
        $this->title(Lang::get('title.messages.send'));

        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_m');

        $this->set_content('messages/send');

        $form = Formo::get('Message_Send');

        if (isset($_POST['preview'])){
                $this->content->pre_message = array(
                    'time' => Date::formatted_time(),
                    'content' => $_POST["Message_Send"]['message'],
                    'head' => $_POST["Message_Send"]['head']
                );
                $form->message->val( $_POST["Message_Send"]['message']);
                $form->head->val( $_POST["Message_Send"]['head']);
        }
        if ($form->submit->val())
        {
            //echo $this->user->id; return;
            if ($form->validate())
            {
                
                $message = ORM::factory('message');
                $message_message = ORM::factory('message_message');

                $message->user_id = $this->user->id;
                $message->title = $form->head->val();
                $message_message->user_id = $this->user->id;
                $message_message->content = $form->message->val();
                $message_message->time = Date::formatted_time();
                $message->time = Date::formatted_time();
//                $message->whom_id = $this->user;
                $message->whom_id = '';
                $message->readed = TRUE;
                $message->readed2 = 0;
                $message->important = $form->important->val() ? 1 : 0;
                $message->deleted = 0;
                $message->deleted2 = 0;

                $message->save();
                $message_message->message_id = $message->id;
                $message_message->save();
                Model_event::instance()->add('EV_MESSAGES_NEW','новое сообщение <a href="/admin/messages/item/'.$message->id.'">'.$message->title.'</a>');
                $head_email = 'Новое сообщение #'.$message->id.' от клиента #'.$this->user->id.' '.$this->user->user_data->name;
                $text_email = 'Тема: '.$message->title."\n\n".'Содержание:'."\n\n".$message_message->content;

                //Email::send_admin($head_email, $text_email);
                
                EmailHelper::send($this->email, $head_email, $text_email, FALSE, TRUE);
                
                
                $text_message = DB::update('users')
                    ->set(array('hide_message' => 1))
                    ->where('id', '=', $this->user)->execute();
                
                $this->request->redirect('messages/item/'.$message->id);
               
                $this->set_content('messages/sended');
            }
        }
        
        $query = DB::select('*')
        ->from('users')
        ->where('id', '=', $this->user)->execute();
        //echo mysql_error();
        //echo print_r($query);
        
        $this->content->user = $this->user;
        $this->content->form = $form;
        $this->content->pipmes = $query['0']['hide_message'];
    }

    public function action_delete($id)
    {
        $message = ORM::factory('messages', $id);

        $user_id = Auth::instance()->get_user()->id;

        if ($user_id != $message->user_id
            AND $user_id != $message->whom_id)
        {
            $this->set_content('messages/not_access');
            return;
        }

        $in = (Auth::instance()->get_user()->id == $message->whom_id) ? TRUE : FALSE;

        if ($in)
        {
            $message->deleted = TRUE;
            $message->save();
            Model_event::instance()->add('EV_MESSAGES_DELETE','удалил сообщение <a href="/admin/messages/item/'.$message->id.'">'.$message->title.'</a>');

        }

        if (isset($_GET['return']))
            $this->request->redirect(isset($_GET['return']) ? $_GET['return'] : '/');

        $this->set_content('messages/deleted');
    }
    
    public function action_archive($in = TRUE)
    {
       // $this->title(Lang::get('title.messages.inbox'));

        $this->set_content('messages/archive');

        $messages = ORM::factory('message');

        $total_messages = $messages
            //->where($in ? 'whom_id' : 'user_id', '=', $this->user->id)
            ->where('deleted', '=', 1)
            ->and_where('whom_id', '=', $this->user->id)
            ->or_where('user_id', '=', $this->user->id)
            //->where('deleted', '=', 1)
            ->count_all();

        $pagination = Pagination::factory(array(
            'total_items'    => $total_messages,
            'items_per_page' => 50,
        ));
        
        $messagess = $messages
            //->where($in ? 'whom_id' : 'user_id', '=', $this->user->id)
            ->where('deleted', '=', 1)
            ->where_open()
            ->where('whom_id', '=', $this->user->id)
            ->or_where('user_id', '=', $this->user->id)
            ->where_close()
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('time', 'DESC')
            ->order_by('id', 'DESC')
            ->find_all();

        $this->content->in = $in;
        $this->content->messages = $messagess;
        $this->content->pagination = $pagination;
        $this->content->userid = $this->user->id;
    }
    
    public function action_archiveopen($id){
        $this->set_content('messages/archive');
        
        $messages = ORM::factory('message')->find($id);
//        echo '<pre>';
//        print_r($messages);
//        echo '<pre>';
        $messages->deleted = 0;
        $messages->save();
        
        $this->request->redirect(Request::$referrer);
    }
    
    public function action_newdelete($id){
        $this->set_content('messages/in_out');
        $messages = ORM::factory('messages')->find($id);
        $messages->deleted = 1;
        $messages->save();
        
        $this->request->redirect(Request::$referrer);
    }
    
} // End Messages