<?php defined('SYSPATH') or die('No direct script access.');

class Controller_News extends Controller_Abstract {

    public $content = 'news/index';

    public function before()
    {
        parent::before();
        
        $this->content->schedule = @file_get_contents(Kohana::config('main.schedule'));

        $twitter = Twitter::factory();
        $tweets = $twitter->get_tweets();

        $this->content->tweets = $tweets;
    }

    public function action_index($all = FALSE)
    {
        $this->add_js('media/lib/fancybox/jquery.fancybox-1.3.4.pack', TRUE);
        $this->add_css('media/lib/fancybox/jquery.fancybox-1.3.4', TRUE);

        $m_news = Model_News::instance();
        $last_news_id = $m_news->get_last_id();
        
        $user = Auth::instance();
        
        if ($user->logged_in() AND $user->get_user()->user_data->last_news_id < $last_news_id)
        {
//            $this->add_js('last_news');
//            $this->add_js_line('last_news('.$last_news_id.');');
/*            $this->add_js_line("$(document).ready(function() {
                $.fancybox({
        href: 'http://vashzakaz.local/news/84'
    }); });");*/

            $user = $user->get_user();
            $user->user_data->last_news_id = $last_news_id;
            $user->user_data->save();
        }

        if (! $all)
        {
            $news = $m_news->get_all(5, FALSE, 'DESC');
        }
        else
        {
            $pagination = Pagination::factory(array(
                'total_items'    => $m_news->count(),
                'items_per_page' => 15,
            ));

            $news = $m_news->get_all($pagination->items_per_page, $pagination->offset, 'DESC');

            $this->content->pagination = $pagination->render();
            $this->content->all_news = TRUE;
        }

        $this->content->news = $news;
    }

    public function action_all()
    {
        $this->set_content('news/all');
        
        $this->action_index($all = TRUE);
    }

    public function action_item($id)
    {
        $news_item = Model_News::instance()->get($id);

        if (Request::$is_ajax)
        {
            $this->content = View::factory('news/news', $news_item);
//            echo $result;
        }
        else
        {
            $this->set_content('news/item');

            $this->content->news = array($news_item);
        }
    }

    public function action_last()
    {
        
    }

} // End News