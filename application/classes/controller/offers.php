<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Offers extends Controller_Abstract {
	
	public function before()
	{
		parent::before();
		
		if (! Auth::instance()->logged_in())
		{
			$this->request->redirect('access_denied?ret='.$this->request->uri);
		}
	}
	
    public function action_index()
    {
        //$this->set_content('not_allow');
        
        $this->title(Lang::get('title.offers.index'));

        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_m');

        $this->set_content('offers/index');

        $form = Formo::get('offer');

        if ($form->sent() AND $form->validate())
        {
            $offer = ORM::factory('offer');

            $offer->user_id = $this->user->id;
            $offer->title = $form->head->val();
            $offer->message = $form->message->val();
            $offer->time = Date::formatted_time();
            $offer->readed = 0;

            $offer->save();

            $this->set_content('offers/sended');
            Model_event::instance()->add('EV_PREDLOG_VIEW','написал новое предложение по улчшению сайта <a href="/admin/offers/item/'.$offer->id.'">'.$offer->title.'</a>');
        }

        $this->content->form = $form;
         
    }

} // End Controller_Offers