<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * 
 * Description:  Common controller for webpages (Articles, News, etc.)
 *
 * @author       Mamontov Andrey <andrvm@andrvm.ru>
 * @copyright    2011 Andrey Mamontov (andrvm)
 * @php          >=PHP 5.3.0
 * 
 * @date		 $Date: 2010-09-10 16:38:58 +0400 (Пт, 10 сен 2010) $
 * @revision	 $Revision: 195 $
 * @changeby	 $Author: andrvm $
 * 
 */

 abstract class Controller_Webpage extends Controller_Base {
 	
 	// model instance
 	//protected $_webpage;
 	// id
 	//protected $_id;
 	
 	
	 public function before(){
    
        parent::before();
        
	 
        $this->add_js('public/js/libs/fancybox/jquery.fancybox-1.3.4.pack', TRUE);
        $this->add_css('public/js/libs/fancybox/jquery.fancybox-1.3.4', TRUE);

	 }
 	
 	
 } // End Controller_WebPage