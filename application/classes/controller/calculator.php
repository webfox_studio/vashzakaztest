<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Calculator extends Controller
{


    public function action_country()
    {
        if ($_GET['country'] == 0) {
            $tmpl = '';
            return;
        }
        if ($_GET) {
            $get = $_GET['country'];
            $city = ORM::factory('calculator_resipient')->where('parent_id', '=', $get)->order_by('name', 'ASC')->order_by('id', 'ASC')->find_all();

            $tmpl = '';
            $tmpl .= '<option value="0">--</option>';
            foreach ($city as $k => $v):
                $tmpl .= "<option value='$v->id'>$v->name</option>";
            endforeach;
            echo $tmpl;
        }
    }

    public function action_alloption()
    {
        $option = ORM::factory('calculator_option')->find_all();
        //print_r($option);
    }

    public function checkSize($type, $value)
    {
        $coef = 0.3937008;
        $length = (int)$value['sizeLength'];
        $width = (int)$value['sizeWidth'];
        $height = (int)$value['sizeHeight'];

        if ($value['sizeType'] == 'in') {
            $length = $length / $coef;
            $width = $width / $coef;
            $height = $height / $coef;
        }
        if ($length > 120) {
            return false;
        }
        if ($width > 80) {
            return false;
        }
        if ($height > 50) {
            return false;
        }
        return true;
    }

    public function action_result()
    {
        $heft = ceil($_POST['heft']);
        $country_in = $_POST['country_in'];
        $city_in = $_POST['city_in'];
        $width = (isset($_POST['width'])) ? (int)$_POST['width'] : '1';
        $height = (isset($_POST['height'])) ? (int)$_POST['height'] : '1';
        $length = (isset($_POST['length'])) ? (int)$_POST['length'] : '1';

        if (empty($width) and empty($height) and empty($length)) {
            $pi_empty_in = 1;
        } else {
            $pi_empty_in = 0;
        }

        $result = DB::select(
            array('calculatoroptions.id', 'opt_id'),
            array('calculatoroptions.name', 'opt_name'),
            array('calculatoroptions.delivery_period', 'opt_delivery_period'),
            array('calculatoroptions.tracing', 'opt_tracing'),
            array('calculatoroptions.insurance', 'opt_insurance'),
            array('calculatoroptions.customsclearance', 'opt_customsclearance'),
            array('calculatoroptions.variant_reception', 'opt_variant_reception'),
            array('calculatorprices.heft', 'pr_heft'),
            array('calculatorprices.heft', 'pr_heft_type'),
            array('calculatorprices.price', 'pr_price'),
            array('calculatoroptions.newDelivery', 'newDelivery')
        )
            ->from('calculatorprices')
            ->join('calculatoroptions', 'RIGHT')
            ->on('calculatorprices.option_id', '=', 'calculatoroptions.id')
            ->where('calculatorprices.heft', '=', $heft)
            ->and_where('calculatoroptions.resipient_id', '=', $country_in)
            ->order_by('sort', 'ASC')
            ->execute();


        $result_new = array();
        for ($i = 0; $i < count($result); $i++):
            $result_new[$i]['opt_id'] = $result[$i]['opt_id'];
            $result_new[$i]['opt_name'] = $result[$i]['opt_name'];
            $result_new[$i]['opt_delivery_period'] = $result[$i]['opt_delivery_period'];
            $result_new[$i]['opt_tracing'] = $result[$i]['opt_tracing'];
            $result_new[$i]['opt_insurance'] = $result[$i]['opt_insurance'];
            $result_new[$i]['opt_customsclearance'] = $result[$i]['opt_customsclearance'];
            $result_new[$i]['opt_variant_reception'] = $result[$i]['opt_variant_reception'];
            $result_new[$i]['pr_heft'] = $result[$i]['pr_heft'];
            $result_new[$i]['pr_heft_type'] = $result[$i]['pr_heft_type'];
            $result_new[$i]['pr_price'] = $result[$i]['pr_price'];
            $result_new[$i]['newDelivery'] = $result[$i]['newDelivery'];

            //если Россия
            if ($country_in == 1) {
                switch ($result[$i]['opt_id']) {
                    //Priority Mail Express
                    case '10':
                        if (($width > 60) OR ((($length + $height) * 2 + $width) > 108)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            $cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = '$' . $result[$i]['pr_price'];
                        }
                        break;
                    //Priority Mail
                    case '11':
                        if (($width > 42) OR ((($length + $height) * 2 + $width) > 79)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            $cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = '$' . $result[$i]['pr_price'];
                        }
                        break;
                    //VashZakaz Cargo
                    case '12':
                        if (($width > 60) OR ((($length + $height) * 2 + $width) > 108)) {
                            $result_new[$i]['pr_price'] = '<span style="color: blue">Негабаритный груз. Стоимость доставки уточняйте у сотрудников сервиса <a href="http://vashzakaz.us/page/contacts" target="_blank">VashZakaz</a> </span>';
                            $result_new[$i]['oversized'] = 1;
                            $cargo = 1;
                        } else {
                            $result_new[$i]['pr_price'] = '$' . $result[$i]['pr_price'];
                            $result_new[$i]['oversized'] = 0;
                            $cargo = 0;
                        }
                        break;
                    //First-Class Mail
                    case '289':
                        if (($width > 24) OR ((($length + $height) * 2 + $width) > 36)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            $cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = '$' . $result[$i]['pr_price'];
                            $cargo = 0;
                        }
                        break;
                        // доставка морем
                    case '1285':
                        if (($width > 41) OR ((($length + $height) * 2 + $width) > 118) OR $heft > 44) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                        } else {
                            $result_new[$i]['pr_price'] = '$' . $result[$i]['pr_price'];
                        }
                        break;
                    case '1286':
                        if (($width > 59) OR ((($length + $height) * 2 + $width) > 118) OR $heft > 66) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                        } else {
                            $result_new[$i]['pr_price'] = '$' . $result[$i]['pr_price'];
                        }
                        break;
                }
                // по России добавлена новая доставка
                $newDelivery = DB::select(
                    array('calculatoroptions.id', 'opt_id'),
                    array('calculatoroptions.name', 'opt_name'),
                    array('calculatoroptions.delivery_period', 'opt_delivery_period'),
                    array('calculatoroptions.tracing', 'opt_tracing'),
                    array('calculatoroptions.insurance', 'opt_insurance'),
                    array('calculatoroptions.customsclearance', 'opt_customsclearance'),
                    array('calculatoroptions.variant_reception', 'opt_variant_reception'),
                    array('calculatorprices.heft', 'pr_heft'),
                    array('calculatorprices.heft', 'pr_heft_type'),
                    array('calculatorprices.price', 'pr_price')
                //array('calculatoroptions.newDelivery')
                )
                    ->from('calculatorprices')
                    ->join('calculatoroptions', 'RIGHT')
                    ->on('calculatorprices.option_id', '=', 'calculatoroptions.id')
                    ->where('calculatorprices.heft', '=', $heft)
                    ->and_where('calculatoroptions.resipient_id', '=', $city_in)
                    ->and_where('calculatoroptions.newDelivery', '=', 1)
                    //  ->order_by('calculatoroptions.newDelivery', 'asc')
                    ->as_object()->execute();
            }

            //если Украина. or $country_in == 195 or $country_in == 196
            if ($country_in == 2) {
                switch ($result[$i]['opt_id']) {
                    //Priority Mail Express
                    case '13':
                        if (($width > 36) OR ((($length + $height) * 2 + $width) > 79)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                        }
                        break;
                    //Priority Mail
                    case '14':
                        if (($width > 42) OR ((($length + $height) * 2 + $width) > 79)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                        }
                        break;
                    //VashZakaz Cargo
                    case '12':
                        if (($width > 60) OR ((($length + $height) * 2 + $width) > 108)) {
                            $result_new[$i]['pr_price'] = '<span style="color: blue">Негабаритный груз. Стоимость доставки уточняйте у сотрудников сервиса <a href="http://vashzakaz.us/page/contacts" target="_blank">VashZakaz</a> </span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                            //$cargo = 0;
                        }
                        break;
                    //First-Class Mail
                    case '290':
                        if (($width > 24) OR ((($length + $height) * 2 + $width) > 36)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                            //$cargo = 0;
                        }
                        break;
                }
                $cargo = 1;
            }

            //если Молдоваю  or $country_in == 195 or $country_in == 196
            if ($country_in == 195) {
                switch ($result[$i]['opt_id']) {
                    //Priority Mail Express
                    case '292':
                        if (($width > 36) OR ((($length + $height) * 2 + $width) > 79)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                        }
                        break;
                    //Priority Mail
                    case '293':
                        if (($width > 42) OR ((($length + $height) * 2 + $width) > 79)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                        }
                        break;
                    //VashZakaz Cargo
                    case '12':
                        if (($width > 60) OR ((($length + $height) * 2 + $width) > 108)) {
                            $result_new[$i]['pr_price'] = '<span style="color: blue">Негабаритный груз. Стоимость доставки уточняйте у сотрудников сервиса <a href="http://vashzakaz.us/page/contacts" target="_blank">VashZakaz</a> </span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                            //$cargo = 0;
                        }
                        break;
                    //First-Class Mail
                    case '291':
                        if (($width > 24) OR ((($length + $height) * 2 + $width) > 36)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                            //$cargo = 1;
                        }
                        break;
                }
                $cargo = 1;
            }

            //если Беларусь or $country_in == 195 or $country_in == 196
            if ($country_in == 196) {
                switch ($result[$i]['opt_id']) {
                    //Priority Mail Express
                    case '295':
                        if (($width > 36) OR ((($length + $height) * 2 + $width) > 79)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                        }
                        break;
                    //Priority Mail
                    case '296':
                        if (($width > 42) OR ((($length + $height) * 2 + $width) > 79)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                        }
                        break;
                    //VashZakaz Cargo
                    case '12':
                        if (($width > 60) OR ((($length + $height) * 2 + $width) > 108)) {
                            $result_new[$i]['pr_price'] = '<span style="color: blue">Негабаритный груз. Стоимость доставки уточняйте у сотрудников сервиса <a href="http://vashzakaz.us/page/contacts" target="_blank">VashZakaz</a> </span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                            //$cargo = 0;
                        }
                        break;
                    //First-Class Mail
                    case '294':
                        if (($width > 24) OR ((($length + $height) * 2 + $width) > 36)) {
                            $result_new[$i]['pr_price'] = '<span style="color: red">Превышены максимальные габариты</span>';
                            //$cargo = 1;
                        } else {

                            $result_new[$i]['pr_price'] = $result[$i]['pr_price'] . ' $';
                            //$cargo = 1;
                        }
                        break;
                }
                $cargo = 1;
            }

        endfor;


        if (count($result) != 0) {
            //$city_in != 7 санкт-петербург - убрал дополнительный вывод
            if ($city_in AND $city_in != 7) {
                $result_city = DB::select(
                    array('calculatoroptions.id', 'opt_id'),
                    array('calculatoroptions.name', 'opt_name'),
                    array('calculatoroptions.delivery_period', 'opt_delivery_period'),
                    array('calculatoroptions.tracing', 'opt_tracing'),
                    array('calculatoroptions.insurance', 'opt_insurance'),
                    array('calculatoroptions.customsclearance', 'opt_customsclearance'),
                    array('calculatoroptions.variant_reception', 'opt_variant_reception'),
                    array('calculatorprices.heft', 'pr_heft'),
                    array('calculatorprices.heft', 'pr_heft_type'),
                    array('calculatorprices.price', 'pr_price')
                //array('calculatoroptions.newDelivery')
                //array('calculatorresipient.price','res_price')
                )
                    ->from('calculatorprices')
                    ->join('calculatoroptions', 'RIGHT')
                    ->on('calculatorprices.option_id', '=', 'calculatoroptions.id')
                    ->where('calculatorprices.heft', '=', $heft)
                    ->and_where('calculatoroptions.resipient_id', '=', $city_in)
                    ->and_where('calculatoroptions.newDelivery', '=', 0)
                    //  ->order_by('calculatoroptions.newDelivery', 'asc')
                    ->as_object()->execute();
            } else {
                $city_in = '';
            }


            $staticText = Kohana::config('onlinecalculator');


            $template = 'onlinecalculator/resultTestNew';

            $resultTotals = array();
            for ($i = 0; $i < count($result_new); $i++) {
                if ($result_new[$i]['opt_id'] == 12) {
                    $type = 'cargo';
                } else {
                    $type = 'usps';
                    if($result_new[$i]['newDelivery'] == 2){
                        $type = 'sea';
                    }

                }
                $resultTotals[$type]['main'][] = array(
                    'opt_id' => $result_new[$i]['opt_id'],
                    'opt_name' => $result_new[$i]['opt_name'],
                    'opt_delivery_period' => $result_new[$i]['opt_delivery_period'],
                    'opt_tracing' => $result_new[$i]['opt_tracing'],
                    'opt_insurance' => $result_new[$i]['opt_insurance'],
                    'opt_customsclearance' => $result_new[$i]['opt_customsclearance'],
                    'opt_variant_reception' => $result_new[$i]['opt_variant_reception'],
                    'pr_heft' => $result_new[$i]['pr_heft'],
                    'pr_heft_type' => $result_new[$i]['pr_heft_type'],
                    'pr_price' => $result_new[$i]['pr_price'],
                    'oversized' => $result_new[$i]['oversized'],
                );
            }


            if (isset($resultTotals['cargo']['main']) && $resultTotals['cargo']['main'][0]['oversized'] == 0) {
                foreach ($result_city as $value):
                    $resultTotals['cargo']['additionally'][] = array(
                        'opt_id' => $value->opt_id,
                        'opt_name' => $value->opt_name,
                        'opt_delivery_period' => $value->opt_delivery_period,
                        'opt_tracing' => $value->opt_tracing,
                        'opt_insurance' => $value->opt_insurance,
                        'opt_customsclearance' => $value->opt_customsclearance,
                        'opt_variant_reception' => $value->opt_variant_reception,
                        'pr_heft' => $value->pr_heft,
                        'pr_heft_type' => $value->pr_heft_type,
                        'pr_price' => $value->pr_price . ' руб.',
                    );
                endforeach;
            }

            if ($newDelivery) {
                foreach ($newDelivery as $value):
                    $value->pr_price = '$' . $value->pr_price;
                    if (!$this->checkSize('new', $_POST['newParams'])) {
                        $value->pr_price = '<span style="color: red">Превышены максимальные габариты</span>';
                    }

                    $resultTotals['new']['main'][] = array(
                        'opt_id' => $value->opt_id,
                        'opt_name' => $value->opt_name,
                        'opt_delivery_period' => $value->opt_delivery_period,
                        'opt_tracing' => $value->opt_tracing,
                        'opt_insurance' => $value->opt_insurance,
                        'opt_customsclearance' => $value->opt_customsclearance,
                        'opt_variant_reception' => $value->opt_variant_reception,
                        'pr_heft' => $value->pr_heft,
                        'pr_heft_type' => $value->pr_heft_type,
                        'pr_price' => $value->pr_price,
                    );
                endforeach;
            }

            if ($_SERVER['REMOTE_ADDR'] == '95.133.232.90') {
                // print_r($resultTotals);
            }


            $view = View::factory($template)
                ->bind('resultTotals', $resultTotals)
                ->bind('result', $result_new)
                ->bind('result_city', $result_city)
                ->bind('cargo', $cargo)
                ->bind('newDelivery', $newDelivery)
                ->bind('pi_empty_in', $pi_empty_in)
                ->bind('city', $city_in)
                ->bind('staticText', $staticText);
            //->bind('city_price',$city_price->price);

            if (isset($_POST['buyont'])) {
                $commission = $this->summCommission($_POST['buyont']);

                $view->bind('commission', $commission);
            }


        } else {
            //$view = 'По выбранным параметрам нет подходящих вариантов доставки';
            // в инчах
            if (($width > 60) OR ((($length + $height) * 2 + $width) > 108)) {
                $price = '<span style="color: blue">Негабаритный груз. Стоимость доставки уточняйте у сотрудников сервиса <a href="http://vashzakaz.us/page/contacts" target="_blank">VashZakaz</a> </span>';
                $cargo = 0;
            } else {
                $price = '$' . $heft * 5.81;
            }
            //$price = $heft * 5.81;
            $view = View::factory('onlinecalculator/cargo')
                ->bind('city', $city_in)
                ->bind('country_in', $country_in)
                ->bind('price', $price)
                ->bind('pi_empty_in', $pi_empty_in);
        }
        echo $view;
    }

    public function summCommission($value = array())
    {
        $newValue = array();
        $newValue['items'] = $value;
        for ($i = 0; $i < count($value); $i++) {
            $value[$i]['summTotal'] = array_sum($value[$i]);
            $tempCommission = $value[$i]['summTotal'] * 0.05;
            $newValue[$i]['temp'] = $tempCommission;
            switch ($tempCommission) {
                case $tempCommission < 5:
                    $commission = 5;
                    break;
                case $tempCommission >= 5 && $tempCommission < 150:
                    $commission = $tempCommission;
                    break;
                case $tempCommission >= 150:
                    $commission = 150;
                    break;
                default:
                    $commission = 0;
                    break;
            }
            $newValue['orders'][$i]['commission'] = $commission;
            $newValue['summTotal'] += $commission;
            $newValue['summTotal_1'] += $value[$i]['totalSummBuyout'];
            $newValue['summTotal_2'] += $value[$i]['totalExtraSummBuyout'];
        }
        $newValue['summTotal'] = $newValue['summTotal'] + (count($value) * 3);
        return $newValue;
    }

    //-----------------admin

    public function action_updateadmin()
    {
        $post = $_POST;
        if ($post) {

            $data = array($post['col'] => $post['val']);

            $res = ORM::factory('calculator_option', $post['id'])
                ->values($data)
                ->save();
            if ($res) {
                echo 'OK';
            }
        }
    }

    public function action_updateprice()
    {
        $post = $_POST;
        if ($post) {

            $data = array($post['col'] => $post['val']);

            $res = ORM::factory('calculator_price', $post['id'])
                ->values($data)
                ->save();
            if ($res) {
                echo 'OK';
            }
        }
    }

    public function action_updateresipient()
    {
        $post = $_POST;
        if ($post) {

            $data = array($post['col'] => $post['val']);

            $res = ORM::factory('calculator_resipient', $post['id'])
                ->values($data)
                ->save();
            if ($res) {
                echo 'OK';
            }
        }
    }

} // End Abstract