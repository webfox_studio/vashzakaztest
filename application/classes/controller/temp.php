<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Temp extends Controller_Template
{
    public $template = 'empty_layout';
    public function action_registration_success()
    {
        echo View::factory('static_templates/registration')
            ->bind('username', Auth::instance()->get_user()->user_data->name);
    }

} // End Templates