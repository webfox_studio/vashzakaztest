<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Useful extends Controller_Faq {

    public $content = 'useful/index';

    public function action_index(){
    
        $m_useful = Model_Useful::instance();

        $useful_cats = $m_useful->get_cats();
       
        $this->content->useful_cats = $useful_cats;
       
    }
    
    // views a single page
	public function action_view(){
		
		$id = (int) Request::instance()->param('id', 0);
		
		$this->set_content('useful/view');
		
		$this->content->useful = Model_Useful::instance()->get_by_id($id);       
        
    }
    
	// views cat's items
	public function action_cat(){
		
		$id = (int) Request::instance()->param('id', 0);
		
		$this->set_content('useful/cat');
		
		$this->content->useful_cat = Model_Useful::instance()->get_by_cat($id);
		$this->content->title 	   = Model_Useful::instance()->get_cat($id);        
        
    }

} // End Useful