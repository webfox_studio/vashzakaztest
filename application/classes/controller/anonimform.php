<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Anonimform extends Controller_Abstract {

    public $content = 'anonimform/index';

    public function before()
    {
        parent::before();
        $this->content->services = @file_get_contents(Kohana::config('main.services'));
    }

    public function action_index($all = FALSE)
    {
//        echo '<pre>';
//            print_r($_POST);
//        echo '</pre>';
        
        if($_POST){
        //yaroslav25mt@gmail.com
            //$email[] = '';
        $email = 'yaroslav25mt@gmail.com';
        $head_email = 'Опрос клиентского мнения';
//        $text_email = '<h2>Ответ пользователя</h2><br /><br />'.
//                	'Содержание:'."\n\n".$message_message->content.'<br /><br />'.
//        HTML::anchor(URL::site('messages/item/'.$message->id, TRUE), 'перейти к сообщению');
        
        $text_email = '<h2>Ответ пользователя</h2><br /><br />'
                        .'Как долго Вы не сотрудничаете с нашим сервисом?<br>'
                        .'<strong>'.$_POST['anonim']['trabl'].'</strong><br>'
                        . '<strong>'.$_POST['anonim']['other'] .'</strong><br><br>'
                        .'Если возможно, напишите более подробно, что повлияло на прекращение сотрудничества с VashZakaz<br>'
                        .'<strong>'.$_POST['anonim']['detail'].'</strong><br><br>'
                        .'Продолжаете ли Вы активно покупать за рубежом, используя услуги других посредников или сервисов? Если да, по каким причинам (критериям) сделали новый выбор?<br>'
                        .'<strong>'.$_POST['anonim']['other_buy'].'</strong><br><br>'
                        .'Какие минусы, на Ваш взгляд, наиболее существенные в работе нашей компании?<br>'
                        .'<strong>'.$_POST['anonim']['minus'].'</strong><br><br>'
                        .'Что бы Вы могли порекомендовать в первую очередь изменить в нашей работе, чтобы ситуация существенно улучшилась?<br>'
                        .'<strong>'.$_POST['anonim']['recommendation'].'</strong><br><br>'
                        .'Запомнились ли Вам какие-либо положительные моменты в работе с нами, которые Вы могли бы охарактеризовать как плюсы, которые можно ставить в пример другим посредникам?<br>'
                        .'<strong>'.$_POST['anonim']['positive'].'</strong><br><br>'
                        .'Имя - ' . $_POST['anonim']['fio'].'<br>'
                        .'Логин - '. $_POST['anonim']['login'].'<br>'
                        .'Контакты - '. $_POST['anonim']['contact'].'<br>';
        
                //Model_event::instance()->add('EV_MESSAGES_REPLY','новый пост в сообщении <a href="/admin/messages/item/'.$message->id.'">'.$message->title.'('.$message->user->user_data->name.')</a>');
                
        //Email::send($email, $head_email, $text_email);
        //$address = Kohana::config('email.admin');
        //Email::send_admin($address, $head_email, $text_email);
        //$address = Kohana::config('email.admin');

        // Email::send($address, $head_email, $text_email);
        
        //Email::send('bylo4ko@mail.ru', $head_email, $text_email);
            
        EmailHelper::send($email, $head_email, $text_email, FALSE, TRUE);
        
                $this->request->redirect('anonimform/good');
        }
    }
    
    public function action_good(){
        $this->set_content('anonimform/good');
    }

    

} // End Nmain