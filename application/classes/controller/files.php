<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Files extends Controller_Abstract {

    public $content = 'files/index';

    public function before()
    {
        parent::before();

    	if (! Auth::instance()->logged_in())
        {
            $this->request->redirect('access_denied?ret='.$this->request->uri);
        }

        $this->user_id = Auth::instance()->get_user()->id;        
    }
    
    public function action_index()
    {
        $this->set_content('files/userfiles');
        if(!empty($_FILES)){
            $files = Validate::factory($_FILES);
            $files->rule('file_name', 'Upload::size', array('3M'));

    //         print_r($_FILES);
    //         print_r($files);
    //         die();
            if($files->check()){
                 $filename = $this->user_id.'_'.substr(md5($_FILES['file_name']['name']),0,15);
                //Upload::save($_FILES['file_name'],$_FILES['file_name'].$this->user->id,'upload');
                Upload::save($_FILES['file_name'], $filename, 'upload');
                DB::insert('files')
                ->columns(array('name','file_name','user_id', 'date_time', 'readed'  ))
                ->values(array($_FILES['file_name']['name'],$filename, $this->user->id, time(),'1'))
                ->execute();
                 
            }else{
                $this->content->error = 'Ошибка загрузки файла на сервер.';
            }
        }
        //$this->set_content('files/userfiles');
        
        $files = ORM::factory('files');
        $count_files = $files->where('user_id', '=', $this->user_id)->count_all();
        $pagination = Pagination::factory(array(
                'total_items'    => $count_files,
                'items_per_page' => 15,
            ));

        $files = ORM::factory('files')
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->where('user_id', '=', $this->user_id)
            ->order_by('readed','ASC')
            ->order_by('id','DESC')
            ->find_all()->as_array();
//            print_r($files);
//         die();
        $this->content->files = $files;
        $this->content->pagination = $pagination;
    }

    public function action_userfiles()
    {
        $this->set_content('files/userfiles');
        $files = ORM::factory('files')
            ->where('user_id', '=', $user_id)
            ->find_all();
        $this->content->files = $files;
    }

    public function action_download($id)
    {

        $files = ORM::factory('files')
            ->where('user_id', '=', $this->user_id)
            ->find($id)->as_array();
        //print_r($files);
        //die();
        if (! file_exists(DOCROOT.'upload/'.$files['file_name'])){

            $this->set_content('files/file_not_find');
        }
        else{
             $file = ORM::factory('files');
            $file = $file->find($id);
            //print_r($files);
            //die();
            $file->readed = true;
            $file->save();
            Request::instance()->send_file(DOCROOT.'upload/'.$files['file_name'], $files['name']);
           
            //Model_event::instance()->add('EV_XLS_DOWNLOAD','скачал xls-отчет');
        }
        
        //    $this->set_content('user/report_unactive');
        
    }
} // End Faq
