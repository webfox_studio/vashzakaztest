<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Statistics extends Controller_Abstract {

    protected $user_id;
    //protected $is_parcel = FALSE;

    //protected $_model_orders;
    //protected $_model_messages;

    public function before()
    {
        parent::before();

    	if (! Auth::instance()->logged_in())
        {
            $this->request->redirect('access_denied?ret='.$this->request->uri);
        }
        
        $this->user_id = Auth::instance()->get_user()->id;
        //$this->is_parcel = $this->request->param('is_parcel', FALSE);

        //$this->_model_orders = ($this->is_parcel) ? 'parcels' : 'orders';
        //$this->_model_messages = ($this->is_parcel) ? 'parcel_messages' : 'order_messages';

        //$this->content->is_parcel = $this->is_parcel;
        
        $this->add_css('/media/css/accordion.css');
        $this->add_js('/media/js/accordion.js');
        
    }

    public function action_index()
    {
        $this->set_content('statistics/main');

        //$find_form = Formo::get('order_find');

//        if ($find_form->sent() AND $find_form->validate())
//        {
//            $order = ORM::factory('orders')->find($find_form->order_num->val());
//
//            if (! $order->pk() OR $order->user_id != $this->user_id)
//            {
//                $find_form->order_num->error('Этот заказ вам не доступен');
//                $find_form->validate();
//            }
//            else
//            {
//                $this->request->redirect((empty($this->is_parcel) ? 'order' : 'parcel').'/id'.$find_form->order_num->val());
//            }
//        }

//        $count_unread = ORM::factory($this->_model_orders)
//            ->where('user_id', '=', $this->user_id)
//            ->where('readed', '=', 0)
//            ->count_all();
//
//        $count_processed = ORM::factory($this->_model_orders)
//            ->where('user_id', '=', $this->user_id)
//            ->where('processed', '=', 1)
//            ->count_all();
//
//        $count_all = ORM::factory($this->_model_orders)
//            ->where('user_id', '=', $this->user_id)
//            ->count_all();
        
          
          
          
          
          
          
          
        
        $this->content->main = $this->set_content('statistics/main');
        
        
        
        
        $this->content->count_unread = $count_unread;
        $this->content->count_processed = $count_processed;
        $this->content->count_all = $count_all;
        $this->content->find_form = $find_form;
    }

    public function action_finance()
    {
        
        
        $this->set_content('statistics/finance');
        
        $count = ORM::factory('statistics_finance')
            ->where('xls_user_id', '=', $this->user_id)
            ->count_all();
        $pagination = Pagination::factory(array(
            'total_items'    => $count,
            'items_per_page' => 20,
        ));
        $content = ORM::factory('statistics_finance')
            ->where('xls_user_id', '=', $this->user_id)
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->find_all();
        $this->content->content = $content;
        $this->content->pagination = $pagination;
        //echo 111111111;
    }
    
    public function action_order()
    {
        $this->set_content('statistics/order');
        $count = ORM::factory('statistics_zakaz')
            ->where('xls_user_id', '=', $this->user_id)
            ->count_all();
        $pagination = Pagination::factory(array(
            'total_items'    => $count,
            'items_per_page' => 20,
        ));
        $content = ORM::factory('statistics_zakaz')
            ->where('xls_user_id', '=', $this->user_id)
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->find_all();
         $this->content->content = $content;
         $this->content->pagination = $pagination;
         //$this->content->content = allmain('order', 'statistics_zakaz', 'xls_user_id', $this->user_id);
        //echo 111111111;
    }
    
     public function action_dispatch()
    {
         
         $count = ORM::factory('statistics_otpravki')
            ->where('xls_user_id', '=', $this->user_id)
            ->find_all();
         $pagination = Pagination::factory(array(
            'total_items'    => $count,
            'items_per_page' => 20,
        ));
         $content = ORM::factory('statistics_otpravki')
            ->where('xls_user_id', '=', $this->user_id)
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->find_all();
         $this->content->content = $content;
        $this->set_content('statistics/dispatch');
        $this->content->pagination = $pagination;
        
        //echo 111111111;
    }   
    
      public function action_storage()
    {
        $this->set_content('statistics/storage');
        $count = ORM::factory('statistics_sklad')
            ->where('xls_user_id', '=', $this->user_id)
            ->count_all();
        $pagination = Pagination::factory(array(
            'total_items'    => $count,
            'items_per_page' => 20,
        ));
        $content = ORM::factory('statistics_sklad')
            ->where('xls_user_id', '=', $this->user_id)
                ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->find_all();
        $this->content->content = $content;
        $this->content->pagination = $pagination;
        //echo 111111111;
    }  
    
    

    
    
    
    
    
    
    public function items(ORM &$orders)
    {
        $this->set_content('orders/items');

        $orders = $orders->find_all();

        $this->content->orders = $orders;
    }

    public function action_new()
    {
        //$this->set_content('not_allow');
		
		$this->add_js('/media/js/sliding.form.js');
		$this->add_js('/media/js/jquery.bubblepopup.v2.3.1.min.js');
		$this->add_js('/media/js/custom.js');
		$this->add_css('/media/css/jquery.bubblepopup.v2.3.1.css');
		$this->add_css('/media/css/style.css');

        $this->set_content('orders/new');
        
        if($this->request->param('id_zakaz')){ $this->content->id_zakaz = $this->request->param('id_zakaz'); } else {$this->content->id_zakaz = '';};
        
        
        $form = Builderform::instance(empty($this->is_parcel) ? 'zakaz'.$this->content->id_zakaz : 'posilka');
        
        $images_html = '<br/>';
        $files_html = "";
        $error_html = "";
        $count = 0;
        $files_to_copy = array();
        if(isset($_POST['images_count'])){
            $images_count = $_POST['images_count'];
        
        $upload_images = array();

//        if(!empty($_FILES)){
            $files = Validate::factory($_FILES);
            $images_html = '';
            //$images_html = 'Изображения товаров:<br/>';
            for($i=1; $i<=$images_count;$i++){
                if (isset($_FILES['image_'.$i])){
                    $upload_images[$i]['name'] = $this->user->id.time().'_'.substr(md5($_FILES['image_'.$i]['name']),10).'.'.substr(strrchr($_FILES['image_'.$i]['name'], "."), 1);
                    $upload_images[$i]['file'] = $_FILES['image_'.$i];
                    $files->rule('image_'.$i, 'Upload::size', array('3M'))->rule('image_'.$i, 'Upload::type', array(Kohana::config('upload.allow_file_extensions')));
                    
                }
                if(isset($_POST['image_filename_'.$i])){
                    $count++;
                    $images_html .= '<a href="http://vashzakaz.us/order_images/'.$_POST['image_filename_'.$i].'">http://vashzakaz.us/order_images/'.$_POST['image_file_realname_'.$i].'</a><br/>';
                    $files_html .= '<p class="mt8" id="image_1_'.$count.'" filename="'.$_POST['image_filename_'.$i].'">'.$_POST['image_file_realname_'.$i].'
                        <input type="hidden" name="image_filename_'.$count.'" value="'.$_POST['image_filename_'.$i].'">
                        <input type="hidden" name="image_file_realname_'.$count.'" value="'.$_POST['image_file_realname_'.$i].'">
                        <a style="margin-left:8px;" href="javascript:void(0)" onClick="del_image_ajax(\'1_'.$count.'\')" >Удалить</a></p>';
                    $files_to_copy[] = $_POST['image_filename_'.$i];
                }
            }




            if($files->check()){
            //Upload::save($_FILES['file_name'],$_FILES['file_name'].$this->user->id,'upload');
            }
            $errors = $files->errors();
            $i=1;
            foreach($upload_images as $k => $v){

                if(!isset($errors['image_'.$k]) ){
                    if( isset($_FILES['image_'.$k]) && $_FILES['image_'.$k]['error']==0){

                        $count++;
                        Upload::save($v['file'], $v['name'], 'order_images/tmp');
                        $images_html .= '<a href="http://vashzakaz.us/order_images/'.$upload_images[$k]['name'].'">http://vashzakaz.us/order_images/'.$upload_images[$k]['name'].'</a><br/>';
                        $files_html .= '<p class="mt8" id="image_1_'.$count.'" filename="'.$v['name'].'">'.$v['file']['name'].'
                            <input type="hidden" name="image_filename_'.$count.'" value="'.$v['name'].'">
                            <input type="hidden" name="image_file_realname_'.$count.'" value="'.$v['file']['name'].'">
                            <a style="margin-left:8px;" href="javascript:void(0)" onClick="del_image_ajax(\'1_'.$count.'\')" >Удалить</a></p>';
                        $files_to_copy[] = $v['name'];
                    }
                }else{
                    if($errors['image_'.$k][0]=='Upload::type'){
                        $error_html .= '<p class="red mt8">Файл '.$v['file']['name'].' не был загружен: Данный файл не является изображением.</p>';
                    }else{
                        $error_html .= '<p class="red mt8">Файл '.$v['file']['name'].' не был загружен: Превышен максимальный размер файла.</p>';
                    }
                }
                $i++;
           }
//        }
        }
//        print_r($_POST);
//        print_r($_FILES);
//        print_r($errors);
        $this->content->files_html = $files_html;
        $this->content->error_html = $error_html;
        $this->content->images_count = $count;
        if ($images_html != ''){
            $images_html = 'Изображения товаров:<br/>'.$images_html;
        }
        if($form->sent() AND isset($_POST['preview'])){
            $this->content->preview = $form->getData().$images_html;
        } elseif ($form->sent() AND $form->validate() )  {
//                print_r($files_html);
//                print_r($files->errors());
//                die();
                foreach($files_to_copy as $k => $v){
                    if (file_exists(DOCROOT.'order_images/tmp/'.$v)){
                        copy(DOCROOT.'order_images/tmp/'.$v,DOCROOT.'order_images/'.$v);
                        unlink(DOCROOT.'order_images/tmp/'.$v);
                    }
                }
                $order = ORM::factory($this->_model_orders);
                $order->user_id = $this->user_id;
                $order->date_time = Date::formatted_time();
                $order->title = $form->getDataSection('a');
                $order->readed = TRUE;
                $order->new = TRUE;
                $order->readed_admin = FALSE;
                $order->save();

                $message = ORM::factory($this->_model_messages);
                if (empty($this->is_parcel))
                    $message->order_id = $order->id;
                else
                    $message->parcel_id = $order->id;
                $message->user_id = $this->user_id;
                $message->date_time = Date::formatted_time();
                $message->message = $form->getData().$images_html;
                $message->save();

                if (empty($this->is_parcel))
                {
                    $head_email = 'Новый заказ #'.$order->id.' от клиента #'.$this->user->id.' '.$this->user->user_data->name;
                    $text_email = 'Это уведомление о новом поступившем заказе #'.$order->id.' от клиента #'.$this->user->id.' '.$this->user->user_data->name .
                    ', '.Date::formatted_time();
                    Model_event::instance()->add('EV_ORDERS','новый заказ <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
                }
                else
                {
                    $head_email = 'Новая посылка #'.$order->id.' от клиента #'.$this->user->id.' '.$this->user->user_data->name;
                    $text_email = 'Это уведомление о новой поступившей посылке #'.$order->id.' от клиента #'.$this->user->id.' '.$this->user->user_data->name .
                    ', '.Date::formatted_time();
                    Model_event::instance()->add('EV_POSTS','новая посылка <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
                }
                $this->clear_tmp_image_directory();

                EmailHelper::send_admin($head_email, $text_email);

                $this->request->redirect((empty($this->is_parcel) ? 'order' : 'parcel') .'/id'.$order->id);
                $this->set_content('orders/success');
            }/*else {
              $this->content->preview = $form->getData().$images_html;
            }*/

        

        $this->content->form = $form;
    }

    public function action_item($id)
    {
        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_m');

        $this->set_content('orders/item');
        
        $order = ORM::factory($this->_model_orders, $id);
        
        if ($order->user_id != $this->user_id)
        {
            $this->set_content('orders/not_access');
            return;
        }

        if ($order->readed == FALSE)
        {
            $order->readed = TRUE;
            $order->save();
        }

        $form = Formo::get('order_message');

         if (isset($_POST['preview'])){
                $this->content->pre_message = array(
                    'time' => Date::formatted_time(),
                    'content' => $_POST["Order_Message"]['message']
                );
                $form->message->val( $_POST["Order_Message"]['message']);
        }
        if ($form->sent() AND $form->validate())
        {
            $message = ORM::factory($this->_model_messages);

            $this->is_parcel ? ($message->parcel_id = $order->id) : ($message->order_id = $order->id);

            $message->user_id = $this->user_id;
            $message->date_time = Date::formatted_time();
            $message->message = $form->message->val();
            $message->save();
            
            $order->readed_admin = FALSE;
            $order->save();

            if (empty($this->is_parcel))
            {
                $head_email = 'Новый пост в заказе #'.$order->id.' клиента #'.$this->user->id.' '.$this->user->user_data->name;
                $text_email = 'Это уведомление о новом посте в заказе #'.$order->id.' клиента #'.$this->user->id.' '.$this->user->user_data->name .
                ', '.Date::formatted_time();
                Model_event::instance()->add('EV_POSTS_ADD_MESSAGE','новое сообщение в посылке<a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
            }
            else
            {
                $head_email = 'Новый пост в посылке #'.$order->id.' клиента #'.$this->user->id.' '.$this->user->user_data->name;
                $text_email = 'Это уведомление о новом посте в посылке #'.$order->id.' клиента #'.$this->user->id.' '.$this->user->user_data->name .
                ', '.Date::formatted_time();
                Model_event::instance()->add('EV_ORDERS_ADD_MESSAGE','новое сообщение в заказе <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' посылка №'.$order->id.'('.$order->title.')</a>');
            }

            EmailHelper::send_admin($head_email, $text_email);

            $this->request->redirect((empty($this->is_parcel) ? 'order' : 'parcel') .'/id'.$order->id);
            $this->set_content('orders/message_sended');
        }
        $this->content->user = $this->user;
        $this->content->form = $form;
        $this->content->user_id = $this->user_id;
        $this->content->order = $order;
        $this->is_parcel ? (Model_event::instance()->add('EV_POSTS_VIEW_POST','просматривает посылку <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' посылка №'.$order->id.'('.$order->title.')</a>')) : (Model_event::instance()->add('EV_ORDERS_VIEW_ORDER','просматривает заказ <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>'));
    }

    public function action_all()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('user_id', '=', $this->user_id);

        $this->items($orders);
        $this->content->status = 'all';
    }

    public function action_current()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('user_id', '=', $this->user_id)
            ->where('processed', '=', FALSE);

        $this->items($orders);
        $this->content->status = 'current';
    }

    public function action_unread()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('user_id', '=', $this->user_id)
            ->where('readed', '=', FALSE);

        $this->items($orders);
        $this->content->status = 'unread';
    }

    public function action_closed()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('user_id', '=', $this->user_id)
            ->where('processed', '=', TRUE);

        $this->items($orders);
        $this->content->status = 'closed';
    }

    public function clear_tmp_image_directory()
    {
        $dir = opendir(DOCROOT."order_images/tmp");
        //print_r($dir);
        while(false !== ($file=readdir($dir)))
        {
                @unlink(DOCROOT."order_images/tmp/".$file);                
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    //2 метода для закрытия заказа клиентом
    public function action_close($id)
    {
        $this->order_processed($id, TRUE);

    }
    
    public function action_open($id)
    {
        $this->order_processed($id, FALSE);
    }
    
    private function order_processed($id, $close = TRUE)
    {
        $order = ORM::factory($this->_model_orders)->find($id);

        if ($order->pk())
        {
            $order->processed = $close;
            $order->save();

            if ($close == TRUE){
                if ($this->is_parcel)
                    Model_event::instance()->add('EV_POSTS_MAKE_CLOSED','закрыл посылку <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
                else
                    Model_event::instance()->add('EV_ORDERS_MAKE_CLOSED','закрыл заказ <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
            } else{
                if ($this->is_parcel)
                    Model_event::instance()->add('EV_POSTS','открыл посылку <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
                else
                    Model_event::instance()->add('EV_ORDERS','открыл заказ <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
            }

        }

        $this->request->redirect(Request::$referrer);
    }
    
    
    
    
} // End Order