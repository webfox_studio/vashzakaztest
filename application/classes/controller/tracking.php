<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Tracking extends Controller_Abstract
{

    public function before()
    {

        parent::before();
        if (!Auth::instance()->logged_in()) {
            $this->request->redirect('access_denied?ret=' . $this->request->uri);
        }
        $this->add_css('media/css/bootstrap/bootstrap.css');
        $this->add_css('media/css/bootstrap/bootstrap-theme.css');

        $this->add_js('media/js/datepicker/jquery.ui.core.js');
        // $this->add_js('media/js/datepicker/jquery.ui.widget.js');
        $this->add_js('media/js/datepicker/jquery.ui.datepicker.js');
        $this->add_js('media/js/datepicker/lang/jquery.ui.datepicker-ru.js');
        $this->add_js('media/js/tracking.js');
        $this->add_css('media/css/datepicker/datepicker.css');
    }

    public function action_index()
    {
        $this->set_content('trackings/index');

        if ($_POST)
        {
            $post = Validate::factory($_POST);
            $post->rule('notes', 'not_empty');
            $post->rule('tracking_id', 'not_empty');
            $post->rule('tracking_number', 'not_empty');
            if ($post->check()) {
                $params = array(
                    'tracking_id' => $post['tracking_id'],
                    'notes' => $post['notes'],
                    'who' => 0,
                    'date_add' => date("Y-m-d H:i:s"),
                );
                $query = ORM::factory('trackingnotes')
                    ->values($params)
                    ->save();
                if ($query) {
                    $_SESSION['add_notes'] = 'Заметка по трекингу ' . $post['tracking_number'] . ' добавлена!';
                    $this->request->redirect('tracking');
                }
            }
            else {
                $this->content->errors = $post->errors('ru');
            }
        }

        $trackings = ORM::factory('tracking');
        $total_trackings = $trackings
            ->where('delete', '=', 0)
            ->where('user_id', '=', Auth::instance()->get_user()->id)
            ->count_all();
        $pagination = Pagination::factory(array(
            'total_items' => $total_trackings,
            'items_per_page' => 10,
        ));
        $trackings = $trackings
            ->where('delete', '=', 0)
            ->where('user_id', '=', Auth::instance()->get_user()->id)
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('date_add', 'DESC')
            ->find_all();

        $this->content->delivery = Kohana::config('tracking.delivery');
        $this->content->form_column = Kohana::config('tracking.form_columns');
        $this->content->trackings = $trackings;

    }

    public static function dateConvertTracking($date = null)
    {
        if($date) {
            return date('Y-m-d', strtotime($date));
        }
    }
} // End Tracking