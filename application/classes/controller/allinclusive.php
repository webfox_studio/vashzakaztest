<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Allinclusive extends Controller_Abstract {


    public function before()
    {
        parent::before();

    }

    public function action_index()
    {
        header("Cache-Control: no-cache, must-revalidate"); 
        
        
        $products = '';
        $category = '';
        
        $cat = (int)$this->request->param('cat');
        
        if(!$cat)
        {
            $category = ORM::factory('allinclusivecategory')->find_all();
        }
        else
        {
            $category = ORM::factory('allinclusivecategory')->find_all();
             //$products = ORM::factory('allinclusive', 1);
                $products = DB::select('allinclusives.*', array('allinclusivetowns.name', 'town_name'), array('allinclusivetowns.description', 'town_description'), array('allinclusivetowns.id', 'town_id'), array('allinclusivetowns.price', 'town_price'))
                    ->from('allinclusives')
                    ->join('allinclusivetowns', 'left')
                    ->on('allinclusives.id', '=', 'allinclusivetowns.allinclusives_id')
                    ->where('allinclusives.cat', '=', $cat)
                    //->limit(5)
                    ->execute()->as_array();

                foreach($products as $key => $value)
                {
                    if($value['name'] != $marker)
                    {
                    $marker = $value['name'];
                    }
                    $products_new[$value['id']]['name'] = $value['name'];
                    $products_new[$value['id']]['desc'] = $value['description'];
                    $products_new[$value['id']]['img'] = $value['img'];
                    if(!empty($value['town_id'])){
                        $products_new[$value['id']]['country'][$value['town_id']] = $value['town_name'];
                        //$products_new[$value['id']]['country'][$value['town_id']]['town_description'] = $value['town_description'];
                        //$products_new[$value['id']]['country'][$value['town_id']]['price'] = $value['town_price'];
                        //$products_new[$value['id']]['country'][$value['town_id']]['id'] = $value['town_id'];


                    }
        //            $products_new[$value['id']][$marker][$value['town_id']]['price'] = $value['town_price'];
        //            $products_new[$value['id']][$marker][$value['town_id']]['image'] = $value['image'];
        //            $products_new[$value['id']][$marker][$value['town_id']]['town_name'] = $value['town_name'];
                }

        //        echo '<pre>';
        //        print_r($products_new);
        //        echo '</pre>';

        //        foreach ($products as $key => $value) 
        //        {
        //            $city = ORM::factory('allinclusivetown')->where('allinclusives_id','=', $value->id)->find_all();
        //            $productsnew[$value->id] = $city;
        //        }
                // $city = $products->allinclusivetowns->find_all();
        }
        
        
        
       $this->set_content('allinclusive/index');
       
        $this->content->productsnew = $products_new;
        $this->content->products = $products;
        $this->content->category = $category;
    }

    
    public function action_ajax()
    {
        $id = (int)$this->request->param('town_id');
        
        $info = ORM::factory('allinclusivetown')->where('id', '=', $id)->find();
//        echo '<pre>';
//        print_r($info);
//        echo '</pre>';
        //print_r($info);
    //    var_dump(json_decode($info));
        $arr = array();
        $arr['id'] = $id;
        $arr['description'] = $info->description;
        $arr['price'] = $info->price;
//        echo '<pre>';
//       print_r($arr);
//        echo '</pre>';
        echo json_encode($arr);
        unset($arr);
    }
    
    
    
} // End Order