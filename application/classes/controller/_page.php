<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Page extends Controller_Abstract {

    public function action_index($page, $subpage = NULL)
    {
        if (Kohana::find_file('views/static/', $page))
        {
            $this->set_content('static/'.$page);
        }
        else if (Kohana::find_file('views/edit/', $page))
        {
        	$this->set_content('edit/'.$page);
        }
        else
        {
            $this->set_content('page/index');

            $page = ORM::factory('page')
                ->where('name', '=', $page)
                ->find();
            
            if (! $subpage)
            {
                $subpages = $page->contents
					->where('display_on_parent', '=', '1')
                    ->order_by('order')
                    ->find_all();

                $this->content->page = $page;
                $this->content->subpages = $subpages;
            }
            else
            {
                $subpage = $page->contents
                    ->where('name', '=', $subpage)
                    ->find();
                
                $this->content->page = $page;
                $this->content->subpage = $subpage;
            }
        }
    }

} // End Page