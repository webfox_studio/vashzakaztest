<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * 
 * Description:  Base controller for all application (Abstract)
 * 		
 *		Example: https://github.com/azampagl/kohana-examples/blob/master/application/classes/controller/application.php
 *
 * @author       Mamontov Andrey <andrvm@andrvm.ru>
 * @copyright    2011 Andrey Mamontov (andrvm)
 * @php          >=PHP 5.3.0
 * 
 * @date		 $Date: 2010-09-10 16:38:58 +0400 (Пт, 10 сен 2010) $
 * @revision	 $Revision: 195 $
 * @changeby	 $Author: andrvm $
 * 
 */

 abstract class Controller_Base extends Controller_Abstract {

 	
 	
 } // End Controller_Base