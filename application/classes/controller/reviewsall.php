<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Reviewsall extends Controller_Abstract {

    public $content = 'reviewsall/index';

    public function before()
    {
        parent::before();
    }

    public function action_all()
    {
        $mreviews = Model_Reviewsall::instance();
        $reviews = $mreviews->get_all(400);
        if(is_array($reviews))
        {
            for($i=0; $i<sizeof($reviews); $i++)
            {
                $reviews[$i]['count'] = $i+1;
            }
        }
        $this->content->reviews = $reviews;
    }

    public function action_last()
    {
        
    }

} // End Nmain