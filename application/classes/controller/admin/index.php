<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Index extends Controller_Admin_Abstract {

    public function action_index()
    {
        if(Auth::instance()->logged_in('admin')) $this->request->redirect('admin/clients');
        if(Auth::instance()->logged_in('redactor')) $this->request->redirect('/admin/editors/news');
    }

    public function action_change_status()
    {
        if (Auth::instance()->logged_in()) {
            $m_user = ORM::factory('user', Auth::instance()->get_user()->id);
            //$m_user->last_activites = time();
            //$m_user->save();
            if (file_exists($_SERVER['DOCUMENT_ROOT'].'/online_array.php')){
                include $_SERVER['DOCUMENT_ROOT'].'/online_array.php';
                if(isset($admin_online)){
                    if ($admin_online[$m_user->user_data->name]['status'] >= 2){
                        $admin_online[$m_user->user_data->name]['status'] = 0;
                    }else{
                        $admin_online[$m_user->user_data->name]['status']++;
                    }
                    switch($admin_online[$m_user->user_data->name]['status']){
                        case 0:Model_event::instance()->add('EV_ADMIN_CHAT_OFF',' Вошел в Offline');break;
                        case 1:Model_event::instance()->add('EV_ADMIN_CHAT_ON',' Вошел в Online');break;
                        case 2:Model_event::instance()->add('EV_ADMIN_CHAT_AWAY',' Вошел в Away');break;
                    }
                        
                    $admin_online[$m_user->user_data->name]['last_act'] = time();
                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/online_array.php', '<?php $online_guest_array = '. var_export($online_guest_array, true). '; $admin_online = '. var_export($admin_online, true).';?>');
                } else {
                    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/online_array.php', '<?php $online_guest_array = '. var_export($online_guest_array, true). '; $admin_online = array();?>');
                }
            }
        }
        $this->request->redirect(Request::$referrer);
    }
    
    public function action_change_status_skype()
    {
        //$new_status = (@$_GET['status']) ? 'online' : 'offline';
        //copy($_SERVER['DOCUMENT_ROOT'].'/images/skype_status_'.$new_status.'.png', $_SERVER['DOCUMENT_ROOT'].'/images/skype_status_real.png');
        $query='UPDATE `kh_skype_status` SET `status`="'.(int)$_GET['status'].'" WHERE `user_id`="'.Auth::instance()->get_user()->id.'" LIMIT 1';
        DB::query(Database::UPDATE,$query)->execute();
        $this->request->redirect(Request::$referrer);
    }

    public function action_all_log(){
        Model_event::instance()->del_old();
        $this->set_content('admin/blocks/log_days_menu');
    }

    public function action_day_log($id){
        $this->set_content('admin/day_log');
        $this->content->day_log = Model_event::instance()->get_day_log($id);
    }

    public function action_xls($id)
    {
        Request::instance()->send_file(Kohana::config('main.dir.reports').'/'.$id.'.xls', 'Отчет клиента #'.$id.' от '.date('d.m.Y H:i:s').'.xls');
    }

} // End Admin_Index