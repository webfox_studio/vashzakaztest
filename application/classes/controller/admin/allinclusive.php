<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Allinclusive extends Controller_Admin_Abstract {
    
    protected $menu;
    
    
    public function after()
    {
        $this->content->editors_menu = $this->menu;

        parent::after();
    }
    
    public function before()
    {
        parent::before();

        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_f');

        $this->menu = array(
            array('url' => Url::site('admin/editors/news'), 'title' => 'Новости'),
            array('url' => Url::site('admin/editors/schedule'), 'title' => 'Расписание'),
            array('url' => Url::site('admin/editors/faq'), 'title' => 'Вопросы'),
            array('url' => Url::site('admin/editors/contacts'), 'title' => 'Контакты'),
            array('url' => Url::site('admin/editors/about'), 'title' => 'О нас'),
            array('url' => Url::site('admin/editors/agreements'), 'title' => 'Соглашение'),
            array('url' => Url::site('admin/editors/useful'), 'title' => 'Полезное'),
            array('url' => Url::site('admin/editors/orderform1'), 'title' => 'Форма заказа 1'),
            array('url' => Url::site('admin/editors/orderform2'), 'title' => 'Форма заказа 2'),
            array('url' => Url::site('admin/editors/orderform3'), 'title' => 'Форма заказа 3'),
            array('url' => Url::site('admin/editors/orderform4'), 'title' => 'Форма заказа 4'),
            array('url' => Url::site('admin/editors/posilkaform'), 'title' => 'Форма посылки'),
            array('url' => Url::site('admin/editors/paymentform'), 'title' => 'Форма перевода'),
            array('url' => Url::site('admin/editors/banners'), 'title' => 'Баннеры'),
            array('url' => Url::site('admin/editors/actions'), 'title' => 'Акции'),
            array('url' => Url::site('admin/editors/reviews'), 'title' => 'Отзывы'),
            array('url' => Url::site('admin/editors/services'), 'title' => 'Об услугах'),
            array('url' => Url::site('admin/editors/subscription'), 'title' => 'Подписка'),
            array('url' => Url::site('admin/editors/additionalpage'), 'title' => 'Дополнительные страницы'),
        );

        $pages = ORM::factory('page')
            ->order_by('order')
            ->find_all();

        foreach($pages as $_page)
        {
            $this->menu[] = array(
                'url' => Url::site('admin/editors/page/'.$_page->name),
                'title' => $_page->title,
                'order' => $_page->order,
            );
        }
    }
    
    public function action_index()
    {
        //получаем переменные
        $type = $this->request->param('type');
        
        
        //переключатель выбора
        switch ($type) {
            case 'add':
                $this->add();
            break;
        
            case 'edit':
                $this->edit();
            break;
        
            case 'delete':
                $this->delete();
            break;

            default:
                $this->index();
            break;
        }
        
    }
    
    
    
    public function add()
    {
        if($_POST)
        {
            $post = Validate::factory($_POST);
                $post->rule('name', 'not_empty')  
                    ->rule('description', 'not_empty');
                    //->rule('img', 'not_empty');

            $uploaddir = '/home/tolich/public_html/upload/actions/';
            
            if($post->check())
            {
                $uploaddir = './upload/allinclusive/';
       
                if(is_uploaded_file($_FILES['img']['tmp_name']))
                    {
                        //if (file_exists($uploaddir . $_FILES['img']['name'])) unlink($uploaddir . $_FILES['img']['name']);

                     if (move_uploaded_file($_FILES['img']['tmp_name'], $uploaddir . $_FILES['img']['name'])) 
                     {
                         $img = $_FILES['img']['name'];
                     }
                     
                     if($img)
                     {
                         $_POST['img'] = $_FILES['img']['name'];
                     $editpage = ORM::factory('allinclusive', $id)
                         ->values($_POST)
                         ->save();
                     }
                 }
                 else{
                     //$errors[] = 'Ошибка загрузки файла';
                 }				

            }
            else{
                $errors[] = 'Не все поля заполнены';
            }
            if(isset($errors)){
                $category = ORM::factory('allinclusivecategory')->find_all();
                $this->content->category = $category;
                $this->content->errors = $errors;
                $this->set_content('admin/allinclusive/add');
                return;
            }
            else{
                $this->request->redirect('admin/allinclusive');
            }
        }
        
        $category = ORM::factory('allinclusivecategory')->find_all();
            
        
        $this->set_content('admin/allinclusive/add');
        
        
        $this->content->category = $category;
       
    }
    
    public function delete()
    {
        $id = $this->request->param('id');

        $del = ORM::factory('allinclusive', $id)
                ->delete();
        
        $this->request->redirect('admin/allinclusive');
    }
    
    public function index(){
        $this->set_content('admin/allinclusive/index');
        
        $category = ORM::factory('allinclusivecategory')->find_all();
        
        $products = DB::select('allinclusives.*', array('allinclusivetowns.name', 'town_name'), array('allinclusivetowns.description', 'town_description'), array('allinclusivetowns.id', 'town_id'), array('allinclusivetowns.price', 'town_price'))
            ->from('allinclusives')
            ->join('allinclusivetowns', 'left')
            ->on('allinclusives.id', '=', 'allinclusivetowns.allinclusives_id')
            //->limit(5)
            ->execute()->as_array();
        
        foreach($products as $key => $value)
        {
            if($value['name'] != $marker)
            {
            $marker = $value['name'];
            }
            $products_new[$value['id']]['name'] = $value['name'];
            $products_new[$value['id']]['desc'] = $value['description'];
            $products_new[$value['id']]['image'] = $value['image'];
            if(!empty($value['town_id'])){
                $products_new[$value['id']]['country'][$value['town_id']] = $value['town_name'];                
            }
        }
        $this->content->productsnew = $products_new;
        $this->content->products = $products;
        $this->content->category = $category;
    }
    
    public function edit()
    {
        $id = $this->request->param('id'); 
        
        if($_POST)
        {
            
            $post = Validate::factory($_POST);
            $post->rule('name', 'not_empty')  
                 ->rule('description', 'not_empty');
                    //->rule('img', 'not_empty');

//            echo '<pre>';
//                print_r($_FILES);
//            echo '</pre>';
//          
//            
//            echo '<pre>';
//                print_r($_POST);
//            echo '</pre>';
//            return;
            
            if($post->check())
            {
                $uploaddir = './upload/allinclusive/';
       
                if(is_uploaded_file($_FILES['img']['tmp_name']))
                    {
                        //if (file_exists($uploaddir . $_FILES['img']['name'])) unlink($uploaddir . $_FILES['img']['name']);

                     if (move_uploaded_file($_FILES['img']['tmp_name'], $uploaddir . $_FILES['img']['name'])) 
                     {
                         $img = $_FILES['img']['name'];
                     }
                     
                     if($img)
                     {
                         $_POST['img'] = $_FILES['img']['name'];
                     $editpage = ORM::factory('allinclusive', $id)
                         ->values($_POST)
                         ->save();
                     }
                 }
                 else{
                     $errors[] = 'Ошибка загрузки файла';
                 }
               


            }
            else{
                $errors[] = 'Не все поля заполнены';
            }
            
            if(isset($errors)){
                $this->content->errors = $errors;
                $this->set_content('admin/allinclusive/edit/'.$id);
            }
            else{
                $this->request->redirect('admin/allinclusive');
            }
        }
            $category = ORM::factory('allinclusivecategory')->find_all();
            $this->content->category = $category;   
            
            $pages = ORM::factory('allinclusive', $id);
            $this->content->pages = $pages;
            
            $delivery = ORM::factory('allinclusivetown')->where('allinclusives_id','=',$id)->find_all();
            $this->content->delivery = $delivery;
            
            $this->set_content('admin/allinclusive/edit');
    }
  
 //=============================================================================================
    
    public function action_delivery()
    {
        //получаем переменные
        $id = $this->request->param('id');
        
        $type = $this->request->param('type');
        
        //переключатель выбора
        switch ($type) {
            case 'add':
                $this->adddelivery();
            break;
        
            case 'edit':
                $this->editdelivery();
            break;
        
            case 'delete':
                $this->deletedelivery();
            break;

            default:
                $this->defauldelivery();
            break;
        }
        
    }
    
    
    public function adddelivery()
    {
        $id = $this->request->param('id');
        if($_POST)
        {
            $post = Validate::factory($_POST);
                $post->rule('name', 'not_empty')  
                    ->rule('description', 'not_empty')
                    ->rule('price', 'not_empty');

           
            
            if($post->check())
            {
                				
                $addpage = ORM::factory('allinclusivetown')
                                ->values($_POST)
                                ->save();
            }
            else{
                $errors[] = 'Не все поля заполнены';
            }
            if(isset($errors)){
                $this->content->errors = $errors;
                $this->set_content('admin/allinclusive/delivery/add/'.$id);
                return;
            }
            else{
                $this->request->redirect('admin/allinclusive/edit/'.$id);
            }
        }
            
        $this->content->id = $id;
        $this->set_content('admin/allinclusive/adddelivery');
        
        
        
       
    }
    
    public function defauldelivery(){
        $this->set_content('admin/allinclusive/index');
        
        $products = DB::select('allinclusives.*', array('allinclusivetowns.name', 'town_name'), array('allinclusivetowns.description', 'town_description'), array('allinclusivetowns.id', 'town_id'), array('allinclusivetowns.price', 'town_price'))
            ->from('allinclusives')
            ->join('allinclusivetowns', 'left')
            ->on('allinclusives.id', '=', 'allinclusivetowns.allinclusives_id')
            //->limit(5)
            ->execute()->as_array();
        
        foreach($products as $key => $value)
        {
            if($value['name'] != $marker)
            {
            $marker = $value['name'];
            }
            $products_new[$value['id']]['name'] = $value['name'];
            $products_new[$value['id']]['desc'] = $value['description'];
            $products_new[$value['id']]['image'] = $value['image'];
            if(!empty($value['town_id'])){
                $products_new[$value['id']]['country'][$value['town_id']] = $value['town_name'];                
            }
        }
        $this->content->productsnew = $products_new;
        $this->content->products = $products;
    }
    
    public function deletedelivery(){
        $id = $this->request->param('id');

        $del = ORM::factory('allinclusivetown', $id)
                ->delete();
        
        $this->request->redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function editdelivery(){
        $id = $this->request->param('id');
        
        if($_POST)
        {
            
            $post = Validate::factory($_POST);
            $post->rule('name', 'not_empty')  
                 ->rule('price', 'not_empty') 
                 ->rule('description', 'not_empty');
                    //->rule('img', 'not_empty');

            
            if($post->check())
            {
                
                $editpage = ORM::factory('allinclusivetown', $id)
                                ->values($_POST)
                                ->save();

            }
            else{
                $errors[] = 'Не все поля заполнены';
            }
            
            if(isset($errors)){
                $this->content->errors = $errors;
                $this->set_content('admin/allinclusive/delivery/edit/'.$id);
                return;
            }
            else{
                $this->request->redirect('admin/allinclusive/edit/'.$_POST['allinclusives_id']);
                return;
            }
        }
               
           
            
            $delivery = ORM::factory('allinclusivetown')->where('id','=',$id)->find();
            $this->content->pages = $delivery;
            
            $this->set_content('admin/allinclusive/editdelivery');
    }
    
    
    public function action_category()
    {
        //получаем переменные
        $type = $this->request->param('type');
        
        
        //переключатель выбора
        switch ($type) {
            case 'add':
                $this->addcategory();
            break;
        
            case 'edit':
                $this->editcategory();
            break;
        
            case 'delete':
                $this->deletecategory();
            break;

            default:
                $this->index();
            break;
        }
        
    }
    
    public function editcategory()
    {
        $id = $this->request->param('id'); 
        $this->set_content('admin/allinclusive/editcategory');
        if($_POST)
        {
            
            $post = Validate::factory($_POST);
            $post->rule('category', 'not_empty')  ;
                 //->rule('description', 'not_empty');
                    //->rule('img', 'not_empty');

//            echo '<pre>';
//                print_r($_FILES);
//            echo '</pre>';
//          
//            
//            echo '<pre>';
//                print_r($_POST);
//            echo '</pre>';
//            return;
            
            if($post->check())
            {
                
       
                
                     $editpage = ORM::factory('allinclusivecategory', $id)
                         ->values($_POST)
                         ->save();
                
               


            }
            else{
                $errors[] = 'Не все поля заполнены';
            }
            
            if(isset($errors)){
                $this->content->errors = $errors;
                $this->set_content('admin/allinclusive/category/edit/'.$id);
            }
            else{
                $this->request->redirect('admin/allinclusive');
            }
        }
               
            $pages = ORM::factory('allinclusivecategory', $id);
            $this->content->pages = $pages;
            
            $delivery = ORM::factory('allinclusivetown')->where('allinclusives_id','=',$id)->find_all();
            $this->content->delivery = $delivery;
            
            
    }
    
    
    public function deletecategory(){
        $id = $this->request->param('id');

        $del = ORM::factory('allinclusivecategory', $id)
                ->delete();
        
        $this->request->redirect($_SERVER['HTTP_REFERER']);
    }
    
    
    public function addcategory()
    {
        if($_POST)
        {
            $post = Validate::factory($_POST);
                $post->rule('category', 'not_empty')  ;
                //    ->rule('description', 'not_empty');
                    //->rule('img', 'not_empty');

            
            
            if($post->check())
            {
                
       
                
                     $editpage = ORM::factory('allinclusivecategory', $id)
                         ->values($_POST)
                         ->save();
                				

            }
            else{
                $errors[] = 'Не все поля заполнены';
            }
            if(isset($errors)){
                $this->content->errors = $errors;
                $this->set_content('admin/allinclusive/addcategory');
                return;
            }
            else{
                $this->request->redirect('admin/allinclusive');
            }
        }
            
        
        $this->set_content('admin/allinclusive/addcategory');
        
        
        
       
    }
} // Admin Editor