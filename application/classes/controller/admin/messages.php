<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Messages extends Controller_Admin_Abstract {

    protected $_model_messages = 'message';
    public function action_index()
    {
        $this->request->redirect('admin/messages/all');
    }

    public function items(ORM &$messages)
    {
        $this->set_content('admin/messages/items');

//        $messages = ORM::factory('message');

//        $in = ($this->request->param('box') == 'in') ? TRUE : FALSE;

        $count_messages = clone $messages;
        
        $count = $count_messages
            ->count_all();

        $pagination = Pagination::factory(array(
            'total_items'    => $count,
            'items_per_page' => 50,
        ));

        $messages = $messages
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('admin_important','DESC')
            ->order_by('id' , 'DESC')
            ->find_all();

        $this->content->admin_id = $this->admin->id;
        $this->content->messages = $messages;
        $this->content->pagination = $pagination;
    }

    public function action_client($id)
    {
        $in = ($this->request->param('box') == 'in') ? TRUE : FALSE;

        $messages = ORM::factory('message')
            ->where($in ? 'user_id' : 'whom_id', '=', $id)
            ->where('deleted2', '=', 0);

        $this->items($messages);

        $user_name = ORM::factory('user', $id)->user_data->name;

        $this->content->in = $in;
        $this->content->user_id = $id;
        $this->content->user_name = $user_name;
    }

    public function action_item($id)
    {
        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_f');
        $this->content->is_admin = true;
        $this->set_content('admin/messages/item');
        
        $message = ORM::factory('message', $id);
        
        $messages = $message->messages
            ->find_all();
        
        //echo $message->whom_id;
        
        $user = ORM::factory('user')->find($message->whom_id);

//        if (! $user->pk())
//        {
//            $this->set_content('admin/clients/not_find');
//            return;
//        }
        
        $nameusers = Model_News::instance();
        $pinameuser = $nameusers->nameuser($message->whom_id);
      
        
        $this->content->username = $pinameuser['0']['name'];
        
        $whom_id = $message->user_id;

        $in =  ($message->whom_id == 0 OR $this->admin->id != $message->user_id) ? TRUE : TRUE;

        if (! $message->readed2)
        {
            $message->readed2 = TRUE;
            $message->save();
        }
        
        $form = Formo::get('message_answer');

        if (isset($_POST['preview'])){
                $this->content->pre_message = array(
                    'time' => Date::formatted_time(),
                    'content' => $_POST["Message_Answer"]['message']
                );
                $form->message->val( $_POST["Message_Answer"]['message']);
        }

        if ($form->sent())
        {
            if ($form->validate())
            {

                
                $message_message = ORM::factory('message_message');
                
                if($message->whom_id == 0){
                    $message->whom_id = $message->user_id;
                    $message->user_id = $this->admin->id;
                }
                
                
                
                $message_message->message_id = $message->id;
                $message_message->user_id = $this->admin->id;
                $message_message->content = $form->message->val();
                $message_message->time = date('Y-m-d H:i:s');
                
                $message->readed = 0;
                $message->readed2 = 1;
                $message->important = $form->important->val() ? 1 : 0;
                if($message->admin_important == 1)
                {
                    $message->admin_important = 1;
                }
                else{
                    $message->admin_important = 0;
                }
               
//                $message->deleted = 0;
//                $message->deleted2 = 0;
                $message->time = Date::formatted_time();
                
                $message->save();
                $message_message->save();
                
                $email_address = $user->email;
                $head_email = 'Ответ на Ваше сообщение';
                $text_email = '<h2>Тема: '.$message->title.'</h2><br /><br />'.
                	'Содержание:'."\n\n".$message_message->content.'<br /><br />'.
                	HTML::anchor(URL::site('messages/item/'.$message->id, TRUE), 'перейти к сообщению');
                Model_event::instance()->add('EV_MESSAGES_REPLY','новый пост в сообщении <a href="/admin/messages/item/'.$message->id.'">'.$message->title.'('.$message->user->user_data->name.')</a>');
                EmailHelper::send($email_address, $head_email, $text_email, FALSE, TRUE);
                
                $this->request->redirect('admin/messages/item/'.$message->id);

                $this->set_content('admin/messages/sended');
            }
        }
        $this->content->user = $this->user;
        $this->content->message = $message;
        $this->content->messages = $messages;
        $this->content->form = $form;
        $this->content->in = $in;
        Model_event::instance()->add('EV_MESSAGES_READ','читает сообщение <a href="/admin/messages/item/'.$message->id.'">'.$message->title.'('.$message->user->user_data->name.')</a>');
        if (Request::$is_ajax)
        {
            echo $this->content->render();
        }
    }

    public function action_clients()
    {
        $this->add_js('jquery.quicksearch');
        $this->add_js('tablesearch');

        $this->set_content('admin/messages/clients');

        $clients = ORM::factory('user');

        $pagination = Pagination::factory(array(
            'total_items'    => $clients->count_clients(),
            'items_per_page' => 50,
        ));

        $clients = $clients->clients_messages()
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->find_all();

        $this->content->clients = $clients;
        $this->content->pagination = $pagination;
    }

    public function action_all()
    {
        $this->add_js('jquery.quicksearch');
        $this->add_js('tablesearch');
        
        $messages = ORM::factory('message');

        $this->items($messages);

        $this->content->status = 'Все';
    }

    public function action_readed()
    {
        $messages = ORM::factory('message')
         //   ->where('whom_id', 'IN', array(0, $this->admin->id))
            ->where('readed2' ,'=', 1)
            ->where('deleted2', '=', 0)
            ->order_by( 'admin_important', 'DESC' );
        
        $this->items($messages);

        $this->content->status = 'Прочитанные';
    }

    public function action_unreaded()
    {
        $messages = ORM::factory('message')
          //  ->where('whom_id', 'IN', array(0, $this->admin->id))
            ->where('readed2' ,'=', 0)
            ->where('deleted2', '=', 0);

        $this->items($messages);

        $this->content->status = 'Непрочитанные';
    }

    public function action_deleted()
    {
        $messages = ORM::factory('message')
          //  ->where('whom_id', 'IN', array(0, $this->admin->id))
            ->where('deleted2', '=', 1);

        $this->items($messages);

        $this->content->status = 'Удаленные';
    }

    public function action_set_important($id)
    {
        $this->msg_important($id, TRUE);
    }

    public function action_set_noimportant($id)
    {
        $this->msg_important($id, FALSE);
        
    }

    private function msg_important($id, $important = TRUE)
    {
        
        $msg = ORM::factory($this->_model_messages)->find($id);

        if ($msg->pk())
        {
            $msg->admin_important = $important;
            $msg->save();
             if($important == 1)
                Model_event::instance()->add('EV_MESSAGES_UPDATE_IMPORTANT_ON','установил важным сообщение <a href="/admin/messages/item/'.$msg->id.'">'.$msg->title.'('.$msg->user->user_data->name.'</a>');
            else
                Model_event::instance()->add('EV_MESSAGES_UPDATE_IMPORTANT_OFF','установил не важным сообщение <a href="/admin/messages/item/'.$msg->id.'">'.$msg->title.'('.$msg->user->user_data->name.'</a>');
        }

        $this->request->redirect(Request::$referrer);
    }
    public function action_important()
    {
        $messages = ORM::factory('message')
            ->where('admin_important' ,'=', 1)
            ->where('deleted2', '=', 0)
            ->order_by( 'id', 'DESC' );

        $this->items($messages);

        $this->content->status = 'Прочитанные';
    }
    public function action_faq()
    {
        $m_faq = Model_Faq::instance();

        $faq_cats = $m_faq->get_cats();
        $faq = $m_faq->get_group_cat();

        $this->content->faq_cats = $faq_cats;
        $this->content->faq = $faq;
        $this->set_content('faq/index');
    }

    public function action_delete_message($id)
    {
        ORM::factory('message_message')->find($id)->delete();
        $this->request->redirect(Request::$referrer);
    }

    public function action_delete_post($id)
    {
        ORM::factory('message')->find($id)->delete();
        $this->request->redirect('admin/messages');
    }

    public function action_send($id = NULL)
    {
        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_f');

        $this->set_content('admin/messages/send');

        $user = ORM::factory('user')->find($id);
      
        if (! $user->pk())
        {
            $this->set_content('admin/clients/not_find');
            return;
        }

        $form = Formo::get('Message_Send');
        if (isset($_POST['preview'])){
                $this->content->pre_message = array(
                    'time' => Date::formatted_time(),
                    'content' => $_POST["Message_Send"]['message'],
                    'head' => $_POST["Message_Send"]['head']
                );
                $form->message->val( $_POST["Message_Send"]['message']);
                $form->head->val( $_POST["Message_Send"]['head']);
        }
        if ($form->submit->val())
        {
            if ($form->validate())
            {
                $message = ORM::factory('message');
                $message_message = ORM::factory('message_message');

                $message->user_id = $this->user->id;
                $message->title = $form->head->val();
                $message_message->user_id = $this->user->id;
                $message_message->content = $form->message->val();
                $message_message->time = Date::formatted_time();
                $message->time = Date::formatted_time();
                $message->whom_id = $id;
                $message->readed = 0;
                $message->readed2 = TRUE;
                $message->important = $form->important->val() ? 1 : 0;
                $message->deleted = 0;
                $message->deleted2 = 0;

                $message->save();
                $message_message->message_id = $message->id;
                $message_message->save();
 
                $email_address = $user->email;
                $head_email = '<Вам было отправлено новое сообщение';
                $text_email = '<h2>Тема: '.$message->title.'</h2><br /><br />'.
                	'Содержание:'."\n\n".$message_message->content.'<br /><br />'.
                	HTML::anchor(URL::site('messages/item/'.$message->id, TRUE), 'перейти к сообщению');
                Model_event::instance()->add('EV_MESSAGES_NEW','новое сообщение <a href="/admin/messages/item/'.$message->id.'">'.$message->title.'</a>');
                EmailHelper::send($email_address, $head_email, $text_email, FALSE, TRUE);
                
                $this->request->redirect('admin/messages/item/'.$message->id);

                $this->set_content('admin/messages/sended');
            }
        }

        $user_name = $user->user_data->name;
        $this->content->user = $this->user;
        $this->content->user_name = $user_name;
        $this->content->form = $form;
    }

} // End Admin Messages