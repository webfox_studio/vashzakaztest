<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Additionalpage extends Controller_Admin_Abstract
{

    protected $menu;

    public function after()
    {
        $this->content->editors_menu = $this->menu;

        parent::after();
    }

    public function before()
    {
        parent::before();

        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_f');

        $this->menu = array(
            array('url' => Url::site('admin/editors/news'), 'title' => 'Новости'),
            array('url' => Url::site('admin/editors/schedule'), 'title' => 'Расписание'),
            array('url' => Url::site('admin/editors/faq'), 'title' => 'Вопросы'),
            array('url' => Url::site('admin/editors/contacts'), 'title' => 'Контакты'),
            array('url' => Url::site('admin/editors/about'), 'title' => 'О нас'),
            array('url' => Url::site('admin/editors/agreements'), 'title' => 'Соглашение'),
            array('url' => Url::site('admin/editors/useful'), 'title' => 'Полезное'),
            array('url' => Url::site('admin/editors/orderform1'), 'title' => 'Форма заказа 1'),
            array('url' => Url::site('admin/editors/orderform2'), 'title' => 'Форма заказа 2'),
            array('url' => Url::site('admin/editors/orderform3'), 'title' => 'Форма заказа 3'),
            array('url' => Url::site('admin/editors/orderform4'), 'title' => 'Форма заказа 4'),
            array('url' => Url::site('admin/editors/posilkaform'), 'title' => 'Форма посылки'),
            array('url' => Url::site('admin/editors/paymentform'), 'title' => 'Форма перевода'),
            array('url' => Url::site('admin/editors/banners'), 'title' => 'Баннеры'),
            array('url' => Url::site('admin/editors/actions'), 'title' => 'Акции'),
            array('url' => Url::site('admin/editors/reviews'), 'title' => 'Отзывы'),
            array('url' => Url::site('admin/editors/services'), 'title' => 'Об услугах'),
            array('url' => Url::site('admin/editors/subscription'), 'title' => 'Подписка'),
            array('url' => Url::site('admin/editors/additionalpage'), 'title' => 'Дополнительные страницы'),
        );
        //echo 4444;

        $pages = ORM::factory('page')
            ->order_by('order')
            ->find_all();

        foreach ($pages as $_page) {
            $this->menu[] = array(
                'url' => Url::site('admin/editors/page/' . $_page->name),
                'title' => $_page->title,
                'order' => $_page->order,
            );
        }
    }

    public function action_index()
    {
        //получаем переменные
        $type = $this->request->param('type');
        $idpage = $this->request->param('idpage');

        //переключатель выбора
        switch ($type) {

            case 'cat': //additionalcat

                $obj = ORM::factory('additionalcat');
                // if(isset($_GET['sort']) && !empty($_GET['sort'])){ $sort = (int)$_GET['sort']; $obj->where('sort','=',$sort);}
                $cats = $obj->find_all();

                $this->content->cats = $cats;
                //$this->request->redirect('admin/editors/additionalpage');
                $this->set_content('admin/additionalpage/indexcat');
                break;

            case 'addcat':
                if ($_POST) {
                    $addpage = ORM::factory('additionalcat')
                        ->values($_POST)
                        ->save();
                    $this->request->redirect('admin/editors/additionalpage/cat');
                }


                $this->set_content('admin/additionalpage/addcat');
                break;

            case 'editcat':

                if ($_POST) {
                    //print_r($_POST); exit();
                    $editpage = ORM::factory('additionalcat', $idpage)
                        ->values($_POST)
                        ->save();
                    $this->request->redirect('admin/editors/additionalpage/cat');
                }
                $pages = ORM::factory('additionalcat', $idpage);
                $this->content->pages = $pages;
                $this->set_content('admin/additionalpage/editcat');
                break;

            case 'delcat':
                $delpage = ORM::factory('additionalcat', $idpage)
                    ->delete();
                $this->request->redirect('admin/editors/additionalpage/cat');
                break;

            case 'add':
                if ($_POST) {
                    $addpage = ORM::factory('additionalpage')
                        ->values($_POST)
                        ->save();
                    $this->request->redirect('admin/editors/additionalpage');
                }

                $obj = ORM::factory('additionalcat');
                $type = $obj->find_all();
                $this->content->type = $type;
                $this->set_content('admin/additionalpage/addpage');
                break;

            case 'edit':
                if ($_POST) {
                    $editpage = ORM::factory('additionalpage', $idpage)
                        ->values($_POST)
                        ->save();
                    $this->request->redirect('admin/editors/additionalpage');
                }
                $pages = ORM::factory('additionalpage', $idpage);
                $this->content->pages = $pages;
                $obj = ORM::factory('additionalcat');
                $type = $obj->find_all();
                $this->content->type = $type;
                $this->set_content('admin/additionalpage/editpage');
                break;

            case 'del':
                $delpage = ORM::factory('additionalpage', $idpage)
                    ->delete();
                $this->request->redirect('admin/editors/additionalpage');
                break;

            case 'archive':
                $obj = ORM::factory('additionalpage', $idpage);

                if($obj->archive == 0) {
                    $obj->values(array('archive' => 1));
                }
                else {
                    $obj->values(array('archive' => 0));
                }
                $obj->save();
                $this->request->redirect('admin/editors/additionalpage');

                break;
            default:

                $obj = ORM::factory('additionalpage');

                if(isset($_GET['show'])) {
                    switch ($_GET['show']){
                        case 'act':
                            $obj->where('archive', '=', 0);
                        break;
                        case 'arch':
                            $obj->where('archive', '=', 1);
                            break;
                        default:
                            break;
                    }
                }

                if (isset($_GET['sort']) && !empty($_GET['sort'])) {
                    $sort = (int)$_GET['sort'];
                    $obj->where('sort', '=', $sort);
                }
                $pages = $obj->find_all();

                $obj = ORM::factory('additionalcat');
                $type = $obj->find_all();
                $this->content->type = $type;

                $this->content->pages = $pages;

                //$this->request->redirect('admin/editors/additionalpage');
                $this->set_content('admin/additionalpage/index');
                break;
        }

    }
} // Admin Editor