<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Orders extends Controller_Admin_Abstract {

    protected $is_parcel = FALSE;
    protected $_model_orders;
    protected $_model_messages;

    public function before()
    {
        parent::before();

        $this->is_parcel = $this->request->param('is_parcel', FALSE);

        $this->_model_orders = ($this->is_parcel) ? 'parcels' : 'orders';
        $this->_model_messages = ($this->is_parcel) ? 'parcel_messages' : 'order_messages';

        $this->content->is_parcel = $this->is_parcel;
    }

    public function action_index()
    {
        $this->request->redirect('admin/'.(empty($this->is_parcel) ? 'orders' : 'parcels').'/all');

        $this->add_js('jquery.quicksearch');
        $this->add_js('tablesearch');

        $this->set_content('admin/orders/index');

        $orders = ORM::factory($this->_model_orders);

        $count = $orders
            ->where('deleted', '=', 0)
            ->where('processed', '=', 0)
            ->count_all();

        $pagination = Pagination::factory(array(
            'total_items' => $count,
            'items_per_page' => 50,
        ));

        $orders = $orders
            ->where('deleted', '=', 0)
            ->where('processed', '=', 0)
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('readed_admin')
            ->order_by('important', 'DESC')
            ->order_by('id', 'DESC')
            ->find_all();

        $this->content->orders = $orders;
        $this->content->pagination = $pagination;
    }

    public function action_item($id)
    {
        $this->set_content('admin/orders/item');

        $order = ORM::factory($this->_model_orders)->find($id);

        if (! $order->pk())
        {
            $this->set_content('orders/not_find');
            return;
        }

        $order->readed_admin = TRUE;
        $order->new = FALSE;
        $order->save();

        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_f');        
        $form = Formo::get('order_message');
        $this->content->is_admin = true;
        if (isset($_POST['preview'])){
                $this->content->pre_message = array(
                    'time' => Date::formatted_time(),
                    'content' => $_POST["Order_Message"]['message']
                );
                $form->message->val( $_POST["Order_Message"]['message']);
        }
        if ($this->request->param('message') AND $this->request->param('act'))
        {
        	switch ($this->request->param('act'))
        	{
        		case 'del':
        			$message = ORM::factory($this->_model_messages);
        			$message->delete($this->request->param('message'));
        			
        			if ($this->is_parcel)
		                $this->request->redirect('admin/parcel/'.$order->id);
		            else
		                $this->request->redirect('admin/order/'.$order->id);
        			break;
        		
        		case 'edit':
        			$message = ORM::factory($this->_model_messages);
        			$message->find($this->request->param('message'));
           			$form->message->val($message->message);
        			break;
        	}
                
        }
        
        if ($form->sent() AND $form->validate())
        {
        	if (! isset($message))
        	{
            	$message = ORM::factory($this->_model_messages);
        	}
            
            $this->is_parcel ? ($message->parcel_id = $order->id) : ($message->order_id = $order->id);

            $message->user_id = Auth::instance()->get_user()->id;
            $message->date_time = Date::formatted_time();
            $message->message = $form->message->val();
            $message->save();

            $order->readed = FALSE;
            $order->save();            
            
            $email = $order->user->email;
            
            if (empty($this->is_parcel))
            {
                $head_email = 'Ответ в Вашем заказе';
                $text_email = 'Это уведомление о новом ответе в вашем заказе: '.HTML::anchor(URL::site('order/id'.$order->id, TRUE)).'<br />'.
                	'<p>'.$message->message.'</p>';
                Model_event::instance()->add('EV_ORDERS_ADD_MESSAGE','сообщение в заказе <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
            }
            else
            {
                $head_email = 'Ответ в Вашей посылке';
                $text_email = 'Это уведомление о новом ответе в вашей посылке: '.HTML::anchor(URL::site('parcel/id'.$order->id, TRUE)).'<br />'.
                	'<p>'.$message->message.'</p>';
                Model_event::instance()->add('EV_POSTS_ADD_MESSAGE','сообщение в посылке <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
            }

            EmailHelper::send($email, $head_email, $text_email, FALSE, TRUE);
            
            if ($this->is_parcel)
                $this->request->redirect('admin/parcel/'.$order->id);
            else
                $this->request->redirect('admin/order/'.$order->id);
            $this->set_content('orders/message_sended');
            
        }
        $this->content->user = $this->user;
        $this->content->form = $form;
        $this->content->order = $order;
        $this->is_parcel ? (Model_event::instance()->add('EV_ORDERS_VIEW_ORDER','просматривает посылку <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' посылка №'.$order->id.'('.$order->title.')</a>')) : (Model_event::instance()->add('EV_ORDERS_VIEW_ORDER','просматривает заказ <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>'));
        
    }

    public function action_clients()
    {
        $this->add_js('jquery.quicksearch');
        $this->add_js('tablesearch');

        $this->set_content('admin/orders/clients');

        $clients = ORM::factory('user');
        
        $pagination = Pagination::factory(array(
            'total_items' => $clients->count_clients(),
            'items_per_page' => 50,
        ));

        $clients = $clients->clients_orders($this->_model_orders)
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->find_all();

        $this->content->clients = $clients;
        $this->content->pagination = $pagination;
    }

    public function action_client($id)
    {
        $client = ORM::factory('user')->find($id);

        if (! $client->pk())
        {
            $this->set_content('admin/clients/not_find');
        }

        $orders = ORM::factory($this->_model_orders)
            ->where('user_id', '=', $id)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->client_name = $client->user_data->name;
    }

    public function action_open($id)
    {
        $this->order_processed($id, FALSE);
    }

    public function action_close($id)
    {
        $this->order_processed($id, TRUE);

    }

    private function order_processed($id, $close = TRUE)
    {
        $order = ORM::factory($this->_model_orders)->find($id);

        if ($order->pk())
        {
            $order->processed = $close;
            $order->save();

            if ($close == TRUE){
                if ($this->is_parcel)
                    Model_event::instance()->add('EV_POSTS_MAKE_CLOSED','закрыл посылку <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
                else
                    Model_event::instance()->add('EV_ORDERS_MAKE_CLOSED','закрыл заказ <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
            } else{
                if ($this->is_parcel)
                    Model_event::instance()->add('EV_POSTS','открыл посылку <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
                else
                    Model_event::instance()->add('EV_ORDERS','открыл заказ <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
            }

        }

        $this->request->redirect(Request::$referrer);
    }

    public function action_set_important($id)
    {
        $this->order_important($id, TRUE);
    }

    public function action_set_notimportant($id)
    {
        $this->order_important($id, FALSE);
    }

    private function order_important($id, $important = TRUE)
    {
        $order = ORM::factory($this->_model_orders)->find($id);

        if ($order->pk())
        {
            $order->important = $important;
            $order->save();
            if (empty($this->is_parcel)){
                if($important == 1)
                    Model_event::instance()->add('EV_POSTS_UPDATE_IMPORTANT_ON','установил важным посылку <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a> ');
                else
                    Model_event::instance()->add('EV_POSTS_UPDATE_IMPORTANT_OFF','установил не важным посылку <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');

            } else{
                if($important == 1)
                    Model_event::instance()->add('EV_ORDERS_UPDATE_IMPORTANT_ON','установил важным заказ <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a> ');
                else
                    Model_event::instance()->add('EV_ORDERS_UPDATE_IMPORTANT_OFF','установил не важным заказ <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');

            }
        }
        //var_dump(Request::$referrer);
        //die();
        //for no caching
       /* if (strstr(Request::$referrer,'all')){
            $this->request->redirect('admin/'.(empty($this->is_parcel) ? 'orders' : 'parcels').'/all');
        }else{
            $this->request->redirect('admin/'.(empty($this->is_parcel) ? 'order' : 'parcel').'/'.$id);
        }*/
        sleep(3);
        $this->request->redirect(Request::$referrer);
    }

    public function items(ORM &$orders)
    {
        $this->set_content('admin/orders/items');

        $count_orders = clone $orders;

        $count = $count_orders
            ->count_all();

        
        $search = "";
        if (isset($_POST['search_by_name']) && $_POST['search_by_name']!= ""){
        $pagination = Pagination::factory(array(
            'total_items' => 4,
            'items_per_page' => 5,
        ));
            $search = $_POST['search_by_name'];
            if (intval($_POST['search_by_name']) != 0){
                $orders = $orders
                ->where('id','=',intval($_POST['search_by_name']))
                ->order_by('new', 'DESC')
                ->order_by('readed_admin', 'ASC')
                ->order_by('important', 'DESC')
    //            ->order_by('processed', 'DESC')
                ->order_by('id', 'DESC')
                ->find_all();
            }else{               
                $orders = $orders
                ->where('title','LIKE','%'.$_POST['search_by_name'].'%')
                ->order_by('new', 'DESC')
                ->order_by('readed_admin', 'ASC')
                ->order_by('important', 'DESC')
    //            ->order_by('processed', 'DESC')
                ->order_by('id', 'DESC')
                ->find_all();
            }

        }else{
        $pagination = Pagination::factory(array(
            'total_items' => $count,
            'items_per_page' => 50,
        ));
        $orders = $orders
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('new', 'DESC')
            ->order_by('readed_admin', 'ASC')
            ->order_by('important', 'DESC')
//            ->order_by('processed', 'DESC')
            ->order_by('id', 'DESC')
            ->find_all();
            
        }
        $this->content->search = $search;
        $this->content->orders = $orders;
        $this->content->pagination = $pagination;
    }

    public function action_all()
    {
        $orders = ORM::factory($this->_model_orders);


        $this->items($orders);

        $this->content->status = 'Все';
    }

    public function action_new()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('new', '=', 1)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Новые';
    }

    public function action_unreaded()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('readed_admin', '=', 0)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Непрочитанные';
    }

    public function action_readed()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('readed_admin', '=', 1)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Прочитанные';
    }

    public function action_important()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('important', '=', 1)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Важные';
    }

    public function action_deleted()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('deleted', '=', 1);

        $this->items($orders);

        $this->content->status = 'Удаленные';
    }

    public function action_closed()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('processed', '=', 1)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Закрытые';
    }

    public function action_opened()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('processed', '=', 0)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Текущие';
    }

} // End Admin Orders