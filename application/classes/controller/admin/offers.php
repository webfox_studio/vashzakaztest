<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Offers extends Controller_Admin_Abstract {

    public function action_index()
    {
        $this->set_content('admin/offers/items');

        $offers = ORM::factory('offer');

        $count = $offers->count_all();

        $offers = $offers
            ->order_by('id', 'DESC')
            ->find_all();

        $pagination = Pagination::factory(array(
            'total_items' => $count,
            'items_per_page' => 50,
        ));

        $this->content->offers = $offers;
        $this->content->pagination = $pagination;
    }

    public function action_item($id)
    {
        $this->set_content('admin/offers/item');

        $offer = ORM::factory('offer')->find($id);

        if (! $offer->pk())
        {
            $this->set_content('admin/offers/not_find');
            return;
        }

        if ($offer->readed == FALSE)
        {
            $offer->readed = TRUE;
            $offer->save();
        }

        $this->content->offer = $offer;
    }

} // End Admin Offers