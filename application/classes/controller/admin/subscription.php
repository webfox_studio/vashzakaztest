<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Subscription extends Controller_Admin_Abstract {
	
	public function before()
	{
		parent::before();
	}
	
	public function action_index()
	{
		// $this->content->statistics_subscription = Controller_Admin_Abstract::statistics_subscription();
		// $this->content->statistics_news = Controller_Admin_Abstract::statistics_news();

        $m_subscription = Model_Subscription::factory('subscription');
        $this->content->subscription_cats = $m_subscription->get_cats_client();

        if(isset($_GET['cat']) && (int)$_GET['cat'] != 0){
            $content = DB::select(array('t1.id', 'userId'), array('t4.name', 'username'), array('t4.country', 'country'), array('t1.email', 'email'), array('t3.name', 'name'), array('t2.hiddenMailing', 'hidden'))
                ->from(array('users', 't1'))

                ->join(array('user_data', 't4'), 'LEFT')
                ->on('t1.id','=', 't4.user_id')

                ->join(array('subscription_subscribers', 't2'))
                ->on('t1.id','=', 't2.uid')

                ->join(array('subscription_categories', 't3'))
                ->on('t2.cat_id','=', 't3.id')

                ->where('t3.id', '=', $_GET['cat'])
                ->order_by('t4.name')

                ->execute()
                ->as_array();
        }
        else {
            $content = DB::select(array('t1.id', 'userId'), array('t4.name', 'username'), array('t4.country', 'country'), array('t1.email', 'email'), array('t3.name', 'name'), array('t2.hiddenMailing', 'hidden'))
                ->from(array('users', 't1'))

                ->join(array('user_data', 't4'), 'LEFT')
                ->on('t1.id','=', 't4.user_id')

                ->join(array('subscription_subscribers', 't2'))
                ->on('t1.id','=', 't2.uid')

                ->join(array('subscription_categories', 't3'))
                ->on('t2.cat_id','=', 't3.id')

                //->where('id', '=', 84)
                ->order_by('t4.name')

                ->execute()
                ->as_array();
        }

		 // WHERE `kh_t3`.`id` =28
        $this->content->option = $content;
		$this->set_content('admin/subscription/main');
	}
	
	public function action_sender()
	{
		if($_POST) {
			$post = Validate::factory($_POST);
			$post->rule('name', 'not_empty');
			
			if ($post->check()) {
				ORM::factory('calculator_sender')
					->values($_POST)
					->save();
				
				$this->request->redirect('admin/onlinecalculator/sender');
			} else {
				$this->content->error = $post -> errors();
			}
		}
		$this->set_content('admin/onlinecalculator/sender');
		$country = ORM::factory('calculator_sender')->find_all();
		
		$this->content->country = $country;
	}
	
	public function action_senderdel($id)
	{
		ORM::factory('calculator_sender', $id)
			->delete();
		$this->request->redirect('admin/onlinecalculator/sender');
	}
	
	public function action_resipient()
	{
		
		if($_POST) {
			$post = Validate::factory($_POST);
			$post->rule('name', 'not_empty');
			
			if ($post->check()) {
				ORM::factory('calculator_resipient')
					->values($_POST)
					->save();
				
				$this->request->redirect('admin/onlinecalculator/resipient');
			} else {
				$this->content->error = $post -> errors();
			}
		}
		$this->set_content('admin/onlinecalculator/resipient');
		$country = ORM::factory('calculator_resipient')->where('parent_id','=','0')->find_all();
		
		$query =  mysql_query("SELECT * FROM kh_calculatorresipient");
		while($row = mysql_fetch_assoc($query))
		{
			$dd[$row[id]] = $row;
		}
		$this->add_js('media/js/calculator.js', TRUE);
		$countrytree = $this->mapTree($dd);
		
		asort($countrytree['1']['childs']);
		
		// echo '<pre>';
		//     print_r($countrytree);
		// echo '</pre>';
		
		$this->content->country = $country;
		$this->content->countrytree = $countrytree;
	}
	public function action_resipientdel($id)
	{
		ORM::factory('calculator_resipient', $id)
			->delete();
		$this->request->redirect('admin/onlinecalculator/resipient');
	}
	
	
	public function action_delivery()
	{
		$this->set_content('admin/onlinecalculator/delivery');
	}
	
	public function action_options($id)
	{
		if($_POST) {
			$post = Validate::factory($_POST);
			$post->rule('name', 'not_empty');
			$_POST['resipient_id'] = $id;
			if ($post->check()) {
				ORM::factory('calculator_option')
					->values($_POST)
					->save();
				
				$this->request->redirect('admin/onlinecalculator/options/'.$id);
			} else {
				$this->content->error = $post -> errors();
			}
		}
		$this->add_js('media/js/calculator.js', TRUE);
		$this->set_content('admin/onlinecalculator/options');
		$options = ORM::factory('calculator_option' )->where('resipient_id','=',$id)->find_all();
		$this->content->id = $id;
		$this->content->options = $options;
	}
	public function action_optiondel($id)
	{
		ORM::factory('calculator_option', $id)
			->delete();
		print_r($_SERVER);
		$this->request->redirect($_SERVER['HTTP_REFERER']);
	}
	
	function mapTree($dataset) {
		$tree = array();
		foreach ($dataset as $id=>&$node)
		{
			if ($node['parent_id'] == 0)
			{
				$tree[$id] = &$node;
			}
			else
			{
				$dataset[$node['parent_id']]['childs'][$id] = &$node;
			}
		}
		// sort($tree);
		// echo '<pre>';
		//print_r($tree);
		//echo '</pre>';
		return $tree;
	}// вызываем функцию и передаем ей наш массив$data = mapTree($data);
	
	public function action_price($id){
		if($_POST)
		{
			$post = Validate::factory($_POST);
			$post->rule('heft', 'not_empty');
			//$post->rule('price', 'not_empty');
			if ($post->check()) {
				ORM::factory('calculator_price')
					->values($_POST)
					->save();
				$this->request->redirect('admin/onlinecalculator/price/'.$_POST['option_id']);
			}
		}
		$this->add_js('media/js/calculator.js', TRUE);
		$this->set_content('admin/onlinecalculator/price');
		
		$this->content->option = ORM::factory('calculator_option')->where('id','=',$id)->find();
		$this->content->price = ORM::factory('calculator_price')->where('option_id','=',$id)->order_by('heft', 'ASC')->find_all();
	}
	
	public function action_pricedel($id)
	{
		ORM::factory('calculator_price', $id)
			->delete();
		print_r($_SERVER);
		$this->request->redirect($_SERVER['HTTP_REFERER']);
	}
	
} // End Admin Parcels