<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Reports extends Controller {

    public function action_index()
    {
        if (! isset($_GET['pass']) OR ($_GET['pass'] != 's0lt'))
        {
            echo 'ACCESS TO SERVER ERROR!';
            return FALSE;
        }
        else {

            if (! isset($_GET['user_id']) OR ! isset($_GET['user_balans']))
            {
                echo 'ERROR!';
                return FALSE;
            }

            $user_id     = intval($_GET['user_id']);
            $user_balans = floatval($_GET['user_balans']);
            
            $result = DB::update('user_data')
                ->set(array(
                    'value' => $user_balans,
                ))
                ->where('user_id', '=', $user_id)
                ->execute();
            
            if ($result)
            {
                echo '_ok_';
            }
            else
            {
                echo 'ERROR!';
            }
        }
    }

    public function action_test()
    {
        echo 'testing...<br /><br />';
        echo file_get_contents('http://www.vashzakaz.us/admin/reports');
    }

} // End Controller_Admin_Reports