<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Requestform extends Controller_Admin_Abstract {

    public function action_index()
    {
        $this->set_content('admin/requestform/index');
        $variable = $this->request->param('wer');
        
        
        $zakaz = ORM::factory('requestform');
        
        $count = $zakaz
            //  ->where('deleted', '=', 0)
            // ->where('processed', '=', 0)
            ->count_all();
        
        $pagination = Pagination::factory(array(
            'total_items' => $count,
            'items_per_page' => 50,
        ));
        
        switch ($variable) {

            case 'new':
                $zakaz = $zakaz
                     ->where('status', '=', 0)
                     ->where('deleted', '=', 0)
                     ->limit($pagination->items_per_page)
                     ->offset($pagination->offset)
                     ->order_by('status')
                    // ->order_by('important', 'DESC')
                    // ->order_by('id', 'DESC')
                     ->find_all();
                break;
            case 'arhiv':
                $zakaz = $zakaz
                    // ->where('status', '=', 0)
                     ->where('deleted', '=', 1)
                     ->limit($pagination->items_per_page)
                     ->offset($pagination->offset)
                     ->order_by('status')
                    // ->order_by('important', 'DESC')
                    // ->order_by('id', 'DESC')
                     ->find_all();
                break;
            case 'readed':
                $zakaz = $zakaz
                     ->where('deleted', '=', 0)
                     ->where('status', '=', 1)
                     ->limit($pagination->items_per_page)
                     ->offset($pagination->offset)
                     ->order_by('status')
                    // ->order_by('important', 'DESC')
                    // ->order_by('id', 'DESC')
                     ->find_all();
                break;
            case 'report':
                $zakaz = $zakaz
                     ->where('important', '=', 1)
                     ->where('deleted', '=', 0)
                     ->limit($pagination->items_per_page)
                     ->offset($pagination->offset)
                     ->order_by('status')
                    // ->order_by('important', 'DESC')
                    // ->order_by('id', 'DESC')
                     ->find_all();
                break;

            default:
                $zakaz = $zakaz
           // ->where('deleted', '=', 0)
            //->where('processed', '=', 0)
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('status', 'ASC')
          //  ->order_by('id', 'DESC')
            ->order_by('processed', 'ASC')
            ->find_all();
                break;
        }
        
        
        
        
        $this->content->zakaz = $zakaz;
        $this->content->pagination = $pagination;
    }
    
    public function action_zakaz(){
        $this->set_content('admin/requestform/zakaz');
        $id = $this->request->param('id');
        $zakaz = ORM::factory('requestform');
        
        $zakaz = $zakaz
                     ->where('id', '=', $id)
                     //->where('processed', '=', 0)

                     ->order_by('status')
                    // ->order_by('important', 'DESC')
                    // ->order_by('id', 'DESC')
                     ->find();
        if($zakaz->status == 0){$zakaz->status = 1; $zakaz->save();}
        $this->content->zakaz = $zakaz;
    }
    
    
    public function action_open($id)
    {
        $this->order_processed($id, FALSE);
    }

    public function action_close($id)
    {
        $this->order_processed($id, TRUE);

    }

    private function order_processed($id, $close = TRUE)
    {
        $order = ORM::factory('requestform')->find($id);

        if ($order->pk())
        {
            $order->processed = $close;
            $order->save();

            

        }

        $this->request->redirect(Request::$referrer);
    }

    public function action_set_important($id)
    {
        $this->order_important($id, TRUE);
    }

    public function action_set_notimportant($id)
    {
        $this->order_important($id, FALSE);
    }

    private function order_important($id, $important = TRUE)
    {
        $order = ORM::factory('requestform')->find($id);

        if ($order->pk())
        {
            $order->important = $important;
            $order->save();
           
        }
        //var_dump(Request::$referrer);
        //die();
        //for no caching
       /* if (strstr(Request::$referrer,'all')){
            $this->request->redirect('admin/'.(empty($this->is_parcel) ? 'orders' : 'parcels').'/all');
        }else{
            $this->request->redirect('admin/'.(empty($this->is_parcel) ? 'order' : 'parcel').'/'.$id);
        }*/
        sleep(1);
        $this->request->redirect(Request::$referrer);
    }

    public function items(ORM &$orders)
    {
        $this->set_content('admin/orders/items');

        $count_orders = clone $orders;

        $count = $count_orders
            ->count_all();

        
        $search = "";
        if (isset($_POST['search_by_name']) && $_POST['search_by_name']!= ""){
        $pagination = Pagination::factory(array(
            'total_items' => 4,
            'items_per_page' => 5,
        ));
            $search = $_POST['search_by_name'];
            if (intval($_POST['search_by_name']) != 0){
                $orders = $orders
                ->where('id','=',intval($_POST['search_by_name']))
                ->order_by('new', 'DESC')
                ->order_by('readed_admin', 'ASC')
                ->order_by('important', 'DESC')
    //            ->order_by('processed', 'DESC')
                ->order_by('id', 'DESC')
                ->find_all();
            }else{               
                $orders = $orders
                ->where('title','LIKE','%'.$_POST['search_by_name'].'%')
                ->order_by('new', 'DESC')
                ->order_by('readed_admin', 'ASC')
                ->order_by('important', 'DESC')
    //            ->order_by('processed', 'DESC')
                ->order_by('id', 'DESC')
                ->find_all();
            }

        }else{
        $pagination = Pagination::factory(array(
            'total_items' => $count,
            'items_per_page' => 50,
        ));
        $orders = $orders
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('new', 'DESC')
            ->order_by('readed_admin', 'ASC')
            ->order_by('important', 'DESC')
//            ->order_by('processed', 'DESC')
            ->order_by('id', 'DESC')
            ->find_all();
            
        }
        $this->content->search = $search;
        $this->content->orders = $orders;
        $this->content->pagination = $pagination;
    }

    public function action_all()
    {
        $orders = ORM::factory($this->_model_orders);


        $this->items($orders);

        $this->content->status = 'Все';
    }

    public function action_new()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('new', '=', 1)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Новые';
    }

    public function action_unreaded()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('readed_admin', '=', 0)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Непрочитанные';
    }

    public function action_readed()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('readed_admin', '=', 1)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Прочитанные';
    }

    public function action_important()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('important', '=', 1)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Важные';
    }

    public function action_deleted()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('deleted', '=', 1);

        $this->items($orders);

        $this->content->status = 'Удаленные';
    }

    public function action_closed()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('processed', '=', 1)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Закрытые';
    }

    public function action_opened()
    {
        $orders = ORM::factory($this->_model_orders)
            ->where('processed', '=', 0)
            ->where('deleted', '=', 0);

        $this->items($orders);

        $this->content->status = 'Текущие';
    }
    
    public function action_arhiv($id)
    {
        
        $order = ORM::factory('requestform')->find($id);

        if ($order->pk())
        {
            if($order->deleted == 1){
                $order->deleted = 0;
            }
            else{
                $order->deleted = 1;
            }
            
            $order->save();
           
        }
        $this->request->redirect(Request::$referrer);
    }



} // End Admin Orders