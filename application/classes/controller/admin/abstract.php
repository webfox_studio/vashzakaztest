<?php defined('SYSPATH') or die('No direct script access.');

abstract class Controller_Admin_Abstract extends Controller_Abstract
{

    public $template = 'layout_admin';

    protected $admin;

    public function before()
    {
        if (!Auth::instance()->logged_in('admin') AND !Auth::instance()->logged_in('redactor')) {
            $this->request->redirect('user/access_denied');
        }

        if (Auth::instance()->logged_in('redactor')) {
            // разрешенные страницы для редактора
            $allowedPage = array('onlinecalculator', 'seo', 'subscription', 'discount',);
            if (array_search($this->request->controller, $allowedPage) === false) {
                if (!strpos($_SERVER['REQUEST_URI'], '/editors/')) {
                    if (!Auth::instance()->logged_in('admin')) {
                        $this->request->redirect('/admin/editors/news');
                    }

                }
            }
        }

        parent::before();

        $this->admin = $this->user;

        $this->title = Lang::get('title.main') . Kohana::config('main.title_separator') . Lang::get('title.admin');


        $this->set_css_js();
        $this->set_menu();
    }

    public function set_css_js()
    {
        $this->add_js('main_admin');
    }

    private function set_menu()
    {
        $menu = array(
            //'messages'   => ORM::factory('messages')->where('whom_id', '=', 0)->where('readed2', '=', 0)->where('deleted2', '=', 0)->count_all(),
            'messages' => ORM::factory('messages')->where('readed2', '=', 0)->where('deleted2', '=', 0)->count_all(),
            'orders_new' => ORM::factory('orders')->where('new', '=', 1)->where('deleted', '=', 0)->count_all(),
            'orders_unreaded' => ORM::factory('orders')->where('readed_admin', '=', 0)->where('deleted', '=', 0)->count_all(),
            'orders_important' => ORM::factory('orders')->where('important', '=', 1)->where('deleted', '=', 0)->count_all(),
            'msg_important' => ORM::factory('message')->where('admin_important', '=', 1)->where('deleted', '=', 0)->count_all(),
            'parcels_new' => ORM::factory('parcels')->where('new', '=', 1)->where('deleted', '=', 0)->count_all(),
            'parcels_unreaded' => ORM::factory('parcels')->where('readed_admin', '=', 0)->where('deleted', '=', 0)->count_all(),
            'parcels_important' => ORM::factory('parcels')->where('important', '=', 1)->where('deleted', '=', 0)->count_all(),
            'payments' => NULL,
            'changes' => ORM::factory('user_data_changes')->where('approved', '=', 0)->count_all(),
            'offers' => ORM::factory('offer')->where('readed', '=', 0)->count_all(),
            'files_unreaded' => ORM::factory('files')->where('readed_admin', '=', 0)->count_all(),
            'files_important' => ORM::factory('files')->where('important', '=', 1)->count_all(),

            'payments_new' => ORM::factory('payments')->where('new', '=', 1)->where('deleted', '=', 0)->count_all(),
            'payments_unreaded' => ORM::factory('payments')->where('readed_admin', '=', 0)->where('deleted', '=', 0)->count_all(),
            'payments_important' => ORM::factory('payments')->where('important', '=', 1)->where('deleted', '=', 0)->count_all(),
            'adminfiles_all' => ORM::factory('shops_shops')->count_all(),
            'adminfiles_important' => ORM::factory('shops_shops')->where('important', '=', 1)->count_all(),


            'requestform_important' => ORM::factory('requestform')->where('important', '=', 1)->and_where('deleted', '=', 0)->count_all(),
            'requestform_new' => ORM::factory('requestform')->where('status', '=', 0)->and_where('deleted', '=', 0)->count_all(),
            'requestform_old' => ORM::factory('requestform')->where('status', '=', 1)->and_where('deleted', '=', 0)->count_all(),
            'requestform_deleted' => ORM::factory('requestform')->where('deleted', '=', 1)->count_all(),
            'requestform_all' => ORM::factory('requestform')->count_all(),

        );
        $nick = "";
        if (Auth::instance()->logged_in()) {
            $nick = Auth::instance()->get_user()->user_data->name;
            $chat = Model_event::instance()->get_chat($nick);
        }
        $this->template->nick = $nick;

        $this->template->menu = $menu;
        $this->template->admin = $this->admin;
        $this->template->admin_name = $this->admin->user_data->name;
        $this->template->logs = array_reverse(Model_event::instance()->get_last(30));
        $this->template->admin_is_client = $this->admin->has('roles', ORM::factory('role', array('name' => 'client')));
    }

    public function statistics_subscription()
    {
        $content = '';
        $result = DB::select('*')
            ->from('subscription')
            ->where('activ', '=', 1)
            ->execute()
            ->as_array();
        if (count($result) > 0) {
            $content = '<p><b style="color:red">Кол-во активных информационных рассылок: ' . count($result) . '</b></p>';
        } else {
            $content = '<p><b style="color:green">Нет активных информационных рассылок.</b></p>';
        }
        // print_r($result);

        return $content;
    }

    public static function statistics_news()
    {
        $content = '';
        $result = DB::select('*')
            ->from('news')
            ->where('to_emails', '=', 1)
            ->execute()
            ->as_array();
        if (count($result) > 0) {
            $content = '<p><b style="color:red">Кол-во активных новостей: ' . count($result) . '</b></p>';
        } else {
            $content = '<p><b style="color: green">Нет активных рассылок новостей.</b></p>';
        }
        // print_r($result);

        return $content;
    }

    public static function dateConvertTracking($date = null) {
        if ( $date ) {
            return date('Y-m-d', strtotime($date));
        }
    }

} // End Admin_Abstract