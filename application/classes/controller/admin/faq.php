<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Faq extends Controller_Admin_Abstract {

    public $content = 'faq/index';     

    public function action_index()
    {
        $m_faq = Model_Faq::instance();

        $faq_cats = $m_faq->get_cats();
        $faq = $m_faq->get_group_cat();

        $this->content->faq_cats = $faq_cats;
        $this->content->faq = $faq;
    }

} // End Faq