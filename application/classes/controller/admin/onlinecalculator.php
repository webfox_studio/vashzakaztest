<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Onlinecalculator extends Controller_Admin_Abstract {

    public function before()
    {
        parent::before();
    }

    public function action_index()
    {
        $this->set_content('admin/onlinecalculator/main');
    }

    public function action_sender()
    {
        if($_POST) {
            $post = Validate::factory($_POST);
            $post->rule('name', 'not_empty');

            if ($post->check()) {
                ORM::factory('calculator_sender')
                    ->values($_POST)
                    ->save();

                $this->request->redirect('admin/onlinecalculator/sender');
            } else {
                $this->content->error = $post -> errors();
            }
        }
        $this->set_content('admin/onlinecalculator/sender');
        $country = ORM::factory('calculator_sender')->find_all();

        $this->content->country = $country;
    }

    public function action_senderdel($id)
    {
        ORM::factory('calculator_sender', $id)
            ->delete();
        $this->request->redirect('admin/onlinecalculator/sender');
    }

    public function action_resipient()
    {

        if($_POST) {
            $post = Validate::factory($_POST);
            $post->rule('name', 'not_empty');

            if ($post->check()) {
                ORM::factory('calculator_resipient')
                    ->values($_POST)
                    ->save();

                $this->request->redirect('admin/onlinecalculator/resipient');
            } else {
                $this->content->error = $post -> errors();
            }
        }
        $this->set_content('admin/onlinecalculator/resipient');
        $country = ORM::factory('calculator_resipient')->where('parent_id','=','0')->order_by('name', 'asc')->find_all();

        $query =  mysql_query("SELECT * FROM kh_calculatorresipient ORDER BY parent_id, name ASC");
        while($row = mysql_fetch_assoc($query))
        {
            $dd[$row[id]] = $row;
        }
        $this->add_js('media/js/calculator.js', TRUE);
        $countrytree = $this->mapTree($dd);

        // asort($countrytree['1']['childs']);

       // echo '<pre>';
       //     print_r($countrytree);
       // echo '</pre>';

        $this->content->country = $country;
        $this->content->countrytree = $countrytree;
    }

    public function action_resipientdel($id)
    {
        ORM::factory('calculator_resipient', $id)
            ->delete();
        $this->request->redirect('admin/onlinecalculator/resipient');
    }

    public function action_delivery()
    {
        $this->set_content('admin/onlinecalculator/delivery');
    }

    public function action_options($id)
    {

        if($_POST) {
            $post = Validate::factory($_POST);
            $post->rule('name', 'not_empty');
            $_POST['resipient_id'] = $id;
            if ($post->check()) {
                ORM::factory('calculator_option')
                    ->values($_POST)
                    ->save();

                $this->request->redirect('admin/onlinecalculator/options/'.$id);
            } else {
                $this->content->error = $post -> errors();
            }
        }
        $this->content->id = $id;
        $this->content->temp = DB::select(array('t1.name', 'nameCity'), array('t2.name', 'nameDelivery'), array('t2.id', 'id'))
            ->from(array('calculatorresipient', 't1'))
            ->join(array('calculatoroptions', 't2'))
            ->on('t1.id', '=', 't2.resipient_id')
            ->order_by('t1.name', 'asc')
            ->as_object()->execute();
        $this->content->resipient = ORM::factory('calculator_resipient')->where('id','=',$id)->find();
        $this->add_js('media/js/calculator.js', TRUE);
        $this->set_content('admin/onlinecalculator/options');
        $options = ORM::factory('calculator_option' )->where('resipient_id','=',$id)->find_all();
        $this->content->id = $id;
        $this->content->options = $options;
    }

    public function action_optiondel($id)
    {
        ORM::factory('calculator_option', $id)
            ->delete();
        print_r($_SERVER);
        $this->request->redirect($_SERVER['HTTP_REFERER']);
    }

    function mapTree($dataset) {
        $tree = array();
        foreach ($dataset as $id=>&$node)
        {
            if ($node['parent_id'] == 0)
            {
            $tree[$id] = &$node;
            }
            else
            {
            $dataset[$node['parent_id']]['childs'][$id] = &$node;
            }
        }
       // sort($tree);
       // echo '<pre>';
        //print_r($tree);
        //echo '</pre>';
        return $tree;
    }// вызываем функцию и передаем ей наш массив$data = mapTree($data);

    public function action_price($id){
        if($_POST)
        {
            $post = Validate::factory($_POST);
            $post->rule('heft', 'not_empty');
            //$post->rule('price', 'not_empty');
            if ($post->check()) {
                ORM::factory('calculator_price')
                    ->values($_POST)
                    ->save();
                $this->request->redirect('admin/onlinecalculator/price/'.$_POST['option_id']);
            }
        }
        $this->add_js('media/js/calculator.js', TRUE);
        $this->set_content('admin/onlinecalculator/price');

        $this->content->temp = DB::select(array('t1.name', 'nameCity'), array('t2.name', 'nameDelivery'), array('t2.id', 'id'))
            ->from(array('calculatorresipient', 't1'))
            ->join(array('calculatoroptions', 't2'))
            ->on('t1.id', '=', 't2.resipient_id')
            ->order_by('t1.name', 'asc')
            ->as_object()->execute();

        $this->content->id = $id;
        $this->content->option = ORM::factory('calculator_option')->where('id','=',$id)->find();

        $this->content->price = ORM::factory('calculator_price')->where('option_id','=',$id)->order_by('heft', 'ASC')->find_all();
    }

    public function action_pricedel($id)
    {
        ORM::factory('calculator_price', $id)
            ->delete();
        print_r($_SERVER);
        $this->request->redirect($_SERVER['HTTP_REFERER']);
    }

    public function action_addprice($id)
    {
        DB::query(Database::DELETE, 'DELETE FROM kh_calculatorprices WHERE option_id = :option_id')
            ->param(':option_id', (int)$_POST['optionId'])->execute();

        $temp = ORM::factory('calculator_price')->where('option_id','=',(int)$_POST['priceId'])->order_by('heft', 'ASC')->find_all();

        foreach($temp as $k => $v):
            $add = ORM::factory('calculator_price');
            $add->heft = $v->heft;
            $add->heft_type = $v->heft_type;
            $add->price = $v->price;
            $add->option_id = (int)$_POST['optionId'];
            $add->save();
        endforeach;

        $this->request->redirect($_SERVER['HTTP_REFERER']);
    }

    public function action_addoption(){
        $temp = ORM::factory('calculator_option')->where('id','=',(int)$_POST['priceId'])->find();

        $add = ORM::factory('calculator_option');
        $add->name = $temp->name;
        $add->delivery_period = $temp->delivery_period;
        $add->tracing = $temp->tracing;
        $add->insurance = $temp->insurance;
        $add->customsclearance = $temp->customsclearance;
        $add->variant_reception = $temp->variant_reception;
        $add->resipient_id = (int)$_POST['optionId'];
        $add->save();
        $this->request->redirect($_SERVER['HTTP_REFERER']);
    }

} // End Admin Parcels