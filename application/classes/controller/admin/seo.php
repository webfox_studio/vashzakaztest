<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Seo extends Controller_Admin_Abstract {
    
    protected $menu;
    
    public function after()
    {
        $this->content->editors_menu = $this->menu;

        parent::after();
    }
    
    public function before()
    {
        parent::before();

        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_f');

        $this->menu = array(
            array('url' => Url::site('admin/editors/news'), 'title' => 'Новости'),
            array('url' => Url::site('admin/editors/schedule'), 'title' => 'Расписание'),
            array('url' => Url::site('admin/editors/faq'), 'title' => 'Вопросы'),
            array('url' => Url::site('admin/editors/contacts'), 'title' => 'Контакты'),
            array('url' => Url::site('admin/editors/about'), 'title' => 'О нас'),
            array('url' => Url::site('admin/editors/agreements'), 'title' => 'Соглашение'),
            array('url' => Url::site('admin/editors/useful'), 'title' => 'Полезное'),
            array('url' => Url::site('admin/editors/orderform1'), 'title' => 'Форма заказа 1'),
            array('url' => Url::site('admin/editors/orderform2'), 'title' => 'Форма заказа 2'),
            array('url' => Url::site('admin/editors/orderform3'), 'title' => 'Форма заказа 3'),
            array('url' => Url::site('admin/editors/orderform4'), 'title' => 'Форма заказа 4'),
            array('url' => Url::site('admin/editors/posilkaform'), 'title' => 'Форма посылки'),
            array('url' => Url::site('admin/editors/paymentform'), 'title' => 'Форма перевода'),
            array('url' => Url::site('admin/editors/banners'), 'title' => 'Баннеры'),
            array('url' => Url::site('admin/editors/actions'), 'title' => 'Акции'),
            array('url' => Url::site('admin/editors/reviews'), 'title' => 'Отзывы'),
            array('url' => Url::site('admin/editors/services'), 'title' => 'Об услугах'),
            array('url' => Url::site('admin/editors/subscription'), 'title' => 'Подписка'),
            array('url' => Url::site('admin/editors/additionalpage'), 'title' => 'Дополнительные страницы'),
        );

    }
    
    public function action_index()
    {
        //получаем переменные
        // $type = $this->request->param('type');
        // $idpage = $this->request->param('idpage');
        

                $pages = ORM::factory('seo')
                    ->find_all();
                $this->content->pages = $pages;
                //$this->request->redirect('admin/editors/additionalpage');
        $this->set_content('admin/seo/index');
    }
	
	public function action_edit()
    {
		// print_r($_POST);
		// date('Y-m-d H:i:s')
		if($_POST && $_POST['id']){
			$seo = ORM::factory('seo')->find($_POST['id']);
				// $seo->id = (int)$_POST['id'];
				$seo->url = $_POST['url'];
				$seo->title = $_POST['title'];
				$seo->desc = $_POST['desc'];
				$seo->key = $_POST['key'];
				$seo->editDate = date('Y-m-d H:i:s');
			$seo->save();
			$this->request->redirect('admin/seo/');
		}
    }
	public function action_add()
    {
		// print_r($_POST);
		// date('Y-m-d H:i:s')
		if($_POST){
			$seo = ORM::factory('seo');
				// $seo->id = (int)$_POST['id'];
				$seo->url = $_POST['url'];
				$seo->title = $_POST['title'];
				$seo->desc = $_POST['desc'];
				$seo->key = $_POST['key'];
				$seo->addDate = date('Y-m-d H:i:s');
			$seo->save();
			$this->request->redirect('admin/seo/');
		}
    }
	
	public function action_delete()
    {
        $id = $this->request->param('id');

        $del = ORM::factory('seo', $id)
                ->delete();
        
        $this->request->redirect('admin/seo');
    }
} // Admin Seo