<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Tracking extends Controller_Admin_Abstract
{

    public function before()
    {

        parent::before();
        $this->add_css('media/css/bootstrap/bootstrap.css');
        $this->add_css('media/css/bootstrap/bootstrap-theme.css');


        $this->add_js('media/js/datepicker/jquery.ui.core.js');
        // $this->add_js('media/js/datepicker/jquery.ui.widget.js');
        $this->add_js('media/js/datepicker/jquery.ui.datepicker.js');
        $this->add_js('media/js/datepicker/lang/jquery.ui.datepicker-ru.js');
        $this->add_js('media/js/tracking.js');
        $this->add_css('media/css/datepicker/datepicker.css');
    }

    public function action_index()
    {
        $this->set_content('admin/tracking/index');

        $trackings = ORM::factory('tracking');
        $total_trackings = $trackings
            ->where('delete', '=', 0)
            ->count_all();
        $pagination = Pagination::factory(array(
            'total_items' => $total_trackings,
            'items_per_page' => 10,
        ));
        $trackings = $trackings
            ->where('delete', '=', 0)
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('date_add', 'DESC')
            ->find_all();

        $this->content->trackings = $trackings;
        $this->content->pagination = $pagination;
        $this->content->delivery = Kohana::config('tracking.delivery');
        $this->content->form_column = Kohana::config('tracking.form_columns');
    }

    public function action_add()
    {
        $this->set_content('admin/tracking/add');
        if ($_POST) {
            $post = Validate::factory($_POST);
            $post->rule('user_id', 'not_empty');
            $post->rule('user_name', 'not_empty');
            $post->rule('user_surname', 'not_empty');
            $post->rule('delivery_id', 'not_empty');
            $post->rule('number', 'not_empty');
            $post->rule('date_add', 'not_empty');
            $post->rule('status', 'not_empty');
            $post->rule('comment', 'min_length', array(':value', 0));
            $post->rule('comment_admin', 'min_length', array(':value', 0));

            if ($post->check()) {
                // $date = explode('.', $post['date_add']);
                // $post['date_add'] = $date[2] . '-' . $date[1] . '-' . $date[0];
                $dataTracking = array(
                    'user_id' => $post['user_id'],
                    'user_name' => $post['user_name'],
                    'user_surname' => $post['user_surname'],
                    'tracking' => time(),
                    'delivery_id' => $post['delivery_id'],
                    'number' => $post['number'],
                    'comment' => $post['comment'],
                    'comment_admin' => $post['comment_admin'],
                    'date_add' => date("Y-m-d H:i:s")
                );
                $trackingQuery = ORM::factory('tracking')
                    ->values($dataTracking)
                    ->save();
                if ($trackingQuery) {
                    $tracking_id = mysql_insert_id();
                    $dataTrackingStatus = array(
                        'tracking_id' => $tracking_id,
                        'status' => $post['status'],
                        'date_add' => $post['date_add'],
                    );
                    $trackingStatusQuery = ORM::factory('trackingstatus')
                        ->values($dataTrackingStatus)
                        ->save();
                    if ($trackingStatusQuery) {
                        $this->request->redirect('admin/tracking');
                    }
                }

            } else {
                $this->content->errors = $post->errors('ru');
                $this->content->post = $post;
            }
        }


        $this->content->delivery = Kohana::config('tracking.delivery');
        $this->content->form_column = Kohana::config('tracking.form_columns');
    }

    public function action_edit()
    {
        $this->set_content('admin/tracking/edit');
        $id = $this->request->param('id');
        if ($id) {
            if ($_POST) {
                $post = Validate::factory($_POST);
                $post->rule('user_id', 'not_empty');
                $post->rule('user_name', 'not_empty');
                $post->rule('user_surname', 'not_empty');
                $post->rule('delivery_id', 'not_empty');
                $post->rule('number', 'not_empty');
                $post->rule('comment', 'min_length', array(':value', 0));
                $post->rule('comment_admin', 'min_length', array(':value', 0));

                if ($post->check()) {

                    $trackingQuery = ORM::factory('tracking', $id)
                        ->values($post)
                        ->save();
                    if ($trackingQuery) {
                        $this->request->redirect('admin/tracking/edit/' . $id);
                    }
                } else {
                    $this->content->errors = $post->errors('ru');
                    $this->content->post = $post;
                }

            }
            $tracking = ORM::factory('tracking', $id);
            if ($tracking->loaded()) {
                $this->content->tracking = $tracking;
                $this->content->delivery = Kohana::config('tracking.delivery');
                $this->content->form_column = Kohana::config('tracking.form_columns');
            }
        }
    }

    public function action_addStatus()
    {
        if ($_POST) {
            $post = Validate::factory($_POST);
            $post->rule('date_add', 'not_empty');
            $post->rule('tracking_id', 'not_empty');
            $post->rule('status', 'not_empty');
            if ($post->check()) {
                $trackingStatusQuery = ORM::factory('trackingstatus')
                    ->values($post)
                    ->save();
                if ($trackingStatusQuery) {
                    $this->request->redirect('admin/tracking/edit/' . $post['tracking_id']);
                }
            }
        }
    }

    public function action_addNotes()
    {
        if ($_POST) {
            $post = Validate::factory($_POST);
            $post->rule('notes', 'not_empty');
            $post->rule('tracking_id', 'not_empty');
            if ($post->check()) {
                $params = array(
                    'tracking_id' => $post['tracking_id'],
                    'notes' => $post['notes'],
                    'who' => 1,
                    'date_add' => date("Y-m-d H:i:s"),
                );
                $query = ORM::factory('trackingnotes')
                    ->values($params)
                    ->save();
                if ($query) {
                    $this->request->redirect('admin/tracking/edit/' . $post['tracking_id']);
                }
            }

        }
    }

    public function action_delStatus()
    {
        $id = $this->request->param('id');
        if ($id) {
            $trackingStatusQuery = ORM::factory('trackingstatus', $id);
            $tracking_id = $trackingStatusQuery->tracking_id;
            $trackingStatusQuery->delete();
            if ($trackingStatusQuery) {
                $this->request->redirect('admin/tracking/edit/' . $tracking_id);
            }
        }
    }

    public function action_editStatus()
    {
        $data = array();
        foreach ($_POST as $key => $value) {
            foreach ($value as $key1 => $value1) {
                $data[$key1][$key] = $value1;
            }
        }
        foreach ($data as $key => $value):
            ORM::factory('trackingstatus', $key)
                ->values($value)
                ->save();
        endforeach;
        $this->request->redirect('admin/tracking/edit/' . $_POST['tracking_id']);
    }

    public function action_delTracking() {
        $id = $this->request->param('id');
        if($id){
            $trackingQuery = ORM::factory('tracking', $id);
            $trackingQuery->delete = 1;
            $trackingQuery->save();
            if ($trackingQuery) {
                $this->request->redirect('admin/tracking');
            }
        }

    }

} // End Admin Tracking