<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Files extends Controller_Admin_Abstract {

    public $content = 'admin/files/index';

    public function before()
    {
        parent::before();    	      
    }
    
    public function action_upload($id)
    {
        $this->set_content('files/items');
        
        $files = Validate::factory($_FILES);
        $files->rule('file_name', 'Upload::size', array('5M'));

//         print_r($_FILES);
//         print_r($files);
//         die();
        if($files->check()){
            //Upload::save($_FILES['file_name'],$_FILES['file_name'].$this->user->id,'upload');
            Upload::save($_FILES['file_name'], $this->user->id.'_'.$_FILES['file_name']['name'], 'upload');
            DB::insert('files')
            ->columns(array('name','file_name','user_id', 'date_time', 'incoming', 'readed_admin'  ))
            ->values(array($_FILES['file_name']['name'],$this->user->id.'_'.$_FILES['file_name']['name'], $this->user->id, time(),'1','1'))
            ->execute();
        }
        //$this->set_content('files/userfiles');
//        $files = ORM::factory('files')
//            ->where('user_id', '=', $id)
//            ->order_by('readed','ASC')
//            ->order_by('date_time','DESC')
//            ->find_all();
//        $this->content->files = $files;
    }

    public function action_userfiles()
    {
        $this->set_content('files/userfiles');
        $files = ORM::factory('files')
            ->where('user_id', '=', $user_id)
            ->find_all();
        $this->content->files = $files;
    }

    public function action_download($id)
    {

        $files = ORM::factory('files')
            ->find($id)->as_array();
        //print_r($files);
        //die();
        if (! file_exists(DOCROOT.'upload/'.$files['file_name'])){

            $this->set_content('files/file_not_find');
        }
        else{
             $file = ORM::factory('files');
            $file = $file->find($id);
            //print_r($files);
            //die();
            $file->readed_admin = true;
            $file->save();
            Request::instance()->send_file(DOCROOT.'upload/'.$files['file_name'], $files['name']);
           
            //Model_event::instance()->add('EV_XLS_DOWNLOAD','скачал xls-отчет');
        }
        
        //    $this->set_content('user/report_unactive');
        
    }

    public function action_delete($id)
    {

        $files = ORM::factory('files')
            ->find($id)->as_array();
        //print_r($files);
        //die();
        $file = ORM::factory('files');
        $file = $file->find($id)->delete();
        if ( file_exists(DOCROOT.'upload/'.$files['file_name'])){
            unlink(DOCROOT.'upload/'.$files['file_name']);
             
            //print_r($files);
            //die();

            //Model_event::instance()->add('EV_XLS_DOWNLOAD','скачал xls-отчет');
        }
        $this->request->redirect(Request::$referrer);
        //    $this->set_content('user/report_unactive');

    }
    
     public function action_index()
    {
       
        $this->add_js('jquery.quicksearch');
        $this->add_js('tablesearch');

        $this->set_content('admin/files/index');

        $files = ORM::factory('files');
        $count_files = $files->count_all();
        $pagination = Pagination::factory(array(
                'total_items'    => $count_files,
                'items_per_page' => 50,
            ));

        $files = ORM::factory('files')
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('readed_admin')
            ->order_by('important', 'DESC')
            ->order_by('id', 'DESC')
            ->find_all();
//            print_r($files);
//         die();
        $this->content->files = $files;
        $this->content->pagination = $pagination;
    
    }

    public function action_item($id)
    {
        $this->set_content('admin/orders/item');

        $order = ORM::factory($this->_model_orders)->find($id);

        if (! $order->pk())
        {
            $this->set_content('orders/not_find');
            return;
        }

        $order->readed_admin = TRUE;
        $order->new = FALSE;
        $order->save();

        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_f');
        $form = Formo::get('order_message');
        $this->content->is_admin = true;
        if (isset($_POST['preview'])){
                $this->content->pre_message = array(
                    'time' => Date::formatted_time(),
                    'content' => $_POST["Order_Message"]['message']
                );
                $form->message->val( $_POST["Order_Message"]['message']);
        }
        if ($this->request->param('message') AND $this->request->param('act'))
        {
        	switch ($this->request->param('act'))
        	{
        		case 'del':
        			$message = ORM::factory($this->_model_messages);
        			$message->delete($this->request->param('message'));

        			if ($this->is_parcel)
		                $this->request->redirect('admin/parcel/'.$order->id);
		            else
		                $this->request->redirect('admin/order/'.$order->id);
        			break;

        		case 'edit':
        			$message = ORM::factory($this->_model_messages);
        			$message->find($this->request->param('message'));
           			$form->message->val($message->message);
        			break;
        	}

        }

        if ($form->sent() AND $form->validate())
        {
        	if (! isset($message))
        	{
            	$message = ORM::factory($this->_model_messages);
        	}

            $this->is_parcel ? ($message->parcel_id = $order->id) : ($message->order_id = $order->id);

            $message->user_id = Auth::instance()->get_user()->id;
            $message->date_time = Date::formatted_time();
            $message->message = $form->message->val();
            $message->save();

            $order->readed = FALSE;
            $order->save();

            $email = $order->user->email;

            if (empty($this->is_parcel))
            {
                $head_email = 'Ответ в Вашем заказе';
                $text_email = 'Это уведомление о новом ответе в вашем заказе: '.HTML::anchor(URL::site('order/id'.$order->id, TRUE)).'<br />'.
                	'<p>'.$message->message.'</p>';
                Model_event::instance()->add('EV_ORDERS_ADD_MESSAGE','сообщение в заказе <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
            }
            else
            {
                $head_email = 'Ответ в Вашей посылке';
                $text_email = 'Это уведомление о новом ответе в вашей посылке: '.HTML::anchor(URL::site('parcel/id'.$order->id, TRUE)).'<br />'.
                	'<p>'.$message->message.'</p>';
                Model_event::instance()->add('EV_POSTS_ADD_MESSAGE','сообщение в посылке <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>');
            }

            EmailHelper::send($email, $head_email, $text_email, FALSE, TRUE);

            if ($this->is_parcel)
                $this->request->redirect('admin/parcel/'.$order->id);
            else
                $this->request->redirect('admin/order/'.$order->id);
            $this->set_content('orders/message_sended');

        }
        $this->content->user = $this->user;
        $this->content->form = $form;
        $this->content->order = $order;
        $this->is_parcel ? (Model_event::instance()->add('EV_ORDERS_VIEW_ORDER','просматривает посылку <a href="/admin/parcel/'.$order->id.'">'.$order->user->user_data->name.' посылка №'.$order->id.'('.$order->title.')</a>')) : (Model_event::instance()->add('EV_ORDERS_VIEW_ORDER','просматривает заказ <a href="/admin/order/'.$order->id.'">'.$order->user->user_data->name.' заказ №'.$order->id.'('.$order->title.')</a>'));

    }

    public function action_clients()
    {
        $this->add_js('jquery.quicksearch');
        $this->add_js('tablesearch');

        $this->set_content('admin/files/clients');

        $clients = ORM::factory('user');

        $pagination = Pagination::factory(array(
            'total_items' => $clients->count_clients(),
            'items_per_page' => 50,
        ));

        $clients = $clients->clients_files('files')
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->find_all();

        $this->content->clients = $clients;
        $this->content->pagination = $pagination;
    }

    public function action_client($id)
    {
        if(!empty($_FILES)){
            $files = Validate::factory($_FILES);
            $files->rule('file_name', 'Upload::size', array('3M'));

    //         print_r($_FILES);
    //         print_r($files);
    //         die();
            if($files->check()){
                $filename = $id.'_'.substr(md5($_FILES['file_name']['name']),0,15);
                //Upload::save($_FILES['file_name'],$_FILES['file_name'].$this->user->id,'upload');
                Upload::save($_FILES['file_name'], $filename, 'upload');
                DB::insert('files')
                ->columns(array('name','file_name','user_id', 'date_time', 'incoming', 'readed_admin'  ))
                ->values(array($_FILES['file_name']['name'],$filename, $id, time(),'1','1'))
                ->execute();
            }else{
                 $this->content->error = 'Ошибка загрузки файла на сервер.';
            }
        }
        $client = ORM::factory('user')->find($id);

        if (! $client->pk())
        {
            $this->set_content('admin/files/not_find');
        }

        $files = ORM::factory('files')
            ->where('user_id', '=', $id);

        $this->items($files);

        $this->content->client_name = $client->user_data->name;
        $this->content->client_id = $client->id;
    }

    


    public function action_set_important($id)
    {
        $this->files_important($id, TRUE);
    }

    public function action_set_notimportant($id)
    {
        $this->files_important($id, FALSE);
    }

    private function files_important($id, $important = TRUE)
    {
        $file = ORM::factory('files')->find($id);

        if ($file->pk())
        {
            $file->important = $important;
            $file->save();
        }
        //var_dump(Request::$referrer);
        //die();
        //for no caching
       /* if (strstr(Request::$referrer,'all')){
            $this->request->redirect('admin/'.(empty($this->is_parcel) ? 'orders' : 'parcels').'/all');
        }else{
            $this->request->redirect('admin/'.(empty($this->is_parcel) ? 'order' : 'parcel').'/'.$id);
        }*/
        sleep(3);
        $this->request->redirect(Request::$referrer);
    }

    public function items(ORM &$files)
    {
        $this->set_content('admin/files/items');
        $count_files = clone $files;

        $count_files = $count_files->count_all();
        $search = "";
        if (isset($_POST['search_by_name']) && $_POST['search_by_name']!= ""){
            $pagination = Pagination::factory(array(
                'total_items' => 4,
                'items_per_page' => 5,
            ));
            $search = $_POST['search_by_name'];
            if (intval($_POST['search_by_name']) != 0){
                $files = $files->where('id','=',intval($_POST['search_by_name']))
                    ->order_by('readed_admin','ASC')
                    ->order_by('important', 'DESC')
                    ->order_by('id', 'DESC')
                    ->find_all();
            }else{
                $files = $files
                    ->where('name','LIKE','%'.$_POST['search_by_name'].'%')
                    ->order_by('readed_admin','ASC')
                    ->order_by('important', 'DESC')
                    ->order_by('id', 'DESC')
                    ->find_all(); 
               
            }

        }else{

            $pagination = Pagination::factory(array(
                    'total_items'    => $count_files,
                    'items_per_page' => 50,
                ));
            $files = $files ->limit($pagination->items_per_page)
                ->offset($pagination->offset)
                ->order_by('readed_admin','ASC')
                ->order_by('important', 'DESC')
                ->order_by('id', 'DESC')
                ->find_all();
//            print_r($files);
//            die();

        }
        $this->content->search = $search;
        $this->content->files = $files;
        $this->content->pagination = $pagination;

    }

    public function action_all()
    {
        $files = ORM::factory('files');


        $this->items($files);

        $this->content->status = 'Все';
    }

    public function action_unreaded()
    {
        $files = ORM::factory('files')
            ->where('readed_admin', '=', 0);

        $this->items($files);

        $this->content->status = 'Непросмотренные';
    }

    public function action_readed()
    {
        $files = ORM::factory('files')
            ->where('readed_admin', '=', 1);

        $this->items($files);

        $this->content->status = 'Просмотренные';
    }

    public function action_important()
    {
        $files = ORM::factory('files')
            ->where('important', '=', 1);

        $this->items($files);

        $this->content->status = 'Важные';
    }




} // End Faq
