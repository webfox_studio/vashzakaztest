<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Editors extends Controller_Admin_Abstract {

    protected $menu;

    public function before()
    {
        parent::before();

        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_f');

        $this->menu = array(
            array('url' => Url::site('admin/editors/news'), 'title' => 'Новости'),
            array('url' => Url::site('admin/editors/schedule'), 'title' => 'Расписание'),
            array('url' => Url::site('admin/editors/faq'), 'title' => 'Вопросы'),
            array('url' => Url::site('admin/editors/contacts'), 'title' => 'Контакты'),
            array('url' => Url::site('admin/editors/about'), 'title' => 'О нас'),
            array('url' => Url::site('admin/editors/agreements'), 'title' => 'Соглашение'),
            array('url' => Url::site('admin/editors/useful'), 'title' => 'Полезное'),
            array('url' => Url::site('admin/editors/orderform'), 'title' => 'Форма заказа'),
            array('url' => Url::site('admin/editors/posilkaform'), 'title' => 'Форма посылки'),
            array('url' => Url::site('admin/editors/banners'), 'title' => 'Баннеры'),
            array('url' => Url::site('admin/editors/actions'), 'title' => 'Акции'),
            array('url' => Url::site('admin/editors/reviews'), 'title' => 'Отзывы'),
            array('url' => Url::site('admin/editors/services'), 'title' => 'Об услугах'),
        );

        $pages = ORM::factory('page')
            ->order_by('order')
            ->find_all();

        foreach($pages as $_page)
        {
            $this->menu[] = array(
                'url' => Url::site('admin/editors/page/'.$_page->name),
                'title' => $_page->title,
                'order' => $_page->order,
            );
        }
    }

    public function after()
    {
        $this->content->editors_menu = $this->menu;

        parent::after();
    }

    public function action_index()
    {
        $this->request->redirect('admin/editors/news');

        $this->set_content('admin/editors/index');
    }
    
//-----------------------------BANNERS---------------------------------------------------
    public function action_banners()
    {
        $this->set_content('admin/editors/banners');
        $news = array();
        $news =  Model::factory('banners')
            ->get_all(99,0);
            
        $this->content->news = $news;
    }
    
    public function action_banners_add()
    {
       $uploaddir = '/home/tolich/public_html/upload/banners/';
       
       if(is_uploaded_file($_FILES['img']['tmp_name']))
	   {
	       if (file_exists($uploaddir . $_FILES['img']['name'])) unlink($uploaddir . $_FILES['img']['name']);
            
            if (move_uploaded_file($_FILES['img']['tmp_name'], $uploaddir . $_FILES['img']['name'])) 
            {
                $img = $_FILES['img']['name'];
            }
            if($img)
            {
                $news = Model::factory('banners')
                    ->add($img, $_POST['link']);
                //Model_event::instance()->add('EV_FAQ_CAT_EDIT','новая категория в разделе "Вопросы" <a href="/admin/editors/cat_edit/'.$_POST['cat_id'].'">'.$_POST['name'].'</a>');
            }
        }
            
        $this->request->redirect('admin/editors/banners');
    }
    
    public function action_banners_delete($id)
    {
        $news = Model::factory('banners')
            ->delete($id);

        $this->request->redirect('admin/editors/banners');
    }

//-----------------------------REVIEWS---------------------------------------------------

    public function action_reviews()
    {
        $this->set_content('admin/editors/reviews');

        $news = Model::factory('reviews')
            ->get_all(99, 0, 'DESC');

        $this->content->news = $news;
    }
    
    public function action_reviews_add()
    {
       //$uploaddir = Kohana::config('upload.reviews');
       
	   if(!empty($_FILES)){
            $files = Validate::factory($_FILES);
            $files->rule('img', 'Upload::size', array('3M'));
			
            if($files->check()){
				$filename = $_FILES['img']['name'];
                $filename = basename(Upload::save($_FILES['img'], $filename, 'upload/reviews'));
				
				$news = Model::factory('reviews')
					->add($_POST['content'], $_POST['tracking'], $filename);
            }else{
                $this->content->error = 'Ошибка загрузки файла на сервер.';
            }
        }
                
        $this->request->redirect('admin/editors/reviews');
    }
    
    public function action_reviews_edit($id)
    {
        $this->set_content('admin/editors/reviews_edit');

        if (isset($_POST['content']))
        {
            $data = array(
                'description' => $_POST['content'],
                'tracking' => $_POST['tracking']
            );
            $uploaddir = '/home/tolich/public_html/upload/actions/';
       if(is_uploaded_file($_FILES['img']['tmp_name']))
	   {
	       if (file_exists($uploaddir . $_FILES['img']['name'])) unlink($uploaddir . $_FILES['img']['name']);
            
            if (move_uploaded_file($_FILES['img']['tmp_name'], $uploaddir . $_FILES['img']['name'])) 
            {
                $data['img'] = $_FILES['img']['name'];
                if($data['img'] == '') unset($data['img']);
            }
        }
        

            $news = Model::factory('reviews')
                ->edit($id, $data);
           // Model_event::instance()->add('EV_NEWS_EDIT','редактируется новость <a href="/admin/editors/news_edit/'.$id.'"> Новость №'.$id.'</a>');

            $this->request->redirect('admin/editors/reviews');
        }

        $news = Model::factory('reviews')
            ->get($id);
        //print_r($news);
        $this->content->news = $news;
    }
    
    public function action_reviews_delete($id)
    {
        $news = Model::factory('reviews')
            ->delete($id);

        $this->request->redirect('admin/editors/reviews');
    }

//-----------------------------ACTIONS---------------------------------------------------
    
    public function action_actions()
    {
        $this->set_content('admin/editors/actions');

        $news = Model::factory('actions')
            ->get_all(500, 0, 'DESC');

        $this->content->news = $news;
    }
    
    public function action_actions_add()
    {
        $uploaddir = '/home/tolich/public_html/upload/actions/';
        
		if(!empty($_FILES)){
            $files = Validate::factory($_FILES);
            $files->rule('img', 'Upload::size', array('3M'));
			
            if($files->check()){
				$filename = $_FILES['img']['name'];
                $filename = basename(Upload::save($_FILES['img'], $filename, 'upload/actions'));
				
				$news = Model::factory('actions')
                    ->add($_POST['title'], $_POST['content'], $filename, $_POST['link'], $_POST['status']);
            }else{
                $this->content->error = 'Ошибка загрузки файла на сервер.';
            }
        }
            
        $this->request->redirect('admin/editors/actions');
    }
    
    public function action_actions_edit($id)
    {
        $this->set_content('admin/editors/actions_edit');

        if (isset($_POST['content']))
        {
            $data = array(
                'title' => $_POST['title'],
                'description' => $_POST['content'],
                'link' => $_POST['link'],
                'status' => $_POST['status']
            );
            $uploaddir = '/home/tolich/public_html/upload/actions/';
       if(is_uploaded_file($_FILES['img']['tmp_name']))
	   {
	       if (file_exists($uploaddir . $_FILES['img']['name'])) unlink($uploaddir . $_FILES['img']['name']);
            
            if (move_uploaded_file($_FILES['img']['tmp_name'], $uploaddir . $_FILES['img']['name'])) 
            {
                $data['img'] = $_FILES['img']['name'];
                if($data['img'] == '') unset($data['img']);
            }
        }
        

            $news = Model::factory('actions')
                ->edit($id, $data);
           // Model_event::instance()->add('EV_NEWS_EDIT','редактируется новость <a href="/admin/editors/news_edit/'.$id.'"> Новость №'.$id.'</a>');

            $this->request->redirect('admin/editors/actions');
        }

        $news = Model::factory('actions')
            ->get($id);
        //print_r($news);
        $this->content->news = $news;
    }
    
    public function action_actions_delete($id)
    {
        $news = Model::factory('actions')
            ->delete($id);

        $this->request->redirect('admin/editors/actions');
    }
    
//-----------------------------------NEWS----------------------------------------
    public function action_news()
    {
        $this->set_content('admin/editors/news');

        $news = Model::factory('news')
            ->get_all(10, 0, 'DESC');

        $this->content->news = $news;
    }

    public function action_news_add()
    {
        if (isset($_POST['content']))
        {
            $news = Model::factory('news')
                ->add($_POST['content']);
            Model_event::instance()->add('EV_NEWS_ADD','новая новость <a href="/admin/editors/news_edit/'.$news[0].'"> Новость №'.$news[0].'</a>');
            if (isset($_POST['to_emails']) AND $_POST['to_emails'])
            {
                $news_content = 'На сайте <a href="http://www.vashzakaz.us/">VashZakaz.US</a> обновился раздел новостей:<hr>' .
                    (str_replace('|||', '', $_POST['content'])).'<hr>Вы получили это письмо, потому что зарегистрированы как клиент на сайте.';

                $clients = ORM::factory('user')->clients()->find_all();

                foreach($clients as $client)
                {
                    $email = $client->email;

                    Email::send($email, 'Новости сайта VashZakaz.US', $news_content, Kohana::config('email.from'), TRUE);
                }
            }
        }


        $this->request->redirect('admin/editors/news');
    }

     public function action_news_edit($id)
    {
        $this->set_content('admin/editors/news_edit');

        if (isset($_POST['content']))
        {
            $data = array(
//                'dt' => isset($date) ? $date : date('Y-m-d H:i:s'),
                'content' => $_POST['content'],
            );

            $news = Model::factory('news')
                ->edit($id, $data);
            Model_event::instance()->add('EV_NEWS_EDIT','редактируется новость <a href="/admin/editors/news_edit/'.$id.'"> Новость №'.$id.'</a>');
            if (isset($_POST['to_emails']) AND $_POST['to_emails'])
            {
                $news_content = 'На сайте <a href="http://www.vashzakaz.us/">VashZakaz.US</a> обновился раздел новостей:<hr>' .
                    $_POST['content'].'<hr>Вы получили это письмо, потому что зарегистрированы как клиент на сайте.';

                $clients = ORM::factory('user')->clients()->find_all();

                foreach($clients as $client)
                {
                    $email = $client->email;

                    Email::send($email, 'Новости сайта VashZakaz.US', $news_content, Kohana::config('email.from'), TRUE);
                }
            }

            $this->request->redirect('admin/editors/news');
        }

        $news = Model::factory('news')
            ->get($id);

        $this->content->news = $news;
    }

     public function action_news_delete($id)
    {
        $news = Model::factory('news')
            ->delete($id);

        $this->request->redirect('admin/editors/news');
    }

    //-----------------------------------------------FAQ------------------------------------------------

    public function action_faq()
    {
        $this->set_content('admin/editors/faq');

        $m_faq = Model_Faq::factory('faq');
        $faq_cats = $m_faq->get_cats();
        $faq = $m_faq->get_group_cat();

        $this->content->faq_cats = $faq_cats;
        $this->content->faq = $faq;
    }
     public function action_faq_add()
    {
        if (isset($_POST['answer']))
        {
            $faq = Model::factory('faq')
                ->add($_POST['cat'],$_POST['num'],$_POST['question'],$_POST['answer']);
            Model_event::instance()->add('EV_USEFUL_ADD','новая страница в разделе "Вопросы" <a href="/admin/editors/faq_edit/'.$_POST['cat'].'/'.$_POST['num'].'">'.$_POST['question'].'</a>');
        }

        $this->request->redirect('admin/editors/faq');
    }

     public function action_faq_edit($id,$num)
    {
        $this->set_content('admin/editors/faq_edit');
       // $cat_arr = explode('.',$id);
        //print_r($_POST);
        if (isset($_POST['answer']))
        {
            $cat_arr = explode('.',$id);
            $faq = Model::factory('faq')
                ->edit($id, $num, $_POST['cat'], $_POST['num'], $_POST['question'], $_POST['answer']);
            Model_event::instance()->add('EV_FAQ_EDIT','редактирование страницы в разделе "Вопросы" <a href="/admin/editors/faq_edit/'.$id.'/'.$num.'">'.$_POST['question'].'</a>');
        //die();

            $this->request->redirect('admin/editors/faq');
        }
        
        $faq = Model::factory('faq')
            ->get($id, $num);

        $this->content->faq = $faq;
    }

    public function action_cat_add()
    {
        if (isset($_POST['name']))
        {
            $faq = Model::factory('faq')
                ->add_cat($_POST['cat_id'], $_POST['name']);
            Model_event::instance()->add('EV_FAQ_CAT_EDIT','новая категория в разделе "Вопросы" <a href="/admin/editors/cat_edit/'.$_POST['cat_id'].'">'.$_POST['name'].'</a>');
        }
        $this->request->redirect('admin/editors/faq');
    }

    public function action_cat_edit($id)
    {
        $this->set_content('admin/editors/cat_edit');
       // $cat_arr = explode('.',$id);
        if (isset($_POST['name']))
        {
            $faq = Model::factory('faq')
                ->edit_cat($id, $_POST['id'], $_POST['name']);
            Model_event::instance()->add('EV_FAQ_CAT_EDIT','редактирование категории в разделе "Вопросы" <a href="/admin/editors/cat_edit/'.$id.'">'.$_POST['name'].'</a>');

            $this->request->redirect('admin/editors/faq');
        }

        $cat = Model::factory('faq')
            ->get_cat($id);

        $this->content->title = 'Название раздела';
        $this->content->page = 'cat_edit';
        $this->content->cat = $cat;
        $this->content->id = $id;
    }

    public function action_faq_delete($id,$num)
    {
        $del_faq = Model::factory('faq')
            ->get($id, $num);
        $faq = Model::factory('faq')
            ->del($id,$num);
        Model_event::instance()->add('EV_FAQ_DELETE','удаление страницы в разделе "Вопросы" <span style="color:red">'.$faq['question'].'</span>');
        $this->request->redirect('admin/editors/faq');
    }

    public function action_cat_delete($id)
    {
        $del_cat = Model::factory('faq')
            ->get_cat($id);
        Model::factory('faq')
            ->del_cat($id);
        Model_event::instance()->add('EV_FAQ_CAT_EDIT','удаление категории в разделе "Вопросы" <span style="color:red">'.$del_cat['name'].'</span>');
        $this->request->redirect('admin/editors/faq');
    }

    //---------------------------------------------USEFUL-------------------------------------------
    public function action_useful()
    {
        $this->set_content('admin/editors/useful');

        $m_useful = Model_Useful::factory('useful');

        $useful_cats = $m_useful->get_cats();
        

        $this->content->useful_cats = $useful_cats;
    }
    
    public function action_useful_add()
    {
        if (isset($_POST['title']))
        {
            $useful = Model::factory('useful')
                ->add($_POST['title'], $_POST['short'],$_POST['content'], $_POST['cat']);
            Model_event::instance()->add('EV_USEFUL_ADD','новая страница в "Полезное" <a href="/admin/editors/useful_edit/'.$useful[0].'">'.$_POST['title'].'</a>');
        }
        $this->request->redirect('admin/editors/useful');
    }

    public function action_useful_category_add()
    {
        if (isset($_POST['add_category']))
        {
            $useful = Model::factory('useful')
                ->add_category($_POST['add_category']);
            Model_event::instance()->add('EV_USEFUL_CATS_ADD','новая категория в "Полезное" <a href="/admin/editors/useful_cat_edit/'.$useful[0].'">'.$_POST['add_category'].'</a>');
        }
        $this->request->redirect('admin/editors/useful');
    }

    public function action_useful_edit($id)
    {
        $this->set_content('admin/editors/useful_edit');
       // $cat_arr = explode('.',$id);
        if (isset($_POST['title']))
        {
            $cat_arr = explode('.',$id);
            $faq = Model::factory('useful')
                ->edit_useful($id, $_POST['title'],$_POST['short'],$_POST['content'],$_POST['cat']);
            Model_event::instance()->add('EV_USEFUL_EDIT','редактирование страницы в "Полезное" <a href="/admin/editors/useful_edit/'.$id.'">'.$_POST['title'].'</a>');

            $this->request->redirect('admin/editors/useful');
        }

        $m_useful = Model_Useful::factory('useful');

        $useful = $m_useful->get_by_id($id);
        $useful_cats = $m_useful->get_cats();
        $this->content->useful_cats = $useful_cats;
        $this->content->useful = $useful;
    }

    public function action_useful_cat_edit($id)
    {
        $this->set_content('admin/editors/cat_edit');
       // $cat_arr = explode('.',$id);
        if (isset($_POST['name']))
        {
            $cat_arr = explode('.',$id);
            $faq = Model::factory('useful')
                ->edit_cat($id,$_POST['id'],$_POST['name']);
            Model_event::instance()->add('EV_USEFUL_CATS_EDIT','редактирование категории в "Полезное" <a href="/admin/editors/useful_cat_edit/'.$id.'">'.$_POST['name'].'</a>');

            $this->request->redirect('admin/editors/useful');
        }

        $cat = Model::factory('useful')
            ->get_cat($id);
        $this->content->title = 'Название категории';
        $this->content->page = 'useful_cat_edit';
        $this->content->cat = $cat;
        $this->content->id = $id;
    }
    
    public function action_useful_cat_delete($id)
    {
        $del_cat = Model::factory('useful')
            ->get_cat($id);
        $news = Model::factory('useful')
            ->del_useful_cat($id);
        Model_event::instance()->add('EV_USEFUL_CATS_DEL','удаление категории в "Полезное" <span style="color:red">'.$del_cat['name'].'</span>');
        $this->request->redirect('admin/editors/useful');
    }

    public function action_useful_delete($id)
    {
        $m_useful = Model_Useful::factory('useful');

        $useful = $m_useful->get_by_id($id);
        $news = Model::factory('useful')
            ->del_useful($id);
        Model_event::instance()->add('EV_USEFUL_DEL','удаление страницы в "Полезное" <span style="color:red">'.$useful['title'].'</span>');
        $this->request->redirect('admin/editors/useful');
    }
//--------------------------------------------ORDERFORM-----------------------------------------
    public function action_orderform()
    {
    	$this->set_content('admin/editors/orderform');
        
        if(isset($_POST['save'])){
            file_put_contents(Kohana::config('ini.path').'zakaz.ini', $_POST['content']);
        }
        if(isset($_POST['preview'])){
            file_put_contents(Kohana::config('ini.path').'zakaz_tmp.ini', $_POST['content']);
            $form = Builderform::instance('zakaz_tmp');
            $tmp_ini = file_get_contents(Kohana::config('ini.path').'zakaz_tmp.ini');
        } else{
            $form = Builderform::instance('zakaz');
            $tmp_ini = file_get_contents(Kohana::config('ini.path').'zakaz.ini');
        }
        $current_ini = file_get_contents(Kohana::config('ini.path').'zakaz.ini');
        $first_ini = file_get_contents(Kohana::config('ini.path').'zakaz_backup.ini');
        $this->content->form = $form;
        $this->content->tmp_ini = $tmp_ini;
        $this->content->current_ini = $current_ini;
        $this->content->first_ini = $first_ini;
        $this->content->page = 'orderform';
    }
    //--------------------------------------------POSTFORM-----------------------------------------
    public function action_posilkaform()
    {
    	$this->set_content('admin/editors/orderform');

        if(isset($_POST['save'])){
            file_put_contents(Kohana::config('ini.path').'posilka.ini', $_POST['content']);
            Model_event::instance()->add('EV_SHIPPING_EDIT',' изменил <a href="/admin/editors/posilkaform">Форму посылки</a>');
        }
        if(isset($_POST['preview'])){
            file_put_contents(Kohana::config('ini.path').'posilka_tmp.ini', $_POST['content']);
            $form = Builderform::instance('posilka_tmp');
            $tmp_ini = file_get_contents(Kohana::config('ini.path').'posilka_tmp.ini');
        } else{
            $form = Builderform::instance('posilka');
            $tmp_ini = file_get_contents(Kohana::config('ini.path').'posilka.ini');
        }
        $current_ini = file_get_contents(Kohana::config('ini.path').'posilka.ini');
        $first_ini = file_get_contents(Kohana::config('ini.path').'posilka_backup.ini');
        $this->content->form = $form;
        $this->content->tmp_ini = $tmp_ini;
        $this->content->current_ini = $current_ini;
        $this->content->first_ini = $first_ini;
        $this->content->page = 'posilkaform';

    }
//--------------------------------------------PAGE ACTIONS----------------------------------------
    public function action_page($page)
    {
    	$this->set_content('admin/editors/page');
        $page_title = $page;
        $page = ORM::factory('page')
                ->where('name', '=', $page)
                ->order_by('order')
                ->find();
        $cats = ORM::factory('page')
                ->order_by('order')
                ->find_all();
        $subpages = $page->contents
            ->order_by('order')
            ->find_all();
        $this->content->cats = $cats;
        $this->content->page = $page_title;
        $this->content->category = $page;
        $this->content->sub_pages = $subpages;
    }

    public function action_add_subpage($page)
    {
        $sub_page = ORM::factory('page_content');
        $sub_page->title = $_POST['new_subitem_title'];
        $sub_page->page_id = $_POST['page_id'];
        $sub_page->order = $_POST['new_subitem_weight'];
        $sub_page->name = $_POST['new_subitem_chpu'];
        $sub_page->content = $_POST['new_subitem_content'];
        $sub_page->save();
        $this->request->redirect('admin/editors/page/'.$page);
    }

    public function action_save_subpages($page)
    {
        foreach($_POST as $k => $v){
            if (strstr($k,'item_')){
                $id = substr($k,5);
                if (isset($_POST['weight_'.$id]) && isset($_POST['cat_'.$id])  && isset($_POST['title_'.$id]) && isset($_POST['link_'.$id]) && isset($_POST['content_'.$id])){
                    $content = $_POST['content_'.$id];
                    if ($content==""){
                        $sub_page = ORM::factory('page_content',$id);
                        $sub_page->delete();
                    }else{
                        $sub_page = ORM::factory('page_content',$id);
                        $sub_page->title = $_POST['title_'.$id];
                        $sub_page->page_id = $_POST['cat_'.$id];
                        $sub_page->order = $_POST['weight_'.$id];
                        $sub_page->name = $_POST['link_'.$id];
                        $sub_page->content = $_POST['content_'.$id];
                        $sub_page->save();

                    }
                }
            }
        }
        $this->request->redirect('admin/editors/page/'.$page);
        
    }
    public function action_save_page($page)
    {
        $page_id = $page;
        $page = ORM::factory('page',$_POST['edit_cat_id']);
        $page->name = $_POST['edit_cat_chpu'];
        $page->title = $_POST['edit_cat_title'];
        $page->order = $_POST['edit_cat_weight'];
        $page->save();
        $this->request->redirect('admin/editors/page/'.$page_id);
    }

    public function action_add_page()
    {
        $page = ORM::factory('page');
        $page->name = $_POST['new_cat'];
        $page->title = $_POST['new_cat'];
        $page->order = 0;
        $page->save();
        $this->request->redirect('admin/editors/page/'.$_POST['new_cat']);
    }

    public function action_delete_page($page)
    {
        $page_id = $page;
        $page = ORM::factory('page')
                ->where('name', '=',$page_id)
                ->find();
        $sub_page = ORM::factory('page_content')
                    ->where('page_id', '=',$page->id)
                    ->delete_all();
        $page = ORM::factory('page')
                ->delete($page->id);
        //$page = $page->delete();
        $this->request->redirect('admin/editors/page/'.$page_id);
    }
    

//--------------------------------------------OTHER METHODS----------------------------------------
    public function edit_page($page, $name = NULL)
    {
    	$this->set_content('admin/editors/index');

    	if ($name === NULL)
    		$name = $page;
    	
        if (isset($_POST['text']))
        {
            file_put_contents(APPPATH.'views/edit/'. $page .EXT, $_POST['text']);
            file_put_contents(APPPATH.'views/static/'. $page .EXT, $_POST['text']);
        }

        $text = View::factory('edit/'.$page);

        $this->content->name = $name;
        $this->content->text = $text;
    }

    
    
    public function action_schedule()
    {
        $this->edit_page('schedule', 'Расписание');
        if (isset($_POST['text'])){
            Model_event::instance()->add('EV_SHEDULLE_EDIT',' изменил <a href="/admin/editors/schedule">Расписание</a>');
        }
    }
    
    public function action_services()
    {
        $this->edit_page('services', 'Об услугах');
        if (isset($_POST['text'])){
            Model_event::instance()->add('EV_SHEDULLE_EDIT',' изменил <a href="/admin/editors/services">Об услугах</a>');
        }
    }
    
    public function action_contacts()
    {
    	$this->edit_page('contacts', 'Контакты');
        if (isset($_POST['text'])){
            Model_event::instance()->add('EV_CONTACTS_EDIT',' изменил <a href="/admin/editors/contacts">Контакты</a>');
        }
    }
    
    
    public function action_about()
    {
    	$this->edit_page('about', 'О нас');
        if (isset($_POST['text'])){
            Model_event::instance()->add('EV_ABOUT_EDIT',' изменил страницу <a href="/admin/editors/about">"О нас"</a>');
        }
    }
    
    public function action_agreements()
    {
    	$this->edit_page('agreements', 'Соглашение');
       
    }

} // Admin Editor