<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Adminfiles extends Controller_Admin_Abstract {

//    public $content = 'admin/clients';
    
    protected $_model_orders = 'shops_shops';
    
    public function action_index()
    {
        $clients = ORM::factory('shops_shops');
        
        $id = $this->request->param('id');
        $operator = $this->request->param('operator');
        
        if(isset($id) AND isset($operator))
        {
            $filename = Kohana::config('main.shop.'.$operator).'/'.$id.'.xls';
            if (file_exists($filename))
            {
                $order = ORM::factory('shops_shops')->find($id);
                Model_event::instance()->add('EV_ORDERS_MAKE_CLOSED',' качает файл интернет магазина <a href="/admin/adminfiles/'.$operator.'/'.$order->id.'">'.$order->shop.'</b> файл №'.$order->id.'</a>');
                //$this->set_content('user/report_not_find');
                Request::instance()->send_file($filename, $operator.' - Интернет магазин #'.$id.' от '.date('d.m.Y H:i:s').'.xls');
                
                
                //Request::instance()->send_file($filename, $id.'.xls');
            }
        }
        else 
        {
            $this->items($clients);
        }

        
        
    }

    public function action_new()
    {
        $clients = ORM::factory('user')
            ->clients_data()
            ->where('user_data.registered', '>=', date('Y:m:d H:i:s', time() - Date::MONTH));

        $this->items($clients);
    }

    public function action_delete($id)
    {
        $client = ORM::factory('user')->find($id);
        $data_changes = $client->user_data_changes;
        $client->delete();
        $data_changes->delete();
        $this->request->redirect('admin/clients');
    }

    public function items(ORM &$clients)
    {
        $this->add_js('jquery.quicksearch');
        $this->add_js('tablesearch');

        $this->set_content('admin/shops/shops');

        $xls_array_yaroslav = scandir(Kohana::config('main.shop.yaroslav'));
        $xls_array_ulyana = scandir(Kohana::config('main.shop.ulyana'));
        $xls_array_alena = scandir(Kohana::config('main.shop.alena'));
        $xls_array_oksana = scandir(Kohana::config('main.shop.oksana'));
        $xls_array_natalya = scandir(Kohana::config('main.shop.natalya'));
        
        //пути к файлам по опереторам
        $path_yaroslav = 'adminfiles/yaroslav';
        $path_ulyana = 'adminfiles/ulyana';
        $path_alena = 'adminfiles/alena';
        $path_oksana = 'adminfiles/oksana';
        $path_natalya = 'adminfiles/natalya';
        
        $time_new = time();
        
        //циклы выбора всег файлов из разных папок согласно оператору
        $xls_yaroslav = array();
        for ($i = 0; $i < count($xls_array_yaroslav); $i++)
        {
            if ($xls_array_yaroslav[$i] != '.' AND $xls_array_yaroslav[$i] != '..')
            {
                $xls_array_yaroslav[$i] = str_replace('.xls', '', $xls_array_yaroslav[$i]);
                $xls_yaroslav[$xls_array_yaroslav[$i]] = TRUE;
                
                $a = $path_yaroslav.'/'.$xls_array_yaroslav[$i].'.xls';
                $time_old = stat($a);
                $time = $time_new - $time_old['mtime'];
                $xls_yaroslav[$xls_array_yaroslav[$i]] = $this->sectime($time);
            }
           
        }
        unset($xls_array_yaroslav);
        
        $xls_ulyana = array();
        for ($i = 0; $i < count($xls_array_ulyana); $i++)
        {
            if ($xls_array_ulyana[$i] != '.' AND $xls_array_ulyana[$i] != '..')
            {
                $xls_array_ulyana[$i] = str_replace('.xls', '', $xls_array_ulyana[$i]);
                $xls_ulyana[$xls_array_ulyana[$i]] = TRUE;
                
                $a = $path_ulyana.'/'.$xls_array_ulyana[$i].'.xls';
                $time_old = stat($a);
                $time = $time_new - $time_old['mtime'];
                $xls_ulyana[$xls_array_ulyana[$i]] = $this->sectime($time);
            }
           
        }
        unset($xls_array_ulyana);
        
        $xls_alena = array();
        for ($i = 0; $i < count($xls_array_alena); $i++)
        {
            if ($xls_array_alena[$i] != '.' AND $xls_array_alena[$i] != '..')
            {
                $xls_array_alena[$i] = str_replace('.xls', '', $xls_array_alena[$i]);
                $xls_alena[$xls_array_alena[$i]] = TRUE;
                
                $a = $path_alena.'/'.$xls_array_alena[$i].'.xls';
                $time_old = stat($a);
                $time = $time_new - $time_old['mtime'];
                $xls_alena[$xls_array_alena[$i]] = $this->sectime($time);
            }
           
        }
        unset($xls_array_alena);
        
		// oksana
		$xls_oksana = array();
        for ($i = 0; $i < count($xls_array_oksana); $i++)
        {
            if ($xls_array_oksana[$i] != '.' AND $xls_array_oksana[$i] != '..')
            {
                $xls_array_oksana[$i] = str_replace('.xls', '', $xls_array_oksana[$i]);
                $xls_oksana[$xls_array_oksana[$i]] = TRUE;
                
                $a = $path_oksana.'/'.$xls_array_oksana[$i].'.xls';
                $time_old = stat($a);
                $time = $time_new - $time_old['mtime'];
                $xls_oksana[$xls_array_oksana[$i]] = $this->sectime($time);
            }
           
        }
        unset($xls_array_oksana);
		
		// natalya
		$xls_natalya = array();
        for ($i = 0; $i < count($xls_array_natalya); $i++)
        {
            if ($xls_array_natalya[$i] != '.' AND $xls_array_natalya[$i] != '..')
            {
                $xls_array_natalya[$i] = str_replace('.xls', '', $xls_array_natalya[$i]);
                $xls_natalya[$xls_array_natalya[$i]] = TRUE;
                
                $a = $path_natalya.'/'.$xls_array_natalya[$i].'.xls';
                $time_old = stat($a);
                $time = $time_new - $time_old['mtime'];
                $xls_natalya[$xls_array_natalya[$i]] = $this->sectime($time);
            }
           
        }
        unset($xls_array_natalya);
        
     //   $form_client_find = Formo::get('client_find');

        

        

//        if (isset($_GET['name'])/*$form_client_find->sent()*/)
//        {
//            $clients = $clients->clients_data()
//                ->where('users.id', '=', $_GET['name'])
//                ->or_where('user_data.name', 'LIKE', '%'.$_GET['name']/*$form_client_find->name->val()*/.'%');
//        }

        $form_client_find = Formo::get('client_find');
        
        if (isset($_GET['searchbyadminfiles'])/*$form_client_find->sent()*/)
        {

           $search_by_adminfiles =  $_GET['searchbyadminfiles']; 
           
            $clients = ORM::factory('shops_shops')
                ->where('id', '=', $search_by_adminfiles)
                ->or_where('shop', 'LIKE', $search_by_adminfiles.'%')
                ->find_all();

            
                //->or_where('user_data.name', 'LIKE', '%'.$_GET['name']/*$form_client_find->name->val()*/.'%');
        }
 else {
        $count_clients = clone $clients;

       $count_clients = $count_clients->count_all();
        
        $pagination = Pagination::factory(array(
            'total_items' => $count_clients,
            'items_per_page' => 30,
        ));
        
            $clients = ORM::factory('shops_shops')
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('important', 'DESC')
            ->order_by('id', 'asc')
            ->find_all();
 }
       
        
        


        
        
        
        $this->content->clients = $clients;
        $this->content->xls_yaroslav = $xls_yaroslav;
        $this->content->xls_ulyana = $xls_ulyana;
        $this->content->xls_alena = $xls_alena;
        $this->content->xls_oksana = $xls_oksana;
        $this->content->xls_natalya = $xls_natalya;
        $this->content->pagination = $pagination;
    }

    public function action_changes()
    {
        $this->set_content('admin/clients/changes');
        $clients = ORM::factory('user')
            ->clients_data_changes()
            ->join('roles_users')
            ->on('users.id', '=', 'roles_users.user_id')
            ->join('roles')
            ->on('roles_users.role_id', '=', 'roles.id')
            ->select('user_data_changes.*')
            ->join('user_data_changes', 'LEFT')
            ->on('user_data_changes.user_id', '=', 'users.id')
            ->where('approved', '=', 0)            
            ->find_all();
//        print_r($clients);
        $this->content->clients = $clients;
    }

    public function action_change($id)
    {
        $this->set_content('admin/clients/change');

        $client = ORM::factory('user')->find($id);

        if (! $client->pk())
        {
            $this->set_content('admin/clients/not_find');
        }

        $this->content->client = $client;
    }

    public function action_approve($id)
    {
        $client = ORM::factory('user', $id);

        $data = $client->user_data;
        $data_changes = $client->user_data_changes;
        
        foreach($data_changes->as_array() as $col => $val)
        {
            if (in_array($col, array('user_id', 'cause', 'approved','delete')))
            {
                continue;
            }
            if ($col == "email"){
                $m_email = Model_Email::instance();
                $m_email->change_email($client->email, $val);
                $client->email = $val;

                continue;
            }

            $data->$col = $val;
        }

        $data_changes->approved = 1;
        $data_changes->save();

        $data->save();
        $client->save();
        $this->request->redirect('admin/clients/changes');
    }

    public function action_changes_denied($id)
    {
        $client = ORM::factory('user', $id);

        $data = $client->user_data;
        $data_changes = $client->user_data_changes;
        if(isset($_POST['changes_denied'])){
            $data_changes->cause = $_POST['changes_denied'];
        }
        $data_changes->approved = 2;
        $data_changes->save();

        $this->request->redirect('admin/clients/changes');
    }

    public function action_find($name = NULL)
    {
        $form_client_find = Formo::get('client_find');

        $this->request->redirect('admin/clients/?name='.$form_client_find->name->val());
    }

    public function action_client($id)
    {
        $this->set_content('admin/clients/client');

        $client = ORM::factory('user')->find($id);
        if (! $client->pk())
        {
            $this->set_content('admin/clients/not_find');
            return FALSE;
        }
        $role = DB::select('*')
                ->from('roles_users')
                ->where('user_id', '=', $id)
                ->where('role_id', '=' ,'2')
                ->execute()->as_array();
        //print_r($role);
        //die();
        $this->content->role = $role;
        $user_changes = ORM::factory('user', $id)->user_data_changes;

//        if ($user_changes->user_id == $id AND $user_changes->approved == 0){
//
//            $user_data = $user_changes;
//        }
//        else{
//            $user_data = ORM::factory('user', $id)->user_data;
//        }
        $user_data = ORM::factory('user', $id)->user_data;
        $form = Formo::get('Changeinfo_User');
        //print_r($user_changes);
        if (! $form->submit->val()){
            $form->email->val($client->email);
            $form->username->val($client->username);
            $form->name->val($user_data->name);
            $form->country->val($user_data->country);
            $form->zip->val($user_data->zip);
            $form->region->val($user_data->region);
            $form->city->val($user_data->city);
            $form->address->val($user_data->address);
            $form->phones->val($user_data->phones);
            $form->icq->val($user_data->icq ? $user_data->icq : '');
            $form->skype->val($user_data->skype);
            $form->mail_ru->val($user_data->mail_ru);
            $form->another->val($user_data->another);
            $form->delete->val($user_changes->delete);
        }

        $this->content->form = $form;
        $this->content->form->admin_submit = "true";
        $this->content->form->admin_mode = 'true';
        if ($form->submit->val()){

            if (! $user_data->user_id){

                $user_data->user_id = $id;
            }
//            $form->email->val($client->email);
//            $form->username->val($client->username);
//            $form->name->val($user_data->name);
//            $form->country->val($user_data->country);
//            $form->zip->val($user_data->zip);
//            $form->region->val($user_data->region);
//            $form->city->val($user_data->city);
//            $form->address->val($user_data->address);
//            $form->phones->val($user_data->phones);
//            $form->icq->val($user_data->icq ? $user_data->icq : '');
//            $form->skype->val($user_data->skype);
//            $form->mail_ru->val($user_data->mail_ru);
//            $form->another->val($user_data->another);
//            $form->delete->val($user_changes->delete);
//            echo "111";
            $m_email = Model_Email::instance();
            $m_email->change_email($client->email, $form->email->val());
            $client->email  = $form->email->val();
            $user_data->name    = $form->name->val();
            $user_data->country = $form->country->val();
            $user_data->region  = $form->region->val();
            $user_data->city    = $form->city->val();
            $user_data->zip     = $form->zip->val();
            $user_data->address = $form->address->val();
            $user_data->phones  = $form->phones->val();
            $user_data->icq     = $form->icq->val();
            $user_data->skype   = $form->skype->val();
            $user_data->mail_ru = $form->mail_ru->val();
            $user_data->another = $form->another->val();

            $user_data->save();
            $client->save();
          
        }

        $this->content->client = $client;
    }
    
    public function sectime($secs){
        $output = '';
        if($secs >= 86400) {
            $days = floor($secs/86400);
            $secs = $secs%86400;
            $output = $days.'/';
            if($days != 1) $output .= '';
            if($secs > 0) $output .= '';
            }
        if($secs>=3600){
            $hours = floor($secs/3600);
            $secs = $secs%3600;
            $output .= $hours.'';
            if($hours != 1) $output .= '';
            if($secs > 0) $output .= ':';
            }
        if($secs>=60){
            $minutes = floor($secs/60);
            $secs = $secs%60;
            $output .= $minutes.'';
            if($minutes != 1) $output .= '';
            if($secs > 0) $output .= ':';
            }
        $output .= $secs.'';
        if($secs != 1) $output .= '';
        return $output;
    }
    
    public function action_open($id)
    {
        
       
        $this->order_processed($id, 1);
    }
    
    public function action_close($id)
    {
        
    
        $this->order_processed($id, 0);

    }

    private function order_processed($id, $close)
    {
        $order = ORM::factory('shops_shops')->find($id);
//            echo $close;
//        echo '<pre>';
//        print_r($order);
//        echo '</pre>';
      //  return;
        
        

           
           

            if ($close == 1){
                $order->important = $close;
                $order->save();
                    Model_event::instance()->add('EV_ORDERS_MAKE_CLOSED',' пометил как важное мазагин <b>'.$order->shop.'</b> файл №'.$order->id.'(<a href="/admin/adminfile/close/'.$order->id.'">Убрать из важных</a>)');
            } else{
                $order->important = $close;
                $order->save();
                
                Model_event::instance()->add('EV_ORDERS',' убрал из важных магазин <b>'.$order->shop.'</b> файл №'.$order->id.'(<a href="/admin/adminfile/open/'.$order->id.'">Пометить важным</a>)');
            }
        
        

        $this->request->redirect(Request::$referrer);
    }
    
    public function action_important()
    {
        
        $xls_array_yaroslav = scandir(Kohana::config('main.shop.yaroslav'));
        $xls_array_ulyana = scandir(Kohana::config('main.shop.ulyana'));
        $xls_array_alena = scandir(Kohana::config('main.shop.alena'));
        $xls_array_oksana = scandir(Kohana::config('main.shop.oksana'));
        $xls_array_natalya = scandir(Kohana::config('main.shop.natalya'));
        
        //пути к файлам по опереторам
        $path_yaroslav = 'adminfiles/yaroslav';
        $path_ulyana = 'adminfiles/ulyana';
        $path_alena = 'adminfiles/alena';
        $path_oksana = 'adminfiles/oksana';
        $path_natalya = 'adminfiles/natalya';
        
        $time_new = time();
        
        //циклы выбора всег файлов из разных папок согласно оператору
        $xls_yaroslav = array();
        for ($i = 0; $i < count($xls_array_yaroslav); $i++)
        {
            if ($xls_array_yaroslav[$i] != '.' AND $xls_array_yaroslav[$i] != '..')
            {
                $xls_array_yaroslav[$i] = str_replace('.xls', '', $xls_array_yaroslav[$i]);
                $xls_yaroslav[$xls_array_yaroslav[$i]] = TRUE;
                
                $a = $path_yaroslav.'/'.$xls_array_yaroslav[$i].'.xls';
                $time_old = stat($a);
                $time = $time_new - $time_old['mtime'];
                $xls_yaroslav[$xls_array_yaroslav[$i]] = $this->sectime($time);
            }
           
        }
        unset($xls_array_yaroslav);
        
        $xls_ulyana = array();
        for ($i = 0; $i < count($xls_array_ulyana); $i++)
        {
            if ($xls_array_ulyana[$i] != '.' AND $xls_array_ulyana[$i] != '..')
            {
                $xls_array_ulyana[$i] = str_replace('.xls', '', $xls_array_ulyana[$i]);
                $xls_ulyana[$xls_array_ulyana[$i]] = TRUE;
                
                $a = $path_ulyana.'/'.$xls_array_ulyana[$i].'.xls';
                $time_old = stat($a);
                $time = $time_new - $time_old['mtime'];
                $xls_ulyana[$xls_array_ulyana[$i]] = $this->sectime($time);
            }
           
        }
        unset($xls_array_ulyana);
        
        $xls_alena = array();
        for ($i = 0; $i < count($xls_array_alena); $i++)
        {
            if ($xls_array_alena[$i] != '.' AND $xls_array_alena[$i] != '..')
            {
                $xls_array_alena[$i] = str_replace('.xls', '', $xls_array_alena[$i]);
                $xls_alena[$xls_array_alena[$i]] = TRUE;
                
                $a = $path_alena.'/'.$xls_array_alena[$i].'.xls';
                $time_old = stat($a);
                $time = $time_new - $time_old['mtime'];
                $xls_alena[$xls_array_alena[$i]] = $this->sectime($time);
            }
           
        }
        unset($xls_array_alena);
		
		// oksana
		$xls_oksana = array();
        for ($i = 0; $i < count($xls_array_oksana); $i++)
        {
            if ($xls_array_oksana[$i] != '.' AND $xls_array_oksana[$i] != '..')
            {
                $xls_array_oksana[$i] = str_replace('.xls', '', $xls_array_oksana[$i]);
                $xls_oksana[$xls_array_oksana[$i]] = TRUE;
                
                $a = $path_oksana.'/'.$xls_array_oksana[$i].'.xls';
                $time_old = stat($a);
                $time = $time_new - $time_old['mtime'];
                $xls_oksana[$xls_array_oksana[$i]] = $this->sectime($time);
            }
           
        }
        unset($xls_array_oksana);
		
		// natalya
		$xls_natalya = array();
        for ($i = 0; $i < count($xls_array_natalya); $i++)
        {
            if ($xls_array_natalya[$i] != '.' AND $xls_array_natalya[$i] != '..')
            {
                $xls_array_natalya[$i] = str_replace('.xls', '', $xls_array_natalya[$i]);
                $xls_natalya[$xls_array_natalya[$i]] = TRUE;
                
                $a = $path_natalya.'/'.$xls_array_natalya[$i].'.xls';
                $time_old = stat($a);
                $time = $time_new - $time_old['mtime'];
                $xls_natalya[$xls_array_natalya[$i]] = $this->sectime($time);
            }
           
        }
        unset($xls_array_natalya);
        
        
        $adminfiles_important = ORM::factory('shops_shops')->where('important', '=', 1)->find_all();
        $this->content->clients = $adminfiles_important;
        $this->content->xls_yaroslav = $xls_yaroslav;
        $this->content->xls_ulyana = $xls_ulyana;
        $this->content->xls_alena = $xls_alena;
        $this->content->xls_oksana = $xls_oksana;
        $this->content->xls_natalya = $xls_natalya;
        $this->set_content('admin/shops/shops');
       // $this->items($adminfiles_important);
    }

} // End Admin Clients