<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Set extends Controller_Admin_Abstract {

    public function action_registration()
    {
        $this->set_content('admin/set/registration');

        $form = Formo::get('registration_allow');

        if (! $form->sent())
        {
            $allow = Settings::instance()->get('registration_allow') ? TRUE : FALSE;

            $form->allow->val($allow);
        }

        if ($form->sent() AND $form->validate())
        {
            Settings::instance()->set('registration_allow', $form->allow->val());
        }

        $this->content->form = $form;
    }

    public function action_admins()
    {
        $this->set_content('admin/set/admins');
        $admins = DB::SELECT('users.*')
                ->from('users')
                ->join('roles_users')
                ->on('users.id', '=', 'roles_users.user_id')
                ->join('roles')
                ->on('roles_users.role_id', '=', 'roles.id')
                ->where('roles.name', '=', 'admin')
                ->select('user_data.*')
                ->join('user_data', 'LEFT')
                ->on('user_data.user_id', '=', 'users.id')
                ->execute()->as_array();
        $this->content->admins = $admins;
    }

    public function action_set_admin($id)
    {
        //$this->set_content('admin/set/admins');
//        $user = ORM::factory('user',$id);
//        $user->add('roles', ORM::factory('role', array('name' => 'admin')));
//        $user->save();
        DB::delete('roles_users')
        ->where('user_id','=', $id)
        ->limit(3)
        ->execute();

        $user = ORM::factory('user',$id);
        $user->add('roles', ORM::factory('role', array('name' => 'admin')));
        $user->add('roles', ORM::factory('role', array('name'=>'client')));
        $user->add('roles', ORM::factory('role', array('name'=>'login')));
        $user->save();

//                ->set(array('role_id'=> '2'))
//                ->where('user_id','=', $id)
        $this->request->redirect(Request::$referrer);
    }

    public function action_unset_admin($id)
    {
        //$this->set_content('admin/set/admins');
        DB::delete('roles_users')
        ->where('user_id','=', $id)
        ->limit(3)
        ->execute();
        
        $user = ORM::factory('user',$id);
        $user->add('roles', ORM::factory('role', array('name'=>'client')));
        $user->add('roles', ORM::factory('role', array('name'=>'login')));
        $user->save();
        $this->request->redirect(Request::$referrer);
    }
    
    public function action_set_redactor($id)
    {
        //$this->set_content('admin/set/admins');
//        $user = ORM::factory('user',$id);
//        $user->add('roles', ORM::factory('role', array('name' => 'admin')));
//        $user->save();
        DB::delete('roles_users')
        ->where('user_id','=', $id)
        ->limit(3)
        ->execute();

        $user = ORM::factory('user',$id);
        //$user->add('roles', ORM::factory('role', array('name' => 'admin')));
        $user->add('roles', ORM::factory('role', array('name'=>'client')));
        $user->add('roles', ORM::factory('role', array('name'=>'login')));
        $user->add('roles', ORM::factory('role', array('name'=>'redactor')));
        $user->save();

//                ->set(array('role_id'=> '2'))
//                ->where('user_id','=', $id)
        $this->request->redirect(Request::$referrer);
    }

    public function action_unset_redactor($id)
    {
        //$this->set_content('admin/set/admins');
        DB::delete('roles_users')
        ->where('user_id','=', $id)
        ->limit(3)
        ->execute();
        
        $user = ORM::factory('user',$id);
        $user->add('roles', ORM::factory('role', array('name'=>'client')));
        $user->add('roles', ORM::factory('role', array('name'=>'login')));
        $user->save();
        $this->request->redirect(Request::$referrer);
    }

    public function action_report()
    {
        $this->set_content('admin/set/report');

        $form = Formo::get('report');

        if (! $form->sent())
        {
            $allow = Settings::instance()->get('report') ? TRUE : FALSE;

            $form->allow->val($allow);           
        }

        if ($form->sent() AND $form->validate())
        {
            Settings::instance()->set('report', $form->allow->val());
        /*
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/is_active_report.php', '<?php $show_report='.$form->allow->val().'?>');
         * 
         */
        }

        $this->content->form = $form;
    }

} // End Controller_Admin_Set