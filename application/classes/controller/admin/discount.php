<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Discount extends Controller_Admin_Abstract {

    public function before()
    {
        parent::before();
        $this->add_js('/media/lib/tiny_mce/tiny_mce');
        $this->add_js('tiny_mce_f');
    }

    public function action_index()
    {

        $this->content->content = ORM::factory('discount')->find_all();
        $this->set_content('admin/discount/index');
    }

    public function action_edit($id) {


        if($_POST) {
            $data = array(
                'title' => $_POST['title'],
                'description' => $_POST['description'],
                'addDate' => date("Y-m-d H:i:s"),
            );
            if (!empty($_FILES['img']['name'])) {
                $files = Validate::factory($_FILES);
                $files->rule('img', 'Upload::size', array('3M'));

                if ($files->check()) {
                    // $filename = $_FILES['img']['name'];
                    $filename = time() . '_' . $_FILES['img']['name'];
                    $filename = basename(Upload::save($_FILES['img'], $filename, 'upload/discount'));
                    $data['img'] = $filename;

                } else {
                    $this->content->error = 'Ошибка загрузки файла на сервер.';
                }
            }

            ORM::factory('discount', $id)
                ->values($data)
                ->save();
            $this->request->redirect('admin/discount');
        }
        $this->set_content('admin/discount/edit');
        // print_r(ORM::factory('discount', $id)->find_all());
        $this->content->content = ORM::factory('discount')->where('id', '=', $id)->find();

    }

    public function action_add() {

        if(isset($_POST) && !empty($_POST['title'])) {
            if (!empty($_FILES)) {
                $files = Validate::factory($_FILES);
                $files->rule('img', 'Upload::size', array('3M'));

                if ($files->check()) {
                    // $filename = $_FILES['img']['name'];
                    $filename = time() . '_' . $_FILES['img']['name'];
                    $filename = basename(Upload::save($_FILES['img'], $filename, 'upload/discount'));

                } else {
                    $this->content->error = 'Ошибка загрузки файла на сервер.';
                }
            }
            $data = array(
                'title' => $_POST['title'],
                'description' => $_POST['description'],
                'img' => $filename,
                'addDate' => date("Y-m-d H:i:s"),
            );
            ORM::factory('discount')
                ->values($data)
                ->save();
            $this->request->redirect('admin/discount');
        }
    }

    public function action_delete($id)
    {
        ORM::factory('discount', $id)
            ->delete();
        $this->request->redirect('admin/discount');
    }

    public function action_sender()
    {
        if($_POST) {
            $post = Validate::factory($_POST);
            $post->rule('name', 'not_empty');

            if ($post->check()) {
                ORM::factory('calculator_sender')
                    ->values($_POST)
                    ->save();

                $this->request->redirect('admin/onlinecalculator/sender');
            } else {
                $this->content->error = $post -> errors();
            }
        }
        $this->set_content('admin/onlinecalculator/sender');
        $country = ORM::factory('calculator_sender')->find_all();

        $this->content->country = $country;
    }

    public function action_senderdel($id)
    {
        ORM::factory('calculator_sender', $id)
            ->delete();
        $this->request->redirect('admin/onlinecalculator/sender');
    }

    public function action_resipient()
    {

        if($_POST) {
            $post = Validate::factory($_POST);
            $post->rule('name', 'not_empty');

            if ($post->check()) {
                ORM::factory('calculator_resipient')
                    ->values($_POST)
                    ->save();

                $this->request->redirect('admin/onlinecalculator/resipient');
            } else {
                $this->content->error = $post -> errors();
            }
        }
        $this->set_content('admin/onlinecalculator/resipient');
        $country = ORM::factory('calculator_resipient')->where('parent_id','=','0')->find_all();

        $query =  mysql_query("SELECT * FROM kh_calculatorresipient");
        while($row = mysql_fetch_assoc($query))
        {
            $dd[$row[id]] = $row;
        }
        $this->add_js('media/js/calculator.js', TRUE);
        $countrytree = $this->mapTree($dd);

        asort($countrytree['1']['childs']);

        // echo '<pre>';
        //     print_r($countrytree);
        // echo '</pre>';

        $this->content->country = $country;
        $this->content->countrytree = $countrytree;
    }
    public function action_resipientdel($id)
    {
        ORM::factory('calculator_resipient', $id)
            ->delete();
        $this->request->redirect('admin/onlinecalculator/resipient');
    }


    public function action_delivery()
    {
        $this->set_content('admin/onlinecalculator/delivery');
    }

    public function action_options($id)
    {
        if($_POST) {
            $post = Validate::factory($_POST);
            $post->rule('name', 'not_empty');
            $_POST['resipient_id'] = $id;
            if ($post->check()) {
                ORM::factory('calculator_option')
                    ->values($_POST)
                    ->save();

                $this->request->redirect('admin/onlinecalculator/options/'.$id);
            } else {
                $this->content->error = $post -> errors();
            }
        }
        $this->add_js('media/js/calculator.js', TRUE);
        $this->set_content('admin/onlinecalculator/options');
        $options = ORM::factory('calculator_option' )->where('resipient_id','=',$id)->find_all();
        $this->content->id = $id;
        $this->content->options = $options;
    }
    public function action_optiondel($id)
    {
        ORM::factory('calculator_option', $id)
            ->delete();
        print_r($_SERVER);
        $this->request->redirect($_SERVER['HTTP_REFERER']);
    }

    function mapTree($dataset) {
        $tree = array();
        foreach ($dataset as $id=>&$node)
        {
            if ($node['parent_id'] == 0)
            {
                $tree[$id] = &$node;
            }
            else
            {
                $dataset[$node['parent_id']]['childs'][$id] = &$node;
            }
        }
        // sort($tree);
        // echo '<pre>';
        //print_r($tree);
        //echo '</pre>';
        return $tree;
    }// вызываем функцию и передаем ей наш массив$data = mapTree($data);

    public function action_price($id){
        if($_POST)
        {
            $post = Validate::factory($_POST);
            $post->rule('heft', 'not_empty');
            //$post->rule('price', 'not_empty');
            if ($post->check()) {
                ORM::factory('calculator_price')
                    ->values($_POST)
                    ->save();
                $this->request->redirect('admin/onlinecalculator/price/'.$_POST['option_id']);
            }
        }
        $this->add_js('media/js/calculator.js', TRUE);
        $this->set_content('admin/onlinecalculator/price');

        $this->content->option = ORM::factory('calculator_option')->where('id','=',$id)->find();
        $this->content->price = ORM::factory('calculator_price')->where('option_id','=',$id)->order_by('heft', 'ASC')->find_all();
    }

    public function action_pricedel($id)
    {
        ORM::factory('calculator_price', $id)
            ->delete();
        print_r($_SERVER);
        $this->request->redirect($_SERVER['HTTP_REFERER']);
    }

} // End Admin Parcels