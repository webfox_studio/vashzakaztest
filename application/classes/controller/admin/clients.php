<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Clients extends Controller_Admin_Abstract {

//    public $content = 'admin/clients';

    public function action_index()
    {
        $clients = ORM::factory('user');

        $this->items($clients);
    }

    public function action_new()
    {
        $clients = ORM::factory('user')
            ->clients_data()
            ->where('user_data.registered', '>=', date('Y:m:d H:i:s', time() - Date::MONTH));

        $this->items($clients);
    }

    public function action_delete($id)
    {
        $client = ORM::factory('user')->find($id);
        $data_changes = $client->user_data_changes;
        $client->delete();
        $data_changes->delete();
        $this->request->redirect('admin/clients');
    }

    public function items(ORM &$clients)
    {
        $this->add_js('jquery.quicksearch');
        $this->add_js('tablesearch');

        $this->set_content('admin/clients');

        $xls_array = scandir(Kohana::config('main.dir.reports'));
        $xls = array();

        for ($i = 0; $i < count($xls_array); $i++)
        {
            if ($xls_array[$i] != '.' AND $xls_array[$i] != '..')
            {
                $xls_array[$i] = str_replace('.xls', '', $xls_array[$i]);
                $xls[$xls_array[$i]] = TRUE;
            }
        }
        unset($xls_array);


        $form_client_find = Formo::get('client_find');

        if (isset($_GET['name'])/*$form_client_find->sent()*/)
        {
            $clients = $clients->clients_data()
                ->where('users.id', '=', $_GET['name'])
                ->or_where('user_data.name', 'LIKE', '%'.$_GET['name']/*$form_client_find->name->val()*/.'%');
        }

        $count_clients = clone $clients;

        $count_clients = $count_clients->count_all();

        $pagination = Pagination::factory(array(
            'total_items' => $count_clients,
            'items_per_page' => 50,
        ));

//        if (isset($_GET['name'])/*$form_client_find->sent()*/)
//        {
//            $clients = $clients->clients_data()
//                ->where('users.id', '=', $_GET['name'])
//                ->or_where('user_data.name', 'LIKE', '%'.$_GET['name']/*$form_client_find->name->val()*/.'%');
//        }

        $clients = $clients->clients_data()
            ->limit($pagination->items_per_page)
            ->offset($pagination->offset)
            ->order_by('id', 'DESC')
            ->find_all();

        $this->content->clients = $clients;
        $this->content->xls = $xls;
        $this->content->pagination = $pagination;
    }

    public function action_changes()
    {
        $this->set_content('admin/clients/changes');
        $clients = ORM::factory('user')
//            ->clients_data_changes()
            ->join('roles_users')
            ->on('users.id', '=', 'roles_users.user_id')
            ->join('roles')
            ->on('roles_users.role_id', '=', 'roles.id')
            ->select('user_data_changes.*')
            ->join('user_data_changes', 'LEFT')
            ->on('user_data_changes.user_id', '=', 'users.id')
            ->where('approved', '=', 0)            
            ->find_all();
//        print_r($clients);
        $this->content->clients = $clients;
    }

    public function action_change($id)
    {
        $this->set_content('admin/clients/change');

        $client = ORM::factory('user')->find($id);

        if (! $client->pk())
        {
            $this->set_content('admin/clients/not_find');
        }

        $this->content->client = $client;
    }

    public function action_approve($id)
    {
        $client = ORM::factory('user', $id);

        $data = $client->user_data;
        $data_changes = $client->user_data_changes;
        
        foreach($data_changes->as_array() as $col => $val)
        {
            if (in_array($col, array('user_id', 'cause', 'approved','delete')))
            {
                continue;
            }
            if ($col == "email"){
                $m_email = Model_Email::instance();
                $m_email->change_email($client->email, $val);
                $client->email = $val;

                continue;
            }

            $data->$col = $val;
        }

        $data_changes->approved = 1;
        $data_changes->save();

        $data->save();
        $client->save();
        $this->request->redirect('admin/clients/changes');
    }

    public function action_changes_denied($id)
    {
        $client = ORM::factory('user', $id);

        $data = $client->user_data;
        $data_changes = $client->user_data_changes;
        if(isset($_POST['changes_denied'])){
            $data_changes->cause = $_POST['changes_denied'];
        }
        $data_changes->approved = 2;
        $data_changes->save();

        $this->request->redirect('admin/clients/changes');
    }

    public function action_find($name = NULL)
    {
        $form_client_find = Formo::get('client_find');

        $this->request->redirect('admin/clients/?name='.$form_client_find->name->val());
    }

    public function action_client($id)
    {
        $this->set_content('admin/clients/client');

        $client = ORM::factory('user')->find($id);
        if (! $client->pk())
        {
            $this->set_content('admin/clients/not_find');
            return FALSE;
        }
        $role = DB::select('*')
                ->from('roles_users')
                ->where('user_id', '=', $id)
                ->where('role_id', '=' ,'2')
                ->execute()->as_array();
        //print_r($role);
        //die();
        $this->content->role = $role;
        $user_changes = ORM::factory('user', $id)->user_data_changes;

//        if ($user_changes->user_id == $id AND $user_changes->approved == 0){
//
//            $user_data = $user_changes;
//        }
//        else{
//            $user_data = ORM::factory('user', $id)->user_data;
//        }
        $user_data = ORM::factory('user', $id)->user_data;
        $form = Formo::get('Changeinfo_User');
        //print_r($user_changes);
        if (! $form->submit->val()){
            $form->email->val($client->email);
            $form->username->val($client->username);
            $form->name->val($user_data->name);
            $form->country->val($user_data->country);
            $form->zip->val($user_data->zip);
            $form->region->val($user_data->region);
            $form->city->val($user_data->city);
            $form->address->val($user_data->address);
            $form->phones->val($user_data->phones);
            $form->icq->val($user_data->icq ? $user_data->icq : '');
            $form->skype->val($user_data->skype);
            $form->mail_ru->val($user_data->mail_ru);
            $form->another->val($user_data->another);
            $form->delete->val($user_changes->delete);
        }

        $this->content->form = $form;
        $this->content->form->admin_submit = "true";
        $this->content->form->admin_mode = 'true';
        if ($form->submit->val()){

            if (! $user_data->user_id){

                $user_data->user_id = $id;
            }
//            $form->email->val($client->email);
//            $form->username->val($client->username);
//            $form->name->val($user_data->name);
//            $form->country->val($user_data->country);
//            $form->zip->val($user_data->zip);
//            $form->region->val($user_data->region);
//            $form->city->val($user_data->city);
//            $form->address->val($user_data->address);
//            $form->phones->val($user_data->phones);
//            $form->icq->val($user_data->icq ? $user_data->icq : '');
//            $form->skype->val($user_data->skype);
//            $form->mail_ru->val($user_data->mail_ru);
//            $form->another->val($user_data->another);
//            $form->delete->val($user_changes->delete);
//            echo "111";
            $m_email = Model_Email::instance();
            $m_email->change_email($client->email, $form->email->val());
            $client->email  = $form->email->val();
            $user_data->name    = $form->name->val();
            $user_data->country = $form->country->val();
            $user_data->region  = $form->region->val();
            $user_data->city    = $form->city->val();
            $user_data->zip     = $form->zip->val();
            $user_data->address = $form->address->val();
            $user_data->phones  = $form->phones->val();
            $user_data->icq     = $form->icq->val();
            $user_data->skype   = $form->skype->val();
            $user_data->mail_ru = $form->mail_ru->val();
            $user_data->another = $form->another->val();

            $user_data->save();
            $client->save();
          
        }

        // выбор категорий на которые подписан пользователь
        $m_subscription = Model_Subscription::factory('subscription');
        $active_cats = $m_subscription->client_active_cats($user_data->user_id);
        $cats = $m_subscription->get_cats_client();
        $this->content->active_cats = $active_cats;
        $this->content->cats = $cats;
	
		$xls_array = scandir(Kohana::config('main.dir.reports'));
		$xls = array();
		$xlsFile = '';
		for ($i = 0; $i < count($xls_array); $i++)
		{
			if ($xls_array[$i] != '.' AND $xls_array[$i] != '..')
			{
				// echo $xls_array[$i].'<br>';
				if($xls_array[$i] == $id.'.xls') {
					$xlsFile = true; break;
				}
			}
		}

		if($xlsFile) {
			$xlsFileUrl = '<a href="/user/report/'.$id.'">Скачать</a>';
		}
		else {
			$xlsFileUrl = '-';
		}
		$this->content->xlsUrl = $xlsFileUrl;
        $this->content->client = $client;
    }

} // End Admin Clients