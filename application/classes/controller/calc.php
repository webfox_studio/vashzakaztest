<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Calc extends Controller {

    public function action_index()
    {
        $this->request->response = View::factory('calc/calc');
    }

} // End Controller_Calc