<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Onlinecalculator extends Controller_Abstract
{

    public $content = 'onlinecalculator/mainTestNew';

    public function before()
    {
        parent::before();

        $this->content->services = @file_get_contents(Kohana::config('main.services'));

    }

    public function action_index($all = FALSE)
    {



        //$this->add_js('media/lib/fancybox/jquery.fancybox-1.3.4.pack', TRUE);
        //$this->add_css('media/lib/fancybox/jquery.fancybox-1.3.4', TRUE);
        $this->add_js('media/js/calculator.js', TRUE);
        $this->add_css('media/css/onlinecalculator.css', TRUE);
        $this->add_css('media/css/font-awesome.min.css', TRUE);
        $this->content->sender = ORM::factory('calculator_sender')->find_all();
        $this->content->resipient = ORM::factory('calculator_resipient')->where('parent_id', '=', '0')->find_all();
        $this->content->staticText = Kohana::config('onlinecalculator');
        $this->content->option = ORM::factory('calculator_option')->find_all();
    }

    public function action_main($all = FALSE)
    {
        //$this->add_js('media/lib/fancybox/jquery.fancybox-1.3.4.pack', TRUE);
        //$this->add_css('media/lib/fancybox/jquery.fancybox-1.3.4', TRUE);
//

        //$this->content->sender = ORM::factory('calculator_sender')->find_all();
        //$this->content->resipient = ORM::factory('calculator_resipient')->where('parent_id','=','0') ->find_all();
        // $this->content->option = ORM::factory('calculator_option')->find_all();


        $this->set_content('onlinecalculator/index');
    }

    public function action_test()
    {
        ini_set("soap.wsdl_cache_enabled", "0");
        $clientS = new SoapClient("http://api.boxberry.de/__soap/1c_public.php?wsdl",
            array('features' => SOAP_SINGLE_ELEMENT_ARRAYS));

        $s=array();
        $s['token']='34049.rrpqbead';

        try {
            // Позволяет получить список городов, в которых есть пункты выдачи.
            // $listCities = $clientS->ListCities($s);
            // $listZips = $clientS->ListZips($s);
            // $s['code']=0;
            // $s['prepaid']=0;
            // $listPoints = $clientS->ListPoints($s);

        }
        catch (SoapFault $exception){
            $errS=$exception;
        }

        if(isset($errS)){
            echo $errS;
        } // если произошла ошибка и ответ не был получен.
        else
        {
            // $this->content->listCities = $listCities->result;
            // $this->content->listPoints = $listPoints->result;
            // $this->content->listZips = $listZips->result;
            // все отлично, ответ получен, теперь в массиве $data.


        }
        $this->set_content('onlinecalculator/test');
    }


} // End Nmain