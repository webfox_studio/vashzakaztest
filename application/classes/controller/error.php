<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Error extends Controller_Template {

    public $template = 'error/404';

    public function action_404() {}

} // End Error