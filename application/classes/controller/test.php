<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test extends Controller_Abstract {

    public function action_index()
    {
    }
	
	public function action_test_email()
	{
		$email = Email::factory('Hello, World', 'This is my body, it is nice.')
			->to("andrey_by@list.ru")
			->from('vashzakaz.news@gmail.com', 'VashZakaz')
			->send()
			;

                $this->request->response = "Email sent to 'andrey_by@list.ru'";
	}
	
	public function action_clientemails()
	{
		$clients = ORM::factory('user')->clients()->find_all();

		$email_str = '';
		
		foreach($clients as $client)
		{
			$email = $client->email;
			
			$email_str .= $email.',';
		}
		
		$email_str = rtrim($email_str, ',');
		
		$this->request->response = $email_str;
		
		//Email::send($email_str, 'VashZakaz Test', 'Test email', FALSE, TRUE);
	}
	
	public function action_email()
	{
		$email = $_GET['email'];
	
		$is_success = Email::send($email, 'VashZakaz Test', 'Test email', FALSE, TRUE);
		
		$this->request->response = 'Email: ' . $email . ', Test email ' . ($is_success ? 'success' : 'fail');
	}

	public function action_discount() {
        $this->set_content('test/discount');
        $content = ORM::factory('discount')->order_by('addDate', 'DESC')->find_all();
        $this->content->content = $content;
    }
}	