<?php defined('SYSPATH') or die('No direct script access.');

class Bform {

    protected static $_instances = array();

    protected $_name;
    protected $_form;
    protected $_data;
    protected $_filename;

    const EXT = '.ini';
    
    public static function instance($name = NULL)
    {
        if (! $name)
            return new Bform();

        if (! isset(Bform::$_instances[$name]))
            Bform::$_instances[$name] = new Bform($name);

        return Bform::$_instances[$name];
    }

    public function __construct($name = NULL)
    {
        if ($name)
        {
            $this->_name = $name;
            $this->load_ini($name);
            $this->init();
        }
    }

    protected function load_ini($filename)
    {
        if (! strstr($filename, Bform::EXT))
            $filename .= Bform::EXT;

        $filename = Kohana::config('ini.path') . $filename;

        if ($this->_data = parse_ini_file($filename, TRUE))
            return TRUE;

        return FALSE;
    }

    protected function init()
    {
        $this->_form = Formo::form($this->_name);

        foreach($this->_data as $name => $section)
        {
            $this->add_section($name, $section);
        }

        $this->_form->add('submit', 'submit')
            ->label('Отправить');

        if (Arr::path($_POST, $this->_form->alias().'.'.$this->_form->submit->alias()))
            $this->_form->load();
    }

    protected function add_section($name, $section)
    {
        if (in_array($section['type'], array('select', 'radio')))
        {
            $values = array();
            foreach(explode('|', $section['value']) as $value)
                $values[$value] = $value;

            $this->_form->add_group($name, $section['type'], $values);
        }
        else
        {
            $this->_form->add($name, $section['type'], $section['value']);
        }

        $this->_form->{$name}->label($section['label']);

        if (! empty($section['required']))
            $this->_form->{$name}->rule('not_empty', 'Заполните поле "'.$section['label'].'"');
    }

    public function generate()
    {
        return $this->_form->generate();
    }

    public function __toString()
    {
        return (string) $this->_form;
    }

} // End Builderform
