<?php defined('SYSPATH') or die('No direct access allowed.');
class EmailHelper {
	public static function send_log($log) {
		$log .= '\r\n\r\n';
		$stream = fopen('./mail.log', 'a');
		fputs($stream, $log);
		fclose($stream);
	}
	public static function send($address, $subject = NULL, $msg = NULL) {
		$msg .= "<br /><br /><b>Это автоматическое уведомление с сайта VashZakaz.US.<br />Отвечать не нужно на это письмо, любые ответы оформляйте на самом сайте http://www.vashzakaz.us</b><br /><br />Администрация VashZakaz.US<br />http://www.vashzakaz.us/page/contacts";
	
		Email::factory($subject)
			->to($address)
			->from(Kohana::config('email.username'), Kohana::config('email.from'))
			->message($msg, 'text/html')
			->send();
			
		return true;
    }
    public static function send_admin($subject = NULL, $msg = NULL, $from = NULL, $as_html = FALSE)
    {
        $address = Kohana::config('email.admin');

        return EmailHelper::send($address, $subject, $msg);
    }
} // End Email