<?php defined('SYSPATH') or die('No direct script access.');

class Model_Trackingnotes extends ORM {

    protected $_table_name = 'tracking_notes';

    protected $_belongs_to = array(
        'notes' => array(
            'model' => 'tracking',
            'foreign_key' => 'tracking_id',
        ),
    );

} // End Model Order_Messages