<?php defined('SYSPATH') or die('No direct script access.');

class Model_Message extends ORM {

    protected $_table_name = 'messages';

    protected $_table_columns = array(
        'id' => array('type' => 'int'),
        'user_id' => array('type' => 'int'),
        'whom_id' => array('type' => 'int'),
        'title' => array('type' => 'string'),
        'time' => array('type' => 'string'),
        'readed' => array('type' => 'int'),
        'readed2' => array('type' => 'int'),
        'important' => array('type' => 'int'),
        'deleted' => array('type' => 'int'),
        'deleted2' => array('type' => 'int'),
        'admin_important' => array('type' => 'int'),
    );

    protected $_has_many = array(
        'messages' => array(
            'model' => 'message_message',
            'foreign_key' => 'message_id'
        ),
    );

    protected $_belongs_to = array(
        'user' => array('model' => 'user', 'foreign_key' => 'user_id'),
        'whom' => array('model' => 'user', 'foreign_key' => 'whom_id'),
    );

    public function find($id = NULL)
    {
        //$this->cached(5);

        return parent::find($id);
    }

    public function find_all()
    {
        //$this->cached(5);

        return parent::find_all();
    }

    public function count_all()
    {
        //$this->cached(5);

        return parent::count_all();
    }

} // End Model_Message