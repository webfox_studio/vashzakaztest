<?php defined('SYSPATH') or die('No direct script access.');

class Model_Order_Messages extends ORM {

    protected $_table_name = 'order_messages';

    protected $_table_columns = array(
        'id' => array('type' => 'int'),
        'order_id' => array('type' => 'int'),
        'user_id' => array('type' => 'int'),
        'date_time' => array('type' => 'string'),
        'message' => array('type' => 'string'),
    );

    protected $_belongs_to = array(
        'order' => array(
            'model' => 'orders',
            'foreign_key' => 'order_id',
        ),

        'user' => array(
            'model' => 'user',
            'foreign_key' => 'user_id',
        ),
    );

} // End Model Order_Messages