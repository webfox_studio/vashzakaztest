<?php defined('SYSPATH') or die('No direct script access.');

class Model_Message_Message extends ORM {

    protected $_table_name = 'message_messages';

    protected $_table_columns = array(
        'id' => array('type' => 'int'),
        'message_id' => array('type' => 'int'),
        'user_id' => array('type' => 'int'),
        'time' => array('type' => 'string'),
        'content' => array('type' => 'string'),
    );

    protected $_belongs_to = array(
        'message' => array('model' => 'message', 'foreign_key' => 'message_id'),
        'user' => array('model' => 'user', 'foreign_key' => 'user_id'),
    );

} // End Model_Message_Message