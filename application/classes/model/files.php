<?php defined('SYSPATH') or die('No direct script access.');

class Model_Files extends ORM {

    protected $_table_name = 'files';

    protected $_table_columns = array(
        'id' => array('type' => 'int'),
        'user_id' => array('type' => 'int'),
        'file_name' => array('type' => 'string'),
        'incomming' => array('type' => 'int'),
        'readed' => array('type' => 'int'),
        'readed_admin' => array('type' => 'int'),
        'important' => array('type' => 'int'),
        'date_time' => array('type' => 'string'),
        'name' => array('type' => 'string'),
        'content' => array('type' => 'string'),
    );


    protected $_belongs_to = array(
        'user' => array(
            'model' => 'user',
            'foreign_key' => 'user_id',
        )
    );
    
    public function find($id = NULL)
    {
       // $this->cached(30);

        return parent::find($id);
    }

    public function find_all()
    {
    //    $this->cached(30);

        return parent::find_all();
    }

    public function count_all()
    {
      //  $this->cached(30);

        return parent::count_all();
    }

} // End Model Orders