<?php defined('SYSPATH') or die('No direct script access.');

class Model_Offer extends ORM {

    protected $_table_name = 'offers';

    protected $_table_columns = array(
        'id' => array('type' => 'int'),
        'user_id' => array('type' => 'int'),
        'title' => array('type' => 'string'),
        'message' => array('type' => 'string'),
        'time' => array('type' => 'string'),
        'readed' => array('type' => 'int'),
    );

    protected $_belongs_to = array(
        'user' => array(
            'model' => 'user',
            'foreign_key' => 'user_id',
        ),
    );

} // End Model_Offer