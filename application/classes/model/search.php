﻿<?php defined('SYSPATH') or die('No direct script access.');

class Model_Search extends Model_Abstract {

    protected static $_instance;

    protected $_tb_news= 'news';
    
    protected $_tb_page_contents= 'page_contents';

    protected $_tb_pages= 'pages';

    protected $_tb_faq= 'faq';

    protected $_tb_faq_cats= 'faq_cats';

    protected $_tb_useful= 'useful';

    protected $_tb_useful_cats= 'useful_cats';
    
    public static function instance()
    {
        if (!is_object(self::$_instance))
        {
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }

    public function find($str,$limit = FALSE, $offset = FALSE)
    {
        if($str != ""){
            $res = array();
            $res = DB::select($this->_tb_page_contents.'.*',
                    array($this->_tb_pages.'.name','page_name'),
                    array($this->_tb_pages.'.id','page_id'),
                    array($this->_tb_pages.'.title','page_title')
                    )
                    ->from($this->_tb_page_contents)
                    ->join($this->_tb_pages, 'LEFT')
                    ->on($this->_tb_page_contents.'.page_id', '=', $this->_tb_pages.'.id')
                    ->where($this->_tb_page_contents.'.content','LIKE','%'.$str.'%')
                    ->execute()
                    ->as_array();
            $faq = DB::select($this->_tb_faq.'.*',$this->_tb_faq_cats.'.*')
                    ->from($this->_tb_faq)
                    ->join($this->_tb_faq_cats,'LEFT')
                    ->on($this->_tb_faq.'.cat', '=', $this->_tb_faq_cats.'.id')
                    ->where($this->_tb_faq.'.question','LIKE','%'.$str.'%')
                    ->or_where($this->_tb_faq.'.answer','LIKE','%'.$str.'%')
                    ->execute()
                    ->as_array();
            $useful = DB::select($this->_tb_useful.'.*',
                    array($this->_tb_useful_cats.'.id','page_id'),
                    array($this->_tb_useful_cats.'.name','page_name'))
                    ->from($this->_tb_useful)
                    ->join($this->_tb_useful_cats,'LEFT')
                    ->on($this->_tb_useful.'.cat', '=', $this->_tb_useful_cats.'.id')
                    ->where($this->_tb_useful.'.content','LIKE','%'.$str.'%')
                    ->or_where($this->_tb_useful.'.short','LIKE','%'.$str.'%')
                    ->or_where($this->_tb_useful.'.title','LIKE','%'.$str.'%')
                    ->execute()
                    ->as_array();
            $news = DB::select('*')
                    ->from($this->_tb_news)
                    ->where('content','LIKE','%'.$str.'%')
                    ->execute()
                    ->as_array();
            //$about =file_get_contents(APPPATH.'views/static/about'.EXT);
            //$about = mb_convert_encoding(html_entity_decode(htmlentities(file_get_contents(APPPATH.'views/static/about'.EXT), ENT_QUOTES, 'UTF-8'), ENT_QUOTES , 'ISO-8859-15'), 'ISO-8859-15', 'UTF-8');
            //$about_cp1251=htmlentities(file_get_contents(APPPATH.'views/static/about'.EXT), ENT_NOQUOTES, "windows-1251");
            //setlocale(LC_ALL, array ('ru_RU.UTF-8'));
            //$about = strip_tags(html_entity_decode(trim(strip_tags($about)),ENT_NOQUOTES, "UTF-8"));
            //preg_match_all("/[а-я]+/u", $about, $matches);
//            $about=preg_replace('/[^a-zA-Zа-яА-Я0-9\.\-\,\(\)\s]/',"", $about);
//            $about = str_replace("\r", "", $about);
//            $about = str_replace("\n", " ", $about);
            //окружаем группы цифр пробелами
            //$str=str_replace('/(\d+)/g'," $1 ",$str);
            //вставяем пробелы в места типа TXкрасн
            //$str=str_replace('/\b/g'," ",$str);
            //удаляем лишние пробелы
            //$str=str_replace('/\s+/g',"",$str);
            //file_put_contents(APPPATH.'views/static/about_1251'.EXT, html_entity_decode($about_cp1251, ENT_NOQUOTES , "windows-1251"));
            //$about_cp1251 = html_entity_decode($about_cp1251, ENT_NOQUOTES , "windows-1251");
            //print_r($matches);
            //die();
            //if(strstr($about,$str)){
               // $res[] = array('title' => 'О нас','content'=>iconv ('UTF-8','UTF-8//IGNORE//TRANSLIT',$about)  ,'category' => 'page','url' => 'page/about');
                //$res[] = array('title' => 'О нас','content'=>  $str ,'category' => 'page','url' => 'page/about');
               // print_r($about);
                //die();
            //}
            //$contact = file_get_contents(APPPATH.'views/static/contacts'.EXT);
            $contact = mb_convert_encoding(html_entity_decode(htmlentities(file_get_contents(APPPATH.'views/static/contacts'.EXT), ENT_QUOTES, 'UTF-8'), ENT_QUOTES , 'ISO-8859-15'), 'ISO-8859-15', 'UTF-8');
            if(strstr($contact,$str)){
                //$res[] = array('title' => 'Контакты', 'content'=>iconv ('','UTF-8//IGNORE//TRANSLIT',$contact) ,'category' => 'page','url' => 'page/contacts');
                //$res[] = array('title' => 'Контакты', 'content'=>$contact ,'category' => 'page','url' => 'page/contacts');
            }
            
            foreach($useful as $v){
                $v['category'] = 'useful';
                $v['title'] = strip_tags($v['title']);
                $v['content'] = strip_tags($v['title'].' '.$v['short'].' '.$v['content']);
                $res[] = $v;
            }
            foreach($faq as $v){
                $v['category'] = 'faq';
                $v['content'] = strip_tags($v['question'].' '.$v['answer']);
                $res[] = $v;
            }
            foreach($news as $v){
                $v['category'] = 'news';
                $res[] = $v;
            }
            return $res;
        }
        return array();

    }
    public function count()
    {
        $count = $this->_count();

        return $count;
    }



    public function get($id = NULL)
    {
        return $this->_get($id);
    }

    public function get_all($limit = FALSE, $offset = FALSE, $direction = 'DESC')
    {
        $result = DB::select('*')
            ->from($this->_tb_name)
            ->limit($limit)
            ->offset($offset)
            ->order_by('id', $direction)
            ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }

    public function add($news_content = NULL, $date = NULL)
    {
        $data = array(
            'dt' => isset($date) ? $date : date('Y-m-d H:i:s'),
            'content' => $news_content,
        );

        return $this->_add($data);
    }

    public function edit($id = NULL, array $data = array())
    {
        return $this->_edit($id, $data);
    }

    public function delete($id = NULL)
    {
        return $this->_delete($id);
    }

    public function get_last_id()
    {
        $result = DB::select(array(DB::expr('MAX(`id`)'), 'id'))
            ->from($this->_tb_name)
            ->cached(240)
            ->execute()
            ->get('id');
        
        return $result;
    }
}