<?php defined('SYSPATH') or die('No direct script access.');

abstract class Model_Abstract extends Model {

    protected static $_instance;

    protected $_tb_name;

    public static function instance()
    {
        if (!is_object(self::$_instance))
        {
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }

    /**
     * __construct()
     * 
     * @param <string> $db
     */
    public function   __construct($db = NULL) {
        parent::__construct($db);
    }

    /**
     * Получает строку по её ID
     *
     * @param <int> $id
     * @return <mixed>
     */
    protected function _get($id = NULL)
    {
        if (! $id)
        {
            return FALSE;
        }

        $result = DB::select('*')
            ->from($this->_tb_name)
            ->where('id', '=', $id)
            ->execute()
            ->current();

        return $result;
    }

    /**
     * Получить все строки из таблицы.
     * $limit - ограничение (кол-во)
     * $offset - номер строки с которой начинается отсчет
     *
     * @param <int> $limit
     * @param <int> $offset
     * @return <array>
     */
    protected function _get_all($limit = FALSE, $offset = FALSE, $direction = 'ASC')
    {
        if ($direction !== 'ASC' AND $direction !== 'DESC' AND $direction !== FALSE)
        {
            $direction = 'ASC';
        }

        $result = DB::select('*')
            ->from($this->_tb_name);

        if ($limit !== FALSE AND $limit !== NULL)
            $result = $result->limit($limit);

        if ($offset !== FALSE AND $offset !== NULL)
            $result = $result->offset($offset);

        if ($direction !== FALSE)
        {
            $result = $result->order_by('id', $direction);
        }

        $result = $result->execute()
            ->as_array();

        return $result;
    }

    /**
     * Добавление новой строки записей в таблицу.
     * Если подан ассоициативный массив, в запрос подставляются имена столбцов,
     * иначе в массиве должны быть указаны все поля.
     *
     * @param array $data
     */
    protected function _add(array $data = array())
    {
        $result = DB::insert($this->_tb_name);

        if (Arr::is_assoc($data))
        {
            $result = $result->columns(array_keys($data));
            $data = array_values($data);
        }

        $result = $result->values($data)
            ->execute();

        return $result;
    }

    /**
     * Редактирование записей строки
     * data = array('colimn1' => 'value1', 'column2' => 'value2', ...)
     * 
     * @param <int> $id
     * @param array $data
     * @return <mixed>
     */
    protected function _edit($id = NULL, array $data = array())
    {
        if (! $id)
        {
            return FALSE;
        }

        $result = DB::update($this->_tb_name)
            ->set($data)
            ->where('id', '=', $id)
            ->execute();

        return $result;
    }

    /**
     * Удаление строки по её ID
     *
     * @param <int> $id
     * @return <mixed>
     */
    protected function _delete($id = NULL)
    {
        if (! $id)
        {
            return FALSE;
        }

        $result = DB::delete($this->_tb_name)
            ->where('id', '=', $id)
            ->execute();

        return $result;
    }

    /**
     * Возвращает количество строк в таблице
     *
     * @return <int>
     */
    protected function _count()
    {
/*        $result = DB::select(array(DB::expr('COUNT(*)'), 'count'))
            ->from($this->_tb_name)
            ->execute()
            ->get('count');*/

        $result = DB::select('*')
            ->from($this->_tb_name)
            ->execute()
            ->count();

        return $result;
    }
}