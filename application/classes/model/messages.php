<?php defined('SYSPATH') or die('No direct script access.');

class Model_Messages extends ORM {

    protected $_table_name = 'messages';

    const READED    = 0x1;
    const READED2   = 0x2;
    const IMPORTANT = 0x4;
    const DELETED   = 0x8;

    protected $_table_columns = array(
        'id' => array('type' => 'int'),
        'user_id' => array('type' => 'int'),
        'whom_id' => array('type' => 'int'),
        'head' => array('type' => 'string'),
        'message' => array('type' => 'string'),
        'date_time' => array('type' => 'string'),
        'readed' => array('type' => 'int'),
        'important' => array('type' => 'int'),
        'deleted' => array('type' => 'int'),
    );

    protected $_has_many = array(
        'messages' => array(
            'model' => 'message_message',
            'foreign_key' => 'message_id',
        )
    );

    protected $_belongs_to = array(
        'user' => array()
    );

    public function clients($limit = 100, $offset = 0)
    {
        $query = 'SELECT
            users.id, user_data.name,
            (SELECT COUNT(id) FROM messages WHERE user_id = users.id AND readed = 0) AS new_msg,
            (SELECT COUNT(id) FROM messages WHERE user_id = users.id AND deleted = 0 ) AS total,
            (SELECT COUNT(id) FROM messages WHERE user_id = users.id) AS total_all,
            (SELECT COUNT(id) FROM messages WHERE user_id = users.id AND important = 1) AS important
            FROM users, user_data
            WHERE users.id = user_data.user_id
            ORDER BY new_msg DESC, important DESC, id DESC';

        $result = DB::query(Database::SELECT, $query)
            ->as_object()
            ->execute()
            ->as_array();
        
        return $result;
    }

    public function count_new()
    {
        $result = $this->where('readed', '=', 0)->count_all();

        return $result;
    }
    
} // End Model Messages