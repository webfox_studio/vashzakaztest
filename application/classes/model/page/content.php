<?php defined('SYSPATH') or die('No direct script access.');

class Model_Page_Content extends ORM {

    protected $_table_name = 'page_contents';

    protected $_table_columns = array(
        'id' => array('type' => 'int'),
        'page_id' => array('type' => 'int'),
        'name' => array('type' => 'string'),
        'title' => array('type' => 'string'),
        'content' => array('type' => 'string'),
        'display_on_parent' => array(),
        'order' => array('type' => 'int'),
    );

    protected $_belongs_to = array(
        'page' => array(
            'model' => 'page',
            'foreign_key' => 'page_id',
        ),
    );

    protected function cache($lifetime = NULL)
    {
        //$this->cached(60);
    }

    public function find($id = NULL)
    {
        //$this->cache();

        return parent::find($id);
    }

    public function find_all()
    {
        //$this->cache();

        return parent::find_all();
    }

} // End Model_Offer