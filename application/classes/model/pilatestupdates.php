<?php defined('SYSPATH') or die('No direct script access.');

class Model_Pilatestupdates extends Model_Abstract {

    protected static $_instance;

    protected $_tb_name = 'news';

    public static function instance()
    {
        if (!is_object(self::$_instance))
        {
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }
    
    public function messages($user_id)
    {
        $result = DB::select('*')
            ->from('messages')
            ->where('user_id', '=', $user_id)
            ->where('deleted', '=', 0)
            ->where('readed', '=', 1)
            ->order_by('id', DESC)
                ->limit(5)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    public function orders($user_id)
    {
        $result = DB::select('*')
            ->from('orders')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 1)
            ->order_by('id', DESC)
            ->limit(5)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    public function parcels($user_id)
    {
        $result = DB::select('*')
            ->from('parcels')
            ->where('user_id', '=', $user_id)
           ->where('readed', '=', 1)
            ->order_by('id', DESC)
            ->limit(5)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    public function payments($user_id)
    {
        $result = DB::select('*')
            ->from('payments')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 1)
            ->order_by('id', DESC)
            ->limit(5)
           // ->cached(0)
            ->execute()
            ->as_array();
        
//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    public function files($user_id)
    {
        $result = DB::select('*')
            ->from('files')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 1)
            ->where('incoming', '=', 1)
            ->order_by('id', DESC)
            ->limit(5)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    
    //страница user/latestupdates
    //выбор 5 последних сообщений
    public function get_all_three()
    {
        $result = DB::select('*')
            ->from('news')
            ->limit(5)
            ->order_by('id', DESC)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    public function messages_new_in($user_id)
    {

        $result = DB::select('*')
            ->from('messages')
               ->where('whom_id', '=', $user_id)
                ->where('deleted', '=', 0)
                ->where('readed', '=', 0)
            ->order_by('id', DESC)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    public function messages_new_out($user_id)
    {

        $result = DB::select('*')
            ->from('messages')
                ->where('user_id', '=', $user_id)
                ->where('deleted', '=', 0)
                ->where('readed', '=', 0)
            ->order_by('id', DESC)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    public function orders_new($user_id)
    {
        $result = DB::select('*')
            ->from('orders')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->order_by('id', DESC)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    public function parcels_new($user_id)
    {
        $result = DB::select('*')
            ->from('parcels')
            ->where('user_id', '=', $user_id)
           ->where('readed', '=', 0)
            ->order_by('id', DESC)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    public function files_new($user_id)
    {
        $result = DB::select('*')
            ->from('files')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
            ->where('incoming', '=', 1)
            ->order_by('id', DESC)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
    
    public function payments_new($user_id)
    {
        $result = DB::select('*')
            ->from('payments')
            ->where('user_id', '=', $user_id)
            ->where('readed', '=', 0)
                
            ->order_by('id', DESC)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
}