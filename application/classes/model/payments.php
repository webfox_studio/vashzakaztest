<?php defined('SYSPATH') or die('No direct script access.');

class Model_Payments extends ORM {

    protected $_table_name = 'payments';

    protected $_table_columns = array(
        'id' => array('type' => 'int'),
        'user_id' => array('type' => 'int'),
        'date_time' => array('type' => 'string'),
        'partner_id' => array('type' => 'int'),
        'title' => array('type' => 'string'),
        'new' => array('type' => 'int'),
        'readed' => array('type' => 'int'),
        'readed_admin' => array('type' => 'int'),
        'important' => array('type' => 'int'),
        'processed' => array('type' => 'int'),
        'deleted' => array('type' => 'int'),
    );

    protected $_has_many = array(
        'messages' => array(
            'model' => 'payments_messages',
            'foreign_key' => 'order_id',
        )
    );

    protected $_belongs_to = array(
        'user' => array(
            'model' => 'user',
            'foreign_key' => 'user_id',
        ),
        'partner' => array(
            'model' => 'user',
            'foreign_key' => 'user_id',
        ),
    );

    public function clients($limit = NULL, $offset = NULL)
    {
/*        $sql = "SELECT user_data.user_id as id, user_data.name as name,
                    (SELECT COUNT(*) FROM orders WHERE orders.user_id = users.id AND readed_admin = 0) as new,
                    (SELECT COUNT(*) FROM orders WHERE orders.user_id = users.id) as total
                FROM users, user_data, roles, roles_users
                WHERE roles.name = 'client'
                AND roles_users.role_id = roles.id
                AND users.id = roles_users.user_id
                AND user_data.user_id = users.id"; */

        $q_count_orders_all = "(SELECT COUNT(*) FROM `$this->_table_name` WHERE user_id = users.id)";
        $q_count_orders_new = "(SELECT COUNT(*) FROM `$this->_table_name` WHERE user_id = users.id AND readed_admin = 0)";
        $q_count_orders_important = "(SELECT COUNT(*) FROM `$this->_table_name` WHERE user_id = users.id AND important = 1)";

        $this->select(
                array('users.id', 'id'),
                array('user_data.name', 'name'),
                array(DB::expr($q_count_orders_all), 'total'),
                array(DB::expr($q_count_orders_new), 'new'),
                array(DB::expr($q_count_orders_important), 'count_important')
            )
            ->from('users', 'user_data', 'roles', 'roles_users')
            ->where('roles.name', '=', 'client')
            ->where(DB::expr('roles_users.role_id'), '=', DB::expr('roles.id'))
            ->where(DB::expr('users.id'), '=', DB::expr('roles_users.user_id'))
            ->where(DB::expr('user_data.user_id'), '=', DB::expr('users.id'));

        if ($limit)
        {
            $this->limit($limit);
            $this->offset($offset);
        }
        
        return $this->find_all();
    }

    public function find($id = NULL)
    {
        //$this->cached(1);

        return parent::find($id);
    }

    public function find_all()
    {
        //$this->cached(1);

        return parent::find_all();
    }

    public function count_all()
    {
        //$this->cached(1);

        return parent::count_all();
    }

} // End Model Orders