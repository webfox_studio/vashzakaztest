<?php defined('SYSPATH') or die('No direct script access.');

class Model_Faq extends Model_Abstract {

    protected static $_instance;

    protected $_tb_name 	 = 'faq';
    protected $_tb_cats_name = 'faq_cats';

    public static function instance()
    {
        if (!is_object(self::$_instance))
        {
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }

    public function get_cats()
    {
        $result = DB::select('*')
            ->from($this->_tb_cats_name)
            //->cached(240)
            ->execute()
            ->as_array();

        return $result;
    }

    public function get_cat($id)
    {
        $result = DB::select('*')
            ->from($this->_tb_cats_name)
            ->where('id', '=', $id)
            ->execute()
            ->get('name');

        return $result;
    }

    public function edit_cat($old_id, $new_id, $new_name)
    {
        $result = DB::update($this->_tb_cats_name)
            ->set(array('name' => $new_name,'id' => $new_id))
            ->where('id', '=', $old_id)
            ->execute();
        $result = DB::update($this->_tb_name)
            ->set(array('cat' => $new_id))
            ->where('cat','=',$old_id)
            ->execute();
        return $result;
    }

    public function add_cat($id,$name)
    {
        $result = DB::insert($this->_tb_cats_name)
            ->columns(array('id','name'))
            ->values(array($id,$name))
            ->execute();

        $insert_id = $result[0];

        return $insert_id;
    }

    public function del_cat($id)
    {
      /*  $result = DB::delete($this->_tb_name)
            ->where('cat', '=', $id)
            ->execute();
*/
        $result = DB::delete($this->_tb_cats_name)
            ->where('id', '=', $id)
            ->execute();
        $result = DB::delete($this->_tb_name)
            ->where('cat', '=', $id)
            ->execute();
        
        return $result;
    }

    public function get($cat_id, $num)
    {
        $result = DB::select('*')
            ->from($this->_tb_name)
            ->where('cat', '=', $cat_id)
            ->where('num', '=', $num)
            ->execute()
            ->current();

        return $result;
    }

    public function get_by_cat($cat_id)
    {
        $result = DB::select('*')
            ->from($this->_tb_name)
            ->where('cat', '=', $cat_id)
            ->execute()
            ->as_array();

        return $result;
    }

    public function get_all()
    {
        $result = DB::select('*')
            ->from($this->_tb_name)
            //->cached(240)
            ->order_by('cat','ASC')
            ->order_by('num','ASC')
            ->execute()
            ->as_array();

        return $result;
    }

    public function get_group_cat()
    {
        $faq = $this->get_all();

        $result = array();

        foreach($faq as $_faq)
        {
            $result[$_faq['cat']][$_faq['num']] = array('question' => $_faq['question'], 'answer' => $_faq['answer']);
        }

        return $result;
    }

    public function add($cat_id, $num, $question, $answer)
    {
        $result = DB::insert($this->_tb_name)
            ->columns(array('cat', 'num', 'question', 'answer'))
            ->values(array($cat_id, $num, $question, $answer))
            ->execute();

        return $result;
    }

    public function edit($old_cat_id, $old_num,$new_cat_id, $new_num, $question, $answer)
    {
        $result = DB::update($this->_tb_name)
            ->set(array('cat' => $new_cat_id, 'num' => $new_num,'question' => $question, 'answer' => $answer))
            ->where('cat','=',$old_cat_id)
            ->where('num','=',$old_num)
            ->execute();
        return $result;
    }

    public function del($cat_id, $num)
    {
        $result = DB::delete($this->_tb_name)
            ->where('cat', '=', $cat_id)
            ->where('num', '=', $num)
            ->execute();
        $faqs = DB::select('*')
                ->from($this->_tb_name)
                ->where('cat', '=', $cat_id)
                ->execute()->as_array();
        $i = 1;
        foreach($faqs as $k => $v){
            DB::update($this->_tb_name)
            ->set(array('cat' => $v['cat'], 'num' => $i,'question' => $v['question'], 'answer' => $v['answer']))
            ->where('cat','=',$v['cat'])
            ->where('num','=',$v['num'])
            ->execute();
            $i++;
        }
        return $result;
    }
}