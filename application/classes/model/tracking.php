<?php defined('SYSPATH') or die('No direct script access.');

class Model_Tracking extends ORM {

    protected $_table_name = 'trackings';

    protected $_has_many = array(
        'statuses' => array(
            'model' => 'trackingstatus',
            'foreign_key' => 'tracking_id',
        ),
        'notes' => array(
            'model' => 'trackingnotes',
            'foreign_key' => 'tracking_id',
        ),
    );

} // End Model Order_Messages