<?php defined('SYSPATH') or die('No direct script access.');

class Model_Reviewsall extends Model_Abstract {

    protected static $_instance;

    protected $_tb_name = 'reviews';

    public static function instance()
    {
        if (!is_object(self::$_instance))
        {
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }

    public function count()
    {
        $count = $this->_count();

        return $count;
    }

    public function get_all($limit = FALSE, $offset = FALSE, $direction = 'DESC')
    {
        $result = DB::select('*')
            ->from($this->_tb_name)
            ->limit($limit)
            ->offset($offset)
            ->order_by('id', $direction)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }
}
