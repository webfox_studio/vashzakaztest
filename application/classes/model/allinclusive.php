<?php defined('SYSPATH') or die('No direct script access.');

class Model_Allinclusive extends ORM {

    protected $_table_name = 'allinclusives';
    protected $_primary_key = 'id';
    
} // End Model Orders