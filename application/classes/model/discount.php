<?php defined('SYSPATH') or die('No direct script access.');

class Model_Discount extends ORM {

    protected $_table_name = 'discount';
    protected $_primary_key = 'id';

} // End Model Orders