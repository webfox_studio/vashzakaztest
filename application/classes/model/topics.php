<?php defined('SYSPATH') or die('No direct script access.');

class Model_Topics extends Model_Abstract {

    protected static $_instance;

    protected $_tb_name = 'phpbb_topics';
    
    public static function instance()
    {
        if (!is_object(self::$_instance))
        {
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }


    public static function get_all()
    {
            
        $result = DB::query(Database::SELECT,"SELECT * FROM phpbb_topics ORDER BY topic_last_post_time DESC LIMIT 5 ")
                ->execute()
                ->as_array();

        return $result;
    }
    
}