<?php defined('SYSPATH') or die('No direct script access.');

class Model_News extends Model_Abstract {

    protected static $_instance;

    protected $_tb_name = 'news';

    public static function instance()
    {
        if (!is_object(self::$_instance))
        {
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }

    public function count()
    {
        $count = $this->_count();

        return $count;
    }

    public function get($id = NULL)
    {
        return $this->_get($id);
    }

    public function get_all($limit = FALSE, $offset = FALSE, $direction = 'DESC')
    {
        $result = DB::select('*')
            ->from($this->_tb_name)
            ->limit($limit)
            ->offset($offset)
            ->order_by('id', $direction)
           // ->cached(0)
            ->execute()
            ->as_array();

//        return $this->_get_all($limit, $offset, $direction);
        return $result;
    }

    public function add($news_content = NULL, $date = NULL, $filename = NULL, $name = NULL)
    {
		$data = array(
		
			'dt' => isset($date) ? $date : date('Y-m-d H:i:s'),
			'img' => isset($filename) ? $filename : '',
			'name' => isset($name) ? $name : '',
			'content' => $news_content,
		);

        return $this->_add($data);
    }

    public function edit($id = NULL, array $data = array())
    {
        return $this->_edit($id, $data);
    }

    public function delete($id = NULL)
    {
        return $this->_delete($id);
    }

    public function get_last_id()
    {
        $result = DB::select(array(DB::expr('MAX(`id`)'), 'id'))
            ->from($this->_tb_name)
          //  ->cached(240)
            ->execute()
            ->get('id');
        
        return $result;
    }
    
    
    public function nameuser($user_id)
    {
       $result = DB::select('*')
            ->from('user_data')
            ->where('user_id', '=', $user_id)
            ->limit(1)
           // ->cached(0)
            ->execute()
            ->as_array();
        
        return $result;
    }
}