<?php defined('SYSPATH') or die('No direct script access.');

class Model_Page extends ORM {

    protected $_table_name = 'pages';

    protected $_table_columns = array(
        'id' => array('type' => 'int'),
        'name' => array('type' => 'string'),
        'title' => array('type' => 'string'),
        'order' => array('type' => 'int'),
    );

    protected $_has_many = array(
        'contents' => array(
            'model' => 'page_content',
            'foreign_key' => 'page_id',
        ),
    );

    /*protected function cache($lifetime = NULL)
    {
       // $this->cached(60);
    }
*/
    public function find($id = NULL)
    {
        //$this->cache();
//        $this->reload_columns();
        return parent::find($id);
    }

    public function find_all()
    {
        //$this->cache();
//        $this->reload_columns();
        return parent::find_all();
    }

} // End Model_Offer