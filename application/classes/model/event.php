<?php defined('SYSPATH') or die('No direct script access.');

class Model_Event extends Model_Abstract {

    protected static $_instance;

    protected $_table_name = 'event_log';

    public static function instance()
    {
        if (!is_object(self::$_instance))
        {
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }

    public function add($type , $what='', $url = '', $to='',$who='', $ipOrig = null, $ipLong = null)
    {
        if ($url == '')
            $url = Request::$referrer;
        if ($who == ""){
            if (Auth::instance()->logged_in()){
                $who = Auth::instance()->get_user()->user_data->name;
            }
        } 
        $result = DB::insert($this->_table_name)
            ->columns(array('dt','who','type','what','url','to'))
            ->values(array(time(),$who,$this->events[$type],$what, $url,$to))
            ->execute();

        $insert_id = $result[0];

        return $insert_id;
    }

    public function get_last($count = 30)
    {
        $result = DB::select('*')
            ->from($this->_table_name)
            ->order_by('id','DESC')
            ->limit($count)
            ->execute()
            ->as_array();

        return $result;
    }

    public function get_from_last_id($id="")
    {
        $this->del_old_signal_messages();
        if ($id == ""){
            return $this->get_last();
        }else{
            $result = DB::select('*')
                ->from($this->_table_name)
                ->where('id','>',$id)
                ->order_by('dt','DESC')
                ->limit(30)
                ->execute()
                ->as_array();

            return $result;
        }
    }

    public function del_old()
    {
        DB::delete($this->_table_name)
        ->where('dt','<',time()-30*24*60*60)
        ->execute();
    }

    public function get_chat($nick,$count=30,$last_id=0)
    {
        $result = DB::select('*')
            ->from($this->_table_name)
            
            ->where('dt','>',(time()-600))
            ->where('type','IN', array('4','5','6'))
            ->where_open()
            ->where('who','=',$nick)
            ->or_where('to','=',$nick)
            ->where_close()
            ->order_by('dt','DESC')
            ->limit($count)
            ->execute()
            ->as_array();
       
        return $result;

    }

    public function get_day_log($day)
    {
         $result = DB::select('*')
                ->from($this->_table_name)
                ->where('dt','>',strtotime('today- '.intval($day).' days' ))
                ->where('dt','<',(strtotime('today- '.intval($day).' days' ))+24*60*60)
                ->order_by('dt',"ASC")
                ->execute()
                ->as_array();
        return $result;
    }

    public function del_old_signal_messages()
    {
        $time = time()-6;
        $del = DB::delete($this->_table_name)
                ->where('type','=','97')
                ->where('dt','<',$time)
                ->execute();
        return $del;
    }

    public function get_signal_messages($last_posted_id)
    {
        $time = time()-6;
        $del = DB::delete($this->_table_name)
                ->where('type','=','97')
                ->where('dt','<',$time)
                ->execute();
         
        $res = DB::select('*')
            ->from($this->_table_name)
            ->where('dt','>',$time)
            ->where('type', '=', '97')
            ->where('id','>',$last_posted_id)
            ->execute()
            ->as_array();

       
        
        return $res;                            
    }

    protected $events = array(
        'EV_LOGIN'                          => 1,
        'EV_LOGOUT'                         => 2,
        'EV_LOGIN_ERROR'                    => 3,
        'EV_CHAT_MESSAGE'                   => 4,
        'EV_CHAT_SIGNAL_MESSAGE'            => 97,
        'EV_CHAT_MESSAGE_FROM_CLIENT'       => 5,
        'EV_CHAT_MESSAGE_TO_CLIENT'         => 6,
        'EV_ADMIN_RENAME'                   => 7,
        'EV_ADMIN_CHAT_ON'                  => 8,
        'EV_ADMIN_CHAT_OFF'                 => 9,
        'EV_ADMIN_SET_PARTNER'              => 10,
        'EV_ADMIN_CHAT_AWAY'                => 11,

        'EV_MESSAGES'                       => 30,
        'EV_MESSAGES_UPDATE_IMPORTANT_ON'   => 32,
        'EV_MESSAGES_UPDATE_IMPORTANT_OFF'  => 31,
        'EV_MESSAGES_REPLY'                 => 33,
        'EV_MESSAGES_NEW'                   => 34,
        'EV_MESSAGES_READ'                  => 35,
        'EV_MESSAGES_DELETE'                => 36,
        'EV_MESSAGES_SKIP'                  => 37,

        'EV_POSTS'                          => 40,
        'EV_POSTS_DELETE'                   => 41,
        'EV_POSTS_VIEW_POST'                => 42,
        'EV_POSTS_MAKE_UNREAD'              => 43,
        'EV_POSTS_MAKE_CLOSED'              => 44,
        'EV_POSTS_ADD_MESSAGE'              => 45,
        'EV_POSTS_UPDATE_IMPORTANT_ON'      => 47,
        'EV_POSTS_UPDATE_IMPORTANT_OFF'     => 46,

        'EV_ORDERS'                         => 50,
        'EV_ORDERS_DELETE'                  => 51,
        'EV_ORDERS_VIEW_ORDER'              => 52,
        'EV_ORDERS_MAKE_UNREAD'             => 53,
        'EV_ORDERS_MAKE_CLOSED'             => 54,
        'EV_ORDERS_ADD_MESSAGE'             => 56,
        'EV_ORDERS_UPDATE_IMPORTANT_ON'     => 58,
        'EV_ORDERS_UPDATE_IMPORTANT_OFF'    => 57,

        'EV_NEWS_ADD'                       => 61,
        'EV_NEWS_EDIT'                      => 62,

        'EV_USEFUL_CATS_ADD'                => 71,
        'EV_USEFUL_CATS_DEL'                => 72,
        'EV_USEFUL_CATS_EDIT'               => 73,
        'EV_USEFUL_CATS_CHANGE'             => 74,
        'EV_USEFUL_ADD'                     => 75,
        'EV_USEFUL_DEL'                     => 76,
        'EV_USEFUL_EDIT'                    => 77,

        'EV_SHEDULLE_EDIT'                  => 81,
        'EV_TARIFFS_EDIT'                   => 82,
        'EV_CONTACTS_EDIT'                  => 83,
        'EV_ABOUT_EDIT'                     => 84,
        'EV_SHIPPING_EDIT'                  => 85,
        'EV_FAQ_EDIT'                       => 86,
        'EV_FAQ_DELETE'                     => 87,

        'EV_PREDLOG_VIEW'                   => 88,
        'EV_PREDLOG_DELETE'                 => 89,
        'EV_XLS_DOWNLOAD'                   => 90,
        'EV_FAQ_CAT_EDIT'                   => 91,

        'EV_CACHE_MENU'                     => 93,
        'EV_CACHE_POS'                      => 94,
        'EV_CACHE_ORDER'                    => 95,
        'EV_CACHE_TWITTER'                  => 96,
    );

} // End Model Event