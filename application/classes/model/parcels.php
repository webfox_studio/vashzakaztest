<?php defined('SYSPATH') or die('No direct script access.');

class Model_Parcels extends ORM {

    protected $_table_name = 'parcels';

    protected $_table_columns = array(
        'id' => array('type' => 'int'),
        'user_id' => array('type' => 'int'),
        'date_time' => array('type' => 'string'),
        'partner_id' => array('type' => 'int'),
        'title' => array('type' => 'string'),
        'new' => array('type' => 'int'),
        'readed' => array('type' => 'int'),
        'readed_admin' => array('type' => 'int'),
        'important' => array('type' => 'int'),
        'processed' => array('type' => 'int'),
        'deleted' => array('type' => 'int'),
    );

    protected $_has_many = array(
        'messages' => array(
            'model' => 'parcel_messages',
            'foreign_key' => 'parcel_id',
        )
    );

    protected $_belongs_to = array(
        'user' => array(
            'model' => 'user',
            'foreign_key' => 'user_id',
        ),
        'partner' => array(
            'model' => 'user',
            'foreign_key' => 'user_id',
        ),
    );

    public function find($id = NULL)
    {
        //$this->cached(5);

        return parent::find($id);
    }

    public function find_all()
    {
        //$this->cached(5);

        return parent::find_all();
    }

    public function count_all()
    {
        //$this->cached(5);

        return parent::count_all();
    }

} // End Model Parcels