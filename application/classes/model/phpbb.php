<?php defined('SYSPATH') or die('No direct script access.');

class Model_Phpbb extends Model_Abstract {

    protected static $_instance;

    protected $_tb_name = 'phpbb_users';

    public static function instance()
    {
        if (!is_object(self::$_instance))
        {
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }

    public function get_by_email($email)
    {
        $result = DB::query(Database::SELECT,"SELECT * FROM phpbb_users WHERE user_email = '$email'")
                ->execute()
                ->current();

        return $result;
    }

}