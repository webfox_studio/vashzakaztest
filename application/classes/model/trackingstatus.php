<?php defined('SYSPATH') or die('No direct script access.');

class Model_Trackingstatus extends ORM {

    protected $_table_name = 'tracking_statuses';

    protected $_belongs_to = array(
        'tracking' => array(
            'model' => 'tracking',
            'foreign_key' => 'tracking_id',
        ),
    );

} // End Model Order_Messages