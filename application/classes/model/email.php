<?php defined('SYSPATH') or die('No direct script access.');

class Model_Email extends Model_Abstract {

    protected static $_instance;

    protected $_tb_name = 'emails';

    public static function instance(){
    
        if (!is_object(self::$_instance)){
        
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }

    public function add($email, $code){
    
        $result = DB::insert($this->_tb_name)
            ->columns(array('email', 'code', 'confirmed'))
            ->values(array($email, $code, FALSE))
            ->execute();

        return (bool) $result;
    }

    public function update($email, $code){
    
        $code = trim($code);

        $query = DB::insert($this->_tb_name)
            ->columns(array('email', 'code', 'confirmed'))
            ->values(array($email, $code, FALSE))
            ->__toString();

        $query = $query . ' ON duplicate KEY UPDATE `code` = \''.$code.'\';';

        $result = DB::query(Database::INSERT, $query)->execute();
    }

    public function delete($email){
    
        $result = DB::delete($this->_tb_name)
            ->where('email', '=', $email)
            ->execute();

        return (bool) $result;
    }

    public function exists($email){
    
        $result = DB::select('email')
            ->from($this->_tb_name)
            ->where('email', '=', $email)
            ->execute()
            ->count();

        return (bool) $result;
    }

    public function change_email($email, $new_email){
//        $result = DB::select('code')
//            ->from($this->_tb_name)
//            ->where('email', '=', $email)
//            ->execute();
        $config = Kohana::config('main');
        $this->config = $config;
        $code = md5(time() . $new_email . $this->config['restore_salt']);
        //$result = DB::delete($this->_tb_name)
         //   ->where('email', '=', $email)
         //   ->execute();
        echo $this->_tb_name;
        $result = DB::update($this->_tb_name)
            ->set(array('email'=>$new_email, 'code'=>$code, 'confirmed'=>TRUE))
            ->where('email','=',$email)
            ->execute();
        return (bool) $result;
    }

    public function is_code_valid($email, $code){
    
        $result = DB::select('*')
            ->from($this->_tb_name)
            ->where('email', '=', $email)
            ->and_where('code', '=', $code)
            ->execute()
            ->count();
               
        return (bool) $result;
    }
    
    public function is_restorecode_valid($email, $code, $time){
    
        $result = $this->_db->query(
        			Database::SELECT,
         			"SELECT * FROM {$this->_db->table_prefix()}{$this->_tb_name}
            				WHERE `email` = {$this->_db->escape($email)}
            					AND `restore_code` = {$this->_db->escape($code)}
            					AND unix_timestamp(now()) < unix_timestamp(`restore_time`) + {$time}",
        			false)
        			->count();
               
        return (bool) $result;
    }

    public function is_confirmed($email){
    
        $result = DB::select('confirmed')
            ->from($this->_tb_name)
            ->where('email', '=', $email)
            ->execute()
            ->current();

        return (bool) $result['confirmed'];
    }

    public function confirm($email){
    
        $result = DB::update($this->_tb_name)
            ->set(array('confirmed' => TRUE))
            ->where('email', '=', $email)
            ->execute();

        return $result;
    }
    
	public function add_restore_code($email, $code){
		
		 $result = $this->_db->query(
        			Database::UPDATE,
         			"UPDATE {$this->_db->table_prefix()}{$this->_tb_name}
            				SET `restore_code` = {$this->_db->escape($code)},
            					`restore_time` = now()
            		 WHERE `email` = {$this->_db->escape($email)}",
        			false);
      
        return $result;
    }
    
	public function del_restore_code($email, $code){
    
        $result = DB::update($this->_tb_name)
            ->set(array('restore_code' => '', 'restore_time' => 0))
            ->where('email', '=', $email)
            ->and_where('restore_code', '=', $code)
            ->execute();

        return $result;
    }
    
} // End Model Email