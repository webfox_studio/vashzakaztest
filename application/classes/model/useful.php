<?php defined('SYSPATH') or die('No direct script access.');

class Model_Useful extends Model_Faq {

    protected static $_instance;

    protected $_tb_name 	 = 'useful';
    protected $_tb_cats_name = 'useful_cats';

    public static function instance(){
    
        if (!is_object(self::$_instance)){
        
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }
   
    
    public function get_group_cat(){
    
       return;
    }
    
	public function get_by_id($id)
    {
        $result = DB::select('*')
            ->from($this->_tb_name)
            ->where('id', '=', $id)
            ->execute()
            ->current();            

        return $result;
    }

    public function add($title, $short, $content, $cat)
    {
        $result = DB::insert($this->_tb_name)
            ->columns(array('title','dt', 'short', 'content', 'cat'))
            ->values(array($title, date('Y-m-d H:i:s',time()), $short, $content, $cat))
            ->execute();

        return $result;
    }
/*
    public function add($title, $short, $content, $cat)
    {
        $result = DB::insert($this->_tb_name)
            ->columns(array('title','dt', 'short', 'content', 'cat'))
            ->values(array($title, date('Y-m-d H:i:s',time()), $short, $content, $cat))
            ->execute();

        return $result;
    }*/

    public function add_category($name)
    {
        $result = DB::insert($this->_tb_cats_name)
            ->columns(array('name'))
            ->values(array($name))
            ->execute();
        return $result;
    }

    public function edit_useful($id, $title, $short, $content, $cat)
    {
        $result = DB::update($this->_tb_name)
            ->set(array('title'=>$title, 'dt' => date('Y-m-d H:i:s',time()), 'short' =>$short, 'content' =>$content, 'cat' =>$cat))
            ->where('id','=',$id)
            ->execute();

        return $result;
    }

     public function edit_category($id,$name)
    {
        $result = DB::update($this->_tb_name)
            ->set(array('title'=>$title, 'dt' => date('Y-m-d H:i:s',time()), 'short' =>$short, 'content' =>$content, 'cat' =>$cat))
            ->where('id','=',$id)
            ->execute();

        return $result;
    }


    public function del_useful($id)
    {
        $result = DB::delete($this->_tb_name)
            ->where('id', '=', $id)
            ->execute();

        return $result;
    }

    public function del_useful_cat($id)
    {
         
        $result = DB::delete($this->_tb_cats_name)
            ->where('id', '=', $id)
            ->execute();

        $result = DB::delete($this->_tb_name)
            ->where('cat', '=', $id)
            ->execute();

        return $result;
    }
   /* public function edit($cat_id, $num, $question, $answer)
    {
        $result = DB::update($this->_tb_name)
            ->set(array('question' => $question, 'answer' => $answer))
            ->execute();

        return $result;
    }
*/
} // End Model_Usefull