<?php defined('SYSPATH') or die('No direct script access.');

class Model_Subscription extends Model_Abstract {
	protected static $_instance;
	public static function instance() {
		if (!is_object(self::$_instance)) {
			$name = str_replace('Model_', '', __CLASS__);
			self::$_instance = Model::factory($name);
		}
		return self::$_instance;
	}
	public function get_cats() {
		$result = DB::select()
			->from('subscription_categories')
			->where('subscription_categories.active', '=', 1)
			->execute()
			->as_array();
		if (count($result) > 0) foreach ($result as $key => $value) {
			$result_2 = DB::select('id')
				->from('subscription_subscribers')
				->where('cat_id', '=', $value['id'])
				->execute()
				->as_array();
			$result[$key]['count'] = count($result_2);
		}
		return $result;
	}
	
	public function get_cats_client() {
		$result = DB::select()
			->from('subscription_categories')
			->execute()
			->as_array();
		if (count($result) > 0) foreach ($result as $key => $value) {
			$result_2 = DB::select('id')
				->from('subscription_subscribers')
				->where('cat_id', '=', $value['id'])
				->execute()
				->as_array();
			$result[$key]['count'] = count($result_2);
		}
		return $result;
	}

	public function my_active_cats() {
		$result = DB::select(
				'subscription_categories.id',
				'subscription_categories.name'
			)
			->from('subscription_subscribers')
			->where('subscription_subscribers.uid', '=', Auth::instance()->get_user()->id)
            ->and_where('subscription_subscribers.hiddenMailing', '=', 0)
            ->and_where('subscription_categories.active', '=', 1)
			->join('subscription_categories', 'INNER')
			->on('subscription_subscribers.cat_id', '=', 'subscription_categories.id')
			->execute()
			->as_array();
		return $result;
	}
    // обновление новостей рассылки на активно/неактивно
    public function subs_update_active($id) {
        $result = DB::select()
            ->from('subscription_categories')
            ->where('id','=',$id)
            ->execute()
            ->as_array();
        if($result[0]['active'] == 0) { $act = 1; } else { $act = 0; }
        DB::update('subscription_categories')
            ->set(array('active' => $act))
            ->where('id','=',$id)
            ->execute();
        return true;
    }

    public function client_active_cats($cliendId) {
        $result = DB::select(
            'subscription_categories.id',
            'subscription_categories.name',
            'subscription_subscribers.hiddenMailing'
        )
            ->from('subscription_subscribers')

            ->join('subscription_categories', 'INNER')
            ->on('subscription_subscribers.cat_id', '=', 'subscription_categories.id')
            ->where('subscription_subscribers.uid', '=', $cliendId)
            ->execute();
            // ->as_array();
        //echo $result;
        return $result;
    }

	public function subscription_update($categories) { echo Auth::instance()->get_user()->id;
		DB::delete('subscription_subscribers')
			->where('uid', '=', Auth::instance()->get_user()->id)
			->execute();
//		foreach ($categories as $key => $value) {
//			DB::insert('subscription_subscribers')
//				->columns(array('uid', 'cat_id'))
//				->values(array(Auth::instance()->get_user()->id, $key))
//				->execute();
//		}
		return true;
	}
	public function add_cat($category_name) {
		$category_name = trim($category_name);
		if (strlen($category_name) == 0)
			return false;
		$result = DB::select('*')
			->from('subscription_categories')
			->where('name', '=', $category_name)
			->execute()
			->as_array();
		if (count($result) > 0)
			return false;
		$result = DB::insert('subscription_categories')
			->columns(array('name'))
			->values(array($category_name))
			->execute();
		$insert_id = $result[0];
		return $insert_id;
	}
	public function cat_del($category_id) {
		$result = DB::delete('subscription_categories')
			->where('id', '=', $category_id)
			->execute();
		return true;
	}
	public function get_cat($cat_id) {
		$result = DB::select('*')
			->from('subscription_categories')
			->where('id', '=', $cat_id)
			->execute()
			->current();
		return $result;
	}
	public function cat_edit($id, $name) {
		$result = DB::update('subscription_categories')
			->set(array('name' => $name))
			->where('id','=',$id)
			->execute();
        return true;
	}
	public function sub_add($title, $cat_id, $content) {
            $date = date('Y-m-d');
            
		$result = DB::insert('subscription')
			->columns(array('title', 'category_id', 'content', 'activ', 'date'))
			->values(array($title, json_encode($cat_id), $content, '1', $date))
			->execute();
		$insert_id = $result[0];
		return $insert_id;
	}
	public function get_subscriptions() {
		$result = DB::select(
				'subscription.id',
				'subscription.title',
				'subscription.category_id'
			)
			->from('subscription')
			->execute()
			->as_array();
		$categories = array();
		$subscriptions = array();
		foreach ($result as $value) {
			$cats = json_decode($value['category_id']);
			for ($i=0; $i<count($cats); $i++) {
				if (!isset($categories[$cats[$i]])) {
					$result_2 = DB::select('name')
						->from('subscription_categories')
						->where('id', '=', $cats[$i])
						->execute()
						->current();
					$categories[$cats[$i]] = $result_2['name'];
				}
				$subscriptions[$categories[$cats[$i]]][] = array(
					'id' => $value['id'],
					'title' => $value['title'],
				);
			}
		}
		return $subscriptions;
	}
	public function sub_show($id) {
		$result = DB::select('id', 'title', 'category_id', 'content')
			->from('subscription')
			->where('id', '=', $id)
			->execute()
			->current();
		return $result;
	}

	public function sub_delete($id) {
		$result = DB::delete('subscription')
			->where('id', '=', $id)
			->execute();
		return true;
	}

	public function sub_edit($id, $title, $cat_id, $content, $active = 0) {
//		$result = DB::update('subscription')
//			->set(array(
//				'title' => $title,
//				'category_id' => json_encode($cat_id),
//				'content' => $content,
//				'activ' => $active,
//			))
//			->where('id','=',$id)
//			->execute();
		
		DB::insert('subscription', array('title', 'category_id', 'content', 'activ', 'date'))
			->values(array($title, json_encode($cat_id), $content, $active, date('Y-m-d')))
            ->execute();

		return true;
	}
        
        public function subscription_add_cat($post, $user_id)
        {
            $result_all = DB::delete('subscription_subscribers')
			->where('uid', '=', $user_id)
			->execute();
//            echo '<pre>';
//                print_r($post);
//            echo '</pre>';
            $date = date('Y-m-d');
            
            foreach($post['categories'] as $key => $value):
//                $result = DB::update('subscription_subscribers')
//			->set(array(
//				'uid' => $user_id,
//				'cat_id' => $key,
//				'date' => $date,
//                                'send' => '0'
//			))
//			->where('cat_id','=',$key)
//			->execute();
//                
//                $result = DB::select('id')
//			->from('subscription_subscribers')
//			->where('cat_id', '=', $key)
//			->where('cat_id', '=', $key)
//			->execute()->count();
//                if($result == 0)
//                {
                    $query = DB::insert('subscription_subscribers', array('uid', 'cat_id', 'date', 'send'))
                            ->values(array($user_id, $key, $date, '0'))->execute();
//                }
//                else 
//                {
//                    $result = DB::update('subscription_subscribers')
//			->set(array(
//				'uid' => $user_id,
//				'cat_id' => $key,
//				'date' => $date,
//                                'send' => '0'
//			))
//			->where('uid','=',$user_id)
//			->execute();
//                }
            endforeach;
        }
}