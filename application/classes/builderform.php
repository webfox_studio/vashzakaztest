<?php defined('SYSPATH') or die('No direct script access.');

class Builderform {

    protected static $_instances = array();

    public static $form_name = 'order';
    public $_filegood;
    protected $_filename;
    protected $_data;
    protected $_errors;
    protected $_form;
    protected $_validated = FALSE;
    protected $_sent = FALSE;

    const DEFAULT_FORM = 'form';
    const EXT = '.ini';

    protected $_drivers = array('textarea', 'text', 'select', 'bool', 'checkbox', 'radio');

    public static function instance($ini_filename = NULL, $form = NULL)
    {
        if (! $ini_filename)
        {
            return new Builderform();
        }

        if (! isset(Builderform::$_instances[$ini_filename]))
        {
            Builderform::$_instances[$ini_filename] = new Builderform($ini_filename, $form);
        }

        return Builderform::$_instances[$ini_filename];
    }

    public function __construct($ini_filename = NULL, $form = NULL)
    {
        if ($ini_filename)
        {
            $this->_filegood = $ini_filename;
            if(! strstr($ini_filename, self::EXT))
                $ini_filename .= self::EXT;
            $this->_filename = $ini_filename;
        }

        if ($form)
            $this->_form = $form;
    }

    public function set_filename($filename)
    {
        $this->_filename = $filename;
    }

    public function load_ini($filename = NULL)
    {
        if (! $filename)
        {
            if (! $this->_filename)
            {
                throw new Exception('Не указано имя файла конфтгурации');
            }
            
            $filename = $this->_filename;
        }

        $filename = Kohana::config('ini.path') . $filename;
        if ($this->_data = parse_ini_file($filename, TRUE))
        {
            return TRUE;
        }

        return FALSE;
    }

    public function save_ini($filename = NULL)
    {
        if (! $filename)
        {
            $filename = $this->_filename;
        }

        return file_put_contents($filename, $this->_data);
    }

    protected function init()
    {
        if (! $this->_data)
        {
            $this->load_ini();
        }

        if (is_array($this->_data))
        {
            foreach($this->_data as $name => &$section)
            {
                $section['name'] = $name;
                $this->init_section($section);
            }
        }

        if (Arr::path($_POST, 'submit'))
        {
            $this->_sent = TRUE;
            $this->validate();
        }

        if (Arr::path($_POST, 'preview'))
        {
            $this->_sent = TRUE;
            $this->validate();
        }
    }

    public function sent()
    {
        if (! $this->_data)
        {
            $this->init();
        }

        return $this->_sent;
    }

    public function validate()
    {
        if (! $this->_data)
        {
            $this->init();
        }

        if (! $this->_validated)
        {
            if (is_array($this->_data))
            {
                foreach($this->_data as $name => $section)
                {
                    if (isset($section['required']) AND $section['required']
                        AND (isset($_POST[$name]) AND ! $_POST[$name])) {
                        $this->_errors[] = 'Заполните поле '.$section['label'];
                    }
                    if ($section['type'] == 'radio' AND isset($section['required']) AND $section['required'] AND (!isset($_POST[$name]) or empty($_POST[$name]))) {
                        $this->_errors[] = 'Заполните поле '.$section['label'];
                    }
                }
            }

            $this->_validated = TRUE;
        }

        return ! (bool) count($this->_errors);
    }

    protected function init_section(&$section)
    {
        if ($section['type'] == 'radio' OR $section['type'] == 'select')
        {
            $section['value'] = explode('|', $section['value']);
        }
    }

    public function form($form = NULL)
    {
        if ($form !== NULL)
        {
            $this->_form = $form;
        }

        else
        {
            return $this->_form;
        }
    }

    public function render()
    {
        if (! $this->_data)
        {
            $this->init();
        }

        if (is_array($this->_data))
        {
            foreach($this->_data as $name => &$section)
            {
                $section['field'] = $this->render_section($section);
            }
        }

        $data['sections'] = $this->_data;
        $data['errors'] = $this->_errors;
       
        
        
        switch ($this->_filegood) {
            case 'payment':
                $a = 'payment';
                $form = 'builderform/forms/'.$a;
                break;
            case 'posilka':
                $a = 'parcel';
                $form = 'builderform/forms/'.$a;
                break;
            case 'zakaz':
                $a = 'order';
                $form = 'builderform/forms/'.$a;
                break;
            default:
                $form = 'builderform/forms/' . ($this->_form ? $this->_form : self::DEFAULT_FORM);
                break;
        }
        

        $result = View::factory($form, $data)->render();

        return $result;
    }

    public function __toString()
    {
        try {
            return (string) $this->render();
        }
        catch(Exception $e)
        {
            return (string) Kohana::exception_handler($e);
        }
    }

    public function render_section($section)
    {
        return View::factory('builderform/section/'.$section['type'], $section)->render();
    }

    public function getDataSection($name)
    {
        if (array_key_exists($name, $this->_data))
        {
            return trim($_POST[$name]);
        }
        else
        {
            return FALSE;
        }
    }

    public function getData()
    {
        if (is_array($this->_data) AND isset($_POST) AND $_POST)
        {
            $data = array();

            foreach ($this->_data as $name => $section)
            {
                $data[$section['label']] = isset($_POST[$name]) ? str_replace("\n", "<br/> \n", Text::auto_link(trim($_POST[$name]))) : '';
            }
            
            $result = View::factory('builderform/data', array('data' => $data))->render();

            return $result;
        }
        else
        {
            return FALSE;
        }
    }

} // End Builderform