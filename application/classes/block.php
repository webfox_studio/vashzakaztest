<?php defined('SYSPATH') or die('No direct script access.');

class Block {

    public static function form($name, $form)
    {
        if (! $form)
        {
            return FALSE;
        }

        if (! is_array($form) OR count($form) > 1)
        {
            $form = array('form' => $form);
        }

        $name = 'forms/' . $name;

        return View::factory($name, $form);
    }

    public static function news(array $news = array())
    {
        $file = Kohana::config('templates.index.news');

        $result = '';

        foreach($news as $_news)
        {
            $result .= View::factory($file, $_news)->render();
            $result .= '<br />';
        }

        return $result;
    }
     
    public static function nnews(array $news = array())
    {
        $file = Kohana::config('templates.index.nnews');

        $result = '';

        foreach($news as $_news)
        {
            $result .= View::factory($file, $_news)->render();
            $result .= '<br />';
        }

        return $result;
    }
    
    public static function ntopics(array $last_topics = array())
    {
        $file = Kohana::config('templates.index.topic');
        $result = '';

        foreach($last_topics as $topic)
        {
            $result .= View::factory($file, $topic)->render();
            $result .= '<br />';
        }

        return $result;
    }
    
    public static function banners(array $banners = array())
    {
        $file = Kohana::config('templates.index.banners');
        $result = '';
        
        foreach($banners as $banner)
        {
            $result .= View::factory($file, $banner)->render();
        }
        return $result;
    }
    
    public static function actions_big(array $actions = array())
    {
        $file = Kohana::config('templates.index.actionsbig');
        $result = '';
        
        foreach($actions as $action)
        {
            $result .= View::factory($file, $action)->render();
        }
        return $result;
    }
    
    public static function actions_mid(array $banners = array())
    {
        $file = Kohana::config('templates.index.actionsmid');
        $result = '';
        
        foreach($banners as $banner)
        {
            $result .= View::factory($file, $banner)->render();
        }
        return $result;
    }
    
    public static function actions_tiny(array $banners = array())
    {
        $file = Kohana::config('templates.index.actionstiny');
        $result = '';
        
        foreach($banners as $banner)
        {
            $result .= View::factory($file, $banner)->render();
        }
        return $result;
    }
    
    public static function twitter(array $tweets = array())
    {
        $file = Kohana::config('templates.index.twitter');

        return View::factory($file, array('tweets' => $tweets))->render();
    }

    public static function reviews(array $reviews = array())
    {
        $file = Kohana::config('templates.index.reviews');
        $result = '';
        
        foreach($reviews as $review)
        {
            $result .= View::factory($file, $review)->render();
        }
        return $result;
    }

    public static function pagination($pagination)
    {
        return View::factory('blocks/pagination', array('pagination' => $pagination))->render();
    }
    
    public static function search_pagination($pagination)
    {
        //var_dump($pagination);
        //die();
        return View::factory('blocks/search_pagination', array('pagination' => $pagination))->render();
    }
}