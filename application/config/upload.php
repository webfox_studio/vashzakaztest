<?php

return array(
	'allow_file_extensions' => array('png','jpg','jpeg','gif','bmp'), 
	'max_file_size' => '8M',
	'reviews' => 'public_html/vashzakaz.us/upload/reviews/',
);

?>
