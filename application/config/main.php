<?php defined('SYSPATH') or die('No direct script access.');

define('NB', "\n");
define('DS', "/");

return array
(
    'lang' => 'ru',

    'user_master_password' => 'hFfye36GUifgw348',

    'path_media' 	=> 'media',
    'path_css' 		=> 'media'.DS.'css'.DS,
    'path_js' 		=> 'media'.DS.'js'.DS,

    'css' => array('main', 'carousel'),
    'js'  => array('jquery-1.4.4.min', 'jquery.hoverIntent.minified.js', 'mel.sound', 'jquery.jcarousel.min.js'),
    'js_line' => array(),

    'title' => 'VashZakaz',
    'title_separator' => ' | ',

    'dir' => array(
        'reports' => 'reports',
    ),

    'schedule' => APPPATH.'views'.DS.'edit'.DS.'schedule'.EXT,
    
    'services' => APPPATH.'views'.DS.'edit'.DS.'services'.EXT,
        
    'restore_link_time' => 86400,
    
    'restore_salt' => 'asasgjhgds65sdsdcndsd8',
    
    'shop' => array(
        'yaroslav' => 'adminfiles/yaroslav',
        'alena' => 'adminfiles/alena',
        'ulyana' => 'adminfiles/ulyana',
        'oksana' => 'adminfiles/oksana',
        'natalya' => 'adminfiles/natalya',
    )
    
);