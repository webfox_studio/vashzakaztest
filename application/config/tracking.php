<?php defined('SYSPATH') OR die('No direct access allowed.');
return array(
    'form_columns' => array(
        'id' => 'ID',
        'user_id' => 'ID пользователя',
        'user_name' => 'Имя',
        'user_surname' => 'Фамилия',
        'delivery_id' => 'Вид доставки',
        'date_add' => 'Дата',
        'status' => 'Статус',
        'comment' => 'Комментарий',
        'comment_admin' => 'Примечания администратора',
        'tracking' => 'Трекинг номер',
        'first_update' => 'Дата первого статуса',
        'last_update' => 'Дата последнего статуса',
        'last_status' => 'Статус последнего статуса',
        'number' => 'Номер посылки',
        'city_delivery' => 'Город доставки',

        'days_delivery' => 'Дней доставки',
        'departure_date' => 'Дата отправки'
    ),
    'delivery' => array(
        '1' => 'USPS',
        '2' => 'Cargo',
        '3' => 'Economy',
        '4' => 'Sea',
        /* '5' => 'доставка 5', */
       /*  '6' => 'доставка 6', */
    ),
);