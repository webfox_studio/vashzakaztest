<?php defined('SYSPATH') or die('No direct script access.');

return array
(
    'index' => array(
        'news' => 'news/news',
        'twitter' => 'news/twitter',
        'nnews' => 'nmain/news',
        'topic' => 'nmain/topic',
        'banners' => 'nmain/banners',
        'actionsbig' => 'nmain/actionsbig',
        'actionsmid' => 'nmain/actionsmid',
        'actionstiny' => 'nmain/actionstiny',
        'reviews' => 'nmain/reviews'
    ),
);