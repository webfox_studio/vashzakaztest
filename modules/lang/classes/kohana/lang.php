<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Lang module
 */
class Kohana_Lang {

    protected static $config;

    public static function get($path = NULL, array $replace = array())
    {
        if (! $path)
        {
            return FALSE;
        }

        if (! self::$config)
        {
            self::$config = Kohana::config('lang');
        }

        // Use the global target language
        $lang = I18n::lang();

        $path = $lang . self::$config->delimiter . $path;
        $parts = explode(self::$config->delimiter, $path, 3);
        $path_var = array_pop($parts);
        $path = implode(DIRECTORY_SEPARATOR, $parts);
        
        if ($res = Kohana::find_file(self::$config->dir, $path))
        {
            $res = Kohana::load($res[0]);
            $result = Arr::path($res, $path_var, NULL, self::$config->delimiter);

            if ($replace)
            {
                $result = strtr($result, $replace);
            }

            return $result;
        }

        return FALSE;
    }
}