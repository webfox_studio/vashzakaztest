<?php defined('SYSPATH') or die('No direct script access.');

return array
(
    'dir' => 'i18n',
    'lang' => 'ru',
    'ext' => EXT,
    'delimiter' => '.',
);