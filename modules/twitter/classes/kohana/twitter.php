<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_Twitter {

    protected $config;
    
    protected $m_twitter;

    protected $last_id;

    protected $data = array();

    public static function factory()
    {
        return new Twitter();
    }

    public function  __construct()
    {
        $this->config = Kohana::config('twitter');
        $this->m_twitter = Model::factory('twitter');
    }

    public function get()
    {
        $settings = Settings::instance();

        if (($settings->get('tweeter_last_time') + $this->config->timer) <= time())
        {
            if (($this->data = $this->m_twitter->get()) !== FALSE)
            {
                $this->save();
                
                return TRUE;
            }
        }
        $this->data = $settings->get('tweeter_tweets');

        return $this->data;
    }

    public function get_tweets()
    {
        $tweets = array();

        if (! $this->data)
        {
            $this->get();
        }

        if (is_array($this->data))
        {
            foreach($this->data as $tweet)
            {
                $tweets[] = $tweet['tweet'];
            }
        }

        return $tweets;
    }

    public function save()
    {
        $settings = Settings::instance();

        if ($this->data != NULL)
        {
            $settings->set('tweeter_tweets', $this->data);
            $result = $settings->set('tweeter_last_time', time());
        }
        else
        {
            $result = FALSE;
        }

        return (bool) $result;
    }
}