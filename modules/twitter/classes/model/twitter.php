<?php defined('SYSPATH') or die('No direct script access.');

class Model_Twitter {

    protected $config;

    protected $url = '';

    public function  __construct($count = 5) {

        $this->config = Kohana::config('twitter');
    }

    protected function _build_url()
    {
        $this->url = strtr($this->config->url,
            array(
                ':id' => $this->config->id,
                ':format' => $this->config->format,
                ':count' => $this->config->count,
            )
        );
        
        return $this->url;
    }

    protected function _get_data()
    {
        $this->_build_url();

        try
        {
            $data = Remote::get($this->url);
        }
        catch (Exception $e)
        {
            $data = FALSE;
        }

        return $data;
    }

    protected function _get_xml($data)
    {
        $result = array();

        $xml = new SimpleXMLElement($data);

        if (is_array($xml->status))
        {
            foreach($xml->status as $status)
            {
                $result[] = array(
                    'id' => $status->id,
                    'tweet' => Text::auto_link($status->text),
                    'login' => $status->user->screen_name,
                );
            }
        }
        else
        {
            return FALSE;
        }

        return $result;
    }

    protected function _get_json($data)
    {
        $data = json_decode($data);

        $result = array();

        if (is_array($data))
        {
            foreach($data as $tweet)
            {
                $result[] = array(
                    'id' => $tweet->id,
                    'tweet' => Text::auto_link($tweet->text),
                    'login' => $tweet->user->screen_name,
                );
            }
        }
        else
        {
            return FALSE;
        }

        return $result;
    }
    
    public function get($count = NULL)
    {
        if ($count)
        {
            $this->config->count = $count;
        }

        $request = $this->_get_data();

        if ($request === FALSE)
        {
            return FALSE;
        }

        switch ($this->config->format)
        {
            case 'xml':
                $result = $this->_get_xml($request);
                break;

            case 'json':
                $result = $this->_get_json($request);
                break;

            default:
                $result = FALSE;
                break;
        }

        return $result;
    }
}