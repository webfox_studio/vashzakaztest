<?php defined('SYSPATH') or die('No direct script access.');

return array
(
    'id' => 'VashZakazUS',
    'url' => 'http://api.twitter.com/1/statuses/user_timeline/:id.:format?count=:count',
    'format' => 'json',
    'count' => 8,
    'timer' => 10*60, // sec
);