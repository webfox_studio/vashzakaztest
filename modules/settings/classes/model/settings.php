<?php defined('SYSPATH') or die('No direct script access.');

class Model_Settings extends Model {

    protected static $_instance;

    public static $default = 'database';

    protected $_tb_name;

    protected $delimiter = '.';

    public static function instance()
    {
        if (!is_object(self::$_instance))
        {
            $name = str_replace('Model_', '', __CLASS__);
            self::$_instance = Model::factory($name);
        }

        return self::$_instance;
    }

    public function __construct()
    {
        $this->delimiter = Arr::$delimiter;
        $this->_tb_name = Kohana::config('settings.table');
    }

    protected function _create_repository()
    {
        $query = 'CREATE TABLE IF NOT EXISTS  `settings` (
                    `name` VARCHAR( 128 ) NOT NULL ,
                    `value` TEXT NOT NULL ,
                    PRIMARY KEY (`name`)
                  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;';

        $result = DB::query(Database::SELECT, $query);

        return $result;
    }

    protected function _delete_repository()
    {
        $query = 'DROP TABLE IF EXISTS `settings`';

        $result = DB::query(Database::SELECT, $query);

        return $result;
    }

    public function get($name = NULL)
    {
        if (! $name)
        {
            return FALSE;
        }

        $result = DB::select('value')
            ->from($this->_tb_name)
            ->where('name', '=', $name)
            ->cached(10)
            ->execute()
            ->get('value');

        if (! $result)
        {
            return FALSE;
        }

        $value = unserialize($result);
        
        return $value;
    }

    /**
     * Set value by path.
     *
     * @param <string> $group
     * @param <mixed> $value
     * @return <bool>
     */
    public function set($name = NULL, $value = NULL)
    {
        if (! $name)
        {
            return FALSE;
        }

        $value = serialize($value);

        $query = DB::insert($this->_tb_name)
            ->columns(array('name', 'value'))
            ->values(array($name, $value))
            ->__toString();

        $query = $query . ' ON duplicate KEY UPDATE `value` = \''.$value.'\';';

        $result = DB::query(Database::INSERT, $query)->execute();

        return (bool) $result;
    }

    /**
     * Check is isset var.
     *
     * @param <string> $group
     * @return <bool>
     */
    protected function _isset($name = NULL)
    {
        if (! $name)
        {
            return FALSE;
        }

        return $this->get($name) ? TRUE : FALSE;
    }

    public function delete($name = NULL)
    {
        if (! $name)
        {
            return FALSE;
        }

        $result = DB::delete($this->_tb_name)
            ->where('name', '=', $name)
            ->execute();

        return $result;
    }
} // End Model Settings