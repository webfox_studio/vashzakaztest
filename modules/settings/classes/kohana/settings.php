<?php defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Settings {

    protected static $_instance;

    protected $_config;

    protected $_m_settings;

    public static function instance()
    {
		if ( ! isset(Settings::$_instance))
		{
			Settings::$_instance = new Settings();
		}

		return Settings::$_instance;
    }

    public function __construct()
    {
        $this->_config = Kohana::config('settings');
        $this->_m_settings = Model_Settings::instance();
    }

    public function get($group)
    {
        $result = $this->_m_settings->get($group);
        
        return $result;
    }

    public function set($group, $value = NULL)
    {
        $result = $this->_m_settings->set($group, $value);

        return $result;
    }

    public function delete($group)
    {
        $result = $this->_m_settings->delete($group);

        return $result;
    }
} // End Settings