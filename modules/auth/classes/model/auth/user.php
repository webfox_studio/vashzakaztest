<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * Default auth user
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Model_Auth_User extends ORM {

    protected $_tb_users;
    protected $_tb_roles;
    protected $_tb_roles_users;
    protected $_tb_user_expr;

    protected $_clients_joined = FALSE;
    protected $_user_data_joined = FALSE;
    protected $_user_data_changes_joined = FALSE;

    protected $_table_columns = array(
        'id' => array(),
        'email' => array(),
        'username' => array(),
        'password' => array(),
        'logins' => array(),
        'last_login' => array(),
    );

	// Relationships
	protected $_has_many = array(
		'user_tokens' => array('model' => 'user_token'),
		'roles'       => array('model' => 'role', 'through' => 'roles_users'),
        'messages'    => array('model' => 'message', 'foreign_key' => 'user_id'),
        'orders'      => array('model' => 'orders', 'foreign_key' => 'user_id'),
        'parcels'     => array('model' => 'parcels', 'foreign_key' => 'user_id'),
        'files'     => array('model' => 'files', 'foreign_key' => 'user_id'),
	);

    protected $_has_one = array(
        'user_data'   => array('model' => 'user_data'),
        'user_data_changes' => array('model' => 'user_data_changes'),
    );

	// Validation rules
	protected $_rules = array(
		'username' => array(
			'not_empty'  => NULL,
			'min_length' => array(4),
			'max_length' => array(32),
			'regex'      => array('/^[-\pL\pN_.]++$/uD'),
		),
		'password' => array(
			'not_empty'  => NULL,
			'min_length' => array(5),
			'max_length' => array(42),
		),
		'password_confirm' => array(
			'matches'    => array('password'),
		),
		'email' => array(
			'not_empty'  => NULL,
			'min_length' => array(4),
			'max_length' => array(127),
			'email'      => NULL,
		),
	);

	// Validation callbacks
	protected $_callbacks = array(
		'username' => array('username_available'),
		'email' => array('email_available'),
	);

	// Field labels
	protected $_labels = array(
		'username'         => 'username',
		'email'            => 'email address',
		'password'         => 'password',
		'password_confirm' => 'password confirmation',
	);

	// Columns to ignore
	protected $_ignored_columns = array('password_confirm');

	/**
	 * Validates login information from an array, and optionally redirects
	 * after a successful login.
	 *
	 * @param   array    values to check
	 * @param   string   URI or URL to redirect to
	 * @return  boolean
	 */
	public function login(array & $array, $redirect = FALSE)
	{
		$fieldname = $this->unique_key($array['username']);
		$array = Validate::factory($array)
			->label('username', $this->_labels[$fieldname])
			->label('password', $this->_labels['password'])
			->filter(TRUE, 'trim')
			->rules('username', $this->_rules[$fieldname])
			->rules('password', $this->_rules['password']);

		// Get the remember login option
		$remember = isset($array['remember']);

		// Login starts out invalid
		$status = FALSE;

		if ($array->check())
		{
			// Attempt to load the user
			$this->where($fieldname, '=', $array['username'])->find();

			if ($this->loaded() AND Auth::instance()->login($this, $array['password'], $remember))
			{
				if (is_string($redirect))
				{
					// Redirect after a successful login
					Request::instance()->redirect($redirect);
				}

				// Login is successful
				$status = TRUE;
			}
			else
			{
				$array->error('username', 'invalid');
			}
		}
        
		return $status;
	}

	/**
	 * Validates an array for a matching password and password_confirm field,
	 * and optionally redirects after a successful save.
	 *
	 * @param   array    values to check
	 * @param   string   URI or URL to redirect to
	 * @return  boolean
	 */
	public function change_password(array & $array, $redirect = FALSE)
	{
		$array = Validate::factory($array)
			->label('password', $this->_labels['password'])
			->label('password_confirm', $this->_labels['password_confirm'])
			->filter(TRUE, 'trim')
			->rules('password', $this->_rules['password'])
			->rules('password_confirm', $this->_rules['password_confirm']);

		if ($status = $array->check())
		{
			// Change the password
			$this->password = $array['password'];

			if ($status = $this->save() AND is_string($redirect))
			{
				// Redirect to the success page
				Request::instance()->redirect($redirect);
			}
		}

		return $status;
	}

	/**
	 * Complete the login for a user by incrementing the logins and saving login timestamp
	 *
	 * @return void 
	 */
	public function complete_login()
	{
		if ( ! $this->_loaded)
		{
			// nothing to do
			return;
		}

		// Update the number of logins
		$this->logins = new Database_Expression('logins + 1');

		// Set the last login date
		$this->last_login = time();

		// Save the user
		$this->save();
	}

	/**
	 * Does the reverse of unique_key_exists() by triggering error if username exists.
	 * Validation callback.
	 *
	 * @param   Validate  Validate object
	 * @param   string    field name
	 * @return  void
	 */
	public function username_available(Validate $array, $field)
	{
		if ($this->unique_key_exists($array[$field], 'username'))
		{
			$array->error($field, 'username_available', array($array[$field]));
		}
	}

	/**
	 * Does the reverse of unique_key_exists() by triggering error if email exists.
	 * Validation callback.
	 *
	 * @param   Validate  Validate object
	 * @param   string    field name
	 * @return  void
	 */
	public function email_available(Validate $array, $field)
	{
		if ($this->unique_key_exists($array[$field], 'email'))
		{
			$array->error($field, 'email_available', array($array[$field]));
		}
	}

	/**
	 * Tests if a unique key value exists in the database.
	 *
	 * @param   mixed    the value to test
	 * @param   string   field name
	 * @return  boolean
	 */
	public function unique_key_exists($value, $field = NULL)
	{
		if ($field === NULL)
		{
			// Automatically determine field by looking at the value
			$field = $this->unique_key($value);
		}

		return (bool) DB::select(array('COUNT("*")', 'total_count'))
			->from($this->_table_name)
			->where($field, '=', $value)
			->where($this->_primary_key, '!=', $this->pk())
			->execute($this->_db)
			->get('total_count');
	}

	/**
	 * Allows a model use both email and username as unique identifiers for login
	 *
	 * @param   string  unique value
	 * @return  string  field name
	 */
	public function unique_key($value)
	{
		return Validate::email($value) ? 'email' : 'username';
	}

	/**
	 * Saves the current object. Will hash password if it was changed.
	 *
	 * @return  ORM
	 */
	public function save()
	{
		if (array_key_exists('password', $this->_changed))
		{
			$this->_object['password'] = Auth::instance()->hash_password($this->_object['password']);
		}

		return parent::save();
	}

    public function is_admin()
    {
        return (bool) $this->has('roles', ORM::factory('role', array('name' => 'admin')));
    }

    public function is_client()
    {
        return (bool) $this->has('roles', ORM::factory('role', array('name' => 'client')));
    }

    public function clients()
    {
        if ($this->_clients_joined)
            return $this;
        
        $this->join('roles_users')
            ->on('users.id', '=', 'roles_users.user_id')
            ->join('roles')
            ->on('roles_users.role_id', '=', 'roles.id')
            ->where('roles.name', '=', 'client');

        $this->_clients_joined = TRUE;

        return $this;
    }

    public function clients_data()
    {
        if ($this->_user_data_joined)
            return $this;

        $this->clients();
        
        $this->select('user_data.*')
            ->join('user_data', 'LEFT')
            ->on('user_data.user_id', '=', 'users.id');

        $this->_user_data_joined = TRUE;

        return $this;
    }

    public function clients_data_changes()
    {
        if ($this->_user_data_changes_joined)
            return $this;

        $this->clients();

        $this->select('user_data_changes.*')
            ->join('user_data_changes', 'LEFT')
            ->on('user_data_changes.user_id', '=', 'users.id');

        $this->_user_data_changes_joined = TRUE;

        return $this;
    }

    public function count_clients()
    {
        $this->clients();
        
        return $this->count_all();
    }

    public function reset($next = TRUE)
    {
        parent::reset($next);

        $this->_clients_joined = FALSE;
        $this->_user_data_joined = FALSE;
    }

    public function clients_messages()
    {
        $this->clients();

        $q_new = DB::select('COUNT("*")')
            ->from('messages')
            ->where('messages.user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->where('readed2', '=', 0)
            ->where('deleted2', '=', 0)
            ->compile($this->_db);
        
        $q_total = DB::select('COUNT("*")')
            ->from('messages')
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->where('deleted2', '=', 0)
            ->compile($this->_db);
/*
        $q_all = DB::select('COUNT("*")')
            ->from('messages')
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->compile($this->_db);
*/
        $q_important = DB::select('COUNT("*")')
            ->from('messages')
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->where('important', '=', 1)
            ->where('deleted2', '=', 0)
            ->compile($this->_db);

        $this->select(
            array('users.id', 'id'),
            array('user_data.name', 'name'),
            array(DB::expr("($q_new)"), 'count_new'),
            array(DB::expr("($q_total)"), 'count_total'),
//            array(DB::expr("($q_all)"), 'count_all'),
            array(DB::expr("($q_important)"), 'count_important')
        )
        ->join('user_data')
        ->on('users.id', '=', 'user_data.user_id')
        ->order_by('count_new', 'DESC')
        ->order_by('count_important', 'DESC')
        ->order_by('id', 'DESC');
        
        return $this;
    }

    public function clients_orders($table = 'orders')
    {
        $this->clients_data();

        $q_new = DB::select('COUNT("*")')
            ->from($table)
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->where('new', '=', 1)
            ->where('deleted', '=', 0)
            ->compile($this->_db);

        $q_total = DB::select('COUNT("*")')
            ->from($table)
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->where('deleted', '=', 0)
            ->compile($this->_db);
/*
        $q_all = DB::select('COUNT("*")')
            ->from($table)
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->compile($this->_db);
*/
        $q_important = DB::select('COUNT("*")')
            ->from($table)
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->where('important', '=', 1)
            ->where('deleted', '=', 0)
            ->compile($this->_db);

        $q_unread = DB::select('COUNT("*")')
            ->from($table)
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->where('readed_admin', '=', 0)
            ->where('deleted', '=', 0)
            ->compile($this->_db);

        $this->clients();

        $this->select(
            array(DB::expr("($q_new)"), 'count_new'),
            array(DB::expr("($q_total)"), 'count_total'),
//            array(DB::expr("($q_all)"), 'count_all'),
            array(DB::expr("($q_important)"), 'count_important'),
            array(DB::expr("($q_unread)"), 'count_unread')
        )
        ->order_by('count_new', 'DESC')
        ->order_by('count_unread', 'DESC')
        ->order_by('count_important', 'DESC')
        ->order_by('id', 'DESC');

        return $this;
    }

    public function clients_files($table = 'files')
    {
        $this->clients_data();
        

        $q_total = DB::select('COUNT("*")')
            ->from($table)
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->compile($this->_db);
/*
        $q_all = DB::select('COUNT("*")')
            ->from($table)
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->compile($this->_db);
*/
        $q_important = DB::select('COUNT("*")')
            ->from($table)
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->where('important', '=', 1)
            ->compile($this->_db);

        $q_unread = DB::select('COUNT("*")')
            ->from($table)
            ->where('user_id', '=', DB::expr($this->_db->quote_identifier('users.id')))
            ->where('readed_admin', '=', 0)
            ->compile($this->_db);

        $this->clients();

        $this->select(
            array(DB::expr("($q_total)"), 'count_total'),
//            array(DB::expr("($q_all)"), 'count_all'),
            array(DB::expr("($q_important)"), 'count_important'),
            array(DB::expr("($q_unread)"), 'count_unread')
        )
        ->order_by('count_unread', 'DESC')
        ->order_by('count_important', 'DESC')
        ->order_by('id', 'DESC');

        return $this;
    }


    public function add_user($username, $email, $password, array $roles = array('login'), array $data = array())
    {
        $user = ORM::factory('user');
        $user->username = $username;
        $user->password = $password;
        $user->email    = $email;
        $user->save();

        foreach($roles as $role)
        {
            $user->add('roles', ORM::factory('role', array('name' => $role)));
        }

        $user->save();

        $user_data = ORM::factory('user_data');
        $user_data->user_id = $user->id;

        foreach($data as $_var => $_data)
        {
            $user_data->$_var = $_data;
        }

        $user_data->save();

        return true;
    }

    public function get_roles_id($roles = array('login'))
    {
        $result = DB::select('id')
                ->from('roles')
                ->where('name', 'IN', $roles)
                ->execute()
                ->as_array(NULL, 'id');

        return $result;
    }

    public function add_admin($login, $password, array $expr = array())
    {
        $this->add_user($login, $password, array('login', 'admin'), $expr);
    }

    public function del($user_id)
    {
        $result = DB::delete($this->_tb_user_expr)
            ->where('user_id', '=', $user_id)
            ->execute();

        $result = DB::delete($this->_tb_roles_users)
            ->where('user_id', '=', $user_id)
            ->execute();

        $result = DB::delete($this->_tb_users)
            ->where('id', '=', $user_id)
            ->execute();

        return $result;
    }

    public function edit($user_id, array $roles = array(), array $expr = array())
    {
        $result = DB::update($this->_tb_user_expr)
            ->set($expr)
            ->where('user_id', '=', $user_id)
            ->execute();

        return $result;
    }
    
    
    public function find_by_email($email){    	
    	
        $id = DB::select('id')
                ->from('users')
                ->where('email', '=', $email)
                ->execute()
                ->get('id');
                
        $user = ORM::factory('user')->find($id);

        return $user;   	
    	
    }

} // End Auth User Model