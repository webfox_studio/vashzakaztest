<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * Default auth user toke
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Model_Auth_User_Data_Changes extends ORM {

    protected $_table_name = 'user_data_changes';

	// Relationships
    protected $_belongs_to = array('user' => array('foreign_key' => 'user_id'));

    protected $_primary_key = 'user_id';

} // End Auth User Data Model