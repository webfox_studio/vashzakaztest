<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * Default auth user toke
 *
 * @package    Kohana/Auth
 * @author     Kohana Team
 * @copyright  (c) 2007-2009 Kohana Team
 * @license    http://kohanaphp.com/license.html
 */
class Model_Auth_User_Data extends ORM {

    protected $_table_name = 'user_data';

	// Relationships
	protected $_belongs_to = array('user' => array());

    protected $_primary_key = 'user_id';

    protected $_table_columns = array(
        'user_id' => array('type' => 'int'),
        'name' => array('type' => 'string'),
        'ip' => array('type' => 'string'),
        'registered' => array('type' => 'string'),
        'referer' => array('type' => 'string'),
        'country' => array('type' => 'string'),
        'region' => array('type' => 'string'),
        'city' => array('type' => 'string'),
        'zip' => array('type' => 'int'),
        'address' => array('type' => 'string'),
        'phones' => array('type' => 'string'),
        'icq' => array('type' => 'int'),
        'skype' => array('type' => 'string'),
        'mail_ru' => array('type' => 'string'),
        'another' => array('type' => 'string'),
        'where' => array('type' => 'string'),
        'last_activity' => array('type' => 'string'),
        'last_news_id' => array('type' => 'int'),
        'forum_id' => array('type' => 'int'),
        'value' => array('type' => 'float'),
    );

} // End Auth User Data Model