tinyMCE.init({
    // General options
    mode : "exact",
    elements : "tinyMCE_m",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,forecolor,backcolor",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image",
    theme_advanced_buttons3 : "",
    theme_advanced_buttons4 :"",
    theme : "advanced",
    language: "ru"
});
