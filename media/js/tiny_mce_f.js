tinyMCE.init({
    // General options
    mode : "exact",
    elements : "tinyMCE_f,tinyMCE_f1",
    theme : "advanced",
    language: "ru",
    height : "480",
    plugins : "style,table,advhr,advimage,advlink,inlinepopups,preview,media,contextmenu,paste,nonbreaking,xhtmlxtras,advlist",

    // Theme options
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
    theme_advanced_buttons4 : "cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,

    // Drop lists for link/image/media/template dialogs
    template_external_list_url : "lists/template_list.js",
    external_link_list_url : "lists/link_list.js",
    external_image_list_url : "lists/image_list.js",
    media_external_list_url : "lists/media_list.js",

    // Style formats
    style_formats : [
    {
        title : 'Bold text',
        inline : 'b'
    },

    {
        title : 'Red text',
        inline : 'span',
        styles : {
            color : '#ff0000'
        }
    },
{
    title : 'Red header',
    block : 'h1',
    styles : {
        color : '#ff0000'
    }
},
{
    title : 'Table styles'
},
{
    title : 'Ячейка таблицы',
    selector : 'td',
    classes : 'bgc1 aj p8'
},
{
    title : 'Заголовок таблицы',
    selector : 'td',
    classes : 'pr8 ac bg1 h18'
},
{
    title : 'Стандартная таблица',
    selector : 'table',
    classes : 'bgc1 w100p mt8 ac'
}
]
});
