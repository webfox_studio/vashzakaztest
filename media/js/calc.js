/*
<?php

$host = "http://".$_SERVER['HTTP_HOST'];
$this_script_relative_path = $_SERVER['SCRIPT_NAME'];
$this_sript_absolut_path = $host.substr($this_script_relative_path, 0, strrpos($this_script_relative_path, "/")+1);
if (!preg_match('!'.$this_sript_absolut_path.'!Ui', $_SERVER['HTTP_REFERER'])) exit;
//$_SERVER['HTTP_REFERER'] != $this_sript_absolut_path) exit;

?>
*/

var order_count = 10;

var display_td = 'table-cell';
var display_tr = 'table-row';
if ((navigator.appName == 'Microsoft Internet Explorer') && (parseInt(navigator.appVersion) <= 6 ))
{
	display_td = 'block';
	display_tr = 'block';
}

function add()
{
	for (var i=2; i<=order_count; i++)
	{
		id = document.getElementById('order_' + i);
		if (id.style.display == 'none')
		{
			id.style.display = display_tr;
			break;
		}
	}
}

function del()
{
	for (var i=order_count; i>=2; i--)
	{
		id = document.getElementById('order_' + i);
		if (id.style.display == display_tr)
		{
			// ������� ��������
			document.getElementById('price_' + i).value = '';
			document.getElementById('ind_payment_' + i).checked = false;
			document.getElementById('bid_' + i + '_1').checked = false;
			document.getElementById('bid_' + i + '_2').checked = false;
			document.getElementById('cost_delivery_' + i).value = '';
			document.getElementById('weightkg_' + i).value = '';
			document.getElementById('weightf_' + i).value = '';
			document.getElementById('commission_' + i).value = 10;
			document.getElementById('additional_cost_' + i).value = '';

			id.style.display = 'none';
			calc(1);
			break;
		}
	}
}

function weight_converter(name, from, num_order)
{
	var value = document.getElementById(name + from + '_' + num_order).value;
	var reg = /[^0-9\.]/g

	value = value.replace(reg, '');
	document.getElementById(name + from + '_' + num_order).value = value;

	if (from == 'kg')
	document.getElementById(name + 'f_' + num_order).value = Math.round(value * 2.206 * 100) / 100;
	else
	document.getElementById(name + 'kg_' + num_order).value =  Math.round(value * 0.454 * 100) / 100;
}

function show_insurance()
{
	var insurance_choise = document.getElementById('insurance');
	if (insurance_choise.checked == true)
	{
		document.getElementById('insurance_td').style.display = display_td;
	}
	else
	{
		document.getElementById('insurance_price').value = '';
		document.getElementById('insurance_td').style.display = 'none';
	}
}

function check_commission(num)
{
	var reg = /[^0-9]/g
	var value = document.getElementById('commission_' + num).value;

	value = value.replace(reg, '');
	if(parseInt(value) > 10) value = 10;
	else if (parseInt(value) < 5) value = 5;

	document.getElementById('commission_' + num).value = value;
}

function check_sum(id)
{
	var reg = /[^0-9\.]/g
	var value = document.getElementById(id).value;

	value = value.replace(reg, '');
	document.getElementById(id).value = value;
}

function check_insurance()
{
	var reg = /[^0-9]/g
	var value = document.getElementById('insurance_price').value;

	value = value.replace(reg, '');
	if(parseInt(value) > 2000) value = 2000;
	document.getElementById('insurance_price').value = value;
}

function hide_res()
{
	document.getElementById('result_table').style.display = 'none';
}

function calc(is_del)
{
	var error = 0;
	var current_price = 0;
	var current_commission = 0;
	var current_commissionw = 0;
	var current_additional_cost = 0;
	var current_weight = document.getElementById('weight_boxesf_0').value;
	if (current_weight == '') current_weight = 0;
	var current_total = 0;
	var res_table = document.getElementById('result_table');


	for (var i=1; i<=order_count; i++)
	{
		if (document.getElementById('order_' + i).style.display != 'none')
		{
			var price = document.getElementById('price_' + i).value;
			if (price == '') price = 0;

			var weightf = document.getElementById('weightf_' + i).value;
			if (weightf == '') weightf = 0;

			var commission = document.getElementById('commission_' + i).value;
			var bid_ebay = document.getElementById('bid_' + i + '_1').checked;
			var bid_im = document.getElementById('bid_' + i + '_2').checked;

			if (price == 0 || weightf == 0 || (bid_ebay == false && bid_im == false))
			{
				error = 1;
				continue;
			}
			else
			{
				var cost_delivery = document.getElementById('cost_delivery_' + i).value;
				if (cost_delivery == '') cost_delivery = 0;

				var ind_payment = document.getElementById('ind_payment_' + i).checked;
				var price_full = 0;
				if (ind_payment == false)
				{
					price_full = (price-1) + (cost_delivery-1) + 2;
				}

				var commission_price = ((price-1) + (cost_delivery-1) + 2) * commission / 100;
				if (commission_price < 5 && bid_ebay == true) commission_price = 5;
				else if (commission_price < 10 && bid_im == true) commission_price = 10;

				var commissionw = 0;
				if (weightf <= 25) commissionw = 2.5;
				else if (weightf > 25 && weightf <= 50) commissionw = 5;
				else if (weightf > 25) commissionw = 10;

				var additional_cost = document.getElementById('additional_cost_' + i).value;
				if (additional_cost == '') additional_cost = 0;

				//����
				var total = (price_full-1) + (commission_price-1) + (commissionw-1) + (additional_cost-1) + 4;
				total = Math.round(total*100)/100;

				//���������� ������ �� ���� ������ �� ��������� ���������� �� �������
				current_price = (current_price-1) + (price_full-1) + 2;
				current_commission = (current_commission-1) + (commission_price-1) + 2;
				current_commissionw = (current_commissionw-1) + (commissionw-1) + 2;
				current_additional_cost = (current_additional_cost-1) + (additional_cost-1) + 2;
				current_weight = (current_weight-1) + (weightf-1) + 2;
				current_total = (current_total-1) + (total-1) + 2;
			}
		}
	}

	// 66 - ������������ ��� � ������, ��� �������� �������� ������
	if (current_total != 0 && current_weight <= 66 && error == 0)
	{
		// ����������� � ���������
		var insurance_price = document.getElementById('insurance_price').value;
		if (insurance_price == '') insurance_price = 0;
		var insurance_ems = 0;
		var insurance_pmi = 0;

		// ������ ��������
		if (insurance_price != 0)
		{
			if (insurance_price <= 50) insurance_pmi = 1.47;
			else if (insurance_price > 50 && insurance_price <= 100) insurance_pmi = 2.16;
			else if (insurance_price > 100 && insurance_price <= 200) { insurance_ems = 0.00; insurance_pmi = 2.85; }
			else if (insurance_price > 200 && insurance_price <= 300) { insurance_ems = 1.47; insurance_pmi = 3.54; }
			else if (insurance_price > 300 && insurance_price <= 400) { insurance_ems = 2.85; insurance_pmi = 4.23; }
			else if (insurance_price > 400 && insurance_price <= 500) { insurance_ems = 3.54; insurance_pmi = 4.92; }
                        else if (insurance_price > 500 && insurance_price <= 600) { insurance_ems = 4.23; insurance_pmi = 5.61; }
                        else if (insurance_price > 600 && insurance_price <= 700) { insurance_ems = 4.92; insurance_pmi = 6.30; }
                        else if (insurance_price > 700 && insurance_price <= 800) { insurance_ems = 5.61; insurance_pmi = 6.99; }
                        else if (insurance_price > 800 && insurance_price <= 900) { insurance_ems = 6.30; insurance_pmi = 7.68; }
                        else if (insurance_price > 900 && insurance_price <= 1000) { insurance_ems = 6.99; insurance_pmi = 8.37; }
                        else if (insurance_price > 1000 && insurance_price <= 1100) { insurance_ems = 7.68; insurance_pmi = 9.06; }
                        else if (insurance_price > 1100 && insurance_price <= 1200) { insurance_ems = 8.37; insurance_pmi = 9.75; }
                        else if (insurance_price > 1200 && insurance_price <= 1300) { insurance_ems = 9.06; insurance_pmi = 10.44; }
                        else if (insurance_price > 1300 && insurance_price <= 1400) { insurance_ems = 9.75; insurance_pmi = 11.13; }
                        else if (insurance_price > 1400 && insurance_price <= 1500) { insurance_ems = 10.44; insurance_pmi = 11.82; }
                        else if (insurance_price > 1500 && insurance_price <= 1600) { insurance_ems = 11.13; insurance_pmi = 12.51; }
                        else if (insurance_price > 1600 && insurance_price <= 1700) { insurance_ems = 11.82; insurance_pmi = 13.20; }
                        else if (insurance_price > 1700 && insurance_price <= 1800) { insurance_ems = 12.51; insurance_pmi = 13.89; }
                        else if (insurance_price > 1800 && insurance_price <= 1900) { insurance_ems = 13.20; insurance_pmi = 14.58; }
                        else if (insurance_price > 1900 && insurance_price <= 2000) { insurance_ems = 13.89; insurance_pmi = 15.27; }
                 }

		// ������ � ������� �� ������������� �������� (EMS|PMI)
		var Array_weight_price_ems = new Array('0',
					
                                      
                              '56.15|44.00',					// ��������� �� 1 ����
					'62.40|50.45',					// 2 ����
					'66.65|54.90',					// 3 ����
					'70.90|59.35',					// 4 ����
					'75.15|63.80',					// 5 ����
					'82.00|70.35',					// 6 ����
					'86.35|74.40',					// 7 ����
					'90.70|78.45',					// 8 ����
					'95.05|82.50',					// 9 ����
					'99.40|86.55',					// 10 ����
					'106.25|93.10',					// 11 ����
					'110.60|97.15',					// 12 ����
					'114.95|101.20',					// 13 ����
					'119.30|105.25',					// 14 ����
					'123.65|109.30',				// 15 ����
					'130.50|115.85',			      // 16 ����
					'134.85|119.90',				// 17 ����
					'139.20|123.95',				// 18 ����
					'143.55|128.00',				// 19 ����
					'147.90|132.05',				// 20 ����
					'154.75|138.60',				// 21 ����
					'159.10|142.65',				// 22 ����
					'163.45|146.70',				// 23 ����
					'167.80|150.75',				// 24 ����
					'172.15|154.80',				// 25 ����
					'176.50|158.85',				// 26 ����
					'180.85|162.90',				// 27 ����
					'185.20|166.95',				// 28 ����
					'189.55|171.00',				// 29 ����
					'193.90|175.05',				// 30 ����
					'200.75|181.60',				// 31 ����
					'205.10|185.65',				// 32 ����
					'209.45|189.70',				// 33 ����
					'213.80|193.75',				// 34 ����
					'218.15|197.80',				// 35 ����
					'222.50|201.85',				// 36 ����
					'226.85|205.90',				// 37 ����
					'231.20|209.95',				// 38 ����
					'235.55|214.00',				// 39 ����
					'239.90|218.05',				// 40 ����
					'246.75|224.60',				// 41 ����
					'251.10|228.65',				// 42 ����
					'255.45|232.70',				// 43 ����
					'259.80|236.75',				// 44 ����, ��� PMI - ��������
					'264.15',						// 45 ����
					'268.50',						// 46 ����
					'272.85',						// 47 ����
					'277.20',						// 48 ����
					'281.55',						// 49 ����
					'285.90',						// 50 ����
					'292.75',						// 51 ����
					'297.10',						// 52 ����
					'301.45',						// 53 ����
					'305.80',						// 54 ����
					'310.15',						// 55 ����
					'314.50',						// 56 ����
					'318.85',						// 57 ����
					'323.20',						// 58 ����
					'327.55',						// 59 ����
					'331.54',						// 60 ����
					'338.75',						// 61 ����
					'343.10',						// 62 ����
					'347.45',						// 63 ����
					'351.80',						// 64 ����
					'356.15',						// 65 ����
					'360.50'						// 66 ����, ��� EMS - ��������
				);
		var index = Math.round(current_weight);
		if (current_weight > 0 && index == 0) index = 1;
		var temp = new Array();
		temp = Array_weight_price_ems[index].split('|');
		var weight_price_ems = (temp[0]-1) + 1;
		var weight_price_pmi = (temp[1]-1) + 1;

		res_table.style.display = display_tr;

		document.getElementById('td_res_price_ems').innerHTML = current_price + ' $';
		document.getElementById('td_res_commission_ems').innerHTML = current_commission + ' $';
		document.getElementById('td_res_commissionw_ems').innerHTML = current_commissionw + ' $';
		document.getElementById('td_res_additional_ems').innerHTML = current_additional_cost + ' $';
		document.getElementById('td_res_delivary_ems').innerHTML = weight_price_ems + ' $';
		document.getElementById('td_res_insurance_ems').innerHTML = insurance_ems + ' $';
		document.getElementById('td_res_total_ems').innerHTML = current_total + weight_price_ems + insurance_ems + ' $';

		// 44 - ������������ ��� � ������, ��� �������� �������� ������ ��� PMI
		if (current_weight <= 44)
		{
			document.getElementById('td_res_price_pmi').innerHTML = current_price + ' $';
			document.getElementById('td_res_commission_pmi').innerHTML = current_commission + ' $';
			document.getElementById('td_res_commissionw_pmi').innerHTML = current_commissionw + ' $';
			document.getElementById('td_res_additional_pmi').innerHTML = current_additional_cost + ' $';
			document.getElementById('td_res_delivary_pmi').innerHTML = weight_price_pmi + ' $';
			document.getElementById('td_res_insurance_pmi').innerHTML = insurance_pmi + ' $';
			document.getElementById('td_res_total_pmi').innerHTML = current_total + weight_price_pmi + insurance_pmi + ' $';
		}
		else
		{
			document.getElementById('td_res_price_pmi').innerHTML = '-';
			document.getElementById('td_res_commission_pmi').innerHTML = '-';
			document.getElementById('td_res_commissionw_pmi').innerHTML = '-';
			document.getElementById('td_res_additional_pmi').innerHTML = '-';
			document.getElementById('td_res_delivary_pmi').innerHTML = '-';
			document.getElementById('td_res_insurance_pmi').innerHTML = '-';
			document.getElementById('td_res_total_pmi').innerHTML = '-';
		}
	}
	else
	{
		res_table.style.display = 'none';
		if (error == 1 && is_del != 1) alert("��������!\n�� ��� ������������ � ���������� ���� ������������.\n��������� ����� �������, ����������!");
		if (current_weight >= 66) alert("����������� ���������� ��� ������� ��� ��������:\nEMS�- 66 ������\nPriority Mail - 44 �����");
	}
}