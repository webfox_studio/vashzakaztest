$(document).ready(function(){
    $(".ajax-resipient").change(function(){
        var col = $(this).data('column');
        var id = $(this).data('id');
        var val = $(this).val();
        params = {
            id : id,
            col : col,
            val : val
        };
        $.ajax({
            url: '/calculator/updateresipient',
            type: 'post',
            data: params,
            beforeSend: function(){},
            success: function(data)
            {
                if(data != ''){
                    $(".pi-ok"+id).html(data).hide(1500);
                }
            }
        });
        return false;
    })

    $(".ajax-price").change(function(){
        var col = $(this).data('column');
        var id = $(this).data('id');
        var val = $(this).val();
        params = {
            id : id,
            col : col,
            val : val
        };
        $.ajax({
            url: '/calculator/updateprice',
            type: 'post',
            data: params,
            beforeSend: function(){},
            success: function(data)
            {
                if(data != ''){
                    $(".pi-ok"+id).html(data).hide(1500);
                }
            }
        });
        return false;
    })


    $(".ajax-option").change(function(){
        var col = $(this).data('column');
        var id = $(this).data('id');
        var val = $(this).val();
        params = {
            id : id,
            col : col,
            val : val
        };
        $.ajax({
            url: '/calculator/updateadmin',
            type: 'post',
            data: params,
            beforeSend: function(){},
            success: function(data)
            {
                if(data != ''){
                    $(".pi-ok"+id).html(data).hide(1500);
                }
            }
        });
        return false;
    })
})