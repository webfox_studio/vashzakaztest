$(document).ready(function(){
    $("#pi-result").html('');
    //-------------admin

    $(".ajax-option").bind("change keyup", function(e){
        var col = $(this).data('column');
        var id = $(this).data('id');
        var val = $(this).val();
        params = {
            id : id,
            col : col,
            val : val
        };
        $.ajax({
            url: '/calculator/updateadmin',
            type: 'post',
            data: params,
            beforeSend: function(){},
            success: function(data)
            {
                if(data != ''){
                    $(".pi-ok"+id).html(data).hide(1500);
                }
            }
        });
        return false;
    })

    $(".ajax-price").change(function(){
        var col = $(this).data('column');
        var id = $(this).data('id');
        var val = $(this).val();
        params = {
            id : id,
            col : col,
            val : val
        };
        $.ajax({
            url: '/calculator/updateprice',
            type: 'post',
            data: params,
            beforeSend: function(){},
            success: function(data)
            {
                if(data != ''){
                    $(".pi-ok"+id).html(data).hide(1500);
                }
            }
        });
        return false;
    })

    $(".ajax-resipient").change(function(){
        var col = $(this).data('column');
        var id = $(this).data('id');
        var val = $(this).val();
        params = {
            id : id,
            col : col,
            val : val
        };
        $.ajax({
            url: '/calculator/updateresipient',
            type: 'post',
            data: params,
            beforeSend: function(){},
            success: function(data)
            {
                if(data != ''){
                    $(".pi-ok"+id).html(data).hide(1500);
                }
            }
        });
        return false;
    })









    $("#pi-result").html('');
    var country = $(".country-in").val();
    if(country == 1){
        $.ajax({
            url: '/calculator/country/?country=' + country,
            type: 'get',
            beforeSend: function(){},
            success: function(data){
                if(data != ''){
                    $("#bv-resipient-city").html(data);
                }
                else{
                    $("#bv-resipient-city").html('<option>--</option>');
                }
            }
        })
    }

    var country_out = $(".country-out").val();
    var city_in = $(".city-in").val();
    var country_in = $(".country-in").val();
    var heft_value = $(".heft-value").val();
    var heft = $('input[name=heft]:checked').val();

    if(heft == 'kg'){
        var koef = 2.2046;
        heft_value = heft_value*koef;
    }
    var size = $('input[name=size]:checked').val();
    var size1 = $(".size-value1").val();
    var size2 = $(".size-value2").val();
    var size3 = $(".size-value3").val();
    if(size == 'sm'){
        var koef1 = 2.54;
        size1 = size1*koef1;
        size2 = size2*koef1;
        size3 = size3*koef1;
    }
    if(country_out != '--' && country_in != '' && heft_value != ''){
        if(country != 1)
        {
            $(".pi-region").hide(); return;
        }
        else
        {
            $(".pi-region").show();
        }
        params = {
            heft : heft_value,
            country_in : country_in,
            city_in : city_in,
            width : size1,
            height : size2,
            length : size3
        };
        $.ajax({
            url: '/calculator/result',
            type: 'post',
            data: params,
            beforeSend: function(){},
            success: function(data){
                if(data != ''){
                    $("#pi-result").html(data);
                }
            }
        })
    }

    $("#bv-resipient").change(function(){
        var country = $(this).val();
        if(country != 1)
        {
            $(".pi-region").hide(); return;
        }
        else
        {
            $(".pi-region").show();
        }
        $.ajax({
            url: '/calculator/country/?country=' + country,
            type: 'get',
            beforeSend: function(){},
            success: function(data)
            {

                if(data != ''){
                    $("#bv-resipient-city").html(data);
                }
                else{
                    $("#bv-resipient-city").html('<option>--</option>');
                }
            }
        })
        //return false;
    });
    function alertObj(obj) {
        var str = "";
        for(k in obj) {
            str += k+": "+ obj[k]+"\r\n";
        }
        alert(str);
    }
    $(".pi-search").bind("change keyup", function(e){

        var country_out = $(".country-out").val();
        var city_in = $(".city-in").val();
        var country_in = $(".country-in").val();
        var heft_value = $(".heft-value").val();
        var heft = $('input[name=heft]:checked').val();

        if(heft == 'kg'){
            var koef = 2.2046;
            heft_value = heft_value*koef;
        }

        var size = $('input[name=size]:checked').val();
        var size1 = $(".size-value1").val();
        var size2 = $(".size-value2").val();
        var size3 = $(".size-value3").val();
        if(size == 'sm'){
            var koef1 = 2.54;
            size1 = size1*koef1;
            size2 = size2*koef1;
            size3 = size3*koef1;
        }

        if(country_out != '--' && country_in != '' && heft_value != ''){

            params = {
                heft : heft_value,
                country_in : country_in,
                city_in : city_in,
                width : size1,
                height : size2,
                length : size3
            };

            if(country_in == '1' && (city_in =='0' || city_in == '--')) { $("#pi-result").html(''); return;}
        $.ajax({
            url: '/calculator/result',
            type: 'post',
            data: params,
            beforeSend: function(){},
            success: function(data)
            {
                if(data != ''){
                    $("#pi-result").html(data);
                }
            }
        })
        return false;
        }
    })


    function test(heft_value){
        alert(heft_value)
    }



    $('input,textarea').focus(function(){
        $(this).data('placeholder',$(this).attr('placeholder'))
        $(this).attr('placeholder','');
    });
    $('input,textarea').blur(function(){
        $(this).attr('placeholder',$(this).data('placeholder'));
    });
});