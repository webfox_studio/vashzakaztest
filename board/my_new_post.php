<?php
if (!empty ($new_post_subject)){ 
    
    define('IN_PHPBB', true);
    define('PHPBB_ROOT_PATH', 'board/');
    
    $phpbb_root_path = PHPBB_ROOT_PATH;
    $phpEx = substr(strrchr(__FILE__, '.'), 1);
    require($phpbb_root_path . 'common.' . $phpEx);
    require_once($phpbb_root_path . 'includes/functions_posting.' . $phpEx); 
    
    // Start session management
    $user->session_begin(false); // no update_session_page
    // session_create($user_id = false, $set_admin = false, $persist_login = false, $viewonline = true)
    $user->session_create( 2, true, false, false);
    $auth->acl($user->data);
    $user->setup();
    
    $my_subject	= utf8_normalize_nfc($new_post_subject);
    $my_text	= utf8_normalize_nfc($new_post_text);
    
    // variables to hold the parameters for submit_post
    $poll = $uid = $bitfield = $options = ''; 
    
    generate_text_for_storage($my_subject, $uid, $bitfield, $options, false, false, false);
    generate_text_for_storage($my_text, $uid, $bitfield, $options, true, true, true);
    
    $data = array( 
    	'forum_id'		=> 20,
    	'icon_id'		=> false,
    
    	'enable_bbcode'		=> true,
    	'enable_smilies'	=> true,
    	'enable_urls'		=> true,
    	'enable_sig'		=> true,
    
    	'message'		=> $my_text,
    	'message_md5'	=> md5($my_text),
    				
    	'bbcode_bitfield'	=> $bitfield,
    	'bbcode_uid'		=> $uid,
    
    	'post_edit_locked'	=> 0,
    	'topic_title'		=> $my_subject,
    	'notify_set'		=> false,
    	'notify'			=> false,
    	'post_time' 		=> 0,
    	'forum_name'		=> '',
    	'enable_indexing'	=> true,
    );
    
   $new_post_url = submit_post('post', $my_subject, '', POST_NORMAL, $poll, $data);
    
   $user->session_kill();
}
?>