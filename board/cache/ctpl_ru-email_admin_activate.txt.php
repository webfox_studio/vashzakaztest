<?php if (!defined('IN_PHPBB')) exit; ?>Subject: Активируйте учётную запись пользователя

Здравствуйте!

Учётная запись пользователя <?php echo (isset($this->_rootref['USERNAME'])) ? $this->_rootref['USERNAME'] : ''; ?> была деактивирована либо заново создана. Вы должны проверить профиль этого пользователя и (если требуется) активировать его.

Ссылка на просмотр профиля пользователя:
<?php echo (isset($this->_rootref['U_USER_DETAILS'])) ? $this->_rootref['U_USER_DETAILS'] : ''; ?>


Ссылка для активации учётной записи:
<?php echo (isset($this->_rootref['U_ACTIVATE'])) ? $this->_rootref['U_ACTIVATE'] : ''; ?>



<?php echo (isset($this->_rootref['EMAIL_SIG'])) ? $this->_rootref['EMAIL_SIG'] : ''; ?>