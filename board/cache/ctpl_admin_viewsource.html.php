<?php if (!defined('IN_PHPBB')) exit; $this->_tpl_include('simple_header.html'); ?>

<div id="acp" style="padding: 0;">
<div class="panel" style="padding: 10px;">
<div style="overflow: auto;">
	<h1><?php echo (isset($this->_rootref['FILENAME'])) ? $this->_rootref['FILENAME'] : ''; ?></h1>

	<table class="type2">
	<tbody>
	<?php $_source_count = (isset($this->_tpldata['source'])) ? sizeof($this->_tpldata['source']) : 0;if ($_source_count) {for ($_source_i = 0; $_source_i < $_source_count; ++$_source_i){$_source_val = &$this->_tpldata['source'][$_source_i]; ?>

		<tr valign="top">
			<td class="sourcenum"><?php echo $_source_val['LINENUM']; ?>&nbsp;&nbsp;</td>
			<td class="source"><?php echo $_source_val['LINE']; ?></td>
		</tr>
	<?php }} ?>

	</tbody>
	</table>

</div>
</div>
</div>
<?php $this->_tpl_include('simple_footer.html'); ?>